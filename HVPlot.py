print("Importing libraries")
import ROOT as R
import sys,os
import printMapping
import numpy as np
import pandas as pd 
from datetime import datetime

print("Done importing")

#get_run_information(run_spec, atlas_partition=True)
year = int(sys.argv[1])
want = ['DETNAME','HVLINES']
inputs={"want":want,"showdecimal":True}
#print("querying printMapping for",want)
#pmout = printMapping.query(**inputs)

#get_run_information(run_spec, atlas_partition=True)
infile = "/afs/cern.ch/user/l/larmon/public/prod/HVTripsDB/trips"+str(year)+".dat"

#infile = open(infile, "r")

cols = ["HVLINE","TripTimeStamp","TripRunNumber","TripLB","StableZero","RampUp","RecoveryTimeStamp","RecoveryRunNumber","RecoveryLB","StableBeams","FillNumber","NumCollBunches","Solenoid","Toroid","Status","1","2","3","4"]


print("Reading input file")
df = pd.read_csv(infile, sep=" ", header=None, engine='python', names=cols)
df = df.drop(['1', '2', '3', '4'], axis=1)


#pmdf = pd.DataFrame(pmout, columns = want)
#print(pmdf)

#print(pmdf.loc[pmdf.HVLINES.str.contains("176013")].DETNAME)
def getDet(row):
    hvline = str(row['HVLINE']).zfill(6)
    detname = pmdf.loc[pmdf.HVLINES.str.contains(hvline)]
    if len(detname) == 0:
        return np.nan
        detname = detname.DETNAME.unique()[0]
        return detname

def getRecoveryTime(row):
    start = row.TripTimeStamp
    end = row.RecoveryTimeStamp
    tripLB = row.TripLB
    recoveryLB = row.RecoveryLB

    if int(recoveryLB) == -1 or row.RecoveryRunNumber == 0:
        return np.nan
    start = datetime.strptime(start, "%Y-%m-%d:%H:%M:%S")
    end = datetime.strptime(end, "%Y-%m-%d:%H:%M:%S")

    if end.year == 9999:
        return np.nan
        print("weird... start run",row.TripRunNumber, "end run",row.RecoveryRunNumber)

    return (end-start).total_seconds()


#df['DETNAME'] = df.apply (lambda row: getDet(row), axis=1)
print("Adding recovery time")
df['RecoveryTime'] = df.apply (lambda row: getRecoveryTime(row), axis=1)

#runs = df['TripRunNumber'].unique()

#print("mean recov", df.RecoveryTime.mean())
#print(df.RecoveryTime.isna().sum())
#print(len(df.RecoveryTime.isna()))
rec = df[~df['RecoveryTime'].isnull()]

#print(rec.to_string())
#print(rec)


null = df[df['RecoveryTime'].isnull()]


test = df[df['RecoveryTime'] < 1]
print("negative recov",len(test))
print(test[['TripLB', 'RecoveryLB','TripTimeStamp', 'RecoveryTimeStamp','RecoveryTime']])


print(len(rec),"recovered and", len(null), "not recovered")
rec = rec[rec.RecoveryTime>0]
print("Mean recovery time:",rec.RecoveryTime.mean())
