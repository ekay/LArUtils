#!/bin/python3
import sys, os, errno,getpass

user = getpass.getuser()

def chmkDir( path ):
    """Safely create a directory"""
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def writeBash( bashdir, bashname, executable, argsFileName, setupscript=None, isexe=False, runwith=None ):
   
    cwd = os.getcwd()
    #####################################
    # Write a bash script to run the code
    #####################################
    chmkDir(bashdir)
    command  = "#!/bin/bash -f\n" # -f to stop globbing
    command += "shopt -s expand_aliases \n" # so that we can use the aliases to scripts set by athena
    command += "echo Now in $PWD \n" 
    command += "jobNo=$1 \n"           
    command += "echo 'sed -n '${jobNo}'p "+argsFileName+"'  \n"
    command += "l=`sed -n \"${jobNo}p\" "+argsFileName+"` \n"
    #command += "IFS=" " read filename treename schema outdir campaign verbose <<< ${l} \n"
    command += "args=($l)\n"
    command += "cd "+cwd+" \n"
    #command += "cd .. \n"
    #command += "echo Now in $PWD \n" 
    if setupscript is not None:
        command += "source "+setupscript+" \n"
    #command += "cd - \n"
    command += "echo Now in $PWD \n" 

    if isexe:
        runexe = executable
    else:
        if runwith is None:
            if executable.endswith(".py"):
                runwith = "python"
            elif executable.endswith(".sh"):
                runwith = "sh"
            else:
                print("NOT SURE WHAT TYPE OF EXECUTABLE... EXITING")
                sys.exit()
        runexe = f"{runwith} {executable}"
    command += "echo "+runexe+" \"${args[@]}\" \n"
    command += runexe+" \"${args[@]}\" \n"
    command += "BACK_PID=$! \n"
    command += "wait $BACK_PID \n"
    # MOVE OUTPUT?
    f_launch = open(bashdir+"/"+bashname+'.sh','w')
    f_launch.write(command)
    f_launch.close()


def writeCond( bashdir, bashname, argsFileName, taskName, runTime=6000, cpus=1, subNow=True, logdir=None, outdir=None, errdir=None, args=None ):
    # writeBash( bashdir, bashname, executable, argsFileName )
    cwd = os.getcwd()
    if logdir is None:
        logdir = bashdir+"/log"
    if outdir is None:
        outdir = bashdir+"/out"
    if errdir is None:
        errdir = bashdir+"/err"
    #if subNow: 
    chmkDir(logdir)
    chmkDir(outdir)
    chmkDir(errdir)
    # If arguments are provided, write an arguments file
    if args is not None:
        outf = open(argsFileName,"w")
        for ra in args:
            outf.write(" ".join([str(r) for r in ra])+"\n")
        outf.close()

    condcmd = "executable = "+bashname+".sh \n"
    condcmd += 'batch_name = "'+taskName+'"\n'
    # Update .log, .err, .out files locally
    condcmd += 'stream_output = True \n'
    condcmd += 'transfer_output_files = "" \n'
    #condcmd += '+JobFlavour = "workday" \n' # 8 hour
    condcmd += '+MaxRuntime = '+str(runTime)+' \n' 
    if cpus>1:
        condcmd += 'RequestCpus = '+str(cpus)+' \n'
    # nodes have been moved to CentOS -> avoid long queues
    #condcmd += "requirements = (OpSysAndVer =?= \"CentOS7\") \n"

    if user == "larcalib": # faster queue for larcalib
        condcmd += "MY.SendCredential=True \n"
        condcmd += '+AccountingGroup="group_u_ATLAS.CAF" \n'
    
    numJobs = sum(1 for line in open(argsFileName))

    condcmd += "arguments = $$([$(Process)+1])\n"  # condor can increment from 0-nproc
    condcmd += "output = "+outdir+"/"+bashname+"_$(Process).out\n"
    condcmd += "error = "+errdir+"/"+bashname+"_$(Process).err\n"
    condcmd += "log = "+logdir+"/"+bashname+"_$(Process).log\n"
    condcmd += "queue "+str(numJobs)+"\n"
    #for job in range(1, numJobs+1):
    #    condcmd += "arguments = "+str(job) +"\n"
    #    condcmd += "output = "+outdir+"/"+bashname+"_"+str(job)+".out\n"
    #    condcmd += "error = "+errdir+"/"+bashname+"_"+str(job)+".err\n"
    #    condcmd += "log = "+logdir+"/"+bashname+"_"+str(job)+".log\n"
    #    condcmd += "queue \n"
    c_launch = open(bashdir + '/' + bashname+'.sub','w')
    c_launch.write(condcmd)
    c_launch.close()
    
    if subNow:
        print("condSub: changing script permissions and submitting jobs")
        os.system('chmod 775 ' + logdir )
        os.system('chmod 775 ' + outdir )
        os.system('chmod 775 ' + errdir )
        os.chdir(bashdir)
        os.system('chmod 744 ' + bashname+'.sh' )
        os.system('chmod 744 ' + bashname+'.sub' )
        os.system('condor_submit '+bashname+'.sub')
        os.chdir(cwd)
    else:
        print("condSub: subNow option set to False - scripts were created but jobs not submitted")
        print("- see scripts in "+bashdir )


