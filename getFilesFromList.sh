#!/bin/sh

# pass input .txt file which contains list of datasets which were stored on disk. Copy them to the requested area
infile=/afs/cern.ch/user/p/pavol/public/apr.txt
gridsite=CERN-PROD_SCRATCHDISK
remote=eos
outdir=test
copy=0   # 0 to make sym links, 1 to copy files/directories

if [ ! -d ${outdir} ]; then
    echo "Output dir does not exist"
    mkdir ${outdir}
fi

while read -r line; do 
    echo "** $line **"
    outname=$(echo $line | cut -d':' -f2)
    echo $outname
    thisoutdir=${outdir}/${outname}
    if [ ! -d ${thisoutdir} ]; then
	echo "Subdirectory dir does not exist"
	mkdir ${thisoutdir}
    fi  
    thefiles=$(rucio list-file-replicas --pfns $line --rse ${gridsite} | grep "/${remote}")
    filearr=($thefiles)
    echo "Found ${#filearr[@]} files"
    for f in ${thefiles[@]};do
	remotepath="/${remote}/${f#*/${remote}/}"
	echo -- $remotepath

	if [ ${copy} -eq 0 ]; then
	    cmd="ln -s ${remotepath} ${thisoutdir}/."
	else
	    cmd="cp -r ${remotepath} ${thisoutdir}/."
	fi
	echo ${cmd}
	eval ${cmd}
    done


done < ${infile}
