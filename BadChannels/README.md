# Tools for manipulating bad channels folders

dumpBad.sh -> dump the bad channels for a given run. Add an argument to dump bad SCs (e.g. sh dumpBad.sh 480957 1)

SConldump.list -> an athena dump of the info needed to construct the SC bad channels (since e.g. slot number does not have the same meaning)


Get online IDs from dump
less filename | awk '{ print $(NF-2) }'

run=XXXXXX
for SC in $SCs; do echo "** $SC **"; for UPD in UPD4 UPD1 UPD3 Bulk; do fname="badSC_${run}_RUN3-$UPD-00.txt"; gr=$(grep $SC $fname); echo "-- $UPD $gr"; done; done


run=XXXXXX
for UPD in UPD4 UPD1 UPD3 Bulk; do fname="badSC_${run}_RUN3-$UPD-00.txt"; wcl=$(less $fname | wc -l); masked=$(less $fname | grep "maskedOSUM" | wc -l); echo "-- $UPD .. $wcl lines, $masked maskedOSUM"; done


less badSC_481278_RUN3-UPD1-00.txt | awk -F# '{print $1}' | cut -d' ' -f7- |


# Print counts of each flag
flags=$(less $fname | awk -F# '{print $1}' | cut -d' ' -f7-|tr "\n" " "); sorted_unique_flags=($(echo "${flags[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' ')); echo "${#sorted_unique_flags[@]} unique flags in $fname"; for fl in ${sorted_unique_flags[@]}; do gr=$(grep $fl $fname | wc -l); echo $fl : $gr; done

