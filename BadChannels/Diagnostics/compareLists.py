import sys, os
import argparse


def readBCfile(inFile):
    if not os.path.isfile(inFile):
        print(f"Requested input file {inFile} does not exist!")
        sys.exit()


    data = {}
    with open(inFile, 'r') as f:
        for line in f:
            line = line.strip("\n")
            chid = line.split("#")[1].strip(" ")
            if "->" in chid:
                chid = chid.split(" ->")[0]
            chid = int(chid,16)
            flags = " ".join(line.split(" ")[6:]).split(" # ")[0].split(" ")
            flags = [ f for f in flags if f != "" ]

            data[chid] = flags
    return data
            
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(dest="file1", type=str, help="Input noisy cell/SC file", default=None)
    parser.add_argument(dest="file2", type=str, help="Input noisy cell/SC file", default=None)
    args = parser.parse_args()

    file1_info = readBCfile(args.file1)
    file2_info = readBCfile(args.file2)
    
    file1_chids = list(file1_info.keys())
    file2_chids = list(file2_info.keys())

    if len(file1_chids) != len(file2_chids):
        print(f"Different number of entries! {args.file1} has {len(file1_chids)} while {args.file2} has {len(file2_chids)}")

    diff1 = [ ch for ch in file1_chids if ch not in file2_chids ]
    if len(diff1) > 0:
        print(f"The following channels are in {args.file1} and not in {args.file2}")
        for d1 in diff1:
            print(f".. {d1} ({' '.join(file1_info[d1])})")
    diff2 = [ ch for ch in file2_chids if ch not in file1_chids ]
    if len(diff2) > 0:
        print(f"The following channels are in {args.file2} and not in {args.file1}")
        for d2 in diff2:
            print(f".. {d2} ({' '.join(file2_info[d2])})")
