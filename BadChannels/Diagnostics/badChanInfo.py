''' Pass a file containing a proposed noisy cell list from the WDE as the input argument. This script will collect some information and summarise it, to help spot common traits between the flagged cells '''
import sys, os
import argparse
sys.path.append("../..")
import printMapping
import pandas as pd


def getMappingInfo(isSC):
    if isSC:
        onlIdName = "SC_ONL_ID"
    else:
        onlIdName = "ONL_ID"
        
    cols = [ onlIdName, "DETNAME", "HVLINES", "CALIB", "CALIB_ID", "FTNAME", "FEB", "CL" ] # "HVSECTOR"

    if isSC:
        cols.extend(["LTDB", "LATOME_SOURCE_ID", "LATOME_FIBRE", "ONL_ID"])
                     
    criteria = {"want":cols, "showdecimal":True}
    pmdf = pd.DataFrame(printMapping.query(**criteria), columns=cols)

    if isSC:
        agg_dict = { c:'first' for c in cols}
        agg_dict['ONL_ID'] = ' '.join
        pmdf = pmdf.groupby(onlIdName, as_index=False).agg(agg_dict)  
        print("OIOI", len(pmdf.index.values))
        print(pmdf)

        cols.remove("ONL_ID")

    pmdf = pmdf.astype({onlIdName:'int'})
    
    #pmdf["CALIB_CL"] = pmdf["CALIB"] + pmdf["CL"]
    pmdf['CALIB_CL'] = pmdf[['CALIB', 'CL']].agg('_'.join, axis=1)
    pmdf = pmdf.set_index(onlIdName)

    return pmdf, onlIdName

def process_list(inFile, isSC=False, badFlags=None, verbose=False, outFile=None, cellsFile=None):
    print("Querying larid.db", isSC)
    pmdf, onlIdName = getMappingInfo(isSC)
    print("Query done")
    geom = {onlIdName:[]}

    toprint = sorted([ c for c in pmdf.columns if c != onlIdName ])
    if isSC:
        toprint.remove("ONL_ID")
    outformat = "{:<12s}{:<40s}"
    longvals = ['HVLINES', 'CALIB_CL', 'CALIB']
    outformat += "".join([ "{:<20s}" if tp not in longvals else "{:<30s}" for tp in toprint ])
    titles = [ onlIdName, "Flag(s)"]
    titles.extend(toprint)
    if isSC:
        titles.append("nch")
        outformat += "{:<5s}"
        titles.append("cellFlags")
        outformat += "{:<60s}"
        
    
    if outFile is not None:
        print(f"Writing output to {outFile}")
        outFile = open(outFile, "w+")
        outFile.write(outformat.format(*titles)+"\n")


    cellsData = None
    if isSC and cellsFile is not None:
        cellsData = {}
        cellsFile = open(cellsFile, 'r')
        for line in cellsFile.readlines():
            line = line.strip("\n")
            chid = line.split("#")[1].strip(" ")
            if "->" in chid:
                chid = chid.split(" ->")[0]
            chid = int(chid,16)
            flags = " ".join(line.split(" ")[6:]).split(" # ")[0].split(" ")
            flags = [ f for f in flags if f != "" ]
            cellsData[chid] = flags
        
    with open(inFile, 'r') as f:
        for line in f:
            if " # 0x" not in line: continue
            line = line.strip("\n")

            flags = " ".join(line.split(" ")[6:]).split(" # ")[0].split(" ")
            flags = [ f for f in flags if f != "" ]

            if badFlags is not None:
                matching = list(set(flags).intersection(badFlags))
                if len(matching) == 0:
                    if verbose: print(f"Skipping line ({', '.join(flags)}), since it has none of the requested flags flags: {', '.join(badFlags)}")
                    continue
            
            chid = line.split("#")[1].strip(" ")
            if "->" in chid:
                chid = chid.split(" ->")[0]
            chid = int(chid,16)
            geom[onlIdName].append(chid)
            row = pmdf.loc[pmdf.index == chid]

            cellFlag = None
            nch = None
            if isSC:
                chids = [ int(ch) for ch in row["ONL_ID"].values[0].split(" ") ]
                nch = len(chids)
                if cellsData is not None:
                    chids = [ int(ch) for ch in row["ONL_ID"].values[0].split(" ") ]
                    cellFlag = []
                    for ch in chids:
                        if ch in cellsData.keys():
                            cellFlag.extend(cellsData[ch])
                    cellFlag = " ".join(cellFlag)

            if outFile is not None:
                printrow = [ str(chid), ",".join(flags) ]
                printrow.extend([ str(row[col].values[0]) for col in toprint ])

                if nch is not None:
                    printrow.append(str(nch))
                
                if cellFlag is not None:
                    printrow.append(cellFlag)
                
                outFile.write(outformat.format(*printrow)+"\n")
                            
            
            for col in pmdf.columns:
                if col not in geom.keys():
                    geom[col] = []
                val = row[col].values[0]
                if len(val.split(" ")) > 1:
                    geom[col].extend([v for v in val.split(" ") if v != "" ])
                else:
                    geom[col].append(val)

    for key in geom.keys():
        thislist = list(set(geom[key]))
        thisnum = len(thislist)

        if key == onlIdName:
            print(f"{thisnum} channels in list")
        else:
            counts = {}
            for item in thislist:
                counts[item] = geom[key].count(item)

            order_counts = counts.values()
            order_counts = list(set([m for m in order_counts if m != 1]) )
            order_counts = sorted(order_counts, reverse=True)
            #maxcount = max(counts.values())
            #maxitem = [ k for k in counts.keys() if counts[k] == maxcount][0]
            print(f"==== {key} {'='*20}")
            if len(thislist) <= 20:
                print(f"{thisnum} {key}: {' '.join(thislist)}")
            else:
                print(f"{thisnum} {key}: {' '.join(thislist[:20])}...")
            for i in range(0, 3):
                if len(order_counts) > i:
                    maxitem = [ k for k in counts.keys() if counts[k] == order_counts[i]][0]
                    if i == 0: 
                        suff = "st"
                    elif i == 1:
                        suff = "nd"
                    elif i ==2 :
                        suff = "rd"
                    print(f"... {i+1}{suff} most frequent is {maxitem} with {order_counts[i]} occurences")


    if outFile is not None:
        outFile.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--inFile", dest="inFile", type=str, help="Input noisy cell/SC file", required=True, default=None)
    parser.add_argument("-SC", "--isSC", dest="isSC", default=False, action='store_true', help="SC list?")
    parser.add_argument('-f', '-badFlag', nargs='+', dest="badFlag", default=None, help='Only print info for cells with these flags? e.g. deadCalib. If no argument is provided, the script will simply consider all flags.')
    parser.add_argument('-o', '-outFile', dest="outFile", default=None, help='Destination txt file to write out a more verbose cell list with geom info.')

    parser.add_argument('-c', '-cellsFile', dest="cellsFile", default=None, help='File with bad cells, in case we are studying bad SC list and want to compare.')
    
    args = parser.parse_args()

    if args.outFile is None:
        inName = os.path.splitext(os.path.basename(args.inFile))[0]
        args.outFile = f"./out_{inName}"
        if args.badFlag is not None:
            args.outFile += "_".join(args.badFlag)
        args.outFile += ".txt"
        
    # Get the list of possible bad channel flags 
    badFlags = [ l.replace("Bit","") for l in list(printMapping.convertBadChanEnum(args.isSC).keys()) ]

    if args.badFlag is not None:
        for bf in args.badFlag:
            if bf not in badFlags:
                print(f"!!! requested bad channel flag {bf} was not found in the known list: {badFlags}")
                sys.exit()
    else:
        args.badFlag = badFlags

    print(f"Possible flags are {', '.join(badFlags)}")
        
    process_list(args.inFile, isSC=args.isSC, badFlags=args.badFlag, outFile=args.outFile, cellsFile=args.cellsFile)
    
