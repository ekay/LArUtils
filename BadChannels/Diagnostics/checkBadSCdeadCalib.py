import sys, os
sys.path.append("../..")
import printMapping
import pandas as pd
import argparse

def badSCInfo(SC):
    SClookup = open("../SConldump.list", "r").readlines()
    theline = [ l for l in SClookup if str(SC) in l ][0].strip()
    fields = theline.split('\t')
    info = [ l.strip() for l in fields[2:] ]
    return " ".join(info)


def readBadChanFile(badChanFile):
    f = open(badChanFile, "r")
    f = f.read()
    test=0
    info = {}
    for line in f.split("\n"):
        line = line.strip()
        if line == "": continue
        try:
            onlId = line.split("# ")[1].split(" ")[0]
        except:
            print("OIOI",line)
            sys.exit()
        #print(onlId)
        line = line.split(" #")[0]
        #print(line)
        flags = line.split(" ")[6:-1]
        #print(flags)
        info[int(onlId,16)] = flags
        test+=1
    return info


def process(run, flagToCheck, badChanfile, badSCfile, outfilepath=None):

    want = ["SC_ONL_ID","ONL_ID","BadChannels_UPD1", "BadSC", "CALIB", "CL"]
    inputs={"want":want,"showdecimal":True, "run":run}
    print("querying printMapping for",inputs)
    pmout = printMapping.query(**inputs)
    pmdf = pd.DataFrame(pmout, columns = want)
    pmdf = pmdf.astype({'SC_ONL_ID':'int', 'ONL_ID':'int'})

    boardline = []

    the_SCs = []
    the_SCs_noDCflag = []
    the_SCs_possUnflag = []

    for SC in list(set(pmdf.SC_ONL_ID.values)):
        if SC == 0: continue
        theseCells = pmdf[pmdf.SC_ONL_ID==SC].ONL_ID.values
        ncells = len(theseCells)
        badSCstatus = list(set(pmdf[pmdf.SC_ONL_ID==SC].BadSC.values))
        badCellstatus = pmdf[pmdf.SC_ONL_ID==SC].BadChannels_UPD1.values

        # Override pm data if we provide bad channel file dumps
        if badSCfile is not None:
            if SC in badSCfile.keys():
                badSCstatus = badSCfile[SC]
            else:
                badSCstatus = '-'
        if badChanfile is not None:
            badCellstatus = ['-']*ncells
            for i,cell in enumerate(theseCells):
                if cell in badChanfile.keys():
                    badCellstatus[i] = ", ".join(badChanfile[cell])

        matching = [s for s in badCellstatus if flagToCheck in s]
        if len(matching) >= ncells/2:        
            if flagToCheck not in badSCstatus:
                the_SCs_noDCflag.append(SC)

            the_SCs.append(SC)
            #print(pmdf[pmdf.SC_ONL_ID==SC])
            this_pmdf = pmdf[pmdf.SC_ONL_ID==SC]
            if badChanfile is not None:
                deadIDs = [ ch for ch in theseCells if ch in badChanfile.keys() and flagToCheck in badChanfile[ch] ]
                dead = this_pmdf[this_pmdf.ONL_ID.isin(deadIDs)]
            else:
                dead = this_pmdf[this_pmdf.BadChannels_UPD1.str.contains(flagToCheck )]

            boards = dead[dead.SC_ONL_ID==SC].CALIB.values
            lines = dead[dead.SC_ONL_ID==SC].CL.values
            this_boardline= [] 
            for board in boards:
                for line in lines:
                    if " " in line:
                        subline = "_".join(line.split(" "))
                        boardline.append(f"{board}_{subline}")
                        this_boardline.append(f"{board}_{subline}")
                    else:
                        boardline.append(f"{board}_{line}")
                        this_boardline.append(f"{board}_{line}")
                    #print(f"{board}_{line}")
            print("*"*4,SC,f"has {flagToCheck} for {len(matching)}/{ncells} constituent cells ({', '.join(list(set(this_boardline)))}) - status in bad SC DB is:",badSCstatus, f", bad cell status is: {badCellstatus}")

        else:
            if flagToCheck in badSCstatus:
                print("!"*4, "ATTENTION", "!"*4, SC,f"has {flagToCheck} for {len(matching)}/{ncells} constituent cells - status in bad SC DB is:",badSCstatus)

                the_SCs_possUnflag.append(SC)


    boardline = sorted(list(set(boardline)))
    print(f"{len(boardline)} calibration boards or lines responsible for {flagToCheck} in SCs:")
    for bl in boardline:
        print(bl)


    if len(the_SCs_noDCflag) > 0:
        print("")
        print("*"*20)
        print(len(the_SCs_noDCflag), "SCs which may need to be flagged:")
        for SC in the_SCs_noDCflag:
            print(SC)
        print("*"*20)


    if len(the_SCs_possUnflag) > 0:
        print("")
        print("*"*20)
        print(len(the_SCs_possUnflag), "SCs which may need to be unflagged:")
        for SC in the_SCs_possUnflag:
            print(SC)
        print("*"*20)

    outfile=None
    if outfilepath is not None:
        outfile = open(outfilepath, 'w')
        print("Writing proposed list to",outfilepath)
    if badSCfile is not None:
        if outfile is None:
            print("")
            print("*"*20)
            print("Proposed new bad SC list")
            print("*"*20)
        f = open(badSCfilePath, "r")
        f = f.read()
        filled = []
        for line in f.split("\n"):
            line = line.strip()
            if line == "": continue
            try:
                onlId = line.split("# ")[1].split(" ")[0]
            except:
                print("OIOI",line)
                sys.exit()
            flags = line.split(" #")[0]
            flags = flags.split(" ")[6:-1]
            id_dec = int(onlId,16)

            if id_dec in the_SCs_noDCflag:
                line = line.replace(" #", f"{flagToCheck} #")
                line += f"... {flagToCheck} added due to >= 50% of LAr cells having this flag"
            if id_dec in the_SCs_possUnflag:
                if len(flags) == 1:
                    continue
                line = line.replace(f"{flagToCheck} ", "")
                line += f"... {flagToCheck} removed due to < 50% of LAr cells having this flag"
            filled.append(id_dec)
            if outfile is not None:
                outfile.write(f"{line}\n")
            else:
                print(line)
        tofill = [ SC for SC in the_SCs_noDCflag if SC not in filled ]
        for SC in tofill:
            this_info = badSCInfo(SC)
            toprint = f"{this_info} {flagToCheck} # {hex(SC)} adding to list due to >= 50% of LAr cells having this flag"
            if outfile is not None:
                outfile.write(f"{toprint}\n")
            else:
                print(toprint)
    if outfilepath is not None:
        outfile.close()
        print("Wrote proposed list to",outfilepath)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Bad SC list amendment')
    parser.add_argument('-r', '-run', dest='run', type=int, default=printMapping.get_latest_run(), help='Run number - used for bad channels. Default is latest run: %(default)s')
    parser.add_argument('-f', '--flag', dest='flagToCheck', default='deadCalib', help='The flag that we are checking and updating. Default %(default)s')
    
    args   = parser.parse_args()

    badSCfile = None
    badChanfile = None
    badSCfilePath = f"../badSC_{args.run}_LAR_ONL-RUN3-UPD1-00.txt" # badSC_{args.run}_RUN3-UPD4-00.txt"
    badChanfilePath = f"../badChan_{args.run}_LAR_ONL-RUN2-UPD1-25.txt"  #"../badChan_{args.run}_RUN2-UPD4-00.txt"

    if os.path.isfile(badSCfilePath):
        badSCfile = readBadChanFile(badSCfilePath)
        print(len(badSCfile.keys()), "bad SCs from file", badSCfilePath)
    if os.path.isfile(badChanfilePath):
        badChanfile = readBadChanFile(badChanfilePath)
        print(len(badChanfile.keys()), "bad channels from file",badChanfilePath)


    outfile = f"badSC_{args.flagToCheck}_{args.run}.txt"

    process(args.run, args.flagToCheck, badChanfile, badSCfile, outfile)
