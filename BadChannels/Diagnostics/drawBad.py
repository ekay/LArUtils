import sys,os
sys.path.append("../..")
import printMapping
from argparse import ArgumentParser
import pandas as pd
import numpy as np
from PulsePattern import makePlot, getEtaPhiRange

import ROOT as R
R.gROOT.SetBatch(True)
R.gStyle.SetPalette(R.kRainbow)
R.gStyle.SetOptStat(0)

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-i', '--inputFile', dest='infile', help='Input bad SC file', type=str, required=True)
    parser.add_argument('-isSC', dest='isSC', action='store_true', default=False, help='SuperCells?')
    parser.add_argument('-f', '--flag', dest='flag', nargs='+', default=['maskedOSUM', 'sporadicBurstNoise', 'highNoiseHG'], help='Flags to plot. Default %(default)s')
    parser.add_argument('-v', '--verbose', default=False, action='store_true')
    parser.add_argument('-n', '--name', default=None, help='optional string to add to output png file name')
    args = parser.parse_args()

    if args.name is None:
        args.name = os.path.basename(args.infile).split(".")[0]

    args.name += f"_{'-'.join(args.flag)}"
        
    if args.isSC:
        onlid = "SC_ONL_ID"
        eta = "SCETA"
        phi = "SCPHI"                
    else:
        onlid = "ONL_ID"
        eta = "ETA"
        phi = "PHI"

    query = [ onlid, eta, phi, "SAM", "DETNAME", "QUADRANT", "FEB", "CALIB", "FTNAME" ]
    if args.isSC: query.extend(["LATOME_NAME", "LATOME_FIBRE", "LTDB"])
    criteria = {"want":query}
    pmdf = pd.DataFrame(printMapping.query(**criteria), columns=query)
    pmdf = pmdf.set_index(onlid)
    

    if not os.path.isfile(args.infile):
        print("Requested file",args.infile,"does not exist")
        sys.exit()
    
    onlIDs = []
    coords = {}
    with open(args.infile, "r") as inf:
        for line in inf.readlines():
            ID = line.split("# ")[1].split(" ->")[0]
            reasons = line.split("  # ")[0].split(" 0 ")[-1]
            if not any([f in reasons for f in args.flag]):
                continue
            row = pmdf.loc[ID]
            thiseta = row[eta]
            thisphi = row[phi]
            thissam = row["SAM"]
            thisdet = row["DETNAME"]
            if not isinstance(thiseta, str):
                thiseta = thiseta.iloc[0]
                thisphi = thisphi.iloc[0]
                thissam = thissam.iloc[0]
                thisdet = thisdet.iloc[0]
                #print(ID, ":", reasons, "("+", ".join([str(c)+":"+str(row.iloc[0][c])  for c in row.keys()])+")")
            #else:
            #    print(ID, ":", reasons, "("+", ".join([str(c)+":"+str(row[c]) for c in row.keys()])+")")
            


            try:
                if thisdet not in coords.keys():
                    coords[thisdet] = []
            except Exception as e:
                print(e)
                sys.exit()
                
            coords[thisdet].append((float(thiseta), float(thisphi), int(thissam)))
    for det in coords.keys():

        if len(coords[det]) > 0:
            #makePlot(coords[det], det, args.name, args.isSC)
            poss_layers = list(set([c[2] for c in coords[det]]))

            etamin,etamax,neta,phimin,phimax,nphi = getEtaPhiRange(det, args.isSC)


            def drawHist(layer=None):
                title = f"{det} {', '.join(args.flag)}"
                name = f"{args.name}_{det}"
                if layer is not None:
                    title += f" layer {layer}"
                    name += f"_layer{layer}"
                    etas = np.array([c[0] for c in coords[det] if int(c[2])==layer], dtype='d')
                    phis = np.array([c[1] for c in coords[det] if int(c[2])==layer], dtype='d')
                else:
                    etas = np.array([c[0] for c in coords[det]], dtype='d')
                    phis = np.array([c[1] for c in coords[det]], dtype='d')
                h = R.TH2D(args.name, title, neta, etamin, etamax, nphi, phimin, phimax)
                
                
                val = np.array([1 for c in etas], dtype='d')
                
                h.GetXaxis().SetTitle("#eta")
                h.GetYaxis().SetTitle("#phi")
                h.SetTitleOffset(1.08)
                h.GetXaxis().SetTitleOffset(0.4)
                h.GetYaxis().SetTitleOffset(0.4)
                h.GetZaxis().SetTitleOffset(0.4)
                h.SetTitleSize(0.05)

                h.FillN(len(etas), etas, phis, val)

                canv = R.TCanvas()
                h.Draw("COL")
                canv.Print(f"{name}.png")
            drawHist()

            for layer in poss_layers:
                drawHist(layer)
                
