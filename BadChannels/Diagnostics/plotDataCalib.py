import sys, os, glob
import ROOT as R
import numpy as np
import awkward as ak
import uproot as U
from argparse import ArgumentParser
import re

R.gROOT.SetBatch(True)
sys.path.append('../..')
sys.path.append('../../pickPhase')
from newShift import getPMinfo, getOFC, getPed
import condSub

R.gStyle.SetOptStat(0)

def getRamp(ped, verbose=False, cut=None):
    ped = str(ped).zfill(8)
    searchTerm = "LArRamp"
    treeName = "RAMPS"
    wildcard=f"/afs/cern.ch/user/e/ekay/APoutput/{ped}*/root_files/{searchTerm}_*.root"
    if verbose: print(f"Looking for {searchTerm} in {wildcard}")
    rampFiles = glob.glob(wildcard)
    if len(rampFiles) == 0:
        print(f"No OFC files found with wildcard {wildcard}")
        sys.exit()

    rampFiles = [ f"{f}:{treeName}" for f in rampFiles ]
    dfs = []
    ch_filled = []
    for arrays,report in U.iterate(rampFiles, expressions=["channelId","ADC", "DAC", "X", "corrUndo"], step_size="1 GB", report=True, cut=cut):
        these_ch = list(set(list(arrays.channelId)))
        if verbose: print("Reading chunk from:", report.file_path, "...", len(these_ch), "channels")
        tofill = [ch for ch in these_ch if ch not in ch_filled ]
        dfs.append(arrays)
        ch_filled.extend(list(arrays.channelId))
        ch_filled = list(set(ch_filled))
    arr = ak.concatenate(dfs, axis=0)
    chids_all = list(arr.channelId)
    chids = list(set(list(arr.channelId)))
    if verbose: print(len(chids), f"channels in {searchTerm} {treeName} data")

    return arr, chids

def getCaliWave(ped, verbose=False, cut=None, retFile=False, inFiles=None):
    ped = str(ped).zfill(8)
    searchTerm = "LArCaliWave"
    treeName = "CALIWAVE"

    if inFiles is not None:
        caliwaveFiles = [ f"{f}:{treeName}" for f in inFiles ]
    else:
        wildcard=f"/afs/cern.ch/user/e/ekay/APoutput/{ped}*/root_files/{searchTerm}_*.root"
        if verbose: print(f"Looking for {searchTerm} in {wildcard}")
        caliwaveFiles = glob.glob(wildcard)
        if len(caliwaveFiles) == 0:
            print(f"No OFC files found with wildcard {wildcard}")
            sys.exit()
        caliwaveFiles = [ f"{f}:{treeName}" for f in caliwaveFiles ]
    dfs = []
    ch_filled = []
    files = []
    for arrays,report in U.iterate(caliwaveFiles, expressions=["channelId", "detector", "layer", "ieta", "iphi", "Amplitude", "corrUndo", "Time", "MaxAmp", "TmaxAmp"], step_size="1 GB", report=True, cut=cut):
        these_ch = list(set(list(arrays.channelId)))
        if verbose: print("Reading chunk from:", report.file_path, "...", len(these_ch), "channels")
        tofill = [ch for ch in these_ch if ch not in ch_filled ]
        dfs.append(arrays)
        ch_filled.extend(list(arrays.channelId))
        ch_filled = list(set(ch_filled))
        if len(ch_filled) != 0:
            files.append(report.file_path)
    arr = ak.concatenate(dfs, axis=0)
    chids_all = list(arr.channelId)
    chids = list(set(list(arr.channelId)))
    if verbose: print(len(chids), f"channels in {searchTerm} {treeName} data")
    if not retFile:
        return arr, chids
    else:
        return arr, chids, files

def getPhysWave(ped, verbose=False, cut=None, retFile=False, inFiles=None):
    ped = str(ped).zfill(8)
    searchTerm = "LArPhysWave"
    treeName = "PHYSWAVE"

    if inFiles is not None:
        physwaveFiles = [ f"{f}:{treeName}" for f in inFiles ]
    else:
        wildcard=f"/afs/cern.ch/user/e/ekay/APoutput/{ped}*/root_files/{searchTerm}_*.root"
        if verbose: print(f"Looking for {searchTerm} in {wildcard}")
        physwaveFiles = glob.glob(wildcard)
        if len(physwaveFiles) == 0:
            print(f"No OFC files found with wildcard {wildcard}")
            sys.exit()
        physwaveFiles = [ f"{f}:{treeName}" for f in physwaveFiles ]
    dfs = []
    ch_filled = []
    files = []
    for arrays,report in U.iterate(physwaveFiles, expressions=["channelId", "Amplitude", "corrUndo", "Time"], step_size="1 GB", report=True, cut=cut):
        these_ch = list(set(list(arrays.channelId)))
        if verbose: print("Reading chunk from:", report.file_path, "...", len(these_ch), "channels")
        tofill = [ch for ch in these_ch if ch not in ch_filled ]
        dfs.append(arrays)
        ch_filled.extend(list(arrays.channelId))
        ch_filled = list(set(ch_filled))
        if len(ch_filled) != 0:
            files.append(report.file_path)
    arr = ak.concatenate(dfs, axis=0)
    chids_all = list(arr.channelId)
    chids = list(set(list(arr.channelId)))
    if verbose: print(len(chids), f"channels in {searchTerm} {treeName} data")
    if not retFile:
        return arr, chids
    else:
        return arr, chids, files

def getPulseAll(prun, verbose=False, cut=None):
    searchTerm = "LArDigits"
    treeName = "SCDIGITS"
    wildcard=f"/eos/home-l/lardaq/P1Results/LATOMERun_PulseAll_{prun}*/{searchTerm}*{prun}*.root"
    if verbose: print(f"Looking for {searchTerm} in {wildcard}")
    digiFiles = glob.glob(wildcard)
    if len(digiFiles) == 0:
        print(f"No PulseAll files found with wildcard {wildcard}")
        sys.exit()

    digiFiles = [ f"{f}:{treeName}" for f in digiFiles ]
    dfs = []
    ch_filled = []
    for arrays,report in U.iterate(digiFiles, expressions=["channelId","mean"], step_size="1 GB", report=True, cut=cut):
        these_ch = list(set(list(arrays.channelId)))
        if verbose: print("Reading chunk from:", report.file_path, "...", len(these_ch), "channels")
        tofill = [ch for ch in these_ch if ch not in ch_filled ]
        dfs.append(arrays)
        ch_filled.extend(list(arrays.channelId))
        ch_filled = list(set(ch_filled))
    arr = ak.concatenate(dfs, axis=0)
    chids_all = list(arr.channelId)
    chids = list(set(list(arr.channelId)))
    if verbose: print(len(chids), f"channels in {searchTerm} {treeName} data")

    return arr, chids



def diagnostics(inputFile, runNum, weekly_ped, channelId, pulseAll=None, treeName="SCDIGITS", highMu=True, outDir=".", step_size="1 GB", outStr=None):

    cut = "|".join([ f"( channelId == {lid} )" for lid in channelId ]) # too long if more than fcal
    print("Using cut:", cut)

    
    ofcData, ofcChids = getOFC(weekly_ped, isCali=False, verbose=True, highMu=highMu, cut=cut)

    pedData, pedChids = getPed(weekly_ped, verbose=True, cut=cut)

    rampData, rampChids = getRamp(weekly_ped, verbose=True, cut=cut)
    rampData_corrUndo = rampData[rampData.corrUndo == 1]
    rampData = rampData[rampData.corrUndo == 0]
    rampChids_corrUndo = list(rampData_corrUndo.channelId)

    caliwaveData, caliwaveChids, caliwaveFiles = getCaliWave(weekly_ped, verbose=True, cut=cut, retFile=True)
    caliwaveData_corrUndo = caliwaveData[caliwaveData.corrUndo == 1]
    caliwaveData = caliwaveData[caliwaveData.corrUndo == 0]
    caliwaveChids_corrUndo = list(caliwaveData_corrUndo.channelId)

    # WE DON@T HAVE DETECTOR AND LAYER IN THE PHYSWAVE NTUPLES
    physwaveData, physwaveChids, physwaveFiles = getPhysWave(weekly_ped, verbose=True, cut=cut, retFile=True)
    physwaveData_corrUndo = physwaveData[physwaveData.corrUndo == 1]
    physwaveData = physwaveData[physwaveData.corrUndo == 0]
    physwaveChids_corrUndo = list(physwaveData_corrUndo.channelId)

    
    if pulseAll is not None:
        digiData, digiChids = getPulseAll(pulseAll, verbose=True, cut=cut)
    else:
        digiData, digiChids = None, None

    pmdf = getPMinfo(run=runNum)

    print("*"*10)

    branches = ["channelId","samples_ADC_BAS"]

    verbose=True



    
    out_txtname = f"{outDir}/scanBadSC"
    if outStr is not None:
        out_txtname += f"_{outStr}"
    out_txtname += ".txt"

    out_txt = open(out_txtname, "w+")


    printcols = [ "Channel", "CurrentFlag", "Missing Calib?", "NoDataPulse?", "BadPhase?", "BadMaxAmp?" ]
    outformat = "{:<12s}{:<45s}"+"{:<18s}"*(len(printcols)-2)
    out_txt.write(outformat.format(*printcols) + "\n")
    
    for arrays,report in U.iterate(files=[f'{inputFile}:{treeName}'], expressions=branches, step_size=step_size, recover=True, library="ak", report=True, allow_missing=True, cut=cut):
        these_ch = list(set(list(arrays.channelId)))
        if verbose: print("Reading chunk from:", report.file_path, "...", len(these_ch), "channels")
        for ch in these_ch:


            missingCalib, noDataPulse, badPhase, badMaxAmp = 0, 0, 0, 0
            
            pm_info = pmdf.loc[int(ch)]
                        
            this_arr = arrays[arrays.channelId==ch]

            if ch in ofcChids:
                ofcs = ofcData[ofcData.channelId==ch]
            else:
                ofcs = None
                print(f"{ch} MISSING OFC")
                missingCalib = 1
            if ch in pedChids:
                ped = pedData[pedData.channelId==ch].ped[0]
            else:
                ped = None
                print(f"{ch} MISSING PED")
                missingCalib = 1
            if ch in rampChids:
                ramp = rampData[rampData.channelId==ch]
            else:
                ramp = None
                print(f"{ch} MISSING RAMP")
                missingCalib = 1
            if ch in rampChids_corrUndo:
                ramp_corrUndo = rampData_corrUndo[rampData_corrUndo.channelId==ch]
            else:
                ramp_corrUndo = None
                
            if ch in caliwaveChids:
                caliwave = caliwaveData[caliwaveData.channelId==ch]
            else:
                caliwave = None
                print(f"{ch} MISSING CALIWAVE")
                missingCalib = 1
            if ch in caliwaveChids_corrUndo:
                caliwave_corrUndo = caliwaveData_corrUndo[caliwaveData_corrUndo.channelId==ch]
            else:
                caliwave_corrUndo = None


            if ch in physwaveChids:
                physwave = physwaveData[physwaveData.channelId==ch]
                physwave["MaxAmp"] = ak.max(physwave.Amplitude)
                print("OIOIOI", physwave, physwave.MaxAmp)
            else:
                physwave = None
                print(f"{ch} MISSING PHYSWAVE")
                missingPhysb = 1
            if ch in physwaveChids_corrUndo:
                physwave_corrUndo = physwaveData_corrUndo[physwaveData_corrUndo.channelId==ch]
                physwave_corrUndo["MaxAmp"] = ak.max(physwave_corrUndo.Amplitude)
                                
            else:
                physwave_corrUndo = None

                
            if ofcs is None:
                # printcols = [ "Channel", "CurrentFlag", "Missing Calib?", "NoDataPulse?", "BadPhase?", "BadMaxAmp?" ]
                printcols = [str(s) for s in [ ch, pm_info["BadSC"],  missingCalib, "-", "-", "-"]]                
                out_txt.write(outformat.format(*printcols) + "\n")
                continue
                
            ofca = ofcs.OFCa[:,0:4]
            ofcb = ofcs.OFCb[:,0:4]

            
            phases = ofcs.Phase

            def getEneTau(ch, these_samples, outstr):
                maxe = [22,-1000]
                mint = [22,1000]
                phdict = {}

                for ph in phases: # probably could do this in a columnar way
                    ofca_ph = ak.Array(np.tile(ofca[ph],(len(these_samples),1)))
                    ofcb_ph = ak.Array(np.tile(ofcb[ph],(len(these_samples),1)))
                    sam_ofca = these_samples*ofca_ph
                    sam_ofcb = these_samples*ofcb_ph
                    ene = ak.sum(sam_ofca,axis=1)
                    tau = ak.sum(sam_ofcb,axis=1) / ene
                    avene = ak.mean(ene)
                    avtau = ak.mean(tau)
                    if avene > maxe[1]:
                        maxe = [ph,avene]
                    if abs(avtau) < abs(mint[1]):
                        mint = [ph,avtau]
                    phdict[ph] = [avene,abs(avtau)]

                enes = np.array([phdict[ph][0] for ph in phdict.keys()], dtype='d')
                taus = np.array([phdict[ph][1] for ph in phdict.keys()], dtype='d')

                tmp_newph = -1
                if maxe[0] == mint[0]:
                    print(f"{ch}  {maxe[0]}  # ") #{fibre}")
                    #if "intime" in outstr:
                    #    fibvals[fibre][0] += 1
                else:
                    sorted_e = np.argsort(enes)[::-1]
                    sorted_t = np.argsort(abs(taus))
                    print(f"Phases for {ch} {outstr} max E, ordered:", list(sorted_e))
                    print(f"Phases for {ch} {outstr} min t, ordered:", list(sorted_t))

                    # See if we can match the phases, if min tau phase is in the top 5 max e phases
                    if np.isin(mint[0], sorted_e[0:10]):
                        tmp_newph = mint[0]
                        print(f"{ch} phase {outstr} FIXED {mint[0]} in top 10 max E")
                    else:
                        intercept = sorted_t[0:10][np.in1d(sorted_t[0:10], sorted_e[0:10])]
                        if len(intercept) > 0:
                            tmp_newph = intercept[0]
                            print(f"{ch} {outstr} FIXED: {intercept[0]}")
                        else:
                            print(f"{ch} {outstr}: Couldn't improve {maxe[0]}, {mint[0]}")
                    if tmp_newph == -1:
                        print(f"!!!! {ch}  {maxe[0]}  {mint[0]} #") # {fibre}")
                    else:
                        print(f"**** {ch}  {maxe[0]}  {mint[0]}  ===> {tmp_newph}")
                    #if "intime" in outstr:
                    #    fibvals[fibre][1] += 1

                
                if tmp_newph == -1:
                    tmp_newph = None

                        
                return enes, taus, maxe, mint, tmp_newph

            
            ADC = [ adc for adc in ramp.ADC if len(adc) != 0 ][0]
            DAC = [ dac for dac in ramp.DAC if len(dac) != 0 ][0]



            print("*"*30)
            print(f"channel {ch}")
            print(f"pedestal {ped}")
            print(f"OFCs {ofca} {len(ofca)} {ofcb} {len(ofcb)} {ofcs.Phase}")
            print(f"ramp {ADC} {DAC}")

            if ped is None: continue
            this_arr["samples"] = this_arr.samples_ADC_BAS/8 - ped


            # Skip events where the max adc-ped is < 2
            maxover = 2
            this_arr = this_arr[(ak.max(this_arr.samples,axis=1) >= maxover)]
            if len(this_arr)==0:
                print(f"All pulses for channel {ch} have peak < {maxover}")
                noDataPulse = 1
                # printcols = [ "Channel", "CurrentFlag", "Missing Calib?", "NoDataPulse?", "BadPhase?", "BadMaxAmp?" ]
                printcols = [str(s) for s in [ ch, pm_info["BadSC"],  missingCalib, noDataPulse, "-", "-"]]
                out_txt.write(outformat.format(*printcols) + "\n")                
                continue

            
            this_arr["intime"] = this_arr.samples[:,1:5] # first and last sample should be out of time
            this_arr["before"] = this_arr.samples[:,0:4] # first and last sample should be out of time
            this_arr["after"] = this_arr.samples[:,2:6] # first and last sample should be out of time

            timings=["before", "after", "intime"]

            meanpulse = np.array(ak.mean(this_arr["samples"], axis=0), dtype='d')
            maxsam = ak.max(this_arr["samples"]) #ak.max(this_arr["samples"],axis=1)[0]
            minsam = ak.min(this_arr["samples"]) #ak.min(this_arr["samples"],axis=1)[0]

            maxind = ak.argmax(this_arr["samples"],axis=1)
            maxind_mean = ak.mean(maxind)
            maxind_std = ak.std(maxind)

            if digiData is None:
                canv = R.TCanvas(f"canv_{ch}", f"canv_{ch}", 2400, 1600)
                canv.Divide(2,2)
            else:
                canv = R.TCanvas(f"canv_{ch}", f"canv_{ch}", 3600, 1600)
                canv.Divide(3,2)

            canv.objs=[]
            canv.cd(1)
            pulsegr = {}
            ngr = 0
            for ev in this_arr["samples"]:
                pulsegr[ngr] = R.TGraph(len(meanpulse), np.arange(0,len(meanpulse),dtype='d'), ev.to_numpy().astype('d'))
                pulsegr[ngr].SetTitle(f"Channel {ch}: Pulse Shape From Run {runNum}")
                pulsegr[ngr].GetHistogram().SetMinimum(minsam-(abs(minsam)*.05))
                pulsegr[ngr].GetHistogram().SetMaximum(maxsam+(abs(maxsam)*.05))
                pulsegr[ngr].GetHistogram().GetXaxis().SetLimits(0,5)
                if ngr == 0:
                    pulsegr[ngr].Draw("Apl")
                    pulsegr[ngr].GetXaxis().SetTitle("Sample")
                    pulsegr[ngr].GetYaxis().SetTitle("ADC_BAS/8 - ped")
                else:
                    pulsegr[ngr].Draw("same,pl")        
                canv.objs.append(pulsegr[ngr])
                ngr+=1
            meangr = R.TGraph(len(meanpulse), np.arange(0,len(meanpulse),dtype='d'), meanpulse)
            meangr.SetMarkerStyle(20)
            meangr.SetMarkerColor(R.kRed)
            meangr.Draw("same,p")

            txt = R.TLatex()
            txt.SetTextColor(R.kBlue)
            txt.SetTextAlign(10)
            txt.SetTextSize(0.04)
            line_run = {}
            towrite = [ f"OnlID: {ch}", f"Fibre: {pm_info['LATOME_FIBRE']}", f"Flag: {pm_info['BadSC']}", f"Mean peak sample (0-5) = {round(maxind_mean,2)}", f"std. dev. max sampling = {round(maxind_std,2)}" ]
            #ytxt = 0.95*maxsam
            #xtxt = 2.5
            xtxt = .5
            ytxt = .85
            for tw in towrite:
                txt.DrawLatexNDC(xtxt, ytxt, str(tw))
                ytxt -= 0.06 #*maxsam


            

            canv.cd(2)

            rampgr = R.TGraph(len(ADC), np.array(DAC, dtype='d'), np.array(ADC, dtype='d'))
            rampgr.SetTitle(f"Channel {ch}: Ramp Slope. Pedestal {weekly_ped}.")
            rampgr.SetMarkerStyle(20)
            rampgr.Draw("Ap")
            rampgr.GetXaxis().SetTitle("DAC")
            rampgr.GetYaxis().SetTitle("ADC")
            canv.objs.append(rampgr)
            ramp_line = {}
            nfit = 0
            rtxt = {}
            xtxt = .5
            ytxt = .85

            for X in ramp.X:
                if len(X) == 0: continue
                ramp_line[nfit] = R.TLine( X[0], 0, (X[1]*DAC[-1])+X[0], DAC[-1])
                ramp_line[nfit].SetLineColor(R.kBlue)
                ramp_line[nfit].SetLineWidth(2)
                ramp_line[nfit].Draw("samel")
                canv.objs.append(ramp_line[nfit])
                rtxt[nfit] = R.TLatex()
                rtxt[nfit].SetTextColor(R.kBlue)
                rtxt[nfit].SetTextAlign(10)
                rtxt[nfit].SetTextSize(0.04)
                towrite = [ f"intercept: {X[0]:.2f} (DAC)", f"gradient: {X[1]:.2f}" ]
                for tw in towrite:
                    rtxt[nfit].DrawLatexNDC(xtxt, ytxt, str(tw))
                    ytxt -= 0.06
                nfit+=1

            if ramp_corrUndo is not None:
                for X in ramp_corrUndo.X:
                    if len(X) == 0: continue
                    ramp_line[nfit] = R.TLine( X[0], 0, (X[1]*DAC[-1])+X[0], DAC[-1])
                    ramp_line[nfit].SetLineColor(R.kRed)
                    ramp_line[nfit].SetLineWidth(2)
                    ramp_line[nfit].Draw("samel")
                    canv.objs.append(ramp_line[nfit])
                    rtxt[nfit] = R.TLatex()
                    rtxt[nfit].SetTextColor(R.kRed)
                    rtxt[nfit].SetTextAlign(10)
                    rtxt[nfit].SetTextSize(0.04)
                    towrite = [ f"corrUndo = 1", f"intercept: {X[0]:.2f} (DAC)", f"gradient: {X[1]:.2f}" ]
                    for tw in towrite:
                        rtxt[nfit].DrawLatexNDC(xtxt, ytxt, str(tw))
                        ytxt -= 0.06

                

            if digiData is not None:
                canv.cd(3)
                p1 = R.TPad("p1", "", 0, 0, 1, 1)
                p2 = R.TPad("p2", "", 0, 0, 1, 1)
                p1.SetFillStyle(4000) # transparent
                p2.SetFillStyle(4000) # transparent
                p1.Draw()
                p1.cd()
                digits = digiData[digiData.channelId==ch].mean
                meandigits = np.array(ak.mean(digits, axis=0), dtype='d')
                meandigits = meandigits - meandigits[0] # minus ped
                #maxsam = ak.max(digits) 
                #minsam = ak.min(digits)
                maxsam = ak.max(meandigits)
                minsam = ak.min(meandigits)
                pulsegr_pa = {}
                ngr = 0
                if caliwave is not None:
                    pmax = caliwave.MaxAmp[0] * 1.05
                    if pmax > maxsam:
                        maxsam = pmax
                if caliwave_corrUndo is not None:
                    pmax = caliwave_corrUndo.MaxAmp[0] * 1.05
                    if pmax > maxsam:
                        maxsam = pmax

                if physwave is not None:
                    pmax = physwave.MaxAmp[0] * 1.05
                    if pmax > maxsam:
                        maxsam = pmax
                if physwave_corrUndo is not None:
                    pmax = physwave_corrUndo.MaxAmp[0] * 1.05
                    if pmax > maxsam:
                        maxsam = pmax

                for ev in digits:
                    samMinPed = ev.to_numpy().astype('d')
                    samMinPed = samMinPed - samMinPed[0]
                    pulsegr_pa[ngr] = R.TGraph(len(meandigits), np.arange(0,len(meandigits),dtype='d'), samMinPed)
                    pulsegr_pa[ngr].SetTitle(f"Channel {ch}: Pulse Shape From PulseAll {pulseAll}")
                    pulsegr_pa[ngr].GetHistogram().SetMinimum(minsam-(abs(minsam)*.05))
                    pulsegr_pa[ngr].GetHistogram().SetMaximum(maxsam+(abs(maxsam)*.05))
                    pulsegr_pa[ngr].GetHistogram().GetXaxis().SetLimits(0,5)
                    if ngr == 0:
                        pulsegr_pa[ngr].Draw("Apl")
                        pulsegr_pa[ngr].GetXaxis().SetTitle("Sample #")
                        pulsegr_pa[ngr].GetYaxis().SetTitle("ADC[sample] - ADC[0]")
                    else:
                        pulsegr_pa[ngr].Draw("same,pl")        
                    canv.objs.append(pulsegr_pa[ngr])
                    ngr+=1
                meangr_pa = R.TGraph(len(meandigits), np.arange(0,len(meandigits),dtype='d'), meandigits)
                meangr_pa.SetMarkerStyle(20)
                meangr_pa.SetMarkerColor(R.kRed)
                meangr_pa.Draw("same,p")
                p1.Update()
                canv.objs.append(p1)
                
                if caliwave is not None:
                    amplitude = caliwave.Amplitude[0]
                    time = caliwave.Time[0] / 25
                    caliwavegr = R.TGraph(len(amplitude), np.array(time, dtype='d'), np.array(amplitude, dtype='d'))
                    #caliwavegr.SetTitle(f"Channel {ch}: Ramp Slope. Pedestal {weekly_ped}.")
                    caliwavegr.SetMarkerColor(R.kBlue)
                    caliwavegr.SetLineColor(R.kBlue)
                    caliwavegr.SetMarkerStyle(5)

                    caliwavegr.GetHistogram().SetMinimum(pulsegr_pa[0].GetHistogram().GetMinimum())

                    xmin = 0 #np.min(time) # p1.GetUxmin()
                    xmax = 125 # np.max(time) # p1.GetUxmax()
                    dx = (xmax - xmin) / 0.8 # 10 percent margins left and right
                    ymin = caliwavegr.GetHistogram().GetMinimum()
                    ymax = caliwavegr.GetHistogram().GetMaximum()
                    dy = (ymax - ymin) / 0.8 # 10 percent margins top and bottom
            
                    p2.Range(xmin-0.1*dx, ymin-0.1*dy, xmax+0.1*dx, ymax+0.1*dy);
                    p2.SetTitle("")
                    p2.Draw()
                    canv.objs.append(p2)
                    p2.cd()

                    caliwavegr.Draw("pl")

                    caliwavegr.GetXaxis().SetTitle("Time")
                    caliwavegr.GetYaxis().SetTitle("Amplitude")

                    canv.objs.append(caliwavegr)
                    cwtxt = R.TLatex()
                    xtxt = .2
                    ytxt = .75
                    cwtxt.SetTextColor(R.kBlue)
                    cwtxt.SetTextAlign(10)
                    cwtxt.SetTextSize(0.04)
                    towrite = [ "Caliwave from weekly", f"MaxAmp: {caliwave.MaxAmp[0]:.2f}", f"TmaxAmp: {caliwave.TmaxAmp[0]:.2f}" ]
                    for tw in towrite:
                        cwtxt.DrawLatexNDC(xtxt, ytxt, str(tw))
                        ytxt -= 0.06
                    p2.Update()

                    # 	TGaxis (Double_t xmin, Double_t ymin, Double_t xmax, Double_t ymax, Double_t wmin, Double_t wmax, Int_t ndiv=510, Option_t *chopt="", Double_t gridlength=0)
                    #axis = R.TGaxis(xmax, ymin, xmax, ymax, ymin, ymax, 510, "+L");
                    #axis = R.TGaxis(xmin, ymax, xmax, ymax, xmin, xmax, 510, "+C")
                    #axis.SetLineColor(R.kBlue)
                    #axis.SetLabelColor(R.kBlue)
                    #axis.SetTitleColor(R.kBlue)
                    #axis.SetTitle(caliwavegr.GetXaxis().GetTitle())
                    #axis.SetTitleOffset(1)
                    #axis.Draw()
                    #p2.Update()


                    #axis2 = R.TGaxis(xmax, ymin, xmax, ymax, ymin, ymax, 510, "-")
                    #axis2.SetLineColor(R.kBlue)
                    #axis2.SetLabelColor(R.kBlue)
                    #axis2.SetTitleColor(R.kBlue)
                    #axis2.SetTitle(caliwavegr.GetYaxis().GetTitle())
                    #axis2.SetTitleOffset(1)
                    #axis2.Draw()
                    #p2.Update()

                if caliwave_corrUndo is not None:
                    amplitude = caliwave_corrUndo.Amplitude[0]
                    time = caliwave_corrUndo.Time[0] / 25
                    caliwavegr_corrUndo = R.TGraph(len(amplitude), np.array(time, dtype='d'), np.array(amplitude, dtype='d'))
                    caliwavegr_corrUndo.SetMarkerColor(R.kRed)
                    caliwavegr_corrUndo.SetLineColor(R.kRed)
                    caliwavegr_corrUndo.SetMarkerStyle(5)
                    caliwavegr_corrUndo.Draw("samepl")
                    caliwavegr_corrUndo.GetXaxis().SetTitle("Time")
                    caliwavegr_corrUndo.GetYaxis().SetTitle("Amplitude")

                    canv.objs.append(caliwavegr_corrUndo)
                    cwtxt = R.TLatex()
                    cwtxt.SetTextColor(R.kRed)
                    cwtxt.SetTextAlign(10)
                    cwtxt.SetTextSize(0.04)
                    towrite = [ "Caliwave: corrUndo = 1", f"MaxAmp: {caliwave_corrUndo.MaxAmp[0]:.2f}", f"TmaxAmp: {caliwave_corrUndo.TmaxAmp[0]:.2f}" ]
                    for tw in towrite:
                        cwtxt.DrawLatexNDC(xtxt, ytxt, str(tw))
                        ytxt -= 0.06
                    p2.Update()


                if physwave is not None:
                    amplitude = physwave.Amplitude[0]
                    time = physwave.Time[0]
                    physwavegr = R.TGraph(len(amplitude), np.array(time, dtype='d'), np.array(amplitude, dtype='d'))
                    physwavegr.SetMarkerColor(R.kBlue-9)
                    physwavegr.SetLineColor(R.kBlue-9)
                    physwavegr.SetMarkerStyle(5)
                    physwavegr.Draw("samepl")
                    canv.objs.append(physwavegr)
                    #cwtxt = R.TLatex()
                    #cwtxt.SetTextColor(R.kRed)
                    #cwtxt.SetTextAlign(10)
                    #cwtxt.SetTextSize(0.04)
                    #towrite = [ "Physwave: corrUndo = 1", f"MaxAmp: {physwave.MaxAmp[0]:.2f}", f"TmaxAmp: {physwave.TmaxAmp[0]:.2f}" ]
                    #for tw in towrite:
                    #    cwtxt.DrawLatexNDC(xtxt, ytxt, str(tw))
                    #    ytxt -= 0.06
                    p2.Update()
                if physwave_corrUndo is not None:
                    amplitude = physwave_corrUndo.Amplitude[0]
                    time = physwave_corrUndo.Time[0]
                    physwavegr_corrUndo = R.TGraph(len(amplitude), np.array(time, dtype='d'), np.array(amplitude, dtype='d'))
                    physwavegr_corrUndo.SetMarkerColor(R.kRed-9)
                    physwavegr_corrUndo.SetLineColor(R.kRed-9)
                    physwavegr_corrUndo.SetMarkerStyle(5)
                    physwavegr_corrUndo.Draw("samepl")
                    canv.objs.append(physwavegr_corrUndo)
                    #cwtxt = R.TLatex()
                    #cwtxt.SetTextColor(R.kRed)
                    #cwtxt.SetTextAlign(10)
                    #cwtxt.SetTextSize(0.04)
                    #towrite = [ "Physwave: corrUndo = 1", f"MaxAmp: {physwave_corrUndo.MaxAmp[0]:.2f}", f"TmaxAmp: {physwave_corrUndo.TmaxAmp[0]:.2f}" ]
                    #for tw in towrite:
                    #    cwtxt.DrawLatexNDC(xtxt, ytxt, str(tw))
                    #    ytxt -= 0.06
                    p2.Update()

            # OFC diagnostics
            tcols = {"before":R.kMagenta-7, "intime":R.kBlack, "after":R.kViolet+5}
            lcols = {"before":R.kMagenta-10, "intime":R.kGray, "after":R.kViolet-9}
            enes, taus, maxe, mint, newph = {}, {}, {}, {}, {}
            for timing in timings:
                enes[timing],taus[timing],maxe[timing],mint[timing], newph[timing] = getEneTau(ch, this_arr[timing], timing)

            def makeLine(gr, val, col, style=1):
                line = R.TLine(val, gr.GetHistogram().GetMinimum(), val, gr.GetHistogram().GetMaximum())
                line.SetLineColor(col)
                line.SetLineWidth(2)
                line.SetLineStyle(style)
                return line

            if newph["intime"] != -1:
                if newph["intime"] == 0 or newph["intime"] == 49:
                    print("OIOI", newph["intime"])
                    badPhase = 1
            else:
                if maxe["intime"] == 0 or maxe["intime"] == 49:
                    print("OIOI", maxe["intime"])
                    badPhase = 1

            gene, gtau = {}, {}
            for timing in timings:
                gene[timing] = R.TGraph(len(enes[timing]), np.array(phases,dtype='d'), enes[timing])
                gene[timing].SetTitle(f"E per phase for chid {ch} from {len(this_arr['samples'])} pulse(s). Run {runNum}. Pedestal {weekly_ped}.")
                gene[timing].SetMarkerStyle(20)
                gene[timing].SetMarkerColor(tcols[timing])
                gene[timing].SetName(f"ene_{timing}")

                gtau[timing] = R.TGraph(len(taus[timing]), np.array(phases,dtype='d'), taus[timing])
                gtau[timing].SetTitle(f"|#tau| per phase for chid {ch} from {len(this_arr['samples'])} pulse(s). Run {runNum}. Pedestal {weekly_ped}.")
                gtau[timing].SetMarkerStyle(20)
                gtau[timing].SetMarkerColor(tcols[timing])
                gtau[timing].SetName("tau")
            for timing in timings: # After we filled all timings
                gene[timing].GetHistogram().SetMaximum(max([gene[k].GetHistogram().GetMaximum() for k in gene.keys()]))
                gene[timing].GetHistogram().SetMinimum(min([gene[k].GetHistogram().GetMinimum() for k in gene.keys()]))

            leg_etau = R.TLegend(.87,.4,.98,.9)
            leg_etau.SetFillStyle(0)
            leg_etau.SetTextColor(R.kBlack)
            leg_etau.SetTextSize(.037)
            leg_etau.SetBorderSize(0)

            margin_l = 0.08
            margin_r = 0.15
            ytitle_off = 0.05
            canv.cd(4)
            R.gPad.SetLeftMargin(margin_l)
            R.gPad.SetRightMargin(margin_r)
            eline = {}
            tline = {}
            npline = {}
            for timing in timings:
                gene[timing].GetHistogram().GetXaxis().SetLimits(0,49)
                if timings.index(timing)==0:
                    gene[timing].Draw("ap")
                else:
                    gene[timing].Draw("same,p")
                gene[timing].GetXaxis().SetTitle("Phase")
                gene[timing].GetYaxis().SetTitle("#Sigma(ai*si)")
                #gene[timing].GetYaxis().SetTitleOffset(0.02)
        
                eline[timing] = makeLine(gene[timing], maxe[timing][0], tcols[timing])
                tline[timing] = makeLine(gene[timing], mint[timing][0], tcols[timing], 9)
                leg_etau.AddEntry(eline[timing], f"max E: {maxe[timing][0]}", "l")
                leg_etau.AddEntry(tline[timing], f"min |#tau|: {mint[timing][0]}", "l")
                eline[timing].Draw("samel")
                tline[timing].Draw("samel")
                canv.objs.append(eline[timing])
                canv.objs.append(tline[timing])

                if newph is not None:
                    if newph[timing] is not None:
                        npline[timing] = makeLine(gene[timing], newph[timing], tcols[timing], 7)
                        leg_etau.AddEntry(npline[timing], f"compr.: {newph[timing]}", "l")
                        npline[timing].Draw("samel")
                        canv.objs.append(npline[timing])
                        canv.objs.append(npline[timing])
        
                leg_etau.AddEntry(gene[timing], timing, "p")

            leg_etau.Draw("same")
            canv.objs.append(gene)

            canv.cd(5)
            eline2 = {}
            tline2 = {}
            npline2 = {}
            R.gPad.SetLeftMargin(margin_l)
            R.gPad.SetRightMargin(margin_r)
            for timing in timings:
                gtau[timing].GetHistogram().SetMaximum(30)
                gtau[timing].GetHistogram().SetMinimum(0)
                gtau[timing].GetHistogram().GetXaxis().SetLimits(0,49)
                if timings.index(timing) == 0:
                    gtau[timing].Draw("ap")
                else:
                    gtau[timing].Draw("same,p")
                gtau[timing].GetXaxis().SetTitle("Phase")
                gtau[timing].GetYaxis().SetTitle("#Sigma(bi*si)/#Sigma(ai*si)")

                eline2[timing] = makeLine(gtau[timing], maxe[timing][0], tcols[timing])
                tline2[timing] = makeLine(gtau[timing], mint[timing][0], tcols[timing], 9)
                eline2[timing].Draw("samel")
                tline2[timing].Draw("samel")
                canv.objs.append(eline2[timing])
                canv.objs.append(tline2[timing])
                if newph is not None:
                    if newph[timing] is not None:
                        npline2[timing] = makeLine(gene[timing], newph[timing], tcols[timing], 7)

                        npline2[timing].Draw("samel")
                        canv.objs.append(npline2[timing])
                        canv.objs.append(npline2[timing])

            leg_etau.Draw("same")
            canv.objs.append(gtau)


            # Compare caliwave maxamp to neighbours
            canv.cd(6)
            cut = f"( channelId != {ch} ) & ( ieta == {caliwave.ieta[0]} ) & ( detector == {caliwave.detector[0]} ) & ( layer == {caliwave.layer[0]} )"
            cw_neighbourData, cw_neighbourChids = getCaliWave(weekly_ped, verbose=True, cut=cut, inFiles=caliwaveFiles)
            cw_neighbourData_corrUndo = cw_neighbourData[cw_neighbourData.corrUndo == 1]
            cw_neighbourData = cw_neighbourData[cw_neighbourData.corrUndo == 0]
            
            maxAmps = np.array(cw_neighbourData.MaxAmp, dtype='d')


            # OIOIOOI HERE
            xmin = ak.min(maxAmps)
            xmax = ak.max(maxAmps)
            mean = ak.mean(maxAmps)
            std = ak.std(maxAmps)
            

            if caliwave.MaxAmp[0] < xmin:
                xmin = caliwave.MaxAmp[0]
            elif caliwave.MaxAmp[0] > xmax:
                xmax = caliwave.MaxAmp[0]

            if caliwave_corrUndo is not None:
                if caliwave_corrUndo.MaxAmp[0] < xmin:
                    xmin = caliwave_corrUndo.MaxAmp[0]
                elif caliwave_corrUndo.MaxAmp[0] > xmax:
                    xmax = caliwave_corrUndo.MaxAmp[0]

                
            h_maxAmp = R.TH1F(f"h_maxAmp{ch}", f"MaxAmp of CaliWave for det {caliwave.detector[0]}, layer {caliwave.layer[0]}, ieta {caliwave.ieta[0]}", 50, xmin, xmax)
            h_maxAmp.SetFillColor(R.kBlue-10)

            h_maxAmp.FillN(len(maxAmps), maxAmps, np.ones_like(maxAmps))

            if len(cw_neighbourData_corrUndo.MaxAmp) > 0:
                maxAmps_corrUndo = np.array(cw_neighbourData_corrUndo.MaxAmp, dtype='d')
                h_maxAmp_corrUndo = R.TH1F(f"h_maxAmp_corrUndo{ch}", f"MaxAmp of CaliWave for det {caliwave.detector[0]}, layer {caliwave.layer[0]}, ieta {caliwave.ieta[0]}", 50, xmin, xmax)
                h_maxAmp_corrUndo.SetFillColor(R.kRed-10)
                
                h_maxAmp_corrUndo.FillN(len(maxAmps_corrUndo), maxAmps_corrUndo, np.ones_like(maxAmps_corrUndo))
            else:
                h_maxAmp_corrUndo = None
            
            newmean = h_maxAmp.GetMean()
            newstd = h_maxAmp.GetStdDev()

            if caliwave.MaxAmp[0] > newmean + 2*newstd or caliwave.MaxAmp[0] < newmean - 2*newstd:
                print(f"!!! out of maxAmp range of {len(cw_neighbourChids)} neighbours. MaxAmp = {caliwave.MaxAmp[0]}, mean of neighbours = {newmean}, 2*std-dev or neighbours = {2*newstd}")
                badMaxAmp = 1
            # printcols = [ "Channel", "CurrentFlag", "Missing Calib?", "NoDataPulse?", "BadPhase?", "BadMaxAmp?" ]
            printcols = [str(s) for s in [ ch, pm_info["BadSC"],  missingCalib, noDataPulse, badPhase, badMaxAmp]]
            out_txt.write(outformat.format(*printcols) + "\n")
            
            h_maxAmp.SetLineColor(R.kBlack)
            h_maxAmp.GetYaxis().SetTitle("Counts")
            h_maxAmp.GetXaxis().SetTitle("MaxAmp")
            h_maxAmp.Draw("hist")
            if h_maxAmp_corrUndo is not None:
                h_maxAmp_corrUndo.Draw("histsame")
            tline_maxAmp = R.TLine(caliwave.MaxAmp[0], h_maxAmp.GetMinimum(), caliwave.MaxAmp[0], h_maxAmp.GetMaximum())
            tline_maxAmp.SetLineColor(R.kBlue)
            tline_maxAmp.SetLineWidth(2)
            tline_maxAmp.Draw("sameL")
            canv.objs.append(h_maxAmp)
            canv.objs.append(tline_maxAmp)

            txt = R.TLatex()
            txt.SetTextColor(R.kBlue)
            txt.SetTextAlign(10)
            txt.SetTextSize(0.04)
            line_run = {}
            towrite = [ f"{len(cw_neighbourChids)} neighbours", f"Mean MaxAmp: {newmean:.2f}", f"2*#sigma MaxAmp: {2*newstd:.2f}", f"{ch} Maxamp: {caliwave.MaxAmp[0]:.2f}" ]
            xtxt = .5
            ytxt = .85
            for tw in towrite:
                txt.DrawLatexNDC(xtxt, ytxt, str(tw))
                ytxt -= 0.06 #*maxsam

            if caliwave_corrUndo is not None:
                tline_maxAmp_corrUndo = R.TLine(caliwave_corrUndo.MaxAmp[0], h_maxAmp.GetMinimum(), caliwave_corrUndo.MaxAmp[0], h_maxAmp.GetMaximum())
                tline_maxAmp_corrUndo.SetLineColor(R.kRed)
                tline_maxAmp_corrUndo.SetLineWidth(2)
                tline_maxAmp_corrUndo.Draw("sameL")
                canv.objs.append(tline_maxAmp_corrUndo)
                txt = R.TLatex()
                txt.SetTextColor(R.kRed)
                txt.SetTextAlign(10)
                txt.SetTextSize(0.04)
                line_run = {}
                towrite = [ f"corrUndo = 1", f"{ch} Maxamp: {caliwave_corrUndo.MaxAmp[0]:.2f}" ]
                for tw in towrite:
                    txt.DrawLatexNDC(xtxt, ytxt, str(tw))
                    ytxt -= 0.06 #*maxsam


            canv.Print(f"{outDir}/diagnostics_{ch}_run{runNum}_ped{weekly_ped}.png")
            
    out_txt.close()

if __name__ == "__main__":
    parser = ArgumentParser()
    #parser.add_argument('-i', '--dataFile', dest='dataFile', default="/eos/home-e/ekay/LAr/LArPrompt/486796_skim/output_skim/output_allskim.root", help="Input SCDIGITS file. Default %(default)s.") 

    parser.add_argument('-i', '--dataFile', dest='dataFile', default="/eos/home-e/ekay/LAr/LArPrompt/486658_skim/output_skim/output_allskim.root", help="Input SCDIGITS file. Default %(default)s.") # Sat Oct 12 2024 14:03:23 − Sun Oct 13, 04:22:39
    parser.add_argument('-d', '--pulseAll', dest='pulseAll', default=489073, type=int, help="Pulse all run to use for drawing calibration pulses. Default %(default)s.")
    parser.add_argument('-o', '--outDir', dest='outDir', default=f"{os.getcwd()}/output_scanBadSC", help="Output directory for log files and plots. Default %(default)s.")
    parser.add_argument('-p', '--ped', dest='weekly_ped', type=int, default=482037, help="Pedestal number from weekly set to be used (for OFCs). Default %(default)s.")
    parser.add_argument('-r', '--runNum', dest='runNum', default=999999, help="Run number. Default %(default)s")
    parser.add_argument('-c', type=int ,dest='channelId', nargs='+', default=[942150656], help='List of SC IDs to scan over (optional)')
    parser.add_argument('--outStr', default=None, help='String to add to output .txt file names. Default will use channel or latome name')
    parser.add_argument('--runAll', action='store_true', default=False, help='Run over all channels, submitting one job per latome to condor')
    args = parser.parse_args()


    

    if args.runNum == 999999:
        runSearch = re.findall(r'(\d{6})_', args.dataFile)
        if len(runSearch) == 1:
            args.runNum = runSearch[0]
    if not os.path.isfile(args.dataFile):
        print(f"PROVIDED FILE {args.dataFile} DOES NOT EXIST!")
        sys.exit()

    condSub.chmkDir(args.outDir)
    treeName = "SCDIGITS"

    if args.runAll is True:
        latome_df = getPMinfo(want=["SC_ONL_ID", "LATOME_SOURCE_ID"])
        latomes = latome_df.LATOME_SOURCE_ID.unique()
        latome_chans = { latome : list(latome_df[latome_df.LATOME_SOURCE_ID == latome].index.values) for latome in latomes }
        latome_chans = { k:v for k,v in latome_chans.items() if v != [0]}


            
        
        print(len(list(latome_chans.keys())), "LATOMEs")
        
        tmpdir = f"{args.outDir}/condor"
        logDir = f"{tmpdir}/logs"
        errDir = f"{tmpdir}/err"
        outDir = f"{tmpdir}/out"
        
        condSub.chmkDir(tmpdir)
        condSub.chmkDir(logDir)
        condSub.chmkDir(errDir)
        condSub.chmkDir(outDir)
        jobtag = f"run_scanBadSC"
        argsname = f"{tmpdir}/runArgs_scanBadSC.txt"

        runargs = []
        for latome in latome_chans.keys():
            pyopts = []
            pyopts.append(f"-i {args.dataFile}")
            pyopts.append(f"-d {args.pulseAll}")
            pyopts.append(f"-o {args.outDir}")
            pyopts.append(f"-p {args.weekly_ped}")
            pyopts.append(f"-r {args.runNum}")
            pyopts.append(f"-c {' '.join([str(ch) for ch in latome_chans[latome]])}")
            
            if args.outStr is None:
                pyopts.append(f"--outStr LATOME-{latome}")
            else:
                pyopts.append(f"--outStr {args.outStr}")
            runargs.append(" ".join(pyopts))
                        
            
        outf = open(argsname,"w")
        for ra in runargs:
            outf.write(f"{ra} \n")
        outf.close()

        
        condSub.writeBash( bashdir=tmpdir,
                           bashname=jobtag,
                           executable=f"{os.getcwd()}/plotDataCalib.py",
                           argsFileName=argsname,
                           isexe=False,
                           runwith="python3",
                           setupscript=os.getcwd()+"/../../setup_LCG.sh" )

        condSub.writeCond( bashdir=tmpdir,
                           bashname=jobtag,
                           argsFileName=argsname,
                           taskName=jobtag,
                           runTime=86400, # args.jobtime,
                           subNow=False,
                           logdir=logDir,
                           errdir=errDir,
                           outdir=outDir )    


    
    else:
        print("*"*30)
        print(f"Looking at run {args.runNum} and calibration set with pedestal # {args.weekly_ped}")
        print(f"Channel(s): {', '.join([str(ch) for ch in args.channelId])}")
        print(f"Data file: {args.dataFile}")
        print(f"Writing output to: {args.outDir}")
        print("*"*30)

        if args.outStr is None:
            if len(args.channelId) == 1:
                args.outStr = f"SC-{args.channelId[0]}"
            elif len(args.channelId) < 5:
                args.outStr = f"SCs_{'-'.join([str(ch) for ch in args.channelId])}"
            else:
                args.outStr = f"SCs_{args.channelId[0]}--{args.channelId[-1]}"
        
        
        diagnostics(args.dataFile, args.runNum, args.weekly_ped, args.channelId, pulseAll=args.pulseAll, treeName=treeName, highMu=True, outDir=args.outDir, outStr=args.outStr)
            
