#!/bin/sh

chid=$1
flag=$2

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

SCdumpfile=${SCRIPT_DIR}/SConldump.list
line=$(grep ${chid} ${SCdumpfile})

if [[ "${line}" == "" ]]; then
    echo "Did not find ${chid} in file"
    exit 1
fi

arr=($line)

outStr="${arr[@]:2:7} ${flag}  # ${arr[0]}"
echo $outStr

