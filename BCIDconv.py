ltdb_bcid_bcr_value= { 0:"0x000", 1:"0x000", 2:"0xd73", 3:"0xd73", 4:"0xd73", 5:"0xd74", 6:"0xd73", 7:"0xd74", 8:"0xd73", 9:"0xd73", 10:"0xd73", 11:"0xd73", 12:"0x000", 13:"0x000", 14:"0xd74", 15:"0xd73", 16:"0xd74", 17:"0xd73", 18:"0xd74", 19:"0xd73", 20:"0xd74", 21:"0xd74", 22:"0xd73", 23:"0xd73", 24:"0x000", 25:"0x000", 26:"0xd73", 27:"0xd73", 28:"0xd73", 29:"0xd74", 30:"0xd73", 31:"0xd72", 32:"0xd73", 33:"0xd73", 34:"0xd74", 35:"0xd71", 36:"0x000", 37:"0x000", 38:"0xd71", 39:"0xd73", 40:"0xd71", 41:"0xd73", 42:"0xd74", 43:"0xd73", 44:"0xd74", 45:"0xd72", 46:"0xd73", 47:"0xd73" }

bcid_calibration_value = { 0:0, 1:0, 2:27623, 3:27622, 4:27622, 5:27629, 6:27621, 7:27629, 8:27621, 9:27621, 10:27621, 11:27621, 12:0, 13:0, 14:27631, 15:27623, 16:27630, 17:27622, 18:27629, 19:27623, 20:27630, 21:27630, 22:27622, 23:27622, 24:0, 25:0, 26:27622, 27:27622, 28:27622, 29:27631, 30:27623, 31:27614, 32:27622, 33:27623, 34:27630, 35:27606, 36:0, 37:0, 38:27606, 39:27622, 40:27606, 41:27623, 42:27630, 43:27622, 44:27630, 45:27614, 46:27623, 47:27623 } # = number 320 clock cycles

#27630 -> 3453.75   27630/8 = 3453.75

# number 320 clock cycles, 320 = 40*8



bcid_ttc_sync_LAR = 27850
bcid_ttc_sync_ATLAS = 27784


#3444 -> 27630
passes = []
fails = []

for fibre in ltdb_bcid_bcr_value.keys():
    before = ltdb_bcid_bcr_value[fibre]
    before_int = int(before,16)
    after = bcid_calibration_value[fibre]
    #if before_int == 0: continue
    #print(before_int, after, after-4, after/before_int, before_int*8, (after-4)/before_int)
    #continue
    #if before_int == 0 : continue
    #print( (after+4)/(before_int*8))
    #print( (after-4)/(before_int*8))
    #print( (after)/((before_int+4)*8))
    #print( (after)/((before_int-4)*8))
    #print( (after+4)/((before_int+4)*8))
    #continue
    #print(fibre, before_int, after)
    print("** before/8", before_int/8)
    print("** before*8", before_int*8)
    print("** after/8", after/8)
    print("** a-(b*8)", after-(before_int*8))
    print("** (a/8)-b", (after/8)-before_int)
    if before_int != 0:
       print("**  a/(b*8)", after/(before_int*8))
    #, after-before_int*8, (before_int+4)*8, after-(before_int+4)*8)

    value = (before_int*8)+80
    bcid_ttc_sync = bcid_ttc_sync_ATLAS+4

    if value > bcid_ttc_sync-128 and value < bcid_ttc_sync:
        print(fibre, "PASS -", value, f"({before_int} {after})")
        passes.append(fibre)
    else:
        print(fibre, "FAIL -", f"{bcid_ttc_sync-128} -> {bcid_ttc_sync}... {value}", f"({before_int} {after})")
        fails.append(fibre)
print(len(passes), "pass,", len(fails), "fail")
