# Script which compares picked phase txt files
import sys, os
import pandas as pd 
sys.path.append("..")
import printMapping
want = ["SC_ONL_ID", "DETNAME", "LATOME_NAME", "LATOME_FIBRE", "BadSC"] 
inputs={"want":want,"showdecimal":True} #,"run":479720}

pd.set_option('display.max_columns', None)

#def get_df(infile, delim='\s+', colnames=None, index=None):
def get_df(infile, delim=' ', colnames=None, index=None):
    df = pd.read_csv(infile, sep=delim, header=None) #, on_bad_lines='skip') # names=colnames
    df = df.dropna(axis=1, how='all')
    df.columns = colnames
    if index is not None:
        df = df.astype({index:'int'}) # careful... assuming we want integer
        df.set_index(index, inplace=True)
        
    #b16 = lambda x: int(x,16)
    ##df["sourceId"] = df["sourceId"].apply(int, base=16)
    #df['fullId'] = df[['sourceId', 'fiberNumber']].agg('_'.join, axis=1)
    #df = df.drop_duplicates(keep='last')
    #duplicate = df[df['fullId'].duplicated() == True]
    #if len(duplicate.index) > 0:
    #    print(infile, "has", len(duplicate.index), "duplicates")
    #df.set_index("fullId", inplace=True)
    return df


infile1 = sys.argv[1]
infile2 = sys.argv[2]

if len(sys.argv) > 3:
    infile_ref = sys.argv[3]
else:
    infile_ref = None
if len(sys.argv) > 4:
    infile_ref2 = sys.argv[4]
else:
    infile_ref2 = None
    
colnames = [ "SC_ONL_ID", "PHASE"]

print("querying printMapping for",inputs)
pmout = printMapping.query(**inputs)
pmdf = pd.DataFrame(pmout, columns = want)
pmdf = pmdf.astype({'SC_ONL_ID':'int'})

def latomeName(onlId):
    onlId = onlId # int(onlId_before,16)
    return pmdf.loc[ ( pmdf.SC_ONL_ID == onlId ) ].LATOME_NAME.unique()[0]
def detName(onlId):
    onlId = onlId # int(onlId_before,16)
    return pmdf.loc[ ( pmdf.SC_ONL_ID == onlId ) ].DETNAME.unique()[0]
def latomeFibre(onlId):
    onlId = onlId # int(onlId_before,16)
    return pmdf.loc[ ( pmdf.SC_ONL_ID == onlId ) ].LATOME_FIBRE.unique()[0]
def BadSC(onlId):
    onlId = onlId # int(onlId_before,16)
    return pmdf.loc[ ( pmdf.SC_ONL_ID == onlId ) ].BadSC.unique()[0]

        
df1 = get_df(infile1, colnames=colnames, index="SC_ONL_ID")
df2 = get_df(infile2, colnames=colnames, index="SC_ONL_ID")

if infile_ref is not None:
    df_ref = get_df(infile_ref, colnames=colnames, index="SC_ONL_ID")
else:
    df_ref = None
if infile_ref2 is not None:
    df_ref2 = get_df(infile_ref2, colnames=colnames, index="SC_ONL_ID")
else:
    df_ref2 = None
diff_id = df2[~df2.isin(df1).all(axis=1)].index.values
diff_id2 = df1[~df1.isin(df2).all(axis=1)].index.values
print(len(diff_id), len(diff_id2), len(df1.index.values), len(df2.index.values))
# merged = df1.join(df2, lsuffix='_1', rsuffix='_2')
merged = df1.join(df2, lsuffix='_1', rsuffix='_2')
merged["index"] = merged.index
merged['LATOME_NAME'] = merged.apply(lambda x: latomeName(x["index"]), axis=1)
merged['DETNAME'] = merged.apply(lambda x: detName(x["index"]), axis=1)
merged['LATOME_FIBRE'] = merged.apply(lambda x: latomeFibre(x["index"]), axis=1)
merged['BadSC'] = merged.apply(lambda x: BadSC(x["index"]), axis=1)

try:
    diff = merged.loc[diff_id]
except Exception as e:
    print(merged)
    print("** ERROR **")
    print(diff_id)
    print(e)
    print(len(merged.index), merged.index.dtype)
    sys.exit()
diff["ABS_PHASE_DIFF"] = ( diff.PHASE_1.astype(int) - diff.PHASE_2.astype(int) ).abs()
diff["PHASE_DIFF"] = diff.PHASE_1.astype(int) - diff.PHASE_2.astype(int)

toprint = ["DETNAME", "LATOME_NAME", "LATOME_FIBRE", "PHASE_1", "PHASE_2", "ABS_PHASE_DIFF", "PHASE_DIFF", "BadSC"]
if df_ref is not None:
    diff = diff.join(df_ref.add_suffix('_REF'))
    toprint.append("PHASE_REF")
if df_ref2 is not None:
    diff = diff.join(df_ref2.add_suffix('_REF2'))
    toprint.append("PHASE_REF2")
    
ndiff = len(diff.index.values)
print(f"{ndiff} SCs have differing values. Max difference is {diff.ABS_PHASE_DIFF.max()}, mean difference is {diff.ABS_PHASE_DIFF.mean()}")
LATOMEs = diff.LATOME_NAME.unique()
fibres = diff.LATOME_FIBRE.unique()
print(f"SCs from {len(LATOMEs)} LATOMEs and {len(fibres)} LATOME fibres in the list") 

pd.options.display.width = 0
pd.options.display.max_rows = None

diff = diff.sort_values('ABS_PHASE_DIFF', ascending=False)
print("*"*30)


if ndiff < 200:
                    
    print(diff[toprint])

else:
    nlargest=500
    topn = diff.nlargest(nlargest, 'ABS_PHASE_DIFF')
    print(f"Showing results for {nlargest} largest deviations between picked phases:")

    print(topn[toprint])

if df_ref is not None:
    print("*"*10, "ROWS WHERE PHASE_1 != PHASE_REF")
    test = diff[diff.PHASE_1 != diff.PHASE_REF]
    topn = test.nlargest(nlargest, 'ABS_PHASE_DIFF')
    print(topn[toprint])




