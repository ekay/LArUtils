import sys
import pandas as pd
import ROOT as R
# sys.path.append("..")
# import printMapping

# def getPMinfo(want=["SC_ONL_ID", "SCNAME", "LTDB", o"LATOME_NAME", "LATOME_FIBRE", "DETNAME", "BadSC", "CALIB"]):

#     inputs={"want":want,"showdecimal":True}
#     print("querying printMapping for",inputs)
#     pmout = printMapping.query(**inputs)
#     pmdf = pd.DataFrame(pmout, columns = want)
#     pmdf = pmdf.astype({'SC_ONL_ID':'int'})

#     if "SCNAME" in want:
#         filter = pmdf['SCNAME'].str.contains("-")
#         pmdf = pmdf[~filter]
#         filter = pmdf['SCNAME'].str.contains("NOT_CONNECTED")
#         pmdf = pmdf[~filter]


#     pmdf = pmdf.set_index("SC_ONL_ID")

    
#     return pmdf


# pmdf = getPMinfo()

import requests
from requests.structures import CaseInsensitiveDict
import urllib3
urllib3.disable_warnings() # warning from larid api

pmdfcols=["DET", "LTDB", "SCA"]

# Get info from LArIdTranslator backend API
url = f"https://atlas-larmon.cern.ch/LArIdTranslatorBackend/api/data?columns={','.join(pmdfcols)}"
headers = CaseInsensitiveDict()
headers["Accept"] = "application/json"
pmdf2 = None
try:
    resp = requests.get(url, headers=headers, verify=False)
    larid_json = resp.json()
    pmdf2 = pd.DataFrame.from_dict(larid_json)
    pmdf2 = pmdf2.astype({'SC_ONL_ID':'int'})
    pmdf2 = pmdf2.set_index("SC_ONL_ID")

    #pmdf2['DETNAME'] = pmdf2.FEB_ID.apply(getDetNameFromFEB)

    print(pmdf2)
except Exception as e:
    print("PROBLEM")
    print(e)


pmdf2["LTDB_SCA"] = pmdf2["LTDB"] + pmdf2["SCA"].astype("str")

R.gROOT.SetBatch(True)
canv=R.TCanvas("canv", "canv", 3000, 600)
detnames = {"EMB":0, "EMEC":1, "HEC":2, "FCAL":3}
def scaProfile(detname, title, df):
    df=df[df.DET == detnames[detname]]
    bins = sorted(df.LTDB_SCA.values)
    print(bins)
    test = R.TProfile(detname, title, len(bins), 0, len(bins)-1)
    test2 = R.TGraph()
    gpoints=0
    for b in range(0, len(bins)):
        test.GetXaxis().SetBinLabel(b+1, bins[b])

        binc = test.GetXaxis().GetBinCenter(b)
        print("OIOI", binc)
        if b == 0 or b == 3 or b == 20 or b == 35:
            print(test2.GetN())
            test2.SetPoint(gpoints, binc, -0.5)
            gpoints+=1
            print(test2.GetN())
            test2.SetPoint(gpoints, binc, 0)
            gpoints+=1
            test2.SetPoint(gpoints, binc, 1)
            gpoints+=1
            test2.SetPoint(gpoints, binc, 0.2)
            gpoints+=1

            print(test2.GetN())
    testsca ="C06L2"
    thebin = test.GetXaxis().FindBin(testsca)
    print("oioi",thebin)

    test2.SetMarkerStyle(20)
    test.Draw("hist")
    test2.Draw("P")
    canv.Print(f"{detname}_SCA.png")
scaProfile("EMEC", "EMECSCA", pmdf2)
