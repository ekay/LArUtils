# Script which compares picked phase txt files
import sys, os
import pandas as pd
import numpy as np
pd.set_option('display.max_columns', None)
#pd.options.display.max_rows = None

from argparse import ArgumentParser



def getPMinfo(want = ["SC_ONL_ID", "DETNAME", "LATOME_NAME", "LATOME_FIBRE", "BadSC"] ):
    sys.path.append("..")
    import printMapping
    inputs={"want":want,"showdecimal":True} #,"run":479720}
    print("querying printMapping for",inputs)
    pmout = printMapping.query(**inputs)
    pmdf = pd.DataFrame(pmout, columns = want)
    pmdf = pmdf.astype({'SC_ONL_ID':'int'})
    return pmdf
    

#def get_df(infile, delim='\s+', colnames=None, index=None):
def get_df(infile, delim=' ', colnames=None, index=None):
    df = pd.read_csv(infile, sep=delim, header=None) #, on_bad_lines='skip') # names=colnames
    df = df.dropna(axis=1, how='all')
    df.columns = colnames
    if index is not None:
        df = df.astype({index:'int'}) # careful... assuming we want integer
        df.set_index(index, inplace=True)
        df["index"] = df.index
    return df



def main(infile, det, latome, latomefibre, fillFile=None, debug=False):
    colnames = [ "SC_ONL_ID", "PHASE"]

    pmdf = getPMinfo()

    def latomeName(onlId):
        onlId = onlId # int(onlId_before,16)
        return pmdf.loc[ ( pmdf.SC_ONL_ID == onlId ) ].LATOME_NAME.unique()[0]
    def detName(onlId):
        onlId = onlId # int(onlId_before,16)
        return pmdf.loc[ ( pmdf.SC_ONL_ID == onlId ) ].DETNAME.unique()[0]
    def latomeFibre(onlId):
        onlId = onlId # int(onlId_before,16)
        return pmdf.loc[ ( pmdf.SC_ONL_ID == onlId ) ].LATOME_FIBRE.unique()[0]


    def decorate_df(df):
        df['LATOME_NAME'] = df.apply(lambda x: latomeName(x["index"]), axis=1)
        df['DETNAME'] = df.apply(lambda x: detName(x["index"]), axis=1)
        df['LATOME_FIBRE'] = df.apply(lambda x: latomeFibre(x["index"]), axis=1)

    
    df = get_df(infile, colnames=colnames, index="SC_ONL_ID")

    decorate_df(df)
    
    df_fill = None
    if fillFile is not None:
        df_fill = get_df(fillFile, colnames=colnames, index="SC_ONL_ID")

        #decorate_df(df_fill)
    
    outfilename = "phases_"
    if det is not None:
        phases = df[df.DETNAME.str.contains("|".join(det), na=False)]
        outfilename += "_".join(det)
    elif latome is not None:
        phases = df[df.LATOME_NAME.str.contains("|".join(latome), na=False)]
        outfilename += "_".join(latome)
    elif latomefibre is not None:
        phases = df[df.LATOME_FIBRE.str.contains("|".join(latomefibre), na=False)]
        outfilename += "_".join(latomefibre)
          
    if debug:
        print(phases)

    outfilename += ".txt"
    outfile = open(outfilename, "w")
    
    print("Writing output to",outfilename)
    if df_fill is not None:
        merged = df_fill.merge(phases[['index','PHASE']], how='left', on='index')
        for index, row in merged.iterrows():
            #print(row["index"], row.PHASE_x)
            if np.isnan(row["PHASE_y"]):
                outfile.write(f'{int(row["index"])} {int(row.PHASE_x)}\n')
            else:
                outfile.write(f'{int(row["index"])} {int(row.PHASE_y)}\n')

            
                
            

    else:
        for index, row in phases.iterrows():
            #print(row["index"], row.PHASE)
            outfile.write(f'{int(row["index"])} {int(row.PHASE)}\n')
    outfile.close()
    
if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument('infile', help="Input phases txt file")
    parser.add_argument('-d', '--detector', dest='det', nargs='+', default=None, help="Detector(s) for which we should extract the phase list")
    parser.add_argument('-l', '--latome', dest='latome', nargs='+', default=None, help="LATOME(s) for which we should extract the phase list")
    parser.add_argument('-f', '--latomeFibre', dest='latomeFibre', nargs='+', default=None, help="LATOME fibre(s) for which we should extract the phase list")
    parser.add_argument('--fillRest', dest='fillFile', default=None, help="Optional file from which we take the remaining phases, other than the ones selected from 'infile'")

    args = parser.parse_args()
    if args.det is None and args.latome is None and args.latomeFibre is None:
        print("No detector, LATOME or LATOME fibre list provided... please request some sub-set of data to extract")
        sys.exit()
    filled = [ x for x in [args.det, args.latome, args.latomeFibre] if x is not None ]
    if len(filled) > 1:
        print("Provided more than one item... not supported for now. Please request just one type")
    
    main(args.infile, args.det, args.latome, args.latomeFibre, args.fillFile)


