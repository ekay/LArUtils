import math
from array import array
import ROOT as R
import numpy as np
R.gROOT.SetBatch(True)

def getContour(zmax, minval=0.001, maxval=None):
    if maxval is None:
        maxval = (2*zmax)*.8
    red   = [  34./255.,  70./255., 129./255., 187./255., 1, 226./255., 216./255., 193./255., 179./255. ]
    green = [  48./255.,  91./255., 147./255., 194./255., 1, 229./255., 196./255., 110./255.,  12./255. ]
    blue  = [ 234./255., 212./255., 216./255., 224./255., 1, 110./255.,  53./255.,  40./255.,  29./255. ]
    #stops_init = [ 0.0000, 0.1250, 0.2500, 0.3750, 0.5000, 0.6250, 0.7500, 0.8750, 1.0000 ]
    red =   [red[0]]   + red   + [red[-1]]
    green = [green[0]] + green + [green[-1]]
    blue =  [blue[0]]  + blue  + [blue[-1]]
    print("OIOI", maxval, zmax, maxval/zmax)

    grad_pos = (maxval/zmax)*0.5
    min_pos = (minval/zmax)*0.5
    grad_start= 0.5-grad_pos  #   0.5-(maxval/zmax)
    min_lo = 0.5-(min_pos)
    min_hi = 0.5+(min_pos)
    grad_end = 0.5+grad_pos   #   0.5+(maxval/zmax)


    stops = [ 0 ]
    if minval != 0:
        ndiv = int((len(red)-3)/2)
        stops.extend([ round(n,2) for n in np.linspace(grad_start, min_lo, ndiv)] )
        stops.append(0.5)
        stops.extend([round(n,2) for n in np.linspace(min_hi, grad_end, ndiv)])
        
    else:
        ndiv = int((len(red))/2)
        stops.extend([ round(n,2) for n in np.linspace(grad_start, 0.5, ndiv)] )
        stops.extend([ round(n,2) for n in np.linspace(0.5, grad_end, ndiv)][1:] )
    stops.append(1)
    
    for st in stops:
        print(st, -zmax+(zmax--zmax)*st) # , red[stops.index(st)], green[stops.index(st)], blue[stops.index(st)])
    # stops = [ 0, 0.4, 0.4001, 0.410, 0.450, 0.499, 0.5000, 0.501, 0.550, 0.590, 0.5999, 0.6, 1 ]
    Number = len(red)
    NCont = len(red)*((2*zmax)+2)
    #NCont = (len(red))*50

    R.TColor.CreateGradientColorTable(Number,array('d',stops),array('d',red),array('d',green),array('d',blue),NCont)

canv=R.TCanvas()
R.gStyle.SetOptStat(0)
xbins = 50
xmaxmin = 5
zmax=14
minval=0
maxval=10
profile = R.TProfile2D("prof", "myprofile", xbins, -xmaxmin, xmaxmin, 32, -math.pi, math.pi)
profile.GetZaxis().SetRangeUser(-zmax, zmax)
profile.GetZaxis().SetNdivisions(20)
profile.GetXaxis().SetTitle("#eta")
profile.GetYaxis().SetTitle("#phi")
profile.SetTitleOffset(1.08)
profile.SetTitleSize(0.05)
profile.Fill(0, 0, 10)
getContour(zmax, minval, maxval)

#profile.SetContour(zmax*2)
profile.Draw("COLZ0")
canv.Print("test1.png")









#zmax=50
#minval=1
#maxval=10
#profile = R.TProfile2D("prof", "myprofile", xbins, -xmaxmin, xmaxmin, 32, -math.pi, math.pi)
#profile.Fill(0, 0, 10)
#profile.GetZaxis().SetRangeUser(-zmax, zmax)

#getContour(zmax, minval, maxval)
#profile.Draw("COLZ0")
#canv.Print("test2.png")
