import sys, os
import pandas as pd
import math
import awkward as ak
import numpy as np
import uproot as U
import glob
import datetime as dt
from argparse import ArgumentParser
from array import array 
import re 
# TO DO protect against phase changes that give -ve 
# add comment in log for very large phase diff
# ability to switch tree and branch names depending on provided ntuplex
# Better palatte for underflow/overflow? Or drawn bins where -1000 or -2000
# Better binning per subdetector
# palette - skip 1
# Save firstsam+30 samples as well as current 4 or 5 - so we can make a chi2
# Coverage plot of RMS per layer
# Coverage plot of flat pulse per layer


import ROOT as R
R.gROOT.SetBatch(True)

# Get SC mapping and bad channel info from printMapping
sys.path.append("..")
import printMapping


def getContour():
    red   = [  34./255.,  70./255., 129./255., 187./255., 1, 226./255., 216./255., 193./255., 179./255. ]
    green = [  48./255.,  91./255., 147./255., 194./255., 1, 229./255., 196./255., 110./255.,  12./255. ]
    blue  = [ 234./255., 212./255., 216./255., 224./255., 1, 110./255.,  53./255.,  40./255.,  29./255. ]
    #stops_init = [ 0.0000, 0.1250, 0.2500, 0.3750, 0.5000, 0.6250, 0.7500, 0.8750, 1.0000 ]
    red = [red[0], red[0]] + red + [red[-1], red[-1]]
    green = [green[0], green[0]] + green + [green[-1], green[-1]]
    blue = [blue[0], blue[0]] + blue + [blue[-1], blue[-1]]
    stops = [ 0, 0.4, 0.4001, 0.410, 0.450, 0.499, 0.5000, 0.501, 0.550, 0.590, 0.5999, 0.6, 1 ]

    stops = [ 0 ]
    # [0, 0.15, 0.23725, 0.3245, 0.41174999999999995, 0.499, 0.5, 0.501, 0.56325, 0.6255, 0.68775, 0.75, 1]

    stops.extend([ n for n in np.linspace(0.15, 0.499, 5)] )
    stops.append(0.5)
    stops.extend([ n for n in np.linspace(0.501, 0.75, 5)] )
    stops.append(1)
    
    print(stops)
    
    Number = len(red)
    NCont = len(red)*20
    
    R.TColor.CreateGradientColorTable(Number,array('d',stops),array('d',red),array('d',green),array('d',blue),NCont)
    return NCont

def getContour2(zmax, minval=0.001, maxval=None):

    if maxval is None:
        maxval = (2*zmax)*.8
    red   = [  34./255.,  70./255., 129./255., 187./255., 1, 226./255., 216./255., 193./255., 179./255. ]
    green = [  48./255.,  91./255., 147./255., 194./255., 1, 229./255., 196./255., 110./255.,  12./255. ]
    blue  = [ 234./255., 212./255., 216./255., 224./255., 1, 110./255.,  53./255.,  40./255.,  29./255. ]
    #stops_init = [ 0.0000, 0.1250, 0.2500, 0.3750, 0.5000, 0.6250, 0.7500, 0.8750, 1.0000 ]
    red =   [red[0]]   + red   + [red[-1]]
    green = [green[0]] + green + [green[-1]]
    blue =  [blue[0]]  + blue  + [blue[-1]]

    grad_pos = (maxval/zmax)*0.5
    min_pos = (minval/zmax)*0.5
    grad_start= 0.5-grad_pos  #   0.5-(maxval/zmax)
    min_lo = 0.5-(min_pos)
    min_hi = 0.5+(min_pos)
    grad_end = 0.5+grad_pos   #   0.5+(maxval/zmax)


    stops = [ 0 ]
    if minval != 0:
        ndiv = int((len(red)-3)/2)
        stops.extend([ round(n,2) for n in np.linspace(grad_start, min_lo, ndiv)] )
        stops.append(0.5)
        stops.extend([round(n,2) for n in np.linspace(min_hi, grad_end, ndiv)])
        
    else:
        ndiv = int((len(red))/2)
        stops.extend([ round(n,2) for n in np.linspace(grad_start, 0.5, ndiv)] )
        stops.extend([ round(n,2) for n in np.linspace(0.5, grad_end, ndiv)][1:] )
    stops.append(1)
    
    Number = len(red)
    NCont = len(red)*((2*zmax)+2)
    R.TColor.CreateGradientColorTable(Number,array('d',stops),array('d',red),array('d',green),array('d',blue),NCont)

def scaHist(scas, label="objname", title="LTDB SCA", ytitle="?"):
    hist = R.TH2D(label, title, len(scas), 0, len(scas), 39, -20, 20)

    for sca in range(0, len(scas)):
        hist.GetXaxis().SetBinLabel(sca+1, scas[sca])
    hist.GetYaxis().SetTitle(ytitle)
    hist.LabelsOption("h")
    hist.GetZaxis().SetTitle("# SCs")
    hist.GetXaxis().SetTitleOffset(0.4)
    hist.GetYaxis().SetTitleOffset(0.4)
    hist.GetZaxis().SetTitleOffset(0.4)
    hist.SetTitleOffset(1.08)
    hist.SetTitleSize(0.05)

    return hist

def coverageProfile(label="objname", title="Coverage", zmax=None, zmin=None, det=None, layer=None):
    xbins = 50
    xmaxmin = 5
    if det is not None:
        if "EMB" in det:
            xmaxmin = 1.8
        elif "EMEC" in det:
            xmaxmin = 3.4
        elif "HEC" in det:
            xmaxmin = 3.6
    profile = R.TProfile2D(label, title, xbins, -xmaxmin, xmaxmin, 32, -math.pi, math.pi)

    if zmax is not None:
        if zmin is None:
            zmin = -zmax
        profile.GetZaxis().SetRangeUser(zmin, zmax)
    profile.GetXaxis().SetTitle("#eta")
    profile.GetYaxis().SetTitle("#phi")
    profile.SetTitleOffset(1.08)
    profile.GetXaxis().SetTitleOffset(0.4)
    profile.GetYaxis().SetTitleOffset(0.4)
    profile.GetZaxis().SetTitleOffset(0.4)
    profile.SetTitleSize(0.05)
    return profile
 


def getLay(DETNAME, SCNAME):
    if "EM" in DETNAME:
        this_layer = re.findall("[a-zA-Z]+",SCNAME.split("_")[-1])[0]
    elif "FCAL" in DETNAME:
        this_layer = SCNAME[-1]
    else:
        this_layer = "ALL"
    return this_layer


def getPMinfo(want=["SC_ONL_ID", "LTDB", "SCNAME", "LATOME_NAME", "LATOME_FIBRE", "DETNAME", "BadSC", "CALIB", "SCA"], run=None):

    pmdfcols=[w for w in want if w!="SCA"]
            
    inputs={"want":pmdfcols,"showdecimal":True,"run":run}
    print("querying printMapping for",inputs)
    pmout = printMapping.query(**inputs)
    pmdf = pd.DataFrame(pmout, columns = pmdfcols)
    pmdf = pmdf.astype({'SC_ONL_ID':'int'})

    if "SCNAME" in pmdfcols:
        filter = pmdf['SCNAME'].str.contains("-")
        pmdf = pmdf[~filter]
        filter = pmdf['SCNAME'].str.contains("NOT_CONNECTED")
        pmdf = pmdf[~filter]


    pmdf = pmdf.set_index("SC_ONL_ID")

    if "SCA" in want:
        import requests
        from requests.structures import CaseInsensitiveDict
        import urllib3
        urllib3.disable_warnings() # warning from larid api

        pmdfcols=["SC_ONL_ID", "SCA"]
        # Get info from LArIdTranslator backend API
        url = f"https://atlas-larmon.cern.ch/LArIdTranslatorBackend/api/data?columns={','.join(pmdfcols)}"
        headers = CaseInsensitiveDict()
        headers["Accept"] = "application/json"
        pmdf2 = None
        try:
            resp = requests.get(url, headers=headers, verify=False)
            larid_json = resp.json()
            pmdf2 = pd.DataFrame.from_dict(larid_json)
            pmdf2 = pmdf2.astype({'SC_ONL_ID':'int'})
            pmdf2 = pmdf2.set_index("SC_ONL_ID")
        except Exception as e:
            print("COULD NOT GET SCA FROM LARID BACKEND")
            print(e)
            sys.exit()

        full_pmdf = pd.concat([pmdf, pmdf2], axis=1)
        full_pmdf["LTDB_SCA"] = full_pmdf["LTDB"] + full_pmdf["SCA"].astype("str")
        
        return full_pmdf
            
    return pmdf


import numba as nb



@nb.njit()
def minPed(adc):
    content = np.zeros((len(adc), len(adc[0])), dtype=np.float64)
    ch = 0
    for adc_ch in adc:
        iadc = 0
        ped = adc_ch[0]
        for i in adc_ch:
            norm = i - ped
            content[ch][iadc] = norm
            iadc+=1
        ch += 1
    return content

@nb.njit()
def maxSampInd(adc):
    content = np.zeros(len(adc), dtype=np.intc)
    ch = 0
    for adc_ch in adc:
        themax = -1000
        maxind = 0
        for i in range(0, len(adc_ch)):
            if adc_ch[i] > themax:
                themax = adc_ch[i]
                maxind = i
        content[ch] = maxind
        
        ch += 1
    return content

@nb.jit()
def phaseDiff(new_arr, ref_arr, maxE=True):
    diff = np.zeros(len(new_arr), dtype=np.intc)
    ch = 0
    for i_new in new_arr:
        chid = i_new.channelId
        i_ref = [ e for e in ref_arr if e.channelId == chid ][0] # here protect if chid not found

        if maxE:
            phase_new = i_new.bestPhase[0]
            phase_ref = i_ref.bestPhase[0]
        else:
            phase_new = i_new.bestPhase[1]
            phase_ref = i_ref.bestPhase[1]
            
        if phase_new == -1000 or phase_ref == -1000: # no OFC
            phase_diff = -1000
        elif phase_ref == -2000 or phase_ref == -2000: # flat reference pulse
            phase_diff = -2000
        else:
            phase_diff = phase_new - phase_ref

        diff[ch] = phase_diff
        
        ch += 1
    return diff

@nb.jit()
def getChi2(new_arr, ref_arr):
    chi2 = np.zeros(len(new_arr), dtype=np.float64)
    ch = 0
    for row in new_arr:
        chid = row.channelId
        new_samples = row.mean
        ref_samples = [ e for e in ref_arr if e.channelId == chid ][0].mean

        chi2[ch] = 0
        for sam in range(0, len(new_samples)):
            if ref_samples[0] != 0:
                chi2[ch] += pow(new_samples[0] - ref_samples[0], 2) / ref_samples[0]
            
        ch += 1
    return chi2

@nb.njit()
def applyPhase(samples, ofc, phases, chids):
    energy_tau = np.zeros((len(samples), 2), dtype=np.float64)
    ch = -1
    for sam in samples:
        ch += 1
        these_ofc = [o for o in ofc if o["channelId"] == sam.channelId]

        if len(these_ofc) > 50: # duplicate channels, not sure how else to skip them
            these_ofc = these_ofc[0:50]
        if len(these_ofc) == 0:
            energy_tau[ch][0] = -1000
            energy_tau[ch][1] = -1000
            continue

        these_samples = sam.selected
        phase_ind = np.where(chids==int(sam.channelId))[0][0]
        thePhase = phases[phase_ind][0]
        if thePhase > 49:
            thePhase = 49 
        
        ofca = [o for o in these_ofc if o["Phase"]== thePhase][0]["OFCa"]
        ofcb = [o for o in these_ofc if o["Phase"]== thePhase][0]["OFCb"]
        ene = 0
        tau = 0
        for i in range(0, len(these_samples)):
            ene += these_samples[i] * ofca[i]
            tau += these_samples[i] * ofcb[i]
        if ene == 0:
            energy_tau[ch][0] = -1000
            energy_tau[ch][1] = -1000
            continue
        else:
            tau /= ene
        energy_tau[ch][0] = ene
        energy_tau[ch][1] = tau
    return energy_tau


@nb.njit()
def isBad(adc, minval=12):
    content = np.zeros(len(adc), dtype=np.intc)
    ch = 0
    for adc_ch in adc:
        ped = adc_ch[0]
        #if len([s for s in sam.selected if s > 10]) == 0:  # flat new pulse
        good = 0
        for i in adc_ch:
            norm = i - ped
            if norm > minval:
                good += 1
        if good == 0:
            content[ch] = 1
        else:
            content[ch] = 0
        ch += 1
    return content


@nb.njit()
def bestPhase(samples, ofc):
    best = np.zeros((len(samples), 2), dtype=np.intc)
    allvals = np.zeros((50, 2), dtype='d')
    ch = -1
    for sam in samples:
        ch += 1
        phase_maxe = 0
        phase_mint = 0
        maxe = -1000
        mint = 1000
        these_ofc = [o for o in ofc if o["channelId"] == sam.channelId]

        if len(these_ofc) > 50: # duplicate channels, not sure how else to skip them
            these_ofc = these_ofc[0:50]

        if len(these_ofc) == 0:
            best[ch][0] = -1000
            best[ch][1] = -1000
            # best[ch][2] = -1000
            continue
                    
        phdict = {}
        these_samples = sam.selected

        # Skip flat pulses
        # if not ak.all(sam.selected) > 10:
        if len([s for s in sam.selected if s > 10]) == 0:  # flat new pulse
            best[ch][0] = -3000
            best[ch][1] = -3000
            continue
        
        for ph in range(0, len(these_ofc)):

            ofca = [o for o in these_ofc if o["Phase"]== ph][0]["OFCa"]
            ofcb = [o for o in these_ofc if o["Phase"]== ph][0]["OFCb"]
            ene = 0
            tau = 0
            for i in range(0, len(these_samples)):
                ene += these_samples[i] * ofca[i]
                tau += these_samples[i] * ofcb[i]
            if ene == 0:  # flat new pulse
                phase_maxe = -3000
                phase_mint = -3000
                break
            else:
                tau /= ene

                if ene > maxe:
                    maxe = ene
                    phase_maxe = ph
                if abs(tau) < abs(mint):
                    mint = tau
                    phase_mint = ph
            allvals[ph][0] = ene
            allvals[ph][1] = abs(tau)

        #sorted_e = np.argsort(allvals[:,0])[::-1]
        #sorted_t = np.argsort(allvals[:,1])
        #tmp_newph = -1
        #if abs(phase_maxe-phase_mint) > 5:
        #    for ph in sorted_e[0:5]:
        #        if phase_mint == ph:
        #            tmp_newph = ph
        #    if tmp_newph != -1:
        #        print("Could do better", sam.channelId)
        #        print("TOP 5 max E:", sorted_e[0:5])
        #        print("TOP 5 min t:", sorted_t[0:5])
        #        print("Could change max E phase from",phase_maxe, "to", tmp_newph)
        
        best[ch][0] = phase_maxe
        best[ch][1] = phase_mint
        # best[ch][2] = tmp_newph        
    return best

def getPed(ped, verbose=False):
    ped = str(ped).zfill(8)
    searchTerm = "LArPedAutoCorr"
    treeName = "PEDESTALS"
    PEDwildcard=f"/afs/cern.ch/user/e/ekay/APoutput/{ped}*/root_files/{searchTerm}_*.root"
    if verbose: print(f"Looking for {searchTerm} in {PEDwildcard}")
    PEDfiles = glob.glob(PEDwildcard)
    if len(PEDfiles) == 0:
        print(f"No PED files found with wildcard {PEDwildcard}")
        sys.exit()

    PEDfiles = [ f"{f}:{treeName}" for f in PEDfiles ]
    PEDfiles = PEDfiles[0]
    #if int(ped) == 479769 and "OFCCali" in searchTerm: # we had a bug in OFCCali for this set
    #    OFCfiles.append( "/afs/cern.ch/user/p/pavol/w0/public/DB_update_24/dumpDB/SCOFC_cali.root:OFCCali" )

    dfs = []
    ch_filled = []
    for arrays,report in U.iterate(PEDfiles, expressions=["channelId","ped"], step_size="1 GB", report=True):
        these_ch = list(set(list(arrays.channelId)))
        if verbose: print("Reading chunk from:", report.file_path, "...", len(these_ch), "channels")
        tofill = [ch for ch in these_ch if ch not in ch_filled ]
        dfs.append(arrays)
        ch_filled.extend(list(arrays.channelId))
        ch_filled = list(set(ch_filled))
    arr = ak.concatenate(dfs, axis=0)
    chids_all = list(arr.channelId)
    chids = list(set(list(arr.channelId)))

    if verbose: print(len(chids), f"channels in {searchTerm} {treeName} data")

    return arr, chids


def getOFC(ped, isCali=True, verbose=False, highMu=False):
    ped = str(ped).zfill(8)
    searchTerm = "LArOFCCali"
    treeName = "OFC"
    if not isCali:
        searchTerm = "LArPhysOFC"
        if highMu:
            treeName = "OFC_1ns_mu"
        else:
            treeName = "OFC_1ns"
    OFCwildcard=f"/afs/cern.ch/user/e/ekay/APoutput/{ped}*/root_files/{searchTerm}_*.root"
    if verbose: print(f"Looking for {searchTerm} in {OFCwildcard}")
    OFCfiles = glob.glob(OFCwildcard)
    if len(OFCfiles) == 0:
        print(f"No OFC files found with wildcard {OFCwildcard}")
        sys.exit()

    OFCfiles = [ f"{f}:{treeName}" for f in OFCfiles ]

    #if int(ped) == 479769 and "OFCCali" in searchTerm: # we had a bug in OFCCali for this set
    #    OFCfiles.append( "/afs/cern.ch/user/p/pavol/w0/public/DB_update_24/dumpDB/SCOFC_cali.root:OFCCali" )

    dfs = []
    ch_filled = []
    for arrays,report in U.iterate(OFCfiles, expressions=["channelId","Phase","OFCa","OFCb","FT"], step_size="1 GB", report=True):
        these_ch = list(set(list(arrays.channelId)))
        if verbose: print("Reading chunk from:", report.file_path, "...", len(these_ch), "channels")
        tofill = [ch for ch in these_ch if ch not in ch_filled ]
        dfs.append(arrays)
        ch_filled.extend(list(arrays.channelId))
        ch_filled = list(set(ch_filled))
    arr = ak.concatenate(dfs, axis=0)
    chids_all = list(arr.channelId)
    chids = list(set(list(arr.channelId)))
    #chidcount = { chid: chids_all.count(chid) for chid in chids }
    #print([ch for ch in chids if chidcount[ch] > 50 ])
    if verbose: print(len(chids), f"channels in {searchTerm} {treeName} data")

    return arr, chids

def makeLine(gr, val, col, style=1):
    line = R.TLine(val, gr.GetHistogram().GetMinimum(), val, gr.GetHistogram().GetMaximum())
    line.SetLineColor(col)
    line.SetLineWidth(2)
    line.SetLineStyle(style)
    return line



def drawPhase(ch, enes, taus, phases, maxe, mint, samples=None, col=R.kBlack, outDir=".", outName="", info=None, improved_phase=None):
    gene = R.TGraph(len(enes), np.array(phases,dtype='d'), enes)
    gene.SetTitle(f"Energy per Phase for Channel {ch}")
    gene.SetMarkerStyle(20)
    gene.SetMarkerColor(col)
    gene.SetName(f"ene_{ch}")

    gtau = R.TGraph(len(taus), np.array(phases,dtype='d'), abs(taus))
    gtau.SetTitle(f"#tau per Phase for Channel {ch}")
    gtau.SetMarkerStyle(20)
    gtau.SetMarkerColor(col)
    gtau.SetName(f"tau_{ch}")

    eline = makeLine(gene, maxe[0], col)
    tline = makeLine(gene, mint[0], col, 2)

    eline_improved = None
    tline_improved = None
    
    if improved_phase is not None:
        eline_improved = makeLine(gene, improved_phase, R.kGreen)
        tline_improved = makeLine(gtau, improved_phase, R.kGreen)

    
    outname_full = f"{outDir}/phases_E_T_{ch}"
    if outName != "":
        outname_full += f"_{outName}"

        #if info is not None:
    def drawTxt(info, canv, x, y):
        stringy = y 
        for i in info:
            txt = R.TLatex()
            txt.SetNDC()
            txt.SetTextFont(42)
            txt.SetTextColor(R.kBlue)
            txt.SetTextAlign(10)
            txt.SetTextSize(0.04)
            txt.DrawLatex(x, stringy, i)
            stringy -= 0.05
            canv.objs.append(txt)
        
    if samples is not None:
        nsplit=3
    else:
        nsplit=2
    canv = R.TCanvas(f"canv_phases_E_T_{ch}_{outName}", f"canv_{ch}", 750*nsplit, 600)
    canv.objs=[]
    
    legend = R.TLegend(.84,.5,.99,.95)
    legend.SetFillStyle(0)
    legend.SetTextColor(R.kBlack)
    legend.SetTextSize(.037)
    legend.SetBorderSize(0)
    
    canv.Divide(nsplit,1)
    canv.cd(1)
    R.gPad.SetRightMargin(.17)
    gene.Draw("ap")
    canv.objs.append(gene)
    gene.GetXaxis().SetTitle("Phase")
    gene.GetYaxis().SetTitle("#Sigma(ai*si)")

    if info is not None:
        drawTxt(info, canv, 0.15, 0.4)

    legend.AddEntry(eline, f"max E: {maxe[0]}", "l")
    eline.Draw("samel")
    legend.AddEntry(tline, f"min |#tau|: {mint[0]}", "l")
    if improved_phase is not None:
        legend.AddEntry(eline_improved, f"proposed", "l")
        eline_improved.Draw("samel")
        canv.objs.append(eline_improved)
    tline.Draw("samel")
    canv.objs.append(eline)
    canv.objs.append(tline)
    legend.Draw("same")
    canv.objs.append(legend)
    eline2 = makeLine(gtau, maxe[0], col)
    tline2 = makeLine(gtau, mint[0], col, 2)

    canv.cd(2)
    R.gPad.SetRightMargin(.17)
    gtau.Draw("ap")
    canv.objs.append(gtau)
    gtau.GetXaxis().SetTitle("Phase")
    gtau.GetYaxis().SetTitle("|#Sigma(bi*si)/#Sigma(ai*si)|")
    eline2.Draw("samel")

    if improved_phase is not None:
        tline_improved.Draw("samel")
        canv.objs.append(tline_improved)
    
    tline2.Draw("samel")
    canv.objs.append(eline2)
    canv.objs.append(tline2)
    legend.Draw("same")

    if samples is not None:
        canv.cd(3)
        #R.gPad.SetRightMargin(.17)
        gsamp = R.TGraph(len(samples), np.arange(0,len(samples),dtype='d'), samples.to_numpy().astype('d'))
        gsamp.SetTitle(f"Picked Samples for Channel {ch}")
        gsamp.SetMarkerStyle(20)
        gsamp.SetMarkerColor(col)
        gsamp.SetLineColor(R.kBlue)
        gsamp.SetName(f"tau_{ch}")
        gsamp.Draw("apl")
        canv.objs.append(gsamp)
        gsamp.GetXaxis().SetTitle("Iteration")
        gsamp.GetYaxis().SetTitle("ADC-ped")


    return canv # canv.Print(f"{outname_full}.png")


class treePA:  
    def __init__(self, pathPA, isCali=True, currentPhases=None):
        self.rootPath = pathPA
        possbranches = ['channelid','mean','rms','eta','phi']
        thefile = U.open(self.rootPath)
        filekeys = [ k for k in thefile.keys() if "TTree" in str(type(thefile[k])) ]
        self.treeName = filekeys[0].replace(";1","")
        print(f"Reading tree {self.treeName}")
        allbranches = thefile[self.treeName].keys() 
        self.branches = [ b for b in allbranches if b.lower() in possbranches ]
        chids = list(set(thefile[self.treeName].arrays(["channelId"], library="np")["channelId"]))
        #print(list(set(iev))) # event indices
        print(len(chids), "channels")
        entries = thefile[self.treeName].num_entries
        events = entries / len(chids)
        print(events, "events per channel")
        thefile.close()
        self.cut = None
        if events > 1:
            if "IEvent" in allbranches:
                self.branches.append("IEvent")
                iev = thefile[self.treeName].arrays(["IEvent"], library="np")["IEvent"]
                print("NOTE: only taking first event per channel")
                self.cut = f"IEvent=={iev[0]}"
            else:
                print("Not sure how to select events...")
                sys.exit()
        #self.treeName = 'Chi2'
        self.isCali = isCali
        self.data = self.getArray()
        self.currentPhases = currentPhases
        
    def getArray(self):
        arrs = []
        for arrays, report in U.iterate(files=[f'{self.rootPath}:{self.treeName}'], expressions=self.branches, step_size="500 MB", recover=True, report=True, cut=self.cut):
            if len(arrays) == 0: continue
            these_ch = list(set(list(arrays.channelId)))
            #if verbose:
            print("Reading chunk from:", report.file_path, "...", len(these_ch), "channels")
            #arrays["adcminped"] = ak.Array( minPed(arrays.mean) )
            # Reduce to the samples that we want
            arrays["selected"] = ak.Array( minPed(arrays.mean) )
            arrays["isBad"] = ak.Array( isBad(arrays.mean, minval=10) )
            
            arrays["maxind"] = ak.Array( maxSampInd(arrays.mean) )
            meanPeakSample = ak.mean(arrays.maxind)
            firstsam = 2
            if "RMS" in self.branches or meanPeakSample < 3:
                firstsam = 0
            print(".. using samples starting from index",firstsam)

            arrays["mean"] = arrays.mean[:,firstsam:firstsam+30] # in case we want to compare the full pulse for averaged and raw data
                
            if self.isCali:
                arrays["selected"] = arrays.selected[:,firstsam:firstsam+5]
            else: # phys OFCs use 4 samples                
                arrays["selected"] = arrays.selected[:,firstsam:firstsam+4]
            arrs.append(arrays)

        if len(arrs) == 1:
            return arrs[0]
        else:
            return ak.concatenate(arrs, axis=0)
        print("done")
        sys.exit()
    def applyOFC(self, ofc):
        self.data["bestPhase"] = ak.Array(bestPhase(self.data, ofc))
        #diffPhase  = ak.flatten(self.data["bestPhase"][:,[0]] - self.data["bestPhase"][:,[1]] >= 5)
        if self.currentPhases is not None:
            phases = self.currentPhases.to_numpy()
            chids = self.currentPhases.index.to_numpy()

            self.data["currentETau"] = ak.Array(applyPhase(self.data, ofc, phases, chids))

    def fillPhaseDiff(self, compdata):
        self.data["phaseDiff"] = ak.Array(phaseDiff(self.data, compdata))
        self.data["phaseDiff_mintau"] = ak.Array(phaseDiff(self.data, compdata, False))
    def fillChi2(self, compdata):
        self.data["chi2"] = ak.Array(getChi2(self.data, compdata))

def getEneTau(these_samples, ofca, ofcb, ch, outstr, verbose=False): # for plotting
    maxe = [22,-1000]
    mint = [22,1000]
    phdict = {}
    phases = list(range(0, len(ofca)))

    if len(phases) > 50: # duplicate channels, not sure how else to skip them
        phases = phases[0:50]

    for ph in phases: # probably could do this in a columnar way
        ofca_ph = ak.Array(np.tile(ofca[ph],(len(these_samples),1)))
        ofcb_ph = ak.Array(np.tile(ofcb[ph],(len(these_samples),1)))
        sam_ofca = these_samples*ofca_ph
        sam_ofcb = these_samples*ofcb_ph
        ene = ak.sum(sam_ofca,axis=1)
        tau = ak.sum(sam_ofcb,axis=1) / ene
        avene = ak.mean(ene)
        avtau = ak.mean(tau)
        if avene > maxe[1]:
            maxe = [ph,avene]
        if abs(avtau) < abs(mint[1]):
            mint = [ph,avtau]
        phdict[ph] = [avene,avtau]


    enes = np.array([phdict[ph][0] for ph in phdict.keys()], dtype='d')
    taus = np.array([phdict[ph][1] for ph in phdict.keys()], dtype='d')


    tmp_newph = -1
    
    if maxe[0] != mint[0]:
        if verbose: print(f"{outstr} CHANNEL {ch} HAS DIFFERENT PHASE FOR MAX E ({maxe[0]}) AND MIN TAU ({mint[0]})")

        sorted_e = np.argsort(enes)[::-1]
        sorted_t = np.argsort(abs(taus))

        # See if we can match the phases, if min tau phase is in the top 5 max e phases
        for ph in sorted_e[0:5]:
            if mint[0] == ph:
                tmp_newph = ph
        #if verbose:
        if tmp_newph == -1: #  and maxe[0]==0 or maxe[0]==49:
            if np.isin(mint[0], sorted_e[0:10]):
                #print(ch, outstr, "YES",mint[0], "in top 10")
                tmp_newph = mint[0]
            else:
                intercept = sorted_t[0:10][np.in1d(sorted_t[0:10], sorted_e[0:10])]
                if len(intercept) > 0:
                    #print(ch, outstr, "YES",intercept[0],"in",intercept, sorted_e[0:10], sorted_t[0:10])
                    tmp_newph = intercept[0]
                else:
                    print(ch, outstr, "Couldn't improve", maxe[0], mint[0])
                    if verbose: print(enes[0:10], sorted_e[0:10])
                    if verbose: print(abs(taus)[0:10], sorted_t[0:10])
                    

            #print("***")
    if tmp_newph == -1:
        tmp_newph = None
    return enes, taus, maxe, mint, phases, tmp_newph


        
def drawPulses(ch, samples_new, samples_ref, outDir): 
    def sampToGr(samples, col, style):
        gr = R.TGraph(len(samples),
                      np.arange(0,len(samples),dtype='d'),
                      samples.to_numpy().astype('d'))
        gr.SetTitle(f"Pulse Shape for Channel {ch}")
        gr.GetXaxis().SetTitle("Sample")
        gr.GetYaxis().SetTitle("ADC")
        gr.SetLineColor(col)
        gr.SetMarkerColor(col)
        gr.SetLineStyle(style)
        return gr
    
    pulsegr_new = sampToGr(samples_new, R.kBlack, 1)
    pulsegr_ref = sampToGr(samples_ref, R.kGreen, 2)
    
    canv = R.TCanvas()
    pulsegr_new.Draw("apl")
    pulsegr_ref.Draw("pl,same")

    canv.Print(f"{outDir}/pulses_{ch}.png")
    
            
def main(weekly_ped, newPA, refPA, currentPhases=None, outDir=".", useOFCPhys=False, useOFCPhysMu=False, plotcut_etaudiff=5, max_phasediff=10, min_phasediff=1, newRunNum=None, refRunNum=None):
    print("Started", dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S.%f"))   

    if newRunNum is None:
        newRunNum = newPA.split("LATOMERun_PulseAll_")[1].split("_")[0]
    if refRunNum is None:
        refRunNum = refPA.split("LATOMERun_PulseAll_")[1].split("_")[0]              


    isCali = not useOFCPhys and not useOFCPhysMu

    currentPhasePath = None
    if currentPhases is not None:
        print(f"Reading current phases from {currentPhases}")
        currentPhasePath = currentPhases
        currentPhases = pd.read_csv(currentPhases, names=["channelId","phase"], sep=" ", index_col="channelId", dtype=int)
    pmdf = getPMinfo(run=int(newRunNum))

    # Define tprofiles for phase distribution
    detectors = sorted(list(set([ det[:-1] for  det in pmdf["DETNAME"].unique() ]))) # Plot per detector, both sides in the same plot
    detlay = { "EMB": ["P", "F", "M", "B"], "EMEC": ["P", "F", "M", "B"], "FCAL": ["1", "2", "3"], "HEC": ["ALL"] }

    SCAs = { det: sorted(list(set(pmdf[pmdf.DETNAME.str.contains(det)].LTDB_SCA.values))) for det in detectors }

    highMu = useOFCPhysMu

    ofcData, ofcChids = getOFC(weekly_ped, isCali=isCali, verbose=True, highMu=highMu)
    print("Preparing new tree",dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S.%f"))
    tree_new = treePA(newPA, isCali, currentPhases=currentPhases)

    ofcStr = "LArOFCCali"
    if useOFCPhys or useOFCPhysMu:
        ofcStr = "LArPhysOFC"
        if highMu:
            ofcStr+= "Mu"
        else:
            ofcStr+= "Mu0"
    # tag for output file names
    txt_strname = f"{str(weekly_ped).zfill(8)}_{ofcStr}_maxDiff_{max_phasediff}_minDiff_{min_phasediff}_ref_{refRunNum}_new_{newRunNum}"
    # string for plot titles
    txt_info = f"Run {newRunNum}, with {weekly_ped}+ DT weekly {ofcStr} OFCs & ref {refRunNum}"

    # document channels for which we don't have calib constants
    chids_new = list(set(list(tree_new.data.channelId)))
    noOFC = [ ch for ch in chids_new if ch not in ofcChids ]
    if len(noOFC) > 0:
        noOFCfile = f"{outDir}/out_noOFC_{txt_strname}.txt"
        print("NO OFC FOR",len(noOFC), "channels. Noting these in",noOFCfile,dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S.%f"))    
        out_noOFC = open(noOFCfile,"w")
        out_noOFC.write(f"/afs/cern.ch/user/e/ekay/APoutput/{weekly_ped}*/root_files/{ofcStr}_*.root\n")
        out_noOFC.write("*"*20 + "\n")
        outformat = "{:<24s}"*5
        out_noOFC.write(outformat.format("SC_ONL_ID", "LATOME_NAME", "LATOME_FIBRE", "CALIB", "BadSC")+"\n")
        for ch in noOFC:
            pm_info = pmdf.loc[int(ch)]
            out_noOFC.write(outformat.format(str(ch), pm_info["LATOME_NAME"], pm_info["LATOME_FIBRE"], pm_info["CALIB"],pm_info["BadSC"])+ '\n')
        out_noOFC.close()
    
    tree_new.applyOFC(ofcData)

    print("Preparing ref tree",dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S.%f"))
    tree_ref = treePA(refPA, isCali, currentPhases=currentPhases)
    tree_ref.applyOFC(ofcData)
    
    chids_ref = list(set(list(tree_ref.data.channelId)))

    all_chids = list(set(chids_new+chids_ref))
    if len(chids_new) != len(chids_ref):
        print(f"Warning: different number of channels in new ({len(chids_new)}) and ref ({len(chids_ref)}).",dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S.%f"))
        
    print("Comparing phase",dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S.%f"))
    tree_new.fillPhaseDiff(tree_ref.data)
    print("done", dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S.%f"))

    print("Comparing pulse shapes",dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S.%f"))
    tree_new.fillChi2(tree_ref.data)
    print("done", dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S.%f"))

    # phase diff between new and ref, coverage style
    plot_phasediff_ref_E = {}
    # plot_phasediff_ref_t = {}
    # plot_phasediff_E_t = {}
    # phase diff between new and ref, per SCA
    h_phasediff_ref_E_SCA = {}

    plot_maxAmp = {}

    plot_meanRMS = {}

    plot_chi2  = {}
    # tau histograms, 1D, to overlay
    h_tau_new = {}
    h_tau_ref = {}

    plot_calibIssues = {}

    for det in detectors:
        plot_phasediff_ref_E[det] = {}
        plot_calibIssues[det] = {}
        plot_maxAmp[det] = {}
        plot_meanRMS[det] = {}
        plot_chi2[det] = {}
        for lay in detlay[det]:
            plot_phasediff_ref_E[det][lay] = coverageProfile(label=f"phasediff_e_{det}_{lay}", title="#splitline{"+f"{det}{lay} New phase - Ref phase to maximise E"+"}{"+ f"({txt_info})"+"}", zmax=max_phasediff+4)
            plot_calibIssues[det][lay] = coverageProfile(label=f"calibIssues_{det}_{lay}", title="#splitline{"f"{det}{lay}: 1 = missing OFC, 2 = flat ref, 3 = flat new+ref, 4 = flat new"+"}{"+ f"({txt_info})"+"}", zmax=5, zmin=0)
            plot_maxAmp[det][lay] = coverageProfile(label=f"maxAmp_{det}_{lay}", title="#splitline{"f"{det}{lay} Max ADC-ped"+"}{"+ f"({txt_info})"+"}", zmax=2000, zmin=-100)
            plot_meanRMS[det][lay] = coverageProfile(label=f"meanRMS_{det}_{lay}", title="#splitline{"f"{det}{lay} Mean RMS"+"}{"+ f"({txt_info})"+"}")
            plot_chi2[det][lay] = coverageProfile(label=f"chi2_{det}_{lay}", title="#splitline{"f"{det}{lay} "+"#chi^{2} (new vs. ref)}{"+ f"({txt_info})"+"}")
            
            
        h_phasediff_ref_E_SCA[det] = scaHist(SCAs[det], label=f"phasediff_e_{det}_SCA", title=f"{det} New phase - Ref phase to maximise E ({txt_info})", ytitle="New phase - Ref phase")
        h_tau_new[det] = R.TH1F(f"h_tau_new_{det}", f"{det} #tau ({txt_info})", 50, -25, 25 )
        h_tau_new[det].GetXaxis().SetTitle("#tau")

        h_tau_ref[det] = R.TH1F(f"h_tau_ref_{det}", f"{det} #tau ({txt_info})", 50, -25, 25)
        h_tau_ref[det].GetXaxis().SetTitle("#tau")
        
    print("Applying some masks")
    # channels which have large difference between phase for maximising E or minimising tau in the new set
    diffPhase_etau  = ak.flatten(abs(tree_new.data["bestPhase"][:,[0]] - tree_new.data["bestPhase"][:,[1]]) >= plotcut_etaudiff)
    # channels which have a different phase between new and ref sets, but smaller than the max allowed 
    diffPhase_newref_limited  = (tree_new.data["phaseDiff"][:] != 0)  & (abs(tree_new.data["phaseDiff"][:]) < max_phasediff ) & (abs(tree_new.data["phaseDiff"][:]) > min_phasediff )
    # channels which have some issue in calibration for new or ref or the weekly
    diffPhase_badCalib = tree_new.data["phaseDiff"][:] <= -1000

    shifted = list(set(list(tree_new.data[diffPhase_newref_limited].channelId)))
    badcal = list(set(list(tree_new.data[diffPhase_badCalib].channelId)))
    etau_largediff = list(set(list(tree_new.data[diffPhase_etau].channelId)))
    print(f"{len(shifted)} channels have a phase difference > {min_phasediff} and < {max_phasediff}. {len(badcal)} channels cannot be shifted due to bad calibration.")
    print(len(etau_largediff), "where peak phase for max E and min tau is different by >=",plotcut_etaudiff, dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S.%f"))
    
    # Store any channels for which we found a better phase
    changed_new = {}
    changed_ref = {}
    
    outname_phasePdf = f"{outDir}/phases_E_T_{txt_strname}.pdf"
    canvs_phase = []

    
    # Write output files"
    out_debug = open(f"{outDir}/out_debug_{txt_strname}.txt","w")
    out_newphase = open(f"{outDir}/out_newphase_{txt_strname}.txt","w")
    out_moreInfo = open(f"{outDir}/out_moreInfo_{txt_strname}.txt","w")
    
    out_debug.write(f"Test set: {newPA}\n")
    out_debug.write(f"Ref set: {refPA}\n")
    out_debug.write(f"Max shift: {max_phasediff}\n")
    out_debug.write(f"OFC type: {ofcStr}\n")
    out_debug.write(f"Using OFCs from weekly calibration with pedestal: {weekly_ped}\n")
    

    
    printcols = ["Channel", "newph_e", "newph_t", "refph_e", "refph_t", "phdiff_e"]
    if currentPhases is not None:
        printcols.extend(["currentph", "newph"])
        out_debug.write(f"Current phases from {currentPhasePath}\n")
    outformat = "{:<12s}"*len(printcols)
    out_debug.write("*"*20+"\n\n")
    # Add db info
    printcols.extend(["LATOME", "FIBRE", "LTDB_SCA", "CALIB", "BadSC"])
    outformat += "{:<24s}{:<24s}{:<10s}{:<20s}{:<30s}"
    out_debug.write(outformat.format(*printcols) + "\n")


    printcols_info = ["Channel", "maxAmp", "maxAmpInd", "meanRMS", "maxRMS", "chi2"]
    outformat_info = "{:<12s}"*len(printcols_info)
    out_moreInfo.write("*"*20+"\n\n")
    # Add db info
    printcols_info.extend(["LATOME", "FIBRE", "LTDB_SCA", "CALIB", "BadSC"])
    outformat_info += "{:<24s}{:<24s}{:<10s}{:<20s}{:<30s}"
    out_moreInfo.write(outformat_info.format(*printcols_info) + "\n")

    
    print("Looping over all channels")
    for chid in all_chids: # One loop over all channels
        percent = ( all_chids.index(chid) / len(all_chids) )*100
        if percent%10 == 0 and all_chids.index(chid)!=0:
            print(f"Processed {percent}% of channels.",dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S.%f"))

        # Isolate tree info
        new = tree_new.data[tree_new.data.channelId == chid][0]
        ref = tree_ref.data[tree_ref.data.channelId == chid][0]

        pm_info = pmdf.loc[int(chid)]
        this_det = pm_info["DETNAME"][:-1]
        this_SC = pm_info["SCNAME"]
        UPD_status = pm_info["BadSC"]
        this_layer = getLay(this_det, this_SC)
        this_SCA = pm_info["LTDB_SCA"]
        scabin = h_phasediff_ref_E_SCA[this_det].GetXaxis().FindBin(this_SCA)
        sca_cent = h_phasediff_ref_E_SCA[this_det].GetXaxis().GetBinCenter(scabin)
        
        ofcs = ofcData[ofcData.channelId==chid]
        if isCali:
            ofca = ofcs.OFCa[:,0:5]
            ofcb = ofcs.OFCb[:,0:5]
        else:
            ofca = ofcs.OFCa[:,0:4]
            ofcb = ofcs.OFCb[:,0:4]
        ch_info = [ f"{k} = {pm_info[k]}" for k in pmdf.columns ]

        if abs(new.bestPhase[0] - new.bestPhase[1]) >= plotcut_etaudiff:
            enes_new, taus_new, maxe_new, mint_new, phases, improved_phase_new = getEneTau(new.selected, ofca, ofcb, chid, "NEW", verbose=False)

            canvs_phase.append(drawPhase(chid, enes_new, taus_new, phases, maxe_new, mint_new, samples=new.selected, col=R.kBlack, outDir=outDir, outName=txt_strname, info=ch_info, improved_phase=improved_phase_new))

            if improved_phase_new is not None:
                changed_new[chid] = improved_phase_new
            
        if abs(ref.bestPhase[0] - ref.bestPhase[1]) >= plotcut_etaudiff:
            enes_ref, taus_ref, maxe_ref, mint_ref, phases, improved_phase_ref = getEneTau(ref.selected, ofca, ofcb, chid, "REF", verbose=False)
            if improved_phase_ref is not None:
                changed_ref[chid] = improved_phase_ref

        new_bestPhase_maxE = new.bestPhase[0]
        new_bestPhase_minT = new.bestPhase[1]
        ref_bestPhase_maxE = ref.bestPhase[0]
        ref_bestPhase_minT = ref.bestPhase[1]

        phasediff = new.phaseDiff

        phasechange_comment = []
        if chid in changed_new.keys():
            phasechange_comment.append(f"Max E (new): {new_bestPhase_maxE} -> {changed_new[chid]}")
            new_bestPhase_maxE = changed_new[chid]
        if chid in changed_ref.keys():
            phasechange_comment.append(f"Max E (ref): {ref_bestPhase_maxE} -> {changed_ref[chid]}")
            ref_bestPhase_maxE = changed_ref[chid]

        if len(phasechange_comment) == 0:
            phasechange_comment = ""
        else:
            phasechange_comment = ". ".join(phasechange_comment)
        test_phasediff = new_bestPhase_maxE - ref_bestPhase_maxE
        
        
        # Comment for log file, which can explain crazy values
        comment = []
        # various calibration issues
        if chid in noOFC:
            comment.append("Missing OFC in weekly set")
        if ref.isBad == 1:
            comment.append("Flat pulse in ref pulseAll")
        if new.isBad == 1:
            comment.append("Flat pulse in new pulseAll")
            
        # improved phase
        if test_phasediff != phasediff and phasechange_comment != "": 
            comment.append(phasechange_comment)
            phasediff = test_phasediff
        # shift too small
        if abs(phasediff) <= min_phasediff and phasediff != 0:  
            comment.append(f"Original phase diff of {phasediff} negligible")
            phasediff = 0
        # shift too big
        elif phasediff >= max_phasediff: 
            comment.append(f"Original phase diff if {phasediff} too large")
            phasediff = 0
            
        if len(comment) > 0:
            comment = f"  ## {'. '.join(comment)}"
        else:
            comment = ""

        # Get rid of the crazy values due to bad calibration    
        if phasediff < -100: 
            phasediff = 0

        # Get shifted version of current phase
        shifted_phase = None
        if currentPhases is not None:
            ph_curr = currentPhases.loc[int(chid)].phase
            shifted_phase = ph_curr+phasediff
            if shifted_phase > 49:
                shifted_phase = 49
            if shifted_phase < 0:
                shifted_phase = 0
                
        # Write to log
        toprint = [ chid, new_bestPhase_maxE, new_bestPhase_minT, ref_bestPhase_maxE, ref_bestPhase_minT, phasediff ]
        if currentPhases is not None:
            toprint.extend([ph_curr, shifted_phase])

        toprint.extend([pm_info["LATOME_NAME"], pm_info["LATOME_FIBRE"], pm_info["LTDB_SCA"], pm_info["CALIB"], pm_info["BadSC"]])

        toprint = [ str(s) for s in toprint ]
        out_debug.write(outformat.format(*toprint) + comment + "\n")

        out_newphase.write(f"{chid} {shifted_phase}\n")

        maxAmp = ak.max(new.selected)
        maxInd = ak.argmax(new.selected)
        meanRMS = ak.mean(new.rms)
        maxRMS = ak.max(new.rms)
        toprint_info = [str(s) for s in [ chid, round(maxAmp,3), maxInd, round(meanRMS,3), round(maxRMS,3), round(new.chi2,3) ]]
        toprint_info.extend([pm_info["LATOME_NAME"], pm_info["LATOME_FIBRE"], pm_info["LTDB_SCA"], pm_info["CALIB"], pm_info["BadSC"]])
        out_moreInfo.write(outformat_info.format(*toprint_info) + "\n")

        # Skip masked SC
        if "maskedOSUM" in UPD_status:
            continue
        # Fill histograms
        h_phasediff_ref_E_SCA[this_det].Fill(sca_cent, phasediff, 1)
        plot_phasediff_ref_E[this_det][this_layer].Fill(new.eta, new.phi, phasediff)


        plot_maxAmp[this_det][this_layer].Fill(new.eta, new.phi, maxAmp)

        plot_meanRMS[this_det][this_layer].Fill(new.eta, new.phi, meanRMS)

        plot_chi2[this_det][this_layer].Fill(new.eta, new.phi, new.chi2)
        
        if chid in noOFC:
            plot_calibIssues[this_det][this_layer].Fill(new.eta, new.phi, 1) # bad weekly
        elif ref.isBad == 1 and new.isBad == 0:
            plot_calibIssues[this_det][this_layer].Fill(new.eta, new.phi, 2) # bad ref PA
        elif ref.isBad == 1 and new.isBad == 1:
            plot_calibIssues[this_det][this_layer].Fill(new.eta, new.phi, 3) # bad ref PA and new PA
        elif ref.isBad == 0 and new.isBad == 1:
            plot_calibIssues[this_det][this_layer].Fill(new.eta, new.phi, 4) # bad new PA
        
        
        # plot_phasediff_ref_t[this_det][this_layer].Fill(new.eta, new.phi, new.phaseDiff_mintau) # underflow look empty in COLZ
        if currentPhases is not None:
            h_tau_new[this_det].Fill(new.currentETau[1])
            h_tau_ref[this_det].Fill(ref.currentETau[1])


        
        
    out_debug.close()
    out_newphase.close()
    out_moreInfo.close()


    for canv in canvs_phase:
        c = canvs_phase.index(canv)
        if c == 0:
            canvs_phase[c].Print(f"{outname_phasePdf}(","pdf")
        elif c == len(canvs_phase)-1:
            canvs_phase[c].Print(f"{outname_phasePdf})","pdf")
        else:
            canvs_phase[c].Print(f"{outname_phasePdf}","pdf")





    # phase diff max E
    #getContour2(max_phasediff+4, 1, max_phasediff)
    NCont = getContour()
    canv = R.TCanvas(f"canv_{txt_strname}", f"canv_{txt_strname}", 750*4, 600*3)
    canv.Divide(4,3)
    R.gStyle.SetOptStat(0)
    drawn=1
    for det in detectors:
        for lay in detlay[det]:
            canv.cd(drawn)
            R.gPad.SetLeftMargin(0.03)
            R.gPad.SetRightMargin(0.18)
            plot_phasediff_ref_E[det][lay].GetZaxis().SetNdivisions(28)
            plot_phasediff_ref_E[det][lay].SetContour(NCont)
            plot_phasediff_ref_E[det][lay].Draw("COLZ0")
            drawn+=1
    drawn=1
    canv.Print(f"{outDir}/coverage_phasediff_maxE_{txt_strname}.png")


    # Tau for current phase - comparison
    canv = R.TCanvas(f"canv_{txt_strname}_2", f"canv_{txt_strname}", 750, 600*4)
    canv.Divide(1,4)
    for det in detectors:
        canv.cd(detectors.index(det)+1)
        h_tau_ref[det].SetLineColor(R.kRed)
        h_tau_new[det].SetLineColor(R.kBlack)
        h_tau_new[det].Draw("hist")
        h_tau_ref[det].Draw("histsame")
        h_tau_new[det].Draw("histsame")
        
    canv.Print(f"{outDir}/HISTtau_{txt_strname}.png")

    # calib issues
    R.gStyle.SetPalette(R.kTemperatureMap)
    canv = R.TCanvas(f"canv_{txt_strname}_3", f"canv_{txt_strname}_3", 750*4, 600*3)
    canv.Divide(4,3)
    R.gStyle.SetOptStat(0)
    drawn=1
    for det in detectors:
        for lay in detlay[det]:
            canv.cd(drawn)
            R.gPad.SetLeftMargin(0.03)
            R.gPad.SetRightMargin(0.18)
            plot_calibIssues[det][lay].Draw("COLZ0")
            drawn+=1
    drawn=1
    canv.Print(f"{outDir}/coverage_calibIssues_{txt_strname}.png")

    # max amp
    R.gStyle.SetPalette(R.kTemperatureMap)
    canv = R.TCanvas(f"canv_{txt_strname}_4", f"canv_{txt_strname}_4", 750*4, 600*3)
    canv.Divide(4,3)
    R.gStyle.SetOptStat(0)
    drawn=1
    for det in detectors:
        for lay in detlay[det]:
            canv.cd(drawn)
            R.gPad.SetLeftMargin(0.03)
            R.gPad.SetRightMargin(0.18)

            plot_maxAmp[det][lay].Draw("COLZ0")
            drawn+=1
    drawn=1
    canv.Print(f"{outDir}/coverage_maxAmp_{txt_strname}.png")
    
    # mean RMS
    R.gStyle.SetPalette(R.kTemperatureMap)
    canv = R.TCanvas(f"canv_{txt_strname}_5", f"canv_{txt_strname}_5", 750*4, 600*3)
    canv.Divide(4,3)
    R.gStyle.SetOptStat(0)
    drawn=1
    for det in detectors:
        for lay in detlay[det]:
            canv.cd(drawn)
            R.gPad.SetLeftMargin(0.03)
            R.gPad.SetRightMargin(0.18)

            plot_meanRMS[det][lay].Draw("COLZ0")
            drawn+=1
    drawn=1
    canv.Print(f"{outDir}/coverage_meanRMS_{txt_strname}.png")
        
    # chi2
    R.gStyle.SetPalette(R.kTemperatureMap)
    canv = R.TCanvas(f"canv_{txt_strname}_6", f"canv_{txt_strname}_6", 750*4, 600*3)
    canv.Divide(4,3)
    R.gStyle.SetOptStat(0)
    drawn=1
    for det in detectors:
        for lay in detlay[det]:
            canv.cd(drawn)
            R.gPad.SetLeftMargin(0.03)
            R.gPad.SetRightMargin(0.18)

            plot_chi2[det][lay].Draw("COLZ0")
            drawn+=1
    drawn=1
    canv.Print(f"{outDir}/coverage_chi2_{txt_strname}.png")


    
    #### SCA plot
    R.gStyle.SetPalette(R.kTemperatureMap)
    lines0 = {}
    lines_LTDB = {}
    latex = {}
    #latex_LTDB = []
    # SCA canv
    canv = R.TCanvas(f"canv_{txt_strname}_SCA", f"canv_{txt_strname}_SCA", 2000, 600*4)
    canv.objs = []
    canv.Divide(1,4)
    for det in detectors:
        ltdb_list = []
        for b in range(0, h_phasediff_ref_E_SCA[det].GetNbinsX()):
            xval = int(h_phasediff_ref_E_SCA[det].GetXaxis().GetBinLowEdge(b+1))
            label = h_phasediff_ref_E_SCA[det].GetXaxis().GetBinLabel(b+1)
            try:
                LTDB = label[:-1]
            except:
                print("ISSUE", label)
                continue
            SCA = label[-1]
            h_phasediff_ref_E_SCA[det].GetXaxis().SetBinLabel(b+1, SCA)
            if len(ltdb_list) == 0:
                ltdb_list.append(LTDB)
                lines_LTDB[det] = []
            elif LTDB not in ltdb_list:
                lines_LTDB[det].append(R.TLine(xval, -20, xval, 20))
                ltdb_list.append(LTDB)
                
        canv.cd(drawn)
        R.gPad.SetRightMargin(.08)
        R.gPad.SetLeftMargin(0.03)
        h_phasediff_ref_E_SCA[det].Draw("COLZ")
        lines0[det] = R.TLine(0, 0, h_phasediff_ref_E_SCA[det].GetXaxis().GetXmax(), 0)
        lines0[det].SetLineStyle(3)
        lines0[det].Draw("samel")
        dl = []
        latex[det] = R.TLatex()
        #latex[det].SetNDC()
        latex[det].SetTextFont(42)
        latex[det].SetTextColor(R.kBlack)
        latex[det].SetTextSize(0.022)
        latex[det].SetTextAlign(12)
        texty = -19
        for line in lines_LTDB[det]:
            linex = line.GetX1()
            if len(dl) == 0:
                latex[det].DrawLatex(0,
                                     texty,
                                     ltdb_list[lines_LTDB[det].index(line)])
                
            else:
                latex[det].DrawLatex(dl[-1], #(linex-dl[-1])/2,
                                     texty,
                                     ltdb_list[lines_LTDB[det].index(line)])
            canv.Modified()
            canv.Update()
            canv.objs.append(latex[det])
            dl.append(linex)
            line.SetLineStyle(3)
            line.Draw("samel")
            canv.objs.append(line)
        latex[det].DrawLatex(dl[-1],
                             texty,
                             ltdb_list[-1])

        canv.objs.append(lines0[det])
        canv.objs.append(h_phasediff_ref_E_SCA[det])
        #h_phasediff_ref_E_SCA[det].Draw("COLZsame")
        drawn+=1
    drawn=1
    canv.Print(f"{outDir}/HISTcoverage_phasediff_maxE_SCA_{txt_strname}_allShifts.png")

    
    ####
    print("Finished", dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S.%f"))
    
if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument('-r', '--refPA', dest='refPA', default="/eos/home-l/lardaq/www/P1Valid/calibration-PHASE1/LATOMERun_PulseAll_479768_240708-095603/output/chi2_hist.root", help="Reference pulse all ntuple. Default %(default)s.")
    parser.add_argument('-n', '--newPA', dest='newPA', default="/eos/home-l/lardaq/www/P1Valid/calibration-PHASE1/LATOMERun_PulseAll_483988_240903-092825/output/chi2_hist.root", help="Test/New pulse all ntuple. Default %(default)s.")
    parser.add_argument('--newRun', dest='newRunNum', default=None, help="New run number. If not provided, will try to extract it from the input path)")
    parser.add_argument('--refRun', dest='refRunNum', default=None, help="Ref run number. If not provided, will try to extract it from the input path)")
    parser.add_argument('-p', '--ped', dest='weekly_ped', type=int, default=479769, help="Pedestal number from weekly set to be used (for OFCs). Default %(default)s.")
    parser.add_argument('-c', '--currentPhases', dest='currentPhases', default=None, help="Full path to .txt file containing list of current phases per SCID. Default %(default)s.")
    parser.add_argument('-o', '--outDir', dest='outDir', default=f"{os.getcwd()}/output_phasePick", help="Output directory for log files and plots. Default %(default)s.")

    parser.add_argument('--ofcPhys', dest='useOFCPhys', action='store_true', help="Use physics OFCs with mu == 0 instead of cali?")
    parser.add_argument('--ofcPhysMu', dest='useOFCPhysMu', action='store_true', help="Use physics OFCs with mu != 0 instead of cali?")
    parser.add_argument('--etauphasediff', dest='plotcut_etaudiff', default=5, help="Difference between optimal phase for max e and min tau required for reporting. Default %(default)s.")
    parser.add_argument('--maxPhaseDiff', dest='max_phasediff', default=10, help="Maximum cut off on phase difference. If the phase has changed from the reference one by this much or more, the old one will be used. Default %(default)s.")
    parser.add_argument('--minPhaseDiff', dest='min_phasediff', default=1, help="Minimum cut off on phase difference. If the phase has changed from the reference one by this much or less, the old one will be used. Default %(default)s.")
    
    #currentPhases="/eos/user/e/echapon/work/shared/ATLAS/LAr/DT/bestphase_max_et_479729_DTWeekly_July08_479769-479798.txt"

    args = parser.parse_args()

    
    if not os.path.isdir(args.outDir):
        print(f"Making requested output directory {args.outDir}")
        os.makedirs(args.outDir)

    main(args.weekly_ped, args.newPA, args.refPA, args.currentPhases, args.outDir, args.useOFCPhys, args.useOFCPhysMu, args.plotcut_etaudiff, args.max_phasediff, args.min_phasediff, args.newRunNum, args.refRunNum)



