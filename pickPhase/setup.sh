#!/bin/sh
export latest_LCG=$(ls -1 -p -t -d  /cvmfs/sft.cern.ch/lcg/views/*ATLAS*/$(uname -p)*el9* | head -1)
echo $latest_LCG
echo $(uname -p)
test=$(ls $latest_LCG/setup.sh)
echo "**" $test "**"
source $latest_LCG/setup.sh

#libs=( awkward-pandas numba )
#for lib in ${libs[@]}; do
#    test=$(pip3 list --disable-pip-version-check --format=columns | grep -F ${lib})
#    if [ -z "${test}" ]; then
#	echo "pip3 install ${lib} --user"
#	pip3 install ${lib} --user > /dev/null 2>&1
#    fi
#done

