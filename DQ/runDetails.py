#!/bin/python3
import sys
import xmlrpc.client
dqmpassfile = "/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt"
dqmpass = None


def getRuns(runNo=None):
    with open(dqmpassfile, "r") as f:
        dqmpass = f.readlines()[0].strip()
    if ":" not in dqmpass:
        print("Problem reading dqmpass")
        sys.exit()
    s = xmlrpc.client.ServerProxy("https://"+dqmpass+"@atlasdqm.cern.ch")

    if runNo is not None:
        high_run = runNo
        low_run = runNo
    else:
        high_run = s.get_latest_run()
        low_run = high_run - 1000

    run_spec = {'stream': 'physics_CosmicCalo', 'source': 'tier0', 'low_run': low_run, 'high_run': high_run}

    defects = s.get_defects_lb(run_spec)
    CLdeadline = s.get_end_of_calibration_period(run_spec)
    run_info = s.get_run_information(run_spec)
    run_streams = s.get_runs_streams(run_spec)
    run_ami = s.get_procpass_amitag_mapping(run_spec)
    beam_info = s.get_run_beamluminfo(run_spec)

    infokeys = ["Run type", "Project tag", "Partition name", "Number of events passing Event Filter", "Run start", "Run end", "Number of luminosity blocks", "Data source", "Detector mask", "Recording enabled", "Number of events in physics streams" ]
    beamkeys = ["Max beam energy during run", "Stable beam flag enabled during run", "ATLAS ready flag enabled during run", "Total integrated luminosity", "ATLAS ready luminosity (/nb)"]
    for ri in run_info.keys():
        run_info[ri] = { ik:li for ik,li in zip(infokeys,run_info[ri]) }
    for bi in beam_info.keys():
        beam_info[bi] = { ik:li for ik,li in zip(beamkeys,beam_info[bi]) }


    print(defects, CLdeadline)

class Run():
    run_spec = {}
    def __init__(self, runno):
        self.run_spec['low_run']= int(runno)
        self.run_spec['high_run']=int(runno) 

    def __str__(self):
        return str(self.run_spec['low_run'])

if __name__=="__main__":
    getRuns()
