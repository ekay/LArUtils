from argparse import ArgumentParser
from datetime import datetime
from CoolLumiUtilities.CoolDataReader import CoolDataReader
import sys, os, errno
import json
sys.path.append("/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-11-02-01/installed/share/bin/")
import extractBunchGroupSet

from plotPrescales import getLBDict

def getRunStartEnd(runnum):
    rundata = CoolDataReader('COOLONL_TDAQ/CONDBR2', '/TDAQ/RunCtrl/EOR')
    rundata.setIOVRangeFromRun(runnum)
    #rundata.setIOVRangeFromRun(runnum)
    rundata.readData()
    for i, data in enumerate(rundata.data):
        if "SORTime" in data.payload().keys() and "EORTime" in data.payload().keys():
            stime = data.payload()["SORTime"]
            etime = data.payload()["EORTime"]
            nlb = data.until() & 0xFFFFFFFF
            return stime, etime, nlb
    print("Problem reading from tdaq db for run info")
    sys.exit()
            

''' Not needed right now - this gives the filling scheme from the LHC per time stamp     
def BunchInfo(runnum):
    #LBdict, maxLB, minLB = getLBDict(runnum)
    runstart, runend, nLB = getRunStartEnd(runnum)
    print(datetime.utcfromtimestamp(runstart/1000000000).strftime("%Y-%m-%d:%H:%M:%S"), datetime.utcfromtimestamp(runend/1000000000).strftime("%Y-%m-%d:%H:%M:%S"))

    bunchdata = CoolDataReader('COOLONL_TDAQ/CONDBR2', '/TDAQ/OLC/LHC/FILLPARAMS')
    bunchdata.setIOVRange(runstart, runend)
    #bunchdata.setIOVRangeFromRun(runnum)
    bunchdata.readData()
    print(dir(bunchdata))
    print("OIOI", bunchdata.data)
    for i, data in enumerate(bunchdata.data):
        tlo = data.since()
        thi = data.until()
        print(i, tlo, thi)
        for k in data.payload().keys():
            print(i, k)
            pl = data.payload()[k]
            print(pl)
            continue
'''

def getInfo(runnum, outdir="."):
    bgInfo = {}
    runstart, runend, nLB = getRunStartEnd(runnum)

    for folder in [ '/TRIGGER/LVL1/BunchGroupKey' ]: #, '/TRIGGER/LVL1/BunchGroupContent', '/TRIGGER/LVL1/BunchGroupDescription' ]:
        bgkeys = CoolDataReader('COOLONL_TRIGGER/CONDBR2', folder)
        bgkeys.setIOVRangeFromRun(runnum)
        bgkeys.readData()
        #LBdict = {}
        print("**", folder)
        for i, data in enumerate(bgkeys.data):
            tlo = data.since()
            thi = data.until()
            r1=tlo>>32
            lb1=tlo & 0xFFFFFFFF
            r2=thi>>32
            lb2=thi & 0xFFFFFFFF
            if r2 == r1:
                lbs = list(range(lb1, lb2))
            else:
                lbs = list(range(lb1, nLB+1))

            vers = f"LBs_{lbs[0]}-{lbs[-1]}"
            print(vers)
            for k in data.payload().keys():
                print("**", i, folder, "**", k)
                pl = data.payload()[k]
                dbalias="TRIGGERDB_RUN3"
                if k == "Lvl1BunchGroupConfigurationKey":
                    bgInfo[vers] = {}
                    with extractBunchGroupSet.BunchGroupSetExtractor(dbalias) as extractor:
                        # Extract bunch group set
                        bgs = json.loads(extractor.extractBunchGroupSet(pl))

                        print(bgs.keys())
                        
                        for bgk in bgs['bunchGroups'].keys():
                            this_info = bgs['bunchGroups'][bgk]
                            # {'bcids': [{'first': 0, 'length': 3540}, {'first': 3561, 'length': 3}], 'id': 0, 'name': 'BCRVETO', 'info': '3543 bunches, 2 groups'}
                            for bcid in this_info['bcids']:
                                therange = list(range(bcid['first'], bcid['first']+bcid['length']))
                                for this_id in therange:
                                    if this_id not in bgInfo[vers].keys():
                                        bgInfo[vers][this_id] = { 'key': [bgk], 'name': [this_info['name']] }
                                    else:
                                        if bgk not in bgInfo[vers][this_id]['key']:
                                            bgInfo[vers][this_id]['key'].append(bgk)
                                        else:
                                            print("OIOI", bgk, bcid, this_id, "**", bgInfo[vers][this_id])
                                            sys.exit()
                                        if this_info['name'] not in bgInfo[vers][this_id]['name']:
                                            bgInfo[vers][this_id]['name'].append(this_info['name'])
                                        else:
                                            print("OIOI", bgk, this_info['name'], bcid, this_id, "**", bgInfo[vers][this_id])
                                            sys.exit()

    for version in bgInfo.keys():
        outfile=f"{outdir}/bcidInfo_{runnum}_{version}.txt"
        print(f"Writing to {outfile}")
        bcids = sorted(list(bgInfo[version].keys()))
        with open(outfile, "w") as f:
            for bcid in bcids:
                f.write(f"{bcid} {','.join([ b.replace('BGRP','') for b in bgInfo[version][bcid]['key']])}    # {', '.join([ b for b in bgInfo[version][bcid]['name']])}\n")




if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(dest='runnum', help='Run number(s) which you would like to make plots for', type=int)
    parser.add_argument('-outDir', '--outDir', dest='outDir', type=str, default='.', help='Output directory for plots. Default %(default)s')

    args = parser.parse_args()

    try:
        os.makedirs(args.outDir)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

    #BunchInfo(args.runnum)

    getInfo(args.runnum, args.outDir)
