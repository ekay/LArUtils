from CoolLumiUtilities.CoolDataReader import CoolDataReader

# runnum=483684
runnum=488534
import sys
sys.path.append("/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-11-02-01/installed/share/bin/")
import extractPrescales
import extractBunchGroupSet
import extractMenu
import json

for folder in [ '/TRIGGER/LVL1/BunchGroupKey', '/TRIGGER/LVL1/BunchGroupContent', '/TRIGGER/LVL1/BunchGroupDescription', '/TRIGGER/HLT/HltConfigKeys' ]:
    bgkeys = CoolDataReader('COOLONL_TRIGGER/CONDBR2', folder)
    bgkeys.setIOVRangeFromRun(runnum)
    bgkeys.readData()
    #LBdict = {}
    print("**", folder)
    for i, data in enumerate(bgkeys.data):
        tlo = data.since()
        thi = data.until()
        for k in data.payload().keys():
            print("**",folder, "**", k)
            test = data.payload()[k]
            print(i, test)
            dbalias="TRIGGERDB_RUN3"
            
            if k == "Lvl1BunchGroupConfigurationKey":
                with extractBunchGroupSet.BunchGroupSetExtractor(dbalias) as extractor:
                    # Extract bunch group set
                    bgs = extractor.extractBunchGroupSet(test)
                    print(json.loads(bgs))

            if k == "MasterConfigurationKey":
                with extractMenu.MenuExtractor(dbalias) as extractor:
                    # Extract menus
                    l1menu, hltmenu, hltjo, mongroups = extractor.extractMenus(test)
                    l1menu = json.loads(l1menu)
                    print(l1menu['items'])
                    print("*"*10)
                    hltmenu = json.loads(hltmenu)
                    chains = hltmenu['chains']
                    print(chains)
                    # print(hltmenu.keys())
