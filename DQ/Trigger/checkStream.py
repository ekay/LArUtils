import sys
import json
from argparse import ArgumentParser
sys.path.append("/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-11-02-01/installed/share/bin/")
import extractPrescales
from plotPrescales import getMenu, extractMenuInfo
from getBunchInfo import getRunStartEnd
from CoolLumiUtilities.CoolDataReader import CoolDataReader


def cleanLBIntervals(ilist):
    ''' Remove overlapping LB intervals '''
    ilist = sorted(ilist, key=lambda nested: nested[1])

    def getOverlap(a, b):
        return max(0, min(a[1], b[1]) - max(a[0], b[0]))

    newlist = []
    def addToList(newvals):
        if newvals not in newlist: 
            if newvals[0] in [ max(x) for x in newlist ]:
                newvals[0] = newvals[0]+1
            if newvals not in newlist: 
                newlist.append(newvals)

    for i in range(0, len(ilist)):
        interval = ilist[i]
        has_ol = 0
        if interval[0] > interval[1]: continue
        for j in range(1, len(ilist)):
            int2 = ilist[j]
            ol = getOverlap(interval,int2)
            if interval == int2: continue
            if ol != 0:
                has_ol += 1
                newint = [min(interval[0], int2[0]), min(interval[1], int2[1])]
                addToList(newint)
                if i != len(ilist)-1:
                    nextmin = ilist[i+1][0]
                    newnextmin = min(interval[1], int2[1])+1
                    # print(i,interval, int2, ol, newint, nextmin, newnextmin)                
                    if nextmin > newnextmin:
                        addToList([nextmin, nextmin+1])
                    else:
                        ilist[j][0] = newnextmin

                #print("=",newlist)
        if has_ol == 0 and interval not in newlist:
            addToList(interval)

    return newlist


def getPSInfo(runnum, l1menu, hltmenu, stream):

    runstart, runend, nLB = getRunStartEnd(runnum)

    dbalias="TRIGGERDB_RUN3"
    folders=['/TRIGGER/LVL1/Lvl1ConfigKey','/TRIGGER/HLT/PrescaleKey']

    trig_info = {}
    
    for folder in folders:
        DBps = CoolDataReader('COOLONL_TRIGGER/CONDBR2', folder)
        DBps.setIOVRangeFromRun(runnum)
        DBps.readData()
        
        for data in DBps.data:
            tlo = data.since()
            thi = data.until()
            r1=tlo>>32
            lb1=tlo & 0xFFFFFFFF
            r2=thi>>32
            lb2=thi & 0xFFFFFFFF
            if r2 == r1:
                lbs = list(range(lb1, lb2))
            else:
                lbs = list(range(lb1, nLB+1))
            vers = f"LBs_{lbs[0]}-{lbs[-1]}"
            
            if vers not in trig_info.keys():
                trig_info[vers] = {}
                
            print(f"******** {folder} ********")
            print(f"** Run {r1} LB {lb1} to run {r2} LB {lb2} ** {vers} **")
            if folder == '/TRIGGER/HLT/PrescaleKey':
                itemkey = 'HltPrescaleKey'
            elif folder  == '/TRIGGER/LVL1/Lvl1ConfigKey':
                itemkey = 'Lvl1PrescaleConfigurationKey'
                
            key = data.payload()[itemkey]

            if folder == '/TRIGGER/HLT/PrescaleKey':
                l1psk = 1
                hltpsk = key
            else:
                l1psk = key
                hltpsk = 1
                
            with extractPrescales.PrescaleExtractor(dbalias) as extractor:
                l1ps, hltps = extractor.extractPrescales(l1psk, hltpsk)
                if folder == '/TRIGGER/HLT/PrescaleKey':
                    pstocheck = hltps
                    keytocheck = 'prescales'
                else:
                    pstocheck = l1ps
                    keytocheck = 'cutValues'

                pstocheck = json.loads(pstocheck)
                prescales = pstocheck[keytocheck]


                count=0
                for trig in prescales.keys():
                    menuInfo =  extractMenuInfo(trig, hltmenu, l1menu)
                    if stream not in menuInfo['streams']:
                        continue
                    
                    filldict = {**menuInfo, **prescales[trig]}
                    print(trig, filldict)
                    if folder == '/TRIGGER/HLT/PrescaleKey':
                        #{'l1item': 'L1_RD0_FIRSTEMPTY', 'streams': ['CosmicCalo']}
                        #{'hash': 3527573027, 'prescale': -1, 'enabled': False}
                        if filldict["enabled"] is False: continue
                        if filldict["l1item"] not in trig_info[vers].keys():
                            trig_info[vers][filldict["l1item"]] = {}
                        trig_info[vers][filldict["l1item"]][trig] = filldict

                    else:
                        #{'hlt_chains': ['HLT_larpsall_L1jJ60p30ETA49_EMPTY', 'HLT_noalg_L1jJ60p30ETA49_EMPTY', 'HLT_noalg_LArPEBNoise_L1jJ60p30ETA49_EMPTY'], 'streams': ['LArCellsEmpty', 'CosmicCalo'], 'bunchgroups': ['BGRP0', 'BGRP3']}
                        #{'cut': 1, 'enabled': True, 'info': 'prescale: 1.0'}
                        if filldict["enabled"] is False: continue
                        if trig not in trig_info[vers].keys():
                            trig_info[vers][trig] = {}
                        trig_info[vers][trig].update(filldict)

    int_keys_before = sorted([ [ int(k.split("LBs_")[1].split("-")[0]), int(k.split("-")[1]) ] for k in trig_info.keys() ])
    int_keys = cleanLBIntervals(int_keys_before)
    int_keys_before = sorted([ [ int(k.split("LBs_")[1].split("-")[0]), int(k.split("-")[1]) ] for k in trig_info.keys() ])

    outfile = f"trig_info_{stream}_{runnum}.txt"
    with open(outfile, "w") as f:
        for ik in int_keys:
            versions = [ f"LBs_{i[0]}-{i[1]}" for i in int_keys_before if ik[0] <= i[1] and ik[1] >= i[0] ]
            l1items = [ [ k for k in trig_info[vers].keys()] for vers in versions ]
            l1items = list(set([item for sublist in l1items for item in sublist]))
                
            if len(l1items) == 0:
                continue
            f.write(f"** LBs {ik[0]} - {ik[1]}, {len(l1items)} L1 Items **\n")

            for item in l1items:
                f.write(f"-- {item}\n")
                for vers in versions:
                    if item in trig_info[vers].keys():
                        for key in trig_info[vers][item].keys():
                            if isinstance(trig_info[vers][item][key], list):
                                f.write(f"  {key}   :   {', '.join([str(s) for s in trig_info[vers][item][key]])}\n")
                            else:
                                f.write(f"  {key}   :   {trig_info[vers][item][key]}\n")
            f.write("\n")

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(dest='runnum', help='Run number(s) which you would like to make plots for', type=int)
    parser.add_argument('-s', '--stream', default="CosmicCalo", help='Stream to check. Default %(default)s')
    
    args = parser.parse_args()

    l1menu, hltmenu = getMenu(int(args.runnum))
    getPSInfo(args.runnum, l1menu, hltmenu, args.stream)
