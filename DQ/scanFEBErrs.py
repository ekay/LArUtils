"""Makes a plot showing the number of corruption errors for each partition in a run, per LB"""
import sys, os
from DataQualityUtils import pathExtract         
import xmlrpc.client
import ROOT as R 
R.gROOT.SetBatch(True)
import ctypes, cppyy.ll

dqmpassfile="/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt"
def setupDqmAPI():
  """ Connect to the atlasDQM web API service: https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DQWebServiceAPIs """
  if (not os.path.isfile(dqmpassfile)):
    print("To connect to the DQ web service APIs, you need to generate an atlasdqm key and store it in the specified location ("+dqmpassfile+"). The contents should be yourname:key")
    print("To generate a key, go here : https://atlasdqm.cern.ch/dqauth/")
    sys.exit()
  passfile = open(dqmpassfile)
  passwd = passfile.read().strip(); passfile.close()
  passurl = 'https://%s@atlasdqm.cern.ch'%passwd
  s = xmlrpc.client.ServerProxy(passurl)
  return s

def findDataPaths(runNumber, stream):
    dqmAPI = setupDqmAPI()
    run_spec = {'stream': 'physics_CosmicCalo', 'proc_ver': 1,'source': 'tier0', 'run_list': [runNumber]}
    run_info= dqmAPI.get_run_information(run_spec)
    if '%d'%runNumber not in list(run_info.keys()) or len(run_info['%d'%runNumber])<2:
      print("Unable to retrieve the data project tag via atlasdqm... Please double check your atlasdqmpass.txt or define it by hand with -t option")
      sys.exit()
    run_ami = dqmAPI.get_procpass_amitag_mapping(run_spec)
    run_info = run_info[str(runNumber)]
    run_ami = run_ami[str(runNumber)]
    run_ami = [r for r in run_ami if stream in r ]
    procver = max([r[0] for r in run_ami])
    run_ami = [r for r in run_ami if r[0] == procver][0]
    
    tag = run_info[1]
    amitag = run_ami[2]

    amitag = amitag.split("_")[0]
    
    stream = stream.replace("physics_","")

    mergedFilePath = pathExtract.returnEosHistPath( runNumber, stream, amitag, tag )
    runFilePath = "root://eosatlas.cern.ch/%s"%(mergedFilePath).rstrip()
    if ("FILE NOT FOUND" in runFilePath):
        print("No merged file found...")
        print(stream, amitag, tag)
        sys.exit()
    print("I have found the merged HIST file %s"%(runFilePath))

    lowerlb=0
    upperlb=999999
    lbFilePathList = pathExtract.returnEosHistPathLB( runNumber,
                                                      lowerlb, upperlb,
                                                      stream, amitag, tag )
    if isinstance(lbFilePathList, str) and "FILE NOT FOUND" in lbFilePathList:
      print("No per-LB files found...")
      print(stream, amitag, tag)
      sys.exit()
    print("I have found %d unmerged HIST files"%(len(lbFilePathList)))
    if len(lbFilePathList) == 0 :
      print("Perhaps the per-LB files have been deleted")
    print("The first one is",lbFilePathList[0])
    print("The last one is",lbFilePathList[-1])
    lbFilePathList = [ "root://eosatlas.cern.ch/"+f.rstrip() for f in lbFilePathList ]

    
    return mergedFilePath, lbFilePathList

def getGrs(hist, graphs, peaks, count, ilb):
  cols = [ R.kRed, R.kBlue, R.kGreen, R.kCyan, R.kOrange+8, 
           R.kViolet, R.kRed+2, R.kBlue+2, R.kGreen+2, R.kPink+8,
           R.kCyan+2, R.kOrange, R.kViolet+6 ]
  nb = hist.GetSize()
  for b in range(1, nb):
    x, y, z = ctypes.c_int(1), ctypes.c_int(1), ctypes.c_int(1)
    hist.GetBinXYZ(b, x, y, z)
    xcent = hist.GetXaxis().GetBinCenter(x.value)
    xlab = hist.GetXaxis().GetBinLabel(x.value)
    ylab = hist.GetYaxis().GetBinLabel(y.value)
    ycent = hist.GetYaxis().GetBinCenter(y.value)
    #print(b, xcent, ycent, xlab, ylab)

    if "labels" not in graphs.keys():
      graphs["labels"] = [str(l) for l in hist.GetXaxis().GetLabels()]
      graphs["parts"] = [str(l) for l in hist.GetYaxis().GetLabels()]
      for part in graphs["parts"]:
        graphs[part] = {}
        peaks[part] = {}
        for label in graphs["labels"]:
          peaks[part][label] = []
          graphs[part][label] = R.TGraph()
          graphs[part][label].SetName(part+"_"+label)
          graphs[part][label].SetTitle(label)
          graphs[part][label].SetLineColor(cols[graphs["parts"].index(part)])
          graphs[part][label].SetMarkerColor(cols[graphs["parts"].index(part)])
          graphs[part][label].SetMarkerStyle(20)
          graphs[part][label].SetMarkerSize(.5)

    for part in graphs["parts"]:
      for label in graphs["labels"]:
        x = graphs["labels"].index(label)+1
        y = graphs["parts"].index(part)+1
        graphs[part][label].SetPoint(count+1, ilb, hist.GetBinContent(x,y))
        xlab = hist.GetXaxis().GetBinLabel(x)
        ylab = hist.GetYaxis().GetBinLabel(y)
        if hist.GetBinContent(x,y) > 0:
          peaks[part][label].append([ilb,hist.GetBinContent(x,y)])
          #print("--",ilb,part,xlab,ylab,label,part,hist.GetBinContent(x,y))
  

def main():
    run = int(sys.argv[1])
    if len(sys.argv) > 2:
      stream = sys.argv[2]
    else:
      stream = "physics_CosmicCalo"

    print("Processing run",run,"in stream",stream)

    mergedFilePath, lbFilePathList = findDataPaths(run, stream)

    listLB = []
    parts = []
    graphs_feb = {}
    peaks_feb = {}
    graphs_dig = {}
    peaks_dig = {}

    for count, lbFile in enumerate(lbFilePathList):
      ilb = int((lbFile.split("_lb")[1]).split("._")[0])
      if ilb not in listLB:
        listLB.append(ilb)
      if (count%100 == 0):
        print("I processed %d/%d files"%(count,len(lbFilePathList)))
      fLB = R.TFile.Open(lbFile)

      histpath = "LAr/FEBMon/Summary/NbOfLArFEBMonErrors_dE"
      hist = fLB.Get("run_"+str(run)+"/"+histpath)     
      if not isinstance(hist, R.TH1):
        #print(f"Could not find {histpath} in the file for LB", ilb)
        continue
      getGrs(hist, graphs_feb, peaks_feb, count, ilb)

      #histpath = "LAr/Digits/Summary/Summary"
      #hist = fLB.Get("run_"+str(run)+"/"+histpath)     
      #if not isinstance(hist, R.TH1):
      #  print(f"Could not find run_{run}/{histpath} in the file for LB", ilb)
      #  sys.exit()
      #  continue
      #getGrs(hist, graphs_dig, peaks_dig, count, ilb)
      
      fLB.Close()

    def doPlot(graphs, peaks, tag):
        labels = graphs["labels"]
        parts = graphs["parts"]
        plotnames = []
        for label in labels:
          gmax = [ R.TMath.MaxElement(graphs[p][label].GetN(),graphs[p][label].GetY()) for p in parts ]
          ymax = max([ g*1.1 for g in gmax ])

          if max(gmax) == 0:
            continue
          canv = R.TCanvas()
          canv.objs = []
          canv.SetRightMargin(.15)
          texes = {}
          leg = R.TLegend(0.855, 0.5, 0.98, 0.85 )
          leg.SetBorderSize(0)
          leg.SetFillStyle(0)
          for part in parts:
            print("**",part)
            graphs[part][label].SetMaximum(ymax)
            leg.AddEntry(graphs[part][label], part)
            if parts.index(part) == 0:
              graphs[part][label].Draw("Apl")
            else:
              graphs[part][label].Draw("same,pl")
            graphs[part][label].SetMaximum(ymax)
            graphs[part][label].GetXaxis().SetTitle("LB")
            graphs[part][label].GetYaxis().SetTitle("# Errors")
            canv.Update()
            for peak in peaks[part][label]:
              p = peaks[part][label].index(peak)
              texes[p] = R.TLatex(peak[0], peak[1], str(peak[0]))
              texes[p].SetTextFont(13)
              texes[p].SetTextSize(16)
              texes[p].SetTextAlign(12)
              texes[p].SetTextAngle(90)
              texes[p].SetTextColor(graphs[part][label].GetLineColor())
              texes[p].Draw("same")
              canv.objs.append(texes[p])
          leg.Draw("same")
          label = label.replace("/","or")
          label = label.replace("#","no")
          plotname = tag+"_"+label.replace(" ","_")+"_"+str(run)+"_"+stream+".png"
          plotnames.append(plotname)
          canv.Print(plotname)

        # make a montage
        cmd = "montage -mode concatenate "+(" ").join(plotnames)+f" montage_{tag}_"+str(run)+"_"+stream+".png"
        print(cmd)
        os.system(cmd)

    doPlot(graphs_feb, peaks_feb, "FebErr")
    #doPlot(graphs_dig, peaks_dig, "DigErr")

if __name__ == "__main__":
    main()
