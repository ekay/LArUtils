import ROOT as R
import glob, sys
R.gROOT.SetBatch(True)

#stream = "physics_CosmicCalo"
stream = "physics_Main"
#stream = "express_express"
camp = "data24_13p6TeV"
express=False
outstr = ""
if express:
    ami="x"
    outstr = "express"
else:
    ami="f"
    outstr = "bulk"

rootpath = f"/eos/atlas/atlastier0/rucio/{camp}/{stream}/*/{camp}.*.{stream}.merge.HIST.{ami}*/{camp}.*.{stream}.merge.HIST.{ami}*.1"
print("Wildcard:",rootpath)
rootfiles = glob.glob(rootpath)
runs = sorted(list([int(r.split("/")[7]) for r in rootfiles]))
runs = sorted(list(set(runs)))
print(len(runs), "runs:", runs)
infiles = {}
counts = {}
for run in runs:
    counts[run] = {}
    rfile = [r for r in rootfiles if "/00"+str(run)+"/" in r]
    if len(rfile)>1:
        #print(len(rfile), run, ":", rfile)
        amis = sorted(list([ r.split(".")[-3] for r in rfile]))
        newest = [ r for r in rfile if "."+amis[-1]+"." in r]
        infiles[run] = newest[0]
    else:
        infiles[run] = rfile[0]

    infile = R.TFile(infiles[run], "READ")
    hpaths = []
    hpaths.append(f"run_{run}/LAr/FEBMon/Summary/NbOfLArFEBMonErrors_dE")
    hpaths.append(f"run_{run}/LAr/Digits/Summary/Summary")
    #print("**", infiles[run])
    for hp in hpaths:
        #print("*", hp)
        hist = infile.Get(hp)
        yax = hist.GetYaxis()
        xax = hist.GetXaxis()
        #print([ l.GetName() for l in xax.GetLabels() ])
        #print([ l.GetName() for l in yax.GetLabels() ])
        for xb in range(1, hist.GetNbinsX()+1):
            for yb in range(1, hist.GetNbinsY()+1):
                thebin = hist.GetBin(xb,yb)
                binval = hist.GetBinContent(thebin)
                xlabel = xax.GetLabels().At(xb-1).GetName()
                ylabel = yax.GetLabels().At(yb-1).GetName()
                #print(run, xlabel, ylabel, xb, yb, binval)
                if ylabel not in counts[run].keys():
                    counts[run][ylabel] = {}
                if xlabel not in counts[run][ylabel].keys():
                    counts[run][ylabel][xlabel] = binval
                else:
                    print("oops")
                    sys.exit()
    infile.Close()

parts = list(counts[runs[0]].keys())
print(parts)

cols = [ R.kBlue, #R.kBlue-4, #R.kBlue+4, R.kBlue+8,
         R.kAzure+8, 
         R.kViolet+6, R.kViolet-6, #R.kViolet-2,
         #R.kMagenta, R.kMagenta-5, #R.kMagenta,
         R.kPink+9, R.kPink+1, #R.kPink-3,
         #R.kRed-3, R.kRed,
         R.kOrange+5, R.kOrange+10, #R.kOrange,
         #R.kYellow, R.kYellow-3,
         R.kSpring+3, R.kSpring-7,
         R.kGreen-7, R.kGreen+1 ]


corrtypes = list(counts[runs[0]][parts[0]].keys())
torm = []
for ct in corrtypes:
    vals = list(set([ counts[r][p][ct] for r in runs for p in parts ]))
    if len(vals) == 1 and vals[0] == 0:
        print('**** REMOVING', ct, "****")
        torm.append(ct)
for tr in torm:
    corrtypes.remove(tr)
    

for part in parts:
    canv = R.TCanvas("canv")
    canv.SetRightMargin(.25)
    canv.SetBottomMargin(.15)
    canv.SetTopMargin(.05)
    canv.SetGridy(1)

    leg = R.TLegend(0.76,0.3,0.98,0.9) 
    leg.SetBorderSize(0)
    leg.SetFillStyle(0)
    leg.SetTextSize(0.037)
    R.gStyle.SetOptStat(0)
    
    #print(part, "...", corrtypes)
    hists = {}

    hstack = R.THStack(f"stack_{part}", part)
    for ct in corrtypes:
        vals = [ counts[r][part][ct] for r in runs ]
        hists[ct] = R.TH1I(f"{part}_{corrtypes.index(ct)}_{stream}_{outstr}", f"{part} {ct} {stream} {outstr}", len(runs), 0, len(runs))

        hists[ct].SetLineColor(cols[corrtypes.index(ct)])
        hists[ct].SetFillColor(cols[corrtypes.index(ct)])
        
        leg.AddEntry(hists[ct], ct, "f")
        hstack.Add(hists[ct])
        
        for run in runs:
            hists[ct].SetBinContent(runs.index(run)+1, counts[run][part][ct])
            hists[ct].GetXaxis().SetBinLabel(runs.index(run)+1, str(run))

        hists[ct].LabelsOption("v")
        hists[ct].GetXaxis().LabelsOption("v")

    R.gPad.SetLogy()
    canv.objs = []
    hstack.Draw()
    canv.objs.append(hstack)
    leg.Draw()
    canv.Print(f"corrHists_{part}_{stream}_{outstr}.png")


    canv2 = R.TCanvas("canv2")
    canv2.SetRightMargin(.15)
    canv2.SetBottomMargin(.1)
    canv2.SetTopMargin(.05)
    canv2.SetGridy(1)

    detnoside = part[:-1]
    febCounts = {}
    binFTSL = {}
    for run in runs:
        hpaths = []
        hpaths.append(f"run_{run}/LAr/FEBMon/{detnoside}/RAW_Parity_{part}")
        hpaths.append(f"run_{run}/LAr/FEBMon/{detnoside}/RAW_RADD_{part}") # sample header mismatch
        hpaths.append(f"run_{run}/LAr/FEBMon/{detnoside}/RAW_EVTID_{part}")
        hpaths.append(f"run_{run}/LAr/FEBMon/{detnoside}/RAW_SCACStatus_{part}")
        hpaths.append(f"run_{run}/LAr/FEBMon/{detnoside}/RAW_gainMismatch_{part}")
        hpaths.append(f"run_{run}/LAr/FEBMon/{detnoside}/RAW_zeroSamp_{part}") # empty FEB data block
        hpaths.append(f"run_{run}/LAr/FEBMon/{detnoside}/RAW_checkSum_{part}")
        hpaths.append(f"run_{run}/LAr/FEBMon/{detnoside}/RAW_missingHeader_{part}")
        hpaths.append(f"run_{run}/LAr/FEBMon/{detnoside}/RAW_badGain_{part}")
        hpaths.append(f"run_{run}/LAr/Digits/{detnoside}/RAW_OutOfRange_{part}")
        hpaths.append(f"run_{run}/LAr/Digits/{detnoside}/RAW_Saturation_{part}")
        hnames = [ h.split("/")[-1].split("_")[-2] for h in hpaths ]
        infile = R.TFile(infiles[run], "READ")
        for hp in hpaths:
            hname = hnames[hpaths.index(hp)]
            if hname not in febCounts.keys(): febCounts[hname] = {}
            febCounts[hname][run] = {}
            hist = infile.Get(hp)
            if "TObject" in str(type(hist)):
                #print("NO", run, hname)
                continue
            xax = hist.GetXaxis()
            yax = hist.GetYaxis()
            xbins = []
            for xb in range(1, hist.GetNbinsX()+1):
                for yb in range(1, hist.GetNbinsY()+1):
                    thebin = hist.GetBin(xb,yb)
                    binval = hist.GetBinContent(thebin)
                    FT = yb-1
                    SL = xb
                    xbin = (15*(yb-1))+(xb)
                    binFTSL[xbin] = {"FT": FT, "SL": SL}
                    #if FT == 0 and SL == 1:
                    #    print(run, hname, "FT",FT, "SL",SL, "b", xbin)
                    xbins.append(xbin)
                    febCounts[hname][run][xbin] = binval
                    #["FT"] = yb
                    #febCounts[hname][run]["Slot"] = xb+1
                    #febCounts[hname][run]["val"] = binval
        infile.Close()
    febHists = {}

    xbins = sorted(xbins)
    R.gStyle.SetPalette(R.kTemperatureMap)
    R.gPad.SetLogy(0)
    hnames = febCounts.keys()
    for hn in hnames:
        febHists[hn] = R.TH2I(f"{part}_{hn}_{stream}_{outstr}", f"{part} {hn} {stream} {outstr}",
                              len(xbins), 0, len(xbins),
                              len(runs), 0, len(runs) )
        febHists[hn].GetXaxis().SetTitle("(15*FT)+SL")
        febHists[hn].GetYaxis().SetTitle("Run #")
        #print(hn, run, febCounts[hn].keys())
        themax = {}
        for run in runs:
            themax[run] = [0, "", -1]
            bins = list(febCounts[hn][run].keys())
            febHists[hn].GetYaxis().SetBinLabel(runs.index(run)+1, str(run))
            for b in bins:
                #print(hn, run, xbins.index(b)+1, runs.index(run)+1, febCounts[hn][run][b])
                febHists[hn].SetBinContent( xbins.index(b)+1, runs.index(run)+1, febCounts[hn][run][b])
                if febCounts[hn][run][b] > themax[run][2]:
                    FTSL = f'FT{binFTSL[b]["FT"]}SL{binFTSL[b]["SL"]}'
                    themax[run] = [ b, FTSL, febCounts[hn][run][b] ]
            if themax[run][2] < 1: del themax[run]
        print("****", part, hn, themax)
        #print(runs, febCounts[hn].keys())
        if febHists[hn].GetEntries() > 0:
            febHists[hn].Draw("COLZ")
            canv2.Print(f"{part}_{hn}_{stream}.png")

print("Run the following to make montages:")
print(f"montage corrHists_*_{stream}.png -tile x4 -geometry 200x200+2+2% montage_datacorr_{stream}.png")

print("for det in EMBA EMBC EMECA EMECC FCalA FCalC HECA HECC; do montage ${det}_*_"+stream+".png -tile x4 -geometry 200x200+2+2% montage_datacorr_${det}_"+stream+".png; done")

print(f"convert montage_datacorr_{stream}.png montage_datacorr_*_{stream}.png dataCorr_{stream}.pdf")
