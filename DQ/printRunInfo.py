import subprocess, sys
import xmlrpc.client

runNo = int(sys.argv[1])
stream = "physics_ZeroBias" #CosmicCalo"
stream = "physics_CosmicCalo"

dqmpassfile = "/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt"
with open(dqmpassfile, "r") as f:       
    cred = f.readline().strip()
    if cred is None:
        print("Problem reading credentials from",dqmpassfile)
dqmapi = xmlrpc.client.ServerProxy('https://'+cred+'@atlasdqm.cern.ch')

infokeys = ["Run type", "Project tag", "Partition name", "Number of events passing Event Filter", "Run start", "Run end", "Number of luminosity blocks", "Data source", "Detector mask", "Recording enabled", "Number of events in physics streams" ]
beamkeys = ["Max beam energy during run", "Stable beam flag enabled during run", "ATLAS ready flag enabled during run", "Total integrated luminosity", "ATLAS ready luminosity (/nb)"]
magkeys = ["Soleniod actual current", "Toroid actual current", "Solenoid set current", "Toroid set current"]

run_spec = {'stream': stream, 'source': 'tier0', 'run_list': [runNo] }
run_info = dqmapi.get_run_information(run_spec) 
run_streams = dqmapi.get_runs_streams(run_spec)
run_ami = dqmapi.get_procpass_amitag_mapping(run_spec)
run_mag = dqmapi.get_run_magfields(run_spec)
beam_info = dqmapi.get_run_beamluminfo(run_spec)
for ri in run_info.keys():
    run_info[ri] = { ik:li for ik,li in zip(infokeys,run_info[ri]) }
for ra in run_ami.keys():
    run_ami[ra] = [str(b)+" (pass "+str(a)+", ["+str(c)+"|http://ami.in2p3.fr:8080/?subapp=tagsShow&userdata="+str(c)+"])" for a,b,c in run_ami[ra]]
for bi in beam_info.keys():
    beam_info[bi] = { ik:li for ik,li in zip(beamkeys,beam_info[bi]) }
for mi in run_mag.keys():
    run_mag[mi] = { ik:li for ik,li in zip(magkeys,run_mag[mi]) }


def printVals(theDict):
    if str(runNo) in theDict.keys():
        theDict = theDict[str(runNo)]
        if isinstance(theDict, dict):
            for key in theDict.keys():
                print(key, ":", theDict[key])
        else:
            for entry in theDict:
                print(entry)
                #print(entry.split(" ")[0], ":", entry.split(" ")[1])
            
printVals(run_info)
printVals(run_streams)
printVals(run_ami)
printVals(beam_info)
printVals(run_mag)
