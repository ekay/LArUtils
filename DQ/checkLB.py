import glob
import ROOT as R

#topdir = "/eos/atlas/atlastier0/tzero/prod/data23_13p6TeV/physics_CosmicCalo/00451022/data23_13p6TeV.00451022.physics_CosmicCalo.recon.HIST.f1344"
#topdir = "/eos/atlas/atlastier0/tzero/prod/data23_13p6TeV/physics_CosmicCalo/00451037/data23_13p6TeV.00451037.physics_CosmicCalo.recon.HIST.x736"
#topdir = "/eos/atlas/atlastier0/tzero/prod/data23_13p6TeV/physics_CosmicCalo/00451022/data23_13p6TeV.00451022.physics_CosmicCalo.recon.HIST.x734/"
#topdir = "/eos/atlas/atlastier0/tzero/prod/data23_13p6TeV/physics_CosmicCalo/00451473/data23_13p6TeV.00451473.physics_CosmicCalo.recon.HIST.x738/"
topdir = "/eos/atlas/atlastier0/tzero/prod/data23_13p6TeV/physics_CosmicCalo/00450997/data23_13p6TeV.00450997.physics_CosmicCalo.recon.HIST.x734/"

infiles = glob.glob(topdir+"/*")

lbs = sorted(list(set([ inf.split("_lb")[1].split(".")[0] for inf in infiles ])))
print(lbs[-1])
#runnum = "451022"
#runnum = "451037"
#runnum = "451473"
runnum = "450997"
histpath = "run_"+runnum
hist = "CaloMonitoring/LArCellMon_NoTrigSel/2d_Occupancy/CellOccupancyVsEtaPhi_EMEC2C_5Sigma_CSCveto"

histpath += "/"+hist

for lb in lbs:
    if int(lb) < 2014: continue
    infile = [ inf for inf in infiles if "_lb"+str(lb)+"." in inf ][0]
    f = R.TFile.Open(infile)
    thishist = f.Get(histpath)
    if not isinstance(thishist,R.TH2): 
        print(infile, "no",histpath)
        continue
    print(lb,thishist.GetEntries())
    thishist.SetTitle("LB "+lb +": "+thishist.GetTitle())

    canv = R.TCanvas()
    thishist.Draw("COLZ")
    input("Press Enter to continue...")
    f.Close()
