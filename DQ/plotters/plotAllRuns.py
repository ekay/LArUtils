import glob
import ROOT as R
import sys
R.gROOT.SetBatch(True)
import argparse
R.gStyle.SetOptStat(0)

def makePlots(files, outname, threshold=0):
    parts = [ "ECC", "BAR", "ECA", "EMECC", "EMBAR", "EMECA" ]
    runs = sorted(list(set([ int(d.split("/")[7]) for d in files ])))

    print("Runs are:",runs)
    canv = R.TCanvas(f"canv_{threshold}", f"canv_{threshold}", 400*3, 800)
    R.gStyle.SetPalette(R.kTemperatureMap)
    canv.Divide(3,2)
    canv.objs = []
    hists = {}
    drawn = 0 
    for run in runs:
        infile =  [ f for f in files if f"00{run}" in f ][0]
        f = R.TFile.Open(infile)
        drawnpart = 0

        for part in parts:
            if "EM" in part:
                hist = f"run_{run}/CaloTopoClusters/Cal{part}/EMThresh{threshold}{part.replace('EM','')}Occ"
            else:
                hist = f"run_{run}/CaloTopoClusters/Cal{part}/Thresh{threshold}{part}Occ"
            thishist = f.Get(f"{hist}")

            #print(infile, histpath)
            if not isinstance(thishist,R.TH2): 
                continue    
                
            newname = f"{thishist.GetName}_{run}"
            thishist.SetName(newname)
            print(run, part, thishist.GetMaximum())
            hists[newname] = thishist
            if "ECC" in part:
                if "EM" in part:
                    hists[newname].GetXaxis().SetRangeUser(-3,-1)
                else:
                    hists[newname].GetXaxis().SetRangeUser(-5,-1)
            elif "ECA" in part:
                if "EM" in part:
                    hists[newname].GetXaxis().SetRangeUser(1,3)
                else:
                    hists[newname].GetXaxis().SetRangeUser(1,5)
            else:
                hists[newname].GetXaxis().SetRangeUser(-2,2)
            drawnpart += 1
            thishist.SetTitle(f"Run {run} : {part} {thishist.GetTitle()}")
            canv.cd(parts.index(part)+1)

            R.gPad.SetRightMargin(.18)
            hists[newname].Draw("COLZ")
            
            canv.objs.append(hists[newname])
        if drawnpart == 0: continue
        print(run,runs.index(run), len(runs))
        if drawn == 0:
            canv.Print(f"CaloTopo_Thr{threshold}_{outname}.pdf(","pdf")
        elif runs.index(run) == len(runs)-1:
            canv.Print(f"CaloTopo_Thr{threshold}_{outname}.pdf)","pdf")        
        else:
            canv.Print(f"CaloTopo_Thr{threshold}_{outname}.pdf","pdf")        
        drawn+=1

        f.Close()









if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='Beam-induced background.')
    parser.add_argument('--stream', dest='stream', type=str, default="physics_CosmicCalo", help='Stream to look at. Default %(default)s')
    parser.add_argument('--tag', dest='tag', type=str, default="data24_hi", help='Data tag to look at. Default %(default)s')
    parser.add_argument('-a', '--ami', dest='ami', default="x", type=str, help='AMI letter to use for cluster info - x or f. Default %(default)s')
    parser.add_argument('--threshold', dest='threshold', type=int, help='Threshold for cluster plots. Default %(default)s')
    args = parser.parse_args()

    topdir = f"/eos/atlas/atlastier0/tzero/prod/{args.tag}/{args.stream}/*/{args.tag}.*.{args.stream}.recon.HIST.{args.ami}*/.1"

    topdir = f"/eos/atlas/atlastier0/rucio/{args.tag}/{args.stream}/*/{args.tag}.*.{args.stream}.merge.HIST.{args.ami}*/{args.tag}.*.{args.stream}.merge.HIST.{args.ami}*.1"

    files = glob.glob(topdir)

    outname = f"{args.tag}_{args.stream}_{args.ami}"
    makePlots(files, outname, args.threshold)


    
