import glob
import ROOT as R
import sys
sys.path.append("..")
import LArMonCoolLib
R.gROOT.SetBatch(True)

topdir = "/eos/atlas/atlastier0/tzero/prod/data24_hi/physics_CosmicCalo/00489895/data24_hi.00489895.physics_CosmicCalo.recon.HIST.f1550/"
#topdir = "/eos/atlas/atlastier0/tzero/prod/data24_hi/physics_CosmicCalo/00489909/data24_hi.00489909.physics_CosmicCalo.recon.HIST.f1550/"
#topdir = "/eos/atlas/atlastier0/tzero/prod/data24_hi/physics_CosmicCalo/00489938/data24_hi.00489938.physics_CosmicCalo.recon.HIST.f1550/"
#topdir = "/eos/atlas/atlastier0/tzero/prod/data24_hi/physics_CosmicCalo/00489961/data24_hi.00489961.physics_CosmicCalo.recon.HIST.f1550/"
#topdir = "/eos/atlas/atlastier0/tzero/prod/data24_hi/physics_CosmicCalo/00490079/data24_hi.00490079.physics_CosmicCalo.recon.HIST.f1550/"
#topdir = "/eos/atlas/atlastier0/tzero/prod/data24_hi/physics_CosmicCalo/00490085/data24_hi.00490085.physics_CosmicCalo.recon.HIST.f1550/"
#topdir = "/eos/atlas/atlastier0/tzero/prod/data24_hi/physics_CosmicCalo/00490145/data24_hi.00490145.physics_CosmicCalo.recon.HIST.f1550/"
#topdir = "/eos/atlas/atlastier0/tzero/prod/data24_hi/physics_CosmicCalo/00490156/data24_hi.00490156.physics_CosmicCalo.recon.HIST.f1550/"
#topdir = "/eos/atlas/atlastier0/tzero/prod/data24_hi/physics_CosmicCalo/00490182/data24_hi.00490182.physics_CosmicCalo.recon.HIST.f1550/"
#topdir = "/eos/atlas/atlastier0/tzero/prod/data24_hi/physics_CosmicCalo/00490198/data24_hi.00490198.physics_CosmicCalo.recon.HIST.f1550/"
#topdir = "/eos/atlas/atlastier0/tzero/prod/data24_hi/physics_CosmicCalo/00490220/data24_hi.00490220.physics_CosmicCalo.recon.HIST.f1550/"
#topdir = "/eos/atlas/atlastier0/tzero/prod/data24_hi/physics_CosmicCalo/00490222/data24_hi.00490222.physics_CosmicCalo.recon.HIST.f1550/"
#topdir = "/eos/atlas/atlastier0/tzero/prod/data24_hi/physics_CosmicCalo/00490223/data24_hi.00490223.physics_CosmicCalo.recon.HIST.f1550/"


infiles = glob.glob(topdir+"/*")

lbs = sorted(list(set([ inf.split("_lb")[1].split(".")[0] for inf in infiles ])))
print(int(lbs[-1]), "LBs")
#runnum = "451022"
#runnum = "451037"
#runnum = "451473"
#runnum = "450997"
tag = topdir.split("/")[-2].split(".")[0]
runnum = str(int(topdir.split("/")[-2].split(".")[1]))
stream = topdir.split("/")[-2].split(".")[2]
sb_flag = LArMonCoolLib.GetStableBeams(int(runnum))

histpath = "run_"+runnum
#hist = "CaloMonitoring/LArCellMon_NoTrigSel/2d_Occupancy/CellOccupancyVsEtaPhi_EMEC2C_5Sigma_CSCveto"

#Thresh0ECCAveE
#Thresh0ECCOcc

Threshold = 0
canv = R.TCanvas(f"canv_{runnum}_{Threshold}", f"canv_{runnum}_{Threshold}", 400*3, 800)
R.gStyle.SetPalette(R.kTemperatureMap)
canv.Divide(3,2)
drawn = 0
canv.objs = []
hists = {}
for lb in lbs:
    #if int(lb) < 2014: continue
    infile = [ inf for inf in infiles if "_lb"+str(lb)+"." in inf ][0]

    parts = [ "ECC", "BAR", "ECA", "EMECC", "EMBAR", "EMECA" ]
        
    f = R.TFile.Open(infile)
    stable = sb_flag[int(lb)]
    stable_label = "NOT Stable Beam"
    if stable:
        stable_label = "STABLE BEAM"
    print(lb, stable)
    drawnpart = 0
    for part in parts:
        if "EM" in part:
            hist = f"CaloTopoClusters/Cal{part}/EMThresh{Threshold}{part.replace('EM','')}Occ"
        else:
            hist = f"CaloTopoClusters/Cal{part}/Thresh{Threshold}{part}Occ"
        thishist = f.Get(f"{histpath}/{hist}")

        #print(infile, histpath)
        if not isinstance(thishist,R.TH2): 
            continue
        drawnpart += 1
        newname = f"{thishist.GetName}_{lb}"
        thishist.SetName(newname)
        hists[newname] = thishist

        thishist.SetTitle(f"LB {lb} : {part} {thishist.GetTitle()} {stable_label}")
        canv.cd(parts.index(part)+1)

        hists[newname].Draw("COLZ")
        canv.objs.append(hists[newname])
    if drawnpart == 0: continue

    if drawn == 0:
        canv.Print(f"CaloTopo_Thr{Threshold}_{tag}_{runnum}_{stream}.pdf(","pdf")
    else:
        canv.Print(f"CaloTopo_Thr{Threshold}_{tag}_{runnum}_{stream}.pdf","pdf")        
    drawn+=1

    f.Close()

canv.Print(f"CaloTopo_Thr{Threshold}_{tag}_{runnum}_{stream}.pdf)","pdf")
