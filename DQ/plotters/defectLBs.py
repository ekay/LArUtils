from argparse import ArgumentParser
import ROOT as R
import xmlrpc.client
import numpy as np
R.gROOT.SetBatch(True)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-r', '--runlist', dest='runList', help='Run number which you would like to get information for', type=int, nargs='+', default=None,required=False)
    parser.add_argument('-d', '--defect', dest='defect', default="SEVNOISEBURST", help='Defect substring to consider. Default %(default)s')
    parser.add_argument('--dqmpassfile', type=str, help='DQM pass file', default='/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt')
    args = parser.parse_args()

    args.runList = sorted(args.runList)
    
    dqmpassfile = args.dqmpassfile
    cred = None
    with open(dqmpassfile, "r") as f:
        cred = f.readline().strip()
    if cred is None:
        print("Problem reading credentials from",dqmpassfile)
    dqmapi = xmlrpc.client.ServerProxy('https://'+cred+'@atlasdqm.cern.ch')
            
    run_spec = {'stream': 'physics_CosmicCalo', 'source': 'tier0', 'run_list': args.runList}

    defects = {}
    chunksize=10
    if len(args.runList) > chunksize:
        this_runspec = run_spec.copy()
        theseRuns = [run_spec['run_list'][i:i+(chunksize)] for i in range(0,len(run_spec['run_list']),chunksize)]
        del this_runspec['run_list']

        for chunk in theseRuns:
            print(f"Chunk {theseRuns.index(chunk)+1} / {len(theseRuns)} ({chunk})")
            this_runspec['run_list'] = chunk
            defects.update(dqmapi.get_defects_lb(this_runspec,"", "HEAD", True, True, "Production", True))
    else:
        defects = dqmapi.get_defects_lb(run_spec,"", "HEAD", True, True, "Production", True)

    nLB = {}
        
    for run in args.runList:
        if str(run) not in defects.keys(): continue

        these_defects = defects[str(run)]
        #these_defects = { k:v for k,v in these_defects.items() if any(d in k for d in args.defect)}
        these_defects = { k:v for k,v in these_defects.items() if args.defect in k }

        nLB[run] = 0
        if len(these_defects.keys()) == 0: continue

        lbs = []
        for key in these_defects.keys():
            for lblist in these_defects[key]:
                lbs.extend(list(range(lblist[0], lblist[1]+1)))
        lbs = list(set(lbs))
        nLB[run] += len(lbs)
        print(run, nLB[run])

    
    xvals = np.array(args.runList, dtype='i')
    yvals = np.array([nLB[run] for run in args.runList], dtype='i')

    defplot = R.TGraph(len(xvals), xvals, yvals)
    defplot.SetMarkerStyle(20)
    defplot.SetMarkerColor(R.kBlue)
    defplot.SetTitle(args.defect)
    canv = R.TCanvas()
    defplot.Draw("Ap")
    defplot.GetXaxis().SetTitle("Run Number")
    defplot.GetYaxis().SetTitle(f"# of LBs with {args.defect} Defect")

    canv.Print(f"{args.defect}_runs{args.runList[0]}-{args.runList[-1]}.png")
