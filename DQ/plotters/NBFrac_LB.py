from argparse import ArgumentParser
import ROOT as R
import glob
import numpy as np
import sys, os, errno
from getStreamRate import getRate

R.gROOT.SetBatch(True)

# Suggested TO DO list if this script proves useful:
# Make it more configurable: output directory, list of runs, some selection on runs? 
# Option to either read root files or from WDs. e.g. with get_hist_objects(run_spec, histogram) form the ATLAS DQM APIs. Returns dictionary with keys being run numbers (as strings) and values being an XML representation of the histogram (as a string). This representation can be turned into a usable ROOT histogram with the TBufferXML::ConvertFromXML
# Other combos, e.g. EMEC or EMB or EM or A-side or C-side together, as well as just "ALL"



def getRootFiles(run, stream, camp, amistr="x"):
    rootpath = "/eos/atlas/atlastier0/tzero/prod/"+camp+"/"+stream+"/*"+str(run)+"*/"+camp+".*."+stream+".recon.HIST."+amistr+"*/"+camp+".*."+stream+".recon.HIST."+amistr+"*SFO*.1"
    print("Wildcard:",rootpath)
    rootfiles = glob.glob(rootpath)
    return rootfiles


def main(args):
    rootfiles = getRootFiles(args.run, args.stream, args.camp, args.amiStr)

    LBs = sorted(list([int(r.split(".")[-4].split("lb")[1]) for r in rootfiles]))
    LBs = list(set(LBs))
    if len( list(set([ "/".join(r.split("/")[:-1]) for r in rootfiles]))) != 1:
        print("WARNING - found more than one directory containing root files. Perhaps you need to specify the ami tag in the amiStr option")
        print(list(set([ "/".join(r.split("/")[:-1]) for r in rootfiles])))
        sys.exit()
        
    print(len(LBs), "LBs found for requested run")
    infiles = {}
    flaggedStdRNB = {}
    flaggedStdRNB_notVeto = {}
    flaggedSatRNB = {}
    flaggedSatRNB_notVeto = {}
    flaggedStdRNB_eff = {}
    flaggedSatRNB_eff = {}

    flaggedTightMNB = {}
    flaggedTightMNB_notVeto = {}
    flaggedTight_PsVetoMNB = {}
    flaggedTight_PsVetoMNB_notVeto = {}
    flaggedLooseMNB = {}
    flaggedLooseMNB_notVeto = {}
    flaggedTightMNB_eff = {}
    flaggedTight_PsVetoMNB_eff = {}
    flaggedLooseMNB_eff = {}

    parts = [ "EMB", "EMEC", "HEC", "FCAL" ]
    sides = [ "A", "C" ]
    allparts = [ p+s for s in sides for p in parts ]
    combos = []

    outstr = str(args.run)+" "+args.camp+" "+args.stream+" "+args.amiStr
    outnamestr = outstr.replace(" ","_")

    for LB in LBs:
        lbfile = [r for r in rootfiles if "_lb"+str(LB).zfill(4)+"." in r]
        if len(lbfile)>1:
            print("problem", lbfile)
            sys.exit()
        else:
            infiles[LB] = lbfile[0]
        infile = R.TFile(infiles[LB], "R")
        print("Reading from file:",infiles[LB])
        histpath = "run_"+str(args.run)
        topdirEMB = histpath+"/LAr/NoisyRO/EMB"
        topdirEMEC = histpath+"/LAr/NoisyRO/EMEC"

        flaggedStdRNB[LB] = {}
        flaggedStdRNB_notVeto[LB] = {}
        flaggedSatRNB[LB] = {}
        flaggedSatRNB_notVeto[LB] = {}
        flaggedStdRNB_eff[LB] = {}
        flaggedSatRNB_eff[LB] = {}

        flaggedTightMNB[LB] = {}
        flaggedTightMNB_notVeto[LB] = {}
        flaggedTight_PsVetoMNB[LB] = {}
        flaggedTight_PsVetoMNB_notVeto[LB] = {}
        flaggedLooseMNB[LB] = {}
        flaggedLooseMNB_notVeto[LB] = {}
        flaggedTightMNB_eff[LB] = {}
        flaggedTight_PsVetoMNB_eff[LB] = {}
        flaggedLooseMNB_eff[LB] = {}

        def getHistSum(histpath):
            try:
                hsum = infile.Get(histpath).GetSumOfWeights()
            except:
                hsum = 0
            return hsum
        def getHistStdDev(histpath):
            try:
                stddev = infile.Get(histpath).GetStdDev()
            except:
                stddev = 0
            return stddev

        def getEff(flagged, notVeto):
            if flagged == 0 :
                return -10
            else:
                return 100 * (( flagged - notVeto ) / flagged)
        def getErr(hist):
            try:
                return hist.GetMeanError()
            except:
                return 0

        for part in allparts:
            if part.startswith("EMB"):
                topdir = topdirEMB
            else:
                topdir = topdirEMEC

            flaggedStdRNB[LB][part] = getHistSum(topdir+"/NoisyEvent_"+part)
            flaggedStdRNB_notVeto[LB][part] = getHistSum(topdir+"/NoisyEvent_TimeVeto_"+part)
            flaggedSatRNB[LB][part] = getHistSum(topdir+"/SaturatedNoisyEvent_"+part)
            flaggedSatRNB_notVeto[LB][part] = getHistSum(topdir+"/SaturatedNoisyEvent_TimeVeto_"+part)

            flaggedStdRNB_eff[LB][part] = getEff(flaggedStdRNB[LB][part], flaggedStdRNB_notVeto[LB][part])
            flaggedSatRNB_eff[LB][part] = getEff(flaggedSatRNB[LB][part], flaggedSatRNB_notVeto[LB][part])

            flaggedTightMNB[LB][part] = getHistSum(topdir+"/MNBTightEvent_"+part)
            flaggedTightMNB_notVeto[LB][part] = getHistSum(topdir+"/MNBTightEvent_TimeVeto_"+part)
            flaggedTight_PsVetoMNB[LB][part] = getHistSum(topdir+"/MNBTight_PsVetoEvent_"+part)
            flaggedTight_PsVetoMNB_notVeto[LB][part] = getHistSum(topdir+"/MNBTight_PsVetoEvent_TimeVeto_"+part)
            flaggedLooseMNB[LB][part] = getHistSum(topdir+"/MNBLooseEvent_"+part)
            flaggedLooseMNB_notVeto[LB][part] = getHistSum(topdir+"/MNBLooseEvent_TimeVeto_"+part)

            flaggedTightMNB_eff[LB][part] = getEff(flaggedTightMNB[LB][part], flaggedTightMNB_notVeto[LB][part])
            flaggedTight_PsVetoMNB_eff[LB][part] = getEff(flaggedTight_PsVetoMNB[LB][part], flaggedTight_PsVetoMNB_notVeto[LB][part])
            flaggedLooseMNB_eff[LB][part] = getEff(flaggedLooseMNB[LB][part], flaggedLooseMNB_notVeto[LB][part])

        # Sum the values from each run and calculate total efficiencies
        flaggedStdRNB[LB]["ALL"] = sum( flaggedStdRNB[LB].values() )
        flaggedStdRNB_notVeto[LB]["ALL"] = sum( flaggedStdRNB_notVeto[LB].values() )
        flaggedSatRNB[LB]["ALL"] = sum( flaggedSatRNB[LB].values() )
        flaggedSatRNB_notVeto[LB]["ALL"] = sum( flaggedSatRNB_notVeto[LB].values() )
        flaggedStdRNB_eff[LB]["ALL"] = getEff(flaggedStdRNB[LB]["ALL"], flaggedStdRNB_notVeto[LB]["ALL"])
        flaggedSatRNB_eff[LB]["ALL"] = getEff(flaggedSatRNB[LB]["ALL"], flaggedSatRNB_notVeto[LB]["ALL"])
        flaggedTightMNB[LB]["ALL"] = sum( flaggedTightMNB[LB].values() )
        flaggedTightMNB_notVeto[LB]["ALL"] = sum( flaggedTightMNB_notVeto[LB].values() )
        flaggedTight_PsVetoMNB[LB]["ALL"] = sum( flaggedTight_PsVetoMNB[LB].values() )
        flaggedTight_PsVetoMNB_notVeto[LB]["ALL"] = sum( flaggedTight_PsVetoMNB_notVeto[LB].values() )
        flaggedLooseMNB[LB]["ALL"] = sum( flaggedLooseMNB[LB].values() )
        flaggedLooseMNB_notVeto[LB]["ALL"] = sum( flaggedLooseMNB_notVeto[LB].values() )
        flaggedTightMNB_eff[LB]["ALL"] = getEff(flaggedTightMNB[LB]["ALL"], flaggedTightMNB_notVeto[LB]["ALL"])
        flaggedTight_PsVetoMNB_eff[LB]["ALL"] = getEff(flaggedTight_PsVetoMNB[LB]["ALL"], flaggedTight_PsVetoMNB_notVeto[LB]["ALL"])
        flaggedLooseMNB_eff[LB]["ALL"] = getEff(flaggedLooseMNB[LB]["ALL"], flaggedLooseMNB_notVeto[LB]["ALL"])
        if "ALL" not in combos:
            combos.append("ALL")


        if flaggedStdRNB[LB]["ALL"] == 0 and flaggedSatRNB[LB]["ALL"] == 0 and flaggedTightMNB[LB]["ALL"] == 0 and flaggedTight_PsVetoMNB[LB]["ALL"] == 0 and flaggedLooseMNB[LB]["ALL"] == 0: 
            continue

        print("*",LB,"*")
        def printvals(eff, flagged, notVeto, tag="?", low=None, high=None, flagged_err=None, notVeto_err=None):
            if low is not None:
                if eff <= low:
                    return
            if high is not None:
                if eff >= high:
                    return
            if flagged_err is not None and notVeto_err is not None:
                print("LB "+str(LB)+" "+tag+" efficiency =", str(round(eff,2))+"% :", flagged, "flagged", "("+str(round(flagged_err,2))+")", notVeto, "not removed by veto", "("+str(round(notVeto_err,2))+")") 
            else:
                print("LB "+str(LB)+" "+tag+" efficiency =", str(round(eff,2))+"% :", flagged, "flagged", notVeto, "not removed by veto") 

        low = -0.0001  # 0
        high = 60  # None  # 90

        for part in allparts+combos:
            printvals(flaggedStdRNB_eff[LB][part], flaggedStdRNB[LB][part], flaggedStdRNB_notVeto[LB][part], "RNB Std "+part, low, high )#, flaggedStdRNB_EMECC_stddev, flaggedStdRNB_notVeto_EMECC_stddev)
            printvals(flaggedSatRNB_eff[LB][part], flaggedSatRNB[LB][part], flaggedSatRNB_notVeto[LB][part], "RNB Sat "+part, low, high )#, flaggedSatRNB_EMECC_stddev, flaggedSatRNB_notVeto_EMECC_stddev)
            printvals(flaggedTightMNB_eff[LB][part], flaggedTightMNB[LB][part], flaggedTightMNB_notVeto[LB][part], "MNB Tight "+part, low, high )#, flaggedTightMNB_EMECC_stddev, flaggedTightMNB_notVeto_EMECC_stddev)
            printvals(flaggedTight_PsVetoMNB_eff[LB][part], flaggedTight_PsVetoMNB[LB][part], flaggedTight_PsVetoMNB_notVeto[LB][part], "MNB Tight_PsVeto "+part, low, high )#, flaggedTight_PsVetoMNB_EMECC_stddev, flaggedTight_PsVetoMNB_notVeto_EMECC_stddev)
            printvals(flaggedLooseMNB_eff[LB][part], flaggedLooseMNB[LB][part], flaggedLooseMNB_notVeto[LB][part], "MNB Loose "+part, low, high )#, flaggedLooseMNB_EMECC_stddev, flaggedLooseMNB_notVeto_EMECC_stddev)

    canv = R.TCanvas()
    

    def drawPlot(yvals, outname, title="", ytitle="", xtitle="LB", verbose=False):
        canv.objs = []
        if all(ele == -10 for ele in yvals): return  
        if verbose:
            print("*"*30)
            print(outname, {LBs[i]: yvals[i] for i in range(len(LBs))})

        gr = R.TGraph(len(yvals), np.array(LBs, dtype='d'), np.array(yvals, dtype='d'))
        gr.GetHistogram().SetMinimum(-1.)
        gr.SetMarkerStyle(20)
        gr.SetTitle(title)
        gr.Draw("AP")
        gr.GetXaxis().SetMaxDigits(6)
        gr.GetXaxis().SetTitle(xtitle)
        gr.GetYaxis().SetTitle(ytitle)
        #xmin = R.TMath.MinElement(gr.GetN(), gr.GetX())
        xmin = gr.GetXaxis().GetBinLowEdge(1)
        #xmax = R.TMath.MaxElement(gr.GetN(), gr.GetX())
        xmax = gr.GetXaxis().GetBinUpEdge(gr.GetXaxis().GetNbins())
        ymin = gr.GetHistogram().GetMinimum()
        ymax = gr.GetHistogram().GetMaximum()
        def getLineY(yval, col, style=9):
            line = R.TLine(xmin,yval,xmax,yval) 
            line.SetLineColor(col)
            line.SetLineStyle(style)
            return line
        def getLineX(xval, col, style=9):
            line = R.TLine(xval,ymin,xval,ymax) 
            line.SetLineColor(col)
            line.SetLineStyle(style)
            return line
        line100 = getLineY(100, R.kRed)
        canv.objs.append(line100)
        line100.Draw("sameL")
        line90 = getLineY(90, R.kGray)
        canv.objs.append(line90)
        line90.Draw("sameL")
        line95 = getLineY(95, R.kGray)
        canv.objs.append(line95)
        line95.Draw("sameL")


        if args.runLines is not None:
            txt = R.TLatex()
            #txt.SetNDC()
            txt.SetTextColor(R.kBlue)
            txt.SetTextAngle(90)
            txt.SetTextAlign(10)
            txt.SetTextSize(0.025)
            line_run = {}
            for rl in args.runLines:
                line_run[rl] = getLineX(rl, R.kBlue)
                canv.objs.append(line_run[rl])
                line_run[rl].Draw("sameL")
                txt.DrawLatex(rl-2, 0.21, str(rl))
            
        gr.Draw("same,P")

        canv.Print(args.outdir+"/"+outname+".png")


    print(len(LBs), "LBs")

    def cleanDict(thisdict, keylist):
        return { key: thisdict[key] for key in keylist }

    def listFromDict(thisdict, keylist, part):
        thisdict = cleanDict(thisdict, keylist)
        thislist = [ thisdict[key][part] if part in thisdict[key].keys() else -10 for key in keylist ]
        return thislist

    for part in allparts + combos:
        drawPlot( listFromDict(flaggedSatRNB_eff, LBs, part), "RNB_Sat_eff_"+part+"_"+outnamestr, "Efficiency of Saturated RNB Flag for "+part+" "+outstr, "RNB Saturated Veto Efficiency")
        drawPlot( listFromDict(flaggedStdRNB_eff, LBs, part), "RNB_Std_eff_"+part+"_"+outnamestr, "Efficiency of Standard RNB Flag for "+part+" "+outstr, "RNB Standard Veto Efficiency")
        drawPlot( listFromDict(flaggedLooseMNB_eff, LBs, part), "MNB_Loose_eff_"+part+"_"+outnamestr, "Efficiency of Loose MNB Flag for "+part+" "+outstr, "MNB Loose Veto Efficiency")
        drawPlot( listFromDict(flaggedTightMNB_eff, LBs, part), "MNB_Tight_eff_"+part+"_"+outnamestr, "Efficiency of Tight MNB Flag for "+part+" "+outstr, "MNB Tight Veto Efficiency")
        drawPlot( listFromDict(flaggedTight_PsVetoMNB_eff, LBs, part), "MNB_Tight_PsVeto_eff_"+part+"_"+outnamestr, "Efficiency of Tight_PsVeto MNB Flag for "+part+" "+outstr, "MNB Tight_PsVeto Veto Efficiency")


if __name__ == "__main__":
    parser = ArgumentParser()
    # parser.add_argument('-n', '--nruns', dest='nruns', default=None, type=int, help='If a run range is not defined - search for this number of runs above high run / below low run')
    parser.add_argument('-s', '--stream', dest='stream', default="physics_CosmicCalo", type=str, help='Stream to look at, e.g. physics_CosmicCalo, express_express, physics_Main')
    parser.add_argument('-a', '--amiStr', dest='amiStr', default="x", type=str, help='AMI tag string - may just be "f" or "x" or a more specific tag')
    parser.add_argument('-c', '--camp', dest='camp', default="data23_13p6TeV", type=str, help='Campaign to look at, e.g. data23_13p6TeV, data_cos')
    #parser.add_argument('--useBulk', dest='isBulk', default=False, action='store_true', help='Look for bulk output?')                         
    parser.add_argument('-o', '--outdir', dest='outdir', default=".", type=str, help='Output directory for plots')
    #parser.add_argument('-s', '--lowrun', dest='lowrun', default=None, type=int, help='Search for runs starting from this run number')
    #parser.add_argument('-e', '--highrun', dest='highrun', default=None, type=int, help='Search for runs with a maximum of this run number')
    #parser.add_argument('-r', '--runlist', dest='runList', help='Run number which you would like to get information for', type=int, nargs='+', default=None,required=False)
    parser.add_argument('-r', '--run', dest='run', help='Run number which you would like to get information for', type=int, default=None,required=False)
    parser.add_argument('-l', '--runlines', dest='runLines', help='Run numbers which should be highlighted on the plots with a vertical, annotated line', type=int, nargs='+', default=None,required=False)
    #parser.add_argument('--onlyLAr', dest='ignoreNoLAr', action='store_true', help='Only show runs where LAr is enabled')
    #parser.add_argument('--onlySB', dest='onlySB', action='store_true', help='Only look at stable beam runs')
    args = parser.parse_args()
    args.isExpress = args.amiStr.startswith("x") 

    args.outdir+="/effPlots"
    try:
        os.makedirs(args.outdir)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

    main(args)

