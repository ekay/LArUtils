import sys
import glob
import xmlrpc.client
import ctypes
import argparse
import datetime
import numpy as np
from getClusters import get_thrnum_EM, get_thrnum_Topo
import ROOT as R
R.gROOT.SetBatch(True)
sys.path.append("..")
import numpy as np

from LArMonCoolLib import GetLumiFromCOOL

def getRootFiles(run, stream, camp, amistr="x"):
    rootpath = "/eos/atlas/atlastier0/tzero/prod/"+camp+"/"+stream+"/*"+str(run)+"*/"+camp+".*."+stream+".recon.HIST."+amistr+"*/"+camp+".*."+stream+".recon.HIST."+amistr+"*SFO*.1"
    print("Wildcard:",rootpath)
    rootfiles = glob.glob(rootpath)
    return rootfiles

def getStr(low, hi, var):
    thestr = ""
    thestr_filename = ""

    if low is not None and hi is not None:
        if low == -hi:
            thestr = "|#"+var+"|<"+str(hi)
            thestr_filename = "abs_"+var+"_lt_"+str(hi)
        elif low > hi:
            thestr = f"|#{var}|<{hi} or |#{var}|>{low}"
            thestr_filename = f"{var}_lt_{hi}_or_{var}_gt_{low}"
        else:
            thestr = str(low)+"<#"+var+"<"+str(hi)
            thestr_filename = str(low)+"_lt_"+var+"_lt_"+str(hi)
    elif low is not None:
        thestr = "#"+var+">"+str(low)
        thestr_filename = var+"_gt_"+str(low)
    elif hi is not None:
        thestr = "#"+var+"<"+str(hi)
        thestr_filename = var+"_lt"+str(hi)

    return thestr, thestr_filename


###################################################
###                   M  A  I  N                ###
###################################################

parser = argparse.ArgumentParser(description='Beam-induced background.')
parser.add_argument('-a', '--ami', dest='ami', default="x", type=str, help='AMI letter to use for cluster info - x or f')
parser.add_argument('-r', '--run', dest='run', help='Run number which you would like to get information for', type=int,  default=None,required=True)
parser.add_argument('-topN', type=int, default=2, help='Number of top points to label')
parser.add_argument('-lowN', type=int, default=2, help='Number of lowest points to label')
parser.add_argument('--bibFrac', dest='bibFrac', action='store_true', default=False, help='Plot the fraction of clusters in the bib region, divided by total clusters')
parser.add_argument('--dqmpassfile', type=str, help='DQM pass file', default='/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt')
parser.add_argument('--nonSB', dest='nonSB', action='store_true', help='Also look at non-stable-beam runs')
parser.add_argument('--elow', '--etalow', dest='etalow', default=None, type=float, help='Low eta value for target region')
parser.add_argument('--ehi', '--etahi', dest='etahi', default=None, type=float, help='High eta value for target region')
parser.add_argument('--plow', '--philow', dest='philow', default=None, type=float, help='Low phi value for target region')
parser.add_argument('--phi', '--phihi', dest='phihi', default=None, type=float, help='High phi value for target region')
parser.add_argument('--dcsFile', dest='DCSfile', default=None, type=str, help='path to .txt file with dcs readings to overlay')
parser.add_argument('-s', '--stream', dest='stream', default="physics_CosmicCalo", type=str, help='Stream to look at, e.g. physics_CosmicCalo, express_express, physics_Main')
parser.add_argument('-c', '--camp', dest='camp', default="data24_13p6TeV", type=str, help='Campaign to look at, e.g. data24_13p6TeV, data_cos')
parser.add_argument('-t', '--cluster_thr', dest='cluster_thr', default="Et > 4/10GeV", type=str, help='Threshold for EM/Topo clusters. Default %(default)s.')

args = parser.parse_args()

rootfiles = getRootFiles(args.run, args.stream, args.camp, args.ami)
if len(rootfiles) == 0:
    print("NO ROOT FILES FOUND")
    sys.exit()
all_LBs = sorted(list([int(r.split(".")[-4].split("lb")[1]) for r in rootfiles]))
all_LBs = list(set(all_LBs))
if len( list(set([ "/".join(r.split("/")[:-1]) for r in rootfiles]))) != 1:
    print("WARNING - found more than one directory containing root files. Perhaps you need to specify the ami tag in the amiStr option")
    print(list(set([ "/".join(r.split("/")[:-1]) for r in rootfiles])))
    sys.exit()

print(len(rootfiles), "files")


LB_lumi = GetLumiFromCOOL(args.run)

thrnum_EM = get_thrnum_EM(args.cluster_thr)
thrnum_Topo = get_thrnum_Topo(args.cluster_thr)

etastr, etastr_filename = getStr(args.etalow, args.etahi, "eta")
phistr, phistr_filename = getStr(args.philow, args.phihi, "phi")

cutstr = ""
cutstr_filename = ""
if etastr != "":
    cutstr = etastr
    cutstr_filename = etastr_filename
    if phistr != "":
        cutstr += ", "+phistr
        cutstr_filename += "_"+cutstr_filename
else:
    cutstr = phistr
    cutstr_filename =  phistr_filename

print(cutstr, cutstr_filename)

nclus = []
lines = []

thrstr = args.cluster_thr.replace(" ","_").replace(">","gt").replace("/","and")

def fillDummy(val, isLow=False):
    if val is None:
        if isLow: 
            return -100000000
        else:
            return 100000000
    else: return val
etalow_dum = fillDummy(args.etalow, True)
etahi_dum = fillDummy(args.etahi)
philow_dum = fillDummy(args.philow, True)
phihi_dum = fillDummy(args.phihi)
print(f"Eta range: {etalow_dum} - {etahi_dum}")
print(f"Phi range: {philow_dum} - {phihi_dum}")
topdir = f"run_{args.run}/CaloTopoClusters"

print(len(rootfiles), "files")

x, y, z = ctypes.c_int(1), ctypes.c_int(1), ctypes.c_int(1)

nclus = []
LBs = []
lumis = []

for rf in rootfiles:
    LB = int(rf.split(".")[-4].split("lb")[1])
    this_lumi = LB_lumi[LB]/1e3
    readFile = R.TFile(rf, "READ")
              
    if not isinstance(readFile.Get(topdir), R.TDirectory):
        continue
    
    histnames = [ f"CalBAR/Thresh{thrnum_Topo}BAROcc",
                  f"CalECC/Thresh{thrnum_Topo}ECCOcc",
                  f"CalECA/Thresh{thrnum_Topo}ECAOcc",
                  f"CalEMBAR/EMThresh{thrnum_EM}BAROcc",
                  f"CalEMECC/EMThresh{thrnum_EM}ECCOcc",
                  f"CalEMECA/EMThresh{thrnum_EM}ECAOcc" ]
    hists = [ readFile.Get(f"{topdir}/{hn}") for hn in histnames ]
    hists = [ h for h in hists if isinstance(h, R.TH2) ]
    histnames = [ h.GetName() for h in hists if isinstance(h, R.TH2) ]
    if len(hists) == 0 : continue

    content_sum = 0
    
    for h in hists:
        for nb in range(h.GetNcells()):
            h.GetBinXYZ(nb, x, y, z)
            xcent = h.GetXaxis().GetBinCenter(x.value)
            xlow = h.GetXaxis().GetBinLowEdge(x.value)
            xhi = h.GetXaxis().GetBinUpEdge(x.value)
            ycent = h.GetYaxis().GetBinCenter(y.value)
            ylow = h.GetYaxis().GetBinLowEdge(y.value)
            yhi = h.GetYaxis().GetBinUpEdge(y.value)
            content = h.GetBinContent(nb)
            
            if xlow > etalow_dum and xhi < etahi_dum:
                if philow_dum > phihi_dum:
                    if abs(ylow) > philow_dum or abs(yhi) < phihi_dum:
                        content_sum += content
                else:
                    if ylow > philow_dum and yhi < phihi_dum:
                        content_sum += content

    if (rootfiles.index(rf)+1) % 100 == 0:
        print(rootfiles.index(rf)+1,"/",len(rootfiles), f"LB {LB} with {this_lumi}, content {content_sum}. Division = {content_sum/this_lumi}. Hists: {', '.join(histnames)}")
        
    content_sum /= this_lumi
    
    nclus.append(content_sum)
    LBs.append(LB)
    lumis.append(this_lumi)

print("Looped all lumis, have", len(LBs))
    
def plotClusLumi():
    def formatgr(gr, col, style=20, size=1):
        gr.SetMarkerColor(col)
        gr.SetLineColor(col)
        gr.SetMarkerStyle(style)
        gr.SetMarkerSize(size)
        gr.SetTitle(f"{args.cluster_thr} Clusters in Run {args.run}")

    xvals = np.array(LBs, dtype="d")
    xtitle = "LB #"

    g_nclus = R.TGraph(len(LBs), xvals, np.array(nclus, dtype="d"))
    formatgr(g_nclus, R.kBlue)

    #  label top 2 
    topN_idx = np.argsort(nclus)[-args.topN:]
    lowN_idx = np.argsort(nclus)[::-1][-args.lowN:]
    labels = []
    #for idx in topN_idx:
    #    print(runs[idx], fills[idx], nclus[idx], g_nclus.GetX()[idx], g_nclus.GetY()[idx])
    #    latex = R.TLatex(g_nclus.GetX()[idx], g_nclus.GetY()[idx],str(runs[idx])+" (fill "+str(fills[idx])+")")
    #    latex.SetTextSize(0.025)
    #    latex.SetTextAngle(90)
    #    g_nclus.GetListOfFunctions().Add(latex)
    #    labels.append(latex)
    #for idx in lowN_idx:
    #    print(runs[idx], fills[idx], nclus[idx], g_nclus.GetX()[idx], g_nclus.GetY()[idx])
    #    latex = R.TLatex(g_nclus.GetX()[idx], g_nclus.GetY()[idx],str(runs[idx])+" (fill "+str(fills[idx])+")")
    #    latex.SetTextSize(0.025)
    #    latex.SetTextAngle(90)
    #    g_nclus.GetListOfFunctions().Add(latex)
    #    labels.append(latex)

    g_lumi = R.TGraph(len(LBs), xvals, np.array(lumis, dtype="d"))
    formatgr(g_lumi, R.kRed)

    canv = R.TCanvas()
    canv.objs = []
    p1 = R.TPad("p1", "", 0, 0, 1, 1)
    p2 = R.TPad("p2", "", 0, 0, 1, 1)
    p1.SetFillStyle(4000) # transparent
    p2.SetFillStyle(4000) # transparent

    p1.Draw()
    p1.cd()
    g_nclus.Draw("APl")
    g_nclus.GetXaxis().SetTitle(xtitle)
    g_nclus.GetYaxis().SetTitle(f"# (Clusters with {args.cluster_thr} in {cutstr} / L [pb^{-1}]")  #oioioio change
    #if args.runnb is True:
    #    g_nclus.GetXaxis().SetMaxDigits(maxdigits)
    #else:
    #    g_nclus.GetXaxis().SetTimeDisplay(1)
    #    g_nclus.GetXaxis().SetTimeOffset(0)
    #    g_nclus.GetXaxis().SetNdivisions(503)
    #    g_nclus.GetXaxis().SetTimeFormat("%d/%m/%y")
    g_nclus.GetXaxis().SetTitleOffset(1.3)
    p1.Update()
    canv.objs.append(p1)

    outname = f"plotBIB_{args.run}_{args.ami}tag_{args.stream}_{thrstr}"
        
    if cutstr_filename != "": outname += "_"+cutstr_filename
    canv.Print(outname+".pdf")
    canv.Print(outname+".png")
    
    def plotSecond(gr, ytitle, outstr):
        xmin = p1.GetUxmin()
        xmax = p1.GetUxmax()
        dx = (xmax - xmin) / 0.8 # 10 percent margins left and right
        ymin = gr.GetHistogram().GetMinimum()
        ymax = gr.GetHistogram().GetMaximum()
        dy = (ymax - ymin) / 0.8 # 10 percent margins top and bottom

        p2.Range(xmin-0.1*dx, ymin-0.1*dy, xmax+0.1*dx, ymax+0.1*dy);

        p2.Draw()
        canv.objs.append(p2)
        p2.cd()
        gr.Draw("l")
        gr.GetXaxis().SetTitle(xtitle)
        gr.GetXaxis().SetMaxDigits(6)
        gr.GetYaxis().SetTitle(ytitle)

        axis = R.TGaxis(xmax, ymin, xmax, ymax, ymin, ymax, 510, "+L");
        axis.SetLineColor(gr.GetLineColor())
        axis.SetLabelColor(gr.GetLineColor())
        axis.SetTitleColor(gr.GetLineColor())
        axis.SetTitle(gr.GetYaxis().GetTitle())
        axis.SetTitleOffset(1.3)
        axis.Draw()

        thisoutname = outname + outstr
        canv.Print(thisoutname+".pdf")
        canv.Print(thisoutname+".png")

    plotSecond(g_lumi, "ATLAS ready luminosity [pb^{-1}]", "_vsLumi")



plotClusLumi()

