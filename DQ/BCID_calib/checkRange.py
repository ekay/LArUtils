#!/bin/python3

bcid_ttc_sync=27750
#27854
fpath = "/afs/cern.ch/work/e/ekay/public/Adriana/BCID_calib_280824/bcidcalib_full_samples_imax.txt"
#fpath = "/afs/cern.ch/work/e/ekay/public/Adriana/BCID_calib_280824/bcidcalib_full_sid_max_of_et.txt"
#2024-Sep-13 12:42:20,340 - [LATOME_EMBA_4] - const bool Latome::CheckBCIDCalibration() :LATOME 'LATOME_EMBA_4' BAD BCID calibration value for fiber 21: value = 27630 not within [bcid_ttc_sync-128, bcid_ttc_sync] with bcid_ttc_sync = 27854

#fpath = "/afs/cern.ch/user/e/ekay/LAr/LArDBTools/python/out_BCID_484909_LAR.txt"
#fpath = "/afs/cern.ch/user/e/ekay/LAr/LArDBTools/python/out_BCID_484909_ATLAS.txt"

#27630 0x6bee
#3443 0xd73
#3444 0xd74 - > 27630

badvals = []
all = []
with open(fpath, 'r') as f:
    for line in f.readlines():
        if "calib" in line: continue        
        line = line.strip()
        LATOME = line.split()[0]
        fibre = int(line.split()[1])
        calib = int(line.split()[2])

        LATOME = LATOME.replace("0x00","0x")
         
        if LATOME != "0x411003": continue
        if fibre != 21: continue
        value = bcid_ttc_sync+calib
        all.append(f"{LATOME}_{fibre}")
        if not (  value > (bcid_ttc_sync - 128) and value < bcid_ttc_sync ):
            print(f"!!! {LATOME} {fibre} {bcid_ttc_sync}+{calib} = {value}, {bcid_ttc_sync} - 128 = {bcid_ttc_sync - 128}")

            badvals.append(f"{LATOME}_{fibre}")
print(len(badvals), "/", len(all))
