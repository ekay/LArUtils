#!/bin/python3


from getpass import getpass, getuser

def getcred():
    user = getuser()
    print("Please provide password for",user)
    pwd = getpass()
    return user, pwd

def findJira(runNb, verbose=False):
    from jira import JIRA
    #if pwd is None: user, pwd = getcred()
    #tokenfile = "/afs/cern.ch/user/l/larmon/public/prod/LArPage1/makeJIRA/jiraToken"
    tokenfile = "jiraToken"
    with open(tokenfile, "r") as f:       
        token = f.readline().strip()
    j=JIRA("https://its.cern.ch/jira", token_auth=token)
    project = 'ATLLARSWDPQ'
    issues = j.search_issues('project=' + project + ' AND summary~"DQ Assessment for run '+str(runNb)+'"', maxResults=False, fields = 'comment')
    if verbose: print(issues)    
    if len(issues) == 1:
        return issues[0]
    elif len(issues) > 1:
        if verbose: print("more than one issue found")
        return None
    else:
        if verbose: print("No issue found")
        return None

def getComments(issue):
    if issue is None: return None
    comments = issue.raw['fields']['comment']['comments']
    #for comm in comments:
    #    thiscomm = comm['body'].replace("=\r\n","")
    #    thiscomm = thiscomm.replace("=\n","")
    #    comments[comments.index(comm)]['body'] = thiscomm
    return comments

def findElog(runNb, user=None, pwd=None, verbose=False):
    from elisa_client_api import elisa
    from elisa_client_api import searchCriteria

    if pwd is None: user, pwd = getcred()
    connectionURL_dq="https://atlasdqlog.cern.ch/elisa/api/DQshift/"
    elisaInterface_dq = elisa.Elisa(connectionURL_dq,user,pwd,None)
    criteria = searchCriteria.SearchCriteria()
    criteria.subject = runNb
    try:
        msgRunSearch = elisaInterface_dq.searchMessages(criteria)
    except Exception as e:
        print("An exception occurred... perhaps the atlasdqlog elisa system is down")
    msgs = {}
    for dqm in msgRunSearch:
        if "ES1" in dqm.subject:
            msgs["ES1"] = dqm
        elif "BLK" in dqm.subject:
            msgs["BLK"] = dqm
        elif "Summary" in dqm.subject:
            msgs["Summary"] = dqm
    if len(msgs.keys()) == 0:
        if verbose: print("No messages found in atlasdqlog for",runNb)
        return None
    else:
        if verbose: print(msgs)
        return msgs



