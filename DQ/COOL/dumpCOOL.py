from CoolLumiUtilities.CoolDataReader import CoolDataReader
import time
from datetime import datetime, timezone, timedelta
import numpy as np
from zlib import crc32
#folderName = "/LAR/Configuration/SCConfig/ADCconstants"
#tag = "LARConfigurationSCConfigADCconstants-UPD1-01"

folderName = "/LAR/BadChannels/MaskedSC"
tag = "LARBadChannelsMaskedSC-RUN3-UPD1-00"

#folderName = "/LAR/BadChannels/TTDisabledCells"
#tag = "LARBadChannelsTTDisabledCells-RUN2-UPD1-00"


import sys
import array
from io import BytesIO, StringIO


def arr_factor(arr):
    #counts = {"cell":195072., "cell2":182468., "SC":34048., "LATOME":116., "TT":5248.}
    counts = {"cell":195072., "cell2":182468., "SC":34048., "TT":5248.}
    factors = {c:len(arr)/counts[c] for c in counts.keys()}
    matched = {c:factors[c].is_integer() for c in counts.keys()}
    nmatches = len([1 for c in counts.keys() if matched[c] is True])
    if nmatches == 1:
        factor = [factors[c] for c in counts.keys() if matched[c] is True][0]
        name = [c for c in counts.keys() if matched[c] is True][0]
        count = [counts[c] for c in counts.keys() if matched[c] is True][0]
        print("yes",name,count,factor)
        return int(factor), count, name
    elif nmatches > 1:
        names = [c for c in counts.keys() if matched[c] is True]
        print(f"PROBLEM: cannot work out the type of Blob. Could be {', '.join(name)}")
    
    elif nmatches == 0:
        print(f"PROBLEM: cannot work out the type of Blob. None of the known types worked")

    print(f"Array has length {len(arr)}")
    return 1, 1, ""
    
    
class payload_info():
    def __init__(self, payload):
        spec=payload.specification()
        spec_size = spec.size()
        #spec_name = spec.name()
        for idx in range(spec_size):
            sp = spec[idx]
            pl = payload[idx]
        if "Blob" not in sp.storageType().name():
            print(sp.storageType().name())
            print(pl)
        else:
            blob = payload[idx]
            print("TYPE",type(blob))
            size=blob.size()
            buf=blob.startingAddress()
            buf.reshape((size,))
            factor, nchan, chantype = arr_factor(buf)
            nbytesUnpack = 4
            Nelements = int(size/nbytesUnpack)
            if factor != 1:
                Nelements = nchan*factor/4
                if not Nelements.is_integer():
                    print("Problem using 4 bytes")
                    sys.exit()
                arrayedBlob = np.frombuffer(buf, dtype=np.int32, count=int(Nelements))
                
                arrayedBlob = np.reshape(arrayedBlob,(-1,int(factor/4)))
            else:
                arrayedBlob = np.frombuffer(buf, dtype=np.int32, count=Nelements)

            #arrayedBlob = np.reshape(arrayedBlob,(-1,2))
            #ids = [ a[0] for a in arrayedBlob ]
            #print(len(ids), len(list(set(ids))))
            print(arrayedBlob ,arrayedBlob.shape)
            print(arrayedBlob[:100])
            
            elementToRead=0
            print("Looping")
            dcs = []
            #while elementToRead+2<np.size(arrayedBlob):
            while elementToRead+1<np.size(arrayedBlob):
                ttid = hex(arrayedBlob[elementToRead])
                print("id",ttid)
                elementToRead+=1
                nDcs=int(arrayedBlob[elementToRead])                
                print("num",nDcs)
                elementToRead+=1; continue 
                for i in range(0,nDcs):
                    # append the dc
                    elementToRead+=1
                    dcs.append([ttid,hex(arrayedBlob[elementToRead])])
                elementToRead+=1
            print(dcs)
            print("*"*5)
            

import time
from time import localtime
from calendar import timegm

from PyCool import cool



def ConvertToUTC(ts_in):
    return time.gmtime(time.mktime(time.strptime(ts_in,"%Y-%m-%d:%H:%M:%S")))


#t = "2023-09-09:07:30:00"
t = "2023-09-19:20:30:00"
ts = ConvertToUTC(t)
TimeStamp=int(timegm(ts))*int(1e9)
dbSvc = cool.DatabaseSvcFactory.databaseService()
#db=dbSvc.openDatabase("oracle://ATLAS_COOLPROD;schema=ATLAS_COOLONL_LAR;dbname=CONDBR2")
db=dbSvc.openDatabase("frontier://ATLF/();schema=ATLAS_COOLONL_LAR;dbname=CONDBR2")

folder=db.getFolder(folderName)
trange=3*60*60*1e9 # 1 day in ns
t1=int(TimeStamp-trange)
t2=int(TimeStamp+trange)
print(t1, t2)
t1 = 1689751800000000000
t2 = 1689845400000000000

runnum1, lb1 = 455924,0
runnum2, lb2= 456110,0


runiov1=(runnum1<<32)+lb1
runiov2=(runnum2<<32)+lb2
#t1 = cool.ValidityKey(runiov1)
#t2 = cool.ValidityKey(runiov2)
print("OIOI", t1, t2)
#itr=folder.browseObjects(t1,t2,cool.ChannelSelection.all(),tag)
itr=folder.browseObjects(t1,t2,cool.ChannelSelection.all(),tag)
while itr.goToNext():
    obj=itr.currentRef()
    payload=obj.payload()
    print({k:payload[k] for k in payload.keys()})
    time_start = time.strftime("%Y-%m-%d:%H:%M:%S",localtime(obj.since()/1e9))
    time_end = time.strftime("%Y-%m-%d:%H:%M:%S",localtime(obj.since()/1e9))
    print("hello",payload, time_start, time_end)
    spec=payload.specification()
    payload_info(payload)
    print("done")
itr.close()
db.closeDatabase()

