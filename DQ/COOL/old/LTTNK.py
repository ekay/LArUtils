#!/bin/python
#############################################################################
# Methods to read out LTTNK for UPD Tools studies
#
# Author : clement.camincher@cern.ch
#
#############################################################################
import sys,time,os
import getLTTNKCellsDB as LTTNK_DB
import runListBuilder
import DisplayUPDResults
import datetime

year = str(datetime.date.today().year)
#year = "2017"


def ExtractLTTNKValues(dateSince, dateUntil):           # New version for LTTNK which works so much faster

    run = []
    failrun = []
    Id = []
    failId = []
    Total_list = []
    Dict_total = {}
    fail_Total_list = []
    fail_Dict_total = {}
    LTTNK = {}
    fail_LTTNK = {}
    run = runListBuilder.runlist(year)[year]

    # Need to make a list for updateTime  
    for i in range(len(run)):                # List contains run number and list Id
        Dict_total = {'run': run[i]<<32, 'Id': 0, 'badchanOld':0, 'badchanNew':256, 'badchanStart':0}
        Total_list.append(Dict_total)  

    run_list = []                            # Find just unique channels in LTTNK
    for part in Total_list:
        if (part['run']>>32)  >= int(dateSince) and (part['run']>>32) <= int(dateUntil):
            if part['run'] not in run_list:
                run_list.append(part['run'])

    # Get the LTTNK disabled cells by reading the Cool DB
    LTTNK_list=dict()
    for run in run_list :
        run_int=run>>32
        LTTNK_list[str(run_int)]=LTTNK_DB.Main(run_int,run_int)

 
    for since in run_list:
        if since>>32 in runListBuilder.runlist(year)[year]:
            LTTNK[since] = []
            for chan in Total_list:
                if since == chan['run']:
                    LTTNK[since].append({'run':chan['run'], 'Id':chan['Id'],'badchanOld':0, 'badchanNew':256, 'badchanStart':0, 'IOVSince':chan['run'], 'updateTime':time})
                    #print("LTTNK[since]", LTTNK[since],"\n")
                   
    print('Summary for LTTNK:')
    #print("LTTNK_list", LTTNK_list,"\n")    

    DisplayUPDResults.DisplayLTTNKSummary(LTTNK_list)
            
    return LTTNK



