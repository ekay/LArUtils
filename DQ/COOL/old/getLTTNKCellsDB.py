#!/usr/bin/env python3
# EMACS settings: -*-  tab-width: 2; indent-tabs-mode: nil -*-
# vim: tabstop=2:softtabstop=2:shiftwidth=2:expandtab
# kate: tab-width 2; replace-tabs on; indent-width 2;
#
# Summary:  Gets the LTTNK disabled and failed cells.
# Comments: -) To run, simply use python. The code now checks if sod has been issued.
#           If not it sources the local sod script automatically
#           -) New COOL uses oracle:
#           "oracle://ATONR_COOL;schema=ATLAS_COOLONL_LAR;dbname=CONDBR2;user=ATLAS_COOL_READER_U;password=LMXTPRO4RED"
#           -) Old COOL was sqlite:
#           "sqlite://;schema=/det/lar/project/databases/LArTTNoiseKiller.db;dbname=CONDBR2"
#
# Change Log:
# 22.09.2016 - Able to parse through Db by single run number, returning disabled channels
# 27.04.2017 - Automatic sod function to make script more standalone
# 31.10.2017 - Able to query run from LB 0 to a specified LB + output to file.
# 24.11.2017 - Able to query runs via timestamp.
# 15.05.2018 - Show TT/Ch on a Run,LB basis
#
# Author: Vincent R. Pascuzzi <vpascuzz@cern.ch>
# Date:   22.09.2016
# Author: Christopher R. Hayes <chhayes@cern.ch>
# Date:   27.04.2017
################################################################################

import string,copy
import os,sys,time
import numpy as np
from struct     import unpack
from argparse   import ArgumentParser
from calendar import timegm
from time import asctime,gmtime,localtime
from itertools import chain

#load check_env from env directory
# dir_path = os.path.dirname(os.path.realpath(__file__))
# local_sod_python_path = dir_path.replace(dir_path[dir_path.rindex("/scripts"):],"/scripts/env")
# try:
#     sys.path.index(local_sod_python_path)
# except ValueError:
#     sys.path.insert(0, local_sod_python_path)
# import check_env
# # source sod if hasn't been already
# check_env.check_sod(' '.join(sys.argv))

#Now that evironment has been sourced, import COOL
from PyCool     import cool
#Collections and Counter also brought in by sod
from collections import Counter

#################
# Print functions
def unique_list(l):
  ulist = []
  [ulist.append(x) for x in l if x not in ulist]
  return ulist

def ts2string(ts):
    if ts==0:
        return "0"
    if ts==cool.ValidityKeyMax:
        return "INF"
    stime=int(ts/1000000000)
    return asctime(gmtime(stime))+" UTC"

def INFO(strMsg):
  print("getLTTNKCellsDB\tINFO"+"::"+strMsg)
  return

def DEBUG(strMsg):
  print("getLTTNKCellsDB\tDEBUG"+"::"+strMsg)
  return

def WARNING(strMsg):
  print("getLTTNKCellsDB\tWARNING"+"::"+strMsg)
  return

def FATAL(strMsg):
  print("getLTTNKCellsDB\tFATAL"+"::"+strMsg)
  return

###########################################################
# Functions to convert TimeStamp to RunLumi

class TimeStampToRunLumi:
  def __init__(self):
    dbSvc = cool.DatabaseSvcFactory.databaseService()
    self._db=dbSvc.openDatabase("COOLONL_TRIGGER/CONDBR2")
    self._folder=self._db.getFolder("/TRIGGER/LUMI/LBTIME")
    pass

  def __del__(self):
    self._db.closeDatabase()

  def getRunLumi(self,since,until):
    retval=dict()
    itr=self._folder.browseObjects(cool.ValidityKey(since),cool.ValidityKey(until),cool.ChannelSelection(0))
    while itr.goToNext():
      obj=itr.currentRef()
      payload=obj.payload()
      pl=obj.payload()
      run=pl["Run"]
      lb=pl["LumiBlock"]
      if run not in retval:
        retval[run]=set()
        retval[run].add(lb)
        itr.close()
      return retval

  def getRunLumi(self,pointInTime):
    obj=self._folder.findObject(cool.ValidityKey(pointInTime),0)
    pl=obj.payload()
    run=pl["Run"]
    lb=pl["LumiBlock"]
    return run,lb
  def getRunLumi(self,tmstmp,guard=1):
    dbSvc = cool.DatabaseSvcFactory.databaseService()
    db=dbSvc.openDatabase("oracle://ATLAS_COOLPROD;schema=ATLAS_COOLONL_TRIGGER;dbname=CONDBR2")
    folder=db.getFolder("/TRIGGER/LUMI/LBTIME")
    range=guard*24*60*60*1e9 # 2 days in ns
    t1=int(tmstmp-range)
    t2=int(tmstmp+range)
    itr=folder.browseObjects(t1,t2,cool.ChannelSelection.all())
    while itr.goToNext():
      obj=itr.currentRef()
      #print "Working on obj.until() =",asctime(localtime(obj.until()/1e9))
      if obj.until()>tmstmp:
        pl=obj.payload()
        run=pl["Run"]
        lb=pl["LumiBlock"]
        print("Found Run/Lumi [%i/%i] lasting from %s to %s" %\
          (run,lb,time.strftime("%Y-%m-%d:%H:%M:%S/%Z",localtime(obj.since()/1e9)),time.strftime("%Y-%m-%d:%H:%M:%S/%Z",localtime(obj.until()/1e9))))
        itr.close()
        db.closeDatabase()
        return (run,lb)
    print("ERROR: No run/lumi block found for time",asctime(localtime(tmstmp/1e9)),"in folder /TRIGGER/LUMI/LBTIME of DB COOLONL_TRIGGER/CONDBR2")
    itr.close()
    db.closeDatabase()
    return None

###########################################################
# Functions to convert RunLumi to TimeStamp

class RunLumiToTimeStamp:
  def __init__(self):
    #from CoolConvUtilities.AtlCoolLib import indirectOpen
    dbSvc = cool.DatabaseSvcFactory.databaseService()
    self._db=dbSvc.openDatabase("COOLONL_TRIGGER/CONDBR2")
    self._folder=self._db.getFolder("/TRIGGER/LUMI/LBLB")
    pass

  def __del__(self):
    self._db.closeDatabase()

  def getRunStartStop(self,run,lb):
    t1=None
    t2=None
    itr=self._folder.browseObjects(cool.ValidityKey(run<<32),cool.ValidityKey((1+run)<<32),cool.ChannelSelection(0))
    while itr.goToNext():
      obj=itr.currentRef()
      pl=obj.payload()
      if t1 is None: t1=pl["StartTime"]
      t2=pl["EndTime"]
      pass
    return (t1,t2)

###################################################
# Open connection to database (with error checking)

def openCOOLCondDB(connStr):
  # get the db service
  dbSvc=cool.DatabaseSvcFactory.databaseService()
  # open db connection; let's not assume this will automagically work and so give it a "try":
  try:
    db=dbSvc.openDatabase(connStr, True) # second parameter TRUE if opening read-only (which we are)
  except Exception as e:
    FATAL("Problem opening database")
    print(e)
    db=None
  return db

#################################
# Get the list of TTDisabledCells

def listTTDCNodes(db,verbose):
  if verbose >1 :
    INFO("Searching nodes for /LAR/BadChannels/TTDisabledCells")
  # get the list of nodes from the db
  nodeList=db.listAllNodes()
  # store the nodes of interest
  nodes=[]
  for n in nodeList:
    node=str(n)
    if node.startswith("/LAR/BadChannels/TTDisabledCells"):
      nodes.append(node)
  return (nodes)

########################################
# Dum the tags in this folder
def dumptag(folder):
  taglist=folder.listTags()
  print("Tags in folder:")
  for i in taglist: print(i)


########################################
# Dumps the contents of a COOL db folder
# Good for debugging

def dumpFolder(folder,tag,runnum,lb):
  print(runnum)
  # count of the number of objects in `folder'
  nobjs = folder.countObjects(cool.ValidityKey(runnum>>32 +lb),
                              cool.ValidityKeyMax,
                              cool.ChannelSelection.all(),
                              tag)
  # print the
  #DEBUG("Number of objects "+str(nobjs))
  # collection of objects in the folder
  objs = folder.browseObjects(runnum >> 32+lb,
                              cool.ValidityKeyMax,
                              cool.ChannelSelection.all(),
                              tag)
  i = 0
  print(nobjs)
  while objs.goToNext():
    obj = objs.currentRef()
    print("since [r,l]: [", obj.since() >> 32,',',obj.since()%0x100000000,']', end=' ')
    print("until [r,l]: [", obj.until() >> 32,',',obj.until()%0x100000000,']', end=' ')
    print("payload", obj.payload(), end=' ')
    print("chan",obj.channelId())
    i += 1
  objs.close()
  
#####################################################
# Gets the total number of lumiblocks in a given run.
# N.B. There must be a better way to do this...

def getNumLBs(folder,tag,runnum,runnume,lb):
  # collection of objects in the folder
  objs = folder.browseObjects(cool.ValidityKeyMin,
                              cool.ValidityKeyMax,
                              cool.ChannelSelection.all(),
                              tag)
  while objs.goToNext():
    obj = objs.currentRef()
    # find first occurence of given runNumber in `since'
    if (obj.since() >> 32) == runnum and 0==(obj.since()%0x100000000):
      print("since [r,l]: [", obj.since() >> 32,',',obj.since()%0x100000000,']', end=' ')
      print("until [r,l]: [", obj.until() >> 32,',',obj.until()%0x100000000,']')
      # found our starting blob
      break;
  while objs.goToNext():
    nextObj=objs.currentRef()
    if runnume == None:
      if (nextObj.until() >> 32) > runnum:
        print("since [r,l]: [", nextObj.since() >> 32,',',nextObj.since()%0x100000000,']', end=' ')
        print("until [r,l]: [", nextObj.until() >> 32,',',nextObj.until()%0x100000000,']')
        break;
    else:
      if (nextObj.until() >> 32) <= runnume:
        print("since [r,l]: [", nextObj.since() >> 32,',',nextObj.since()%0x100000000,']', end=' ')
        print("until [r,l]: [", nextObj.until() >> 32,',',nextObj.until()%0x100000000,']')
      else:
        print("since [r,l]: [", nextObj.since() >> 32,',',nextObj.since()%0x100000000,']', end=' ')
        print("until [r,l]: [", nextObj.until() >> 32,',',nextObj.until()%0x100000000,']')
        break;

  objs.close()

##########################################################################################
# Find disabled cells object provided run+LB, if it exists.
# Returns the object corresponding to the provided run+LB, if it exists, `None' otherwise.

def findObjByRunLB(folder,tag,runnum,lb):
  runiov=(runnum<<32)+lb
  try:
    obj=f.findObject(runiov,1)
    return obj
  except Exception as e:
    return None

###################################################################################################
# Get the disabled cells in provided run interval/for a given run.
# Returns a list of disabled cells for specified run intervals, if it exists. `None' otherwise.

def getDisabledCellsByRunLB(folder,tag,run1,run2,lb1,lb2,verbose):
  itr = 0
  data = [] 
  # Now, we have several cases to cover. We'll enumerate them for clarity
  # First, we query only one given runnum, 1)with or 2)without a lumiblock. If without one provided
  # the default lb = 3000 (i.e. end of the run) is used.

  if run2 is None and lb2 is None:
    obj=findObjByRunLB(folder,tag,run1,lb1)
    if obj is not None:
      #DEBUG("Object found for disabled cells corresponding to (runNumber,LB)=("+str(runnum)+","+str(lb)+")")
      # we have a candidate -- get the payload/blob
      blob=obj.payload()["testTTs"]
      data.append([run1,(obj.since() & 0xFFFFFFFF),getBlobData(blob)])
      print(data)
      return data
    else:
      WARNING("No object found for disabled cells corresponding to (runNumber,LB)=("+str(run1)+","+str(lb1)+")")
      return None
  
  # Next, 3) Query multiple run numbers, but without lumiblock information.
  elif lb1 is None and lb2 is None:
    if verbose >1 : 
      INFO("Getting disabled cells from runNumber "+str(run1)+" to "+str(run2) )
    itr = folder.browseObjects(cool.ValidityKey((run1+1)<<32),cool.ValidityKey((run2+1)<<32),
        cool.ChannelSelection(1),tag) 
  
  # Next, 4) Query same run number for a given lumiblock interval.
  elif run2 is None:
    if verbose :
      INFO("Getting disabled cells for runNumber "+str(run1)+"Lumiblock "+str(lb1)+"to "+str(lb2))
    itr=folder.browseObjects(cool.ValidityKey((run1<<32)+lb1),cool.ValidityKey((run1<<32)+lb2),
        cool.ChannelSelection(1),tag)
  
  if itr is not None:
    while itr.goToNext():
      obj=itr.currentRef()
      # we have a candidate -- get the payload/blob
      blob=obj.payload()["testTTs"]
      if (getBlobData(blob) != None):
        data.append([obj.since()>>32,(obj.since() & 0xFFFFFFFF),getBlobData(blob)])
    itr.close()
    itr = 0 # Blatant misuse of variable re-assignment
    # Make a copy of the data vector for substitution
    data2 = copy.deepcopy(data)
    while itr<len(data[:])-1:
      # Here, you want to check the same run number and only re-assign
      # if the new IOV list is longer than the old one. 
      # This counting protects the case of multiple disabling due to TTC Restart
      size_next = sum(len(x) for x in data[itr+1][2])
      size_current = sum(len(x) for x in data[itr][2])
      # But, now you get weird cases where you quickly disable two cells in one LB. So, more tricks
      if data[itr][0] == data[itr+1][0] and ((size_next > size_current) or (len( data[itr+1][2] ) >  len( data[itr][2] ))):
        # Flatten the output using chain from itertools
        # Then, look for the unique values in the TT,Ch pair 
        c = (list(set(map(tuple,list(chain(*data[itr+1][2])))) ^ set(map(tuple,list(chain(*data[itr][2]))))))
        # Overwrite the data entry in the appropriate format for TT,Ch Pairs
        data2[itr+1][2] = [[list(x) for x in c]]
      itr+=1
    return data2
  else:
    if(lb1 and lb2) is None:
      WARNING("No object found for disabled cells corresponding to (run1,run2)=("+str(run1)+","+str(run2)+")")
    else:
      WARNING("No object found for disabled cells corresponding to (run1,LB1,LB2)=("+str(run1)+","+str(lb1)+","+str(lb2)+")")
    return None

####################################################################################################
# Get the disabled cells in provided time interval, if it exists.
# Returns a list of disabled cells for specified interval, if it exists, `None' otherwise.

def getDisabledCellsByTS(folder,tag,timestart,timestop):
  itr = 0
  # first check if we're checking multiple runs 
  tstrl=TimeStampToRunLumi()
  r1,l1=tstrl.getRunLumi(timestart)
  r2,l2=tstrl.getRunLumi(timestop)
  if r1==r2:
    getDisabledCellsByRunLB(folder,tag,r1,None,l1,l2)
  else:
    getDisabledCellsByRunLB(folder,tag,r1,r2,None,None)

##################################################################################################
# Parses blobs, assuming the format:
#   1st 4 bytes: TTID; 2nd 4 bytes: NumDisabledCells; next NumDisabledCells*4 bytes: DisabledCells
# Returns a list TTIDs and associated disabled channels:
#   [[TTID1,[DisabledChan1,...,DisabledChanN]],...,[TTIDN,[DisabledChan1,...,DisabledChanN]]]

def getBlobData(b):
  # to check if it's worth parsing, get the blob size
  minsize=12
  size=b.size()
  # based on the assumed format, a worthy blob should be at least 12 bytes (minsize),
  # i.e. TTID(4 bytes)+DisabledCell(4 bytes)+DisableCellID(4 bytes)
  # means we have only one TTID with a single disabled cell
  if size < minsize:
    return None
  else: # we have a worthy blob -- parse the hell out of it!
    # put the blob contents into a buffer
    #DEBUG("blob size="+str(size))
    # number of bytes for 'unpack'
    nbytesUnpack=4
    # get the starting address of the blob
    buf=b.startingAddress()
    # adjust size of buffer
    buf.reshape((size,))
    Nelements=int(size/nbytesUnpack) # read the blob 4-by-4 bytes
    # store the data we want
    data=[]
    data=extractTtidAndDcs(buf,Nelements)
    return data


######################################################################
# Extracts TTID and associated disabled channels from provided string.
# Returns list of format: [TTID,[DC1,...,DCN]]
# where DC is a disabled channel.

def extractTtidAndDcs(buf,Nelements):
  #https://cppyy.readthedocs.io/en/latest/lowlevel.html?highlight=cppyy.LowLevelView#numpy-castsx
  #put the buffer into an np array of the form: TT1, N disabled channels, [disabled chan], TT2...
  arrayedBlob = np.frombuffer(buf, dtype=np.int32, count=Nelements)
  elementToRead=0
  # where to store the dcs
  dcs=[]
  while elementToRead+2<np.size(arrayedBlob):
    ttid=hex(arrayedBlob[elementToRead])
    elementToRead+=1
    #print("elementToRead",elementToRead)
    # Number of disabled channels is the second element
    nDcs=int(arrayedBlob[elementToRead])
    # get the dcs!
    for i in range(0,nDcs):
      # append the dc
      elementToRead+=1
      dcs.append([ttid,hex(arrayedBlob[elementToRead])])
    elementToRead+=1
  return dcs

#######################################################$
# Extracts decimal value from buffer unpack (i.e. tuple)
def unpackToHex(t):
  return hex(t[0])

########################################
# Print TTID and associated DCs fancily.
def printFancily(data,runnum,output,outdchs):
  if output != "":
    if "." not in output:
      sys.stdout = open(output+".txt","w")
    else:
      sys.stdout = open(output,"w")
  lb = 0
  rlbt=RunLumiToTimeStamp()
  runlbtime = rlbt.getRunStartStop(runnum,lb)
  print("")
  print("*******************************************************************************************")
  print("***                      N.B. Still in beta! Please cross-check with                    ***")
  print("*** https://atlasop.cern.ch/twiki/bin/view/Main/LArTriggerTowerNoiseKillerActionHistory ***")
  print("***                       and note any discrepancies to the author!                     ***")
  print("*******************************************************************************************")
  print("")
  print("==========================================================")
  print("   RUN#  \t  LB#  \t     TTID\t   Cell ID")
  print("==========================================================")
  
  # number of TTIDs with disabled cells
  nTTID = 0
  for i in range(0,len(data[:])):
    nTTID+=len(data[:][i][2])
  # total number of disabled cells
  nDCs=0
  # total number of runs
  nruns=0
  runtmp=0
  # loop over the TTIDs and print associated DCs
  Chs=[]
  for d in data:
    # get Run Number
    runn=d[0]
    if runtmp == 0 or runtmp != runn:
      nruns+=1
      runtmp = runn
    # get LB Number
    lbn=d[1]
    # get TTID list
    ttid_list=d[2]
    if ttid_list == [[]]:
      ttid_list = []
    if not ttid_list:
      continue
    for i in range(0,len(ttid_list[:])):
      ttid=ttid_list[i][0]
      # get DCs
      dcs=ttid_list[i][1]  # due to changes in query, no longer a list
      Chs.append(dcs)
      print(" ",runn,"\t ",lbn,"\t  ",ttid,"\t ",dcs)
    print("----------------------------------------------------------")
  print("")
  print("Summary of TTIDs and disabled cells for run: "+str(runnum))
  print("Total # of Runs: "+str(nruns))
  print("Total TTID/Cell Pairs: "+str(nTTID))
  if( outdchs == True):
    # Count the most frequent channels
    mfchs = Counter(Chs)
    a = mfchs.most_common()
    print("====================================")
    print("   Cell ID  \t # of Times Disabled ")
    print("====================================")
    for i in range(0,len(a)):
      print(" ",a[i][0], " \t\t ", a[i][1])

def return_list(data,runnum):
  lb=0
  rlbt=RunLumiToTimeStamp()
  runlbtime = rlbt.getRunStartStop(runnum,lb)
  # number of TTIDs with disabled cells
  nTTID = 0
  for i in range(0,len(data[:])):
    nTTID+=len(data[:][i][2])
  # total number of disabled cells
  nDCs=0
  # total number of runs
  nruns=0
  runtmp=0
  # loop over the TTIDs and print associated DCs
  Chs=[]
  disabled_cell_list=list()
  for d in data:
    # get Run Number
    runn=d[0]
    if runtmp == 0 or runtmp != runn:
      nruns+=1
      runtmp = runn
    # get LB Number
    lbn=d[1]
    # get TTID list
    ttid_list=d[2]
    if ttid_list == [[]]:
      ttid_list = []
    if not ttid_list:
      continue

    for i in range(0,len(ttid_list[:])):
      ttid=ttid_list[i][0]
      # get DCs
      dcs=ttid_list[i][1]  # due to changes in query, no longer a list
      Chs.append(dcs)
      disabled_cell_list.append({"run":runn,"lb":lbn,"TT":ttid,"Id":dcs})
      
  return disabled_cell_list

##############
# Main program
def Main(r1,r2,lb=0,slb=None,outputfile=None,ts=0,tf=0,dchsp=False,style=False,verbose=0):

  if ts and tf is not None:
    tsToRl=TimeStampToRunLumi()
    print(args.ts, args.tf)
    ts = time.strptime(args.ts+'/CET','%Y-%m-%d:%H:%M:%S/%Z')
    tf = time.strptime(args.tf+'/CET','%Y-%m-%d:%H:%M:%S/%Z')
    ## stupid conversion to UTC
    os.environ['TZ']='Europe/Zurich'
    time.tzset()
    if time.strftime("%Z",time.localtime(time.time()))=='CET': dst=1
    else: dst=2
    ts2 = time.struct_time((ts[0],ts[1],ts[2],ts[3]-dst,ts[4],ts[5],ts[6],ts[7],ts[8]))
    tf2 = time.struct_time((tf[0],tf[1],tf[2],tf[3]-dst,tf[4],tf[5],tf[6],tf[7],tf[8]))
    ts=int(timegm(ts2))*1000000000
    tf=int(timegm(tf2))*1000000000
    r1,lb = tsToRl.getRunLumi(ts,1)
    r2,slb = tsToRl.getRunLumi(tf,1)
  # check for lower-bound runNumber (the oldest runNumber available in the DB is 303819)
  if r1 < 303819 or (r2 != None and r2 < 303819):
    FATAL("Supplied `runNumber' is invalid; database currently contains only runs as of 303819 (read the -h!).")
    exit()
  if lb < 0:
    FATAL("Supplied `lumiBlock' is invalid; lumiBlock must be greater than 0.")
    exit()

  # pick your flavour
  dbtagStr="LARBadChannelsTTDisabledCells-RUN2-UPD1-00"
  dbStr="frontier://ATLF/()"
  #dbStr="oracle://ATLR"
  schemaStr="schema=ATLAS_COOLONL_LAR"
  dbnameStr="dbname=CONDBR2"
  dbuserStr="user=ATLAS_COOL_READER"
  dbpassStr="password=COOLRED4PRO"
  connStr=dbStr+";"+schemaStr+";"+dbnameStr#+";"+dbuserStr+";"+dbpassStr
  
  # previous connection string
  #connStr="sqlite://;schema=/det/lar/project/databases/LArTTNoiseKiller.db;dbname=CONDBR2"
  
  # connect to the db
  if verbose >1 :
    INFO("Using connection string "+connStr)
    INFO("Attempting to open database...")
  db=openCOOLCondDB(connStr)
  # are we connected?
  if db is not None:
    if verbose >1 :
      INFO("Opened database "+connStr)
  
    # get the list of all nodes in the db
    ttdcNodes=listTTDCNodes(db,verbose)
    # why run this if there were no interesting nodes found?
    if ttdcNodes:
      # number of nodes found
      numNodesStr=str(len(ttdcNodes))
      if verbose >1 :
        INFO("Node found. Looping over node.")
      # loop over folders in the TTDisabledCell nodes found
      for ttdcnode in ttdcNodes:
        f=db.getFolder(ttdcnode)
        #dumptag(f)
        #dumpFolder(f,"LARBadChannelsTTDisabledCells-RUN2-UPD1-00", r1,lb)
        #getNumLBs(f,"LARBadChannelsTTDisabledCells-RUN2-UPD1-00",r1,339590,lb)
        chans=f.listChannels()
        # Now, we'll call the different functions again: 1) & 2) with/out lumiblock provided
        if r2 is None and slb is None:
          data=getDisabledCellsByRunLB(f,dbtagStr,r1,None,lb,None,verbose)
        # Now run #s
        elif r1 is not None and r2 is not None:
          data=getDisabledCellsByRunLB(f,dbtagStr,r1,r2,None,None,verbose)
        # Now lumiblock interval
        elif slb is not None:
          data=getDisabledCellsByRunLB(f,dbtagStr,r1,None,lb,slb,verbose)
        # Now with time stamps
        elif tf is not None and ts is not None:
          data=getDisabledCellsTS(f,dbtagStr,tf,ts)
    if verbose > 1 :
      INFO("Closing database")
    try:
      db.closeDatabase()
    except Exception as e:
      WARNING("Could not properly close database!")
      WARNING("Execution finished, but with errors.")
      exit()
    if verbose >1 :
      INFO("Database closed")
      INFO("Done.")
    if data is not None:
      if style:
        printFancily(data,r1,"",dchsp)
        if outputfile is not None:
          printFancily(data,r1,outputfile,dchsp)
      else : #if one does not want a style return a python list
        return return_list(data,r1)
  else:
    FATAL("Unable to open db "+connStr)


#Local running setup
if __name__ =="__main__":

  ############################
  # setup up an ArgumentParser
  parser=ArgumentParser()
  # add arguments
  parser.add_argument('-r','--runNumber',dest='r1',metavar='r1',type=int,required=False,help='Run number to start search. Db currently contains only runs since 303819')
  
  parser.add_argument('-e','--endrunNumber',dest='r2',metavar='r2',type=int,required=False,help='Run number to end search. Must be greater than r1')
  
  parser.add_argument('-b','--beginTime',dest='ts',metavar='ts',type=str,required=False,help='Time to start query. Can input human readable time (y-m-d:H:M:S) or time since epoch')
  
  parser.add_argument('-f','--finalTime',dest='tf',metavar='tf',type=str,required=False,help='Time at end of query. Can input human readable time (y-m-d:H:M:S) or time since epoch')
  
  parser.add_argument('-l','--lumiBlock',dest='lb',metavar='lumiBlock',type=int,required=False,help='Luminosity block of run number to search.',default=3000)
  
  parser.add_argument('-s','--secondlumiBlock',dest='slb',metavar='secondlumiBlock',type=int,required=False,help='2nd Luminosity block of run number to search.',default=None)
  
  parser.add_argument('-o','--output-file',dest='outputfile',metavar='outputfile',required=False,help='Write TTIDs and corresponding disabled cells to this file.')
  
  parser.add_argument('-d','--disabled-print',dest='dchsp',required=False,action='store_true',help='Print frequency of cell disabling',default=False)

  parser.add_argument('-y','--style-print',dest='style',required=False,action='store_false',help='Print with style',default=True)

  parser.add_argument('-v','--verbose',dest='verbose',required=False,action='store',help='Level of verbose',default=0)

  # get arguments an assign to local variables (less typing...)
  args=parser.parse_args()
  r1=args.r1
  r2=args.r2
  lb=args.lb
  slb=args.slb
  outputfile=args.outputfile
  ts=args.ts
  tf=args.tf
  dchsp=args.dchsp
  style=args.style
  verbose=args.verbose
  Main(r1,r2,lb,slb,outputfile,ts,tf,dchsp,style,verbose)

