#!/bin/python3
#import xml.etree.ElementTree as ET
import os, sys
from xml.dom import minidom
from glob import glob
import time, datetime



#20220818/data22_13p6TeV.periodD_DetStatus-v108-pro28_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore__TRIG_HLT_MUO_Upstream_Barrel_problem__TRIG_HLT_MUO_Upstream_Endcap_problem__TRIG_L1_MUB_coverage__TRIG_L1_MUE_misconf_electronics__TRIG_L1_MUB_lost_sync.xml"

def getRunsFromGRL(grl):
    xmldoc = minidom.parse(grl)
    itemlist = xmldoc.getElementsByTagName('Metadata')
    runs = []
    for i in itemlist:
        if i.attributes['Name'].value == 'RunList':
            for ch in i.childNodes:
                if ch.nodeType == ch.TEXT_NODE:
                    runs.extend([ int(r) for r in ch.data.split(",") ])
    return runs


def getAllRuns(tag="data22_13p6TeV", verbose=False):
    grldir = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/"+tag+"/"
    if verbose: print("Reading GRL from",grldir)
    grlfiles = glob(grldir+"/*/*All_Good*.xml")
    grlfiles.sort(key=os.path.getmtime)
    if verbose: print(len(grlfiles), "files found")
    allruns = []
    for grl in grlfiles:
        filetime = os.path.getmtime(grl)
        filetime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(filetime))
        if verbose: print("**", grl, filetime)
        if not os.access(grl, os.R_OK):
            print("Not permitted to read",grl)
            continue
        runs = getRunsFromGRL(grl)
        if verbose: print(len(runs),"runs:",runs)
        absent = [ ar for ar in allruns if ar not in runs ]
        if verbose: print("absent:",absent)
        for ab in absent:
            if min(runs) < ab:

                if verbose: print("Removing",ab,"from final run list")
                allruns.remove(ab)
        
        allruns.extend(runs)
    allruns = sorted(set(list(allruns)))
    return allruns 

if __name__ == "__main__":
    verbose = False

    if len(sys.argv) > 1:
        tag = sys.argv[1]
        allruns = getAllRuns(tag, verbose=verbose)
    else:
        allruns = getAllRuns(verbose=verbose)


    print()
    print(len(allruns),"runs:")
    for ar in allruns:
        print(ar)
    #print(", ".join([str(ar) for ar in allruns]))

