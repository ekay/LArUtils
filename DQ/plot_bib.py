import sys
import xmlrpc.client
import argparse
import datetime
import numpy as np
from getClusters import getClusters
from getFill import getRunFill

import ROOT as R
R.gROOT.SetBatch(True)

import numpy as np



def readPumpVals(filePath):
    import pandas as pd
    headers = [ "LAR", "TERMO", "EMECC", "ECC6", "Probe4", "temperature", "Date", "Time", "Value" ]
    pumpFile = pd.read_csv(filePath, sep="\s+", names=headers, usecols=["Date", "Time", "Value"])

    def getRootDate(row):
        datetime_str = row.Date+" "+row.Time
        datetime_str=(":".join(datetime_str.split(":")[:-1]))
        date = datetime.datetime.strptime(datetime_str, '%d-%m-%Y %H:%M:%S')
        roodate = R.TDatime(date.year, date.month, date.day, date.hour, date.minute, date.second).Convert(True)
        return roodate
    pumpFile["ROOTdate"] = pumpFile.apply(getRootDate, axis=1)
    return pumpFile.ROOTdate.values, pumpFile.Value.values



###################################################
###                   M  A  I  N                ###
###################################################

parser = argparse.ArgumentParser(description='Beam-induced background.')
parser.add_argument('-n', '--nruns', dest='nruns', default=None, type=int, help='If a run range is not defined - search for this number of runs above high run / below low run')
parser.add_argument('-s', '--lowrun', dest='lowrun', default=None, type=int, help='Search for runs starting from this run number')
parser.add_argument('-e', '--highrun', dest='highrun', default=None, type=int, help='Search for runs with a maximum of this run number')
parser.add_argument('-a', '--ami', dest='ami', default="x", type=str, help='AMI letter to use for cluster info - x or f')
parser.add_argument('-r', '--runlist', dest='runlist', help='Run number which you would like to get information for', type=int, nargs='+', default=None,required=False)
parser.add_argument('--runnb', dest='runnb', action='store_true', default=False, help='Use run nb. instead of date')
#parser.add_argument('--fills', action='store_true', default=False, help='Use fill nb. instead of date')
parser.add_argument('-topN', type=int, default=2, help='Number of top points to label')
parser.add_argument('-lowN', type=int, default=2, help='Number of lowest points to label')
parser.add_argument('--bibFrac', dest='bibFrac', action='store_true', default=False, help='Plot the fraction of clusters in the bib region, divided by total clusters')
#parser.add_argument('--bylumi', action='store_true', help='Divide number of clusters by lumi')
parser.add_argument('--dqmpassfile', type=str, help='DQM pass file', default='/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt')
parser.add_argument('--nonSB', dest='nonSB', action='store_true', help='Also look at non-stable-beam runs')
parser.add_argument('--elow', '--etalow', dest='etalow', default=None, type=float, help='Low eta value for target region')
parser.add_argument('--ehi', '--etahi', dest='etahi', default=None, type=float, help='High eta value for target region')
parser.add_argument('--plow', '--philow', dest='philow', default=None, type=float, help='Low phi value for target region')
parser.add_argument('--phi', '--phihi', dest='phihi', default=None, type=float, help='High phi value for target region')
parser.add_argument('--dcsFile', dest='DCSfile', default=None, type=str, help='path to .txt file with dcs readings to overlay')
args = parser.parse_args()

#etalow = -4 #None
#etahi = -3 #None
#philow = 2 # None
#phihi = None



dqmpassfile = args.dqmpassfile
cred = None
with open(dqmpassfile, "r") as f:
    cred = f.readline().strip()
if cred is None:
    print("Problem reading credentials from",dqmpassfile)
s = xmlrpc.client.ServerProxy('https://'+cred+'@atlasdqm.cern.ch')

run_spec = {'stream': 'physics_CosmicCalo', 'source': 'tier0'}

runs = []
dates = []
lumis = []
nclus = []
lines = []
fills = []


if args.runlist is None:
    if args.highrun is None:
        if args.lowrun is not None:
            if args.nruns is not None:
                args.highrun=args.lowrun+args.nruns
            else:
                args.highrun=s.get_latest_run()
        else:
            args.highrun=s.get_latest_run()

    if args.lowrun is None:
        if args.nruns is not None:
            args.lowrun = args.highrun-args.nruns
        else:
            args.lowrun = args.highrun-1000
    run_spec['low_run'] = args.lowrun
    run_spec['high_run'] = args.highrun 
else:
    run_spec['run_list'] = args.runlist


for key in run_spec.keys():
    print(f'{key}: {run_spec[key]}')

infokeys = ["Run type", "Project tag", "Partition name", "Number of events passing Event Filter", "Run start", "Run end", "Number of luminosity blocks", "Data source", "Detector mask", "Recording enabled", "Number of events in physics streams" ]
beamkeys = ["Max beam energy during run", "Stable beam flag enabled during run", "ATLAS ready flag enabled during run", "Total integrated luminosity", "ATLAS ready luminosity (/nb)"]
run_info = s.get_run_information(run_spec)
for ri in run_info.keys():
    run_info[ri] = { ik:li for ik,li in zip(infokeys,run_info[ri]) }
beam_info = s.get_run_beamluminfo(run_spec)
for bi in beam_info.keys():
    beam_info[bi] = { ik:li for ik,li in zip(beamkeys,beam_info[bi]) }
#cluster_thr = "Et > 4/10GeV"
cluster_thr = "Et > 0/0GeV"
for run in sorted(list(beam_info.keys())):
    if beam_info[run]["Stable beam flag enabled during run"] == 0 and args.nonSB is False:
        continue
    lumi=beam_info[run]["ATLAS ready luminosity (/nb)"]
    if lumi == 0: 
        continue

    clusters = getClusters(int(run), ami=args.ami, thr=cluster_thr)[f"stream:{cluster_thr}"]

    if len(clusters) == 0:
        bibclusters = []
        continue

    print("APPENDING",run)
    runs.append(run)
    date_start=datetime.datetime.fromtimestamp(run_info[run]["Run start"])
    dates.append(date_start)
    lumis.append(lumi/1000)
    fill = getRunFill(run)
    fills.append(fill)

    def fillDummy(val, isLow=False):
        if val is None:
            if isLow: 
                return -100000000
            else:
                return 100000000
        else: return val
    etalow_dum = fillDummy(args.etalow, True)
    etahi_dum = fillDummy(args.etahi)
    philow_dum = fillDummy(args.philow, True)
    phihi_dum = fillDummy(args.phihi)
    print(f"Eta range: {etalow_dum} - {etahi_dum}")
    print(f"Phi range: {philow_dum} - {phihi_dum}")

    if philow_dum > phihi_dum:
        bibclusters = [ c[-1] for c in clusters if ( (c[0] > etalow_dum and c[0] < etahi_dum) and (abs(c[1]) > philow_dum or abs(c[1]) < phihi_dum) ) ]
    else:
        bibclusters = [ c[-1] for c in clusters if ( (c[0] > etalow_dum and c[0] < etahi_dum) and (c[1] > philow_dum and c[1] < phihi_dum) ) ]

    def getStr(low, hi, var):
        thestr = ""
        thestr_filename = ""

        if low is not None and hi is not None:
            if low == -hi:
                thestr = "|#"+var+"|<"+str(hi)
                thestr_filename = "abs_"+var+"_lt_"+str(hi)
            elif low > hi:
                thestr = f"|#{var}|<{hi} or |#{var}|>{low}"
                thestr_filename = f"{var}_lt_{hi}_or_{var}_gt_{low}"
            else:
                thestr = str(low)+"<#"+var+"<"+str(hi)
                thestr_filename = str(low)+"_lt_"+var+"_lt_"+str(hi)
        elif low is not None:
            thestr = "#"+var+">"+str(low)
            thestr_filename = var+"_gt_"+str(low)
        elif hi is not None:
            thestr = "#"+var+"<"+str(hi)
            thestr_filename = var+"_lt"+str(hi)

        return thestr, thestr_filename

    etastr, etastr_filename = getStr(args.etalow, args.etahi, "eta")
    phistr, phistr_filename = getStr(args.philow, args.phihi, "phi")

    cutstr = ""
    cutstr_filename = ""
    if etastr != "":
        cutstr = etastr
        cutstr_filename = etastr_filename
        if phistr != "":
            cutstr += ", "+phistr
            cutstr_filename += "_"+cutstr_filename
    else:
        cutstr = phistr
        cutstr_filename =  phistr_filename
    #bibclusters = [ c[-1] for c in clusters if abs(c[1]) < 0.2 ]
    #bibclusters = [ c[-1] for c in clusters if ( ( c[0] < -3 and c[0] > -4) and (c[1] >2 ) ) ]

    clusters = [ c[-1] for c in clusters ]
    #print(bibclusters)
    n = sum(bibclusters)
    #if args.bylumi:
    if lumis[-1] != 0:
        n = n/lumis[-1]
    else:
        n = 0
        

    if args.bibFrac is True:
        if len(clusters) == 0:
            n = 0
        else:
            #n = len(bibclusters)/len(clusters)
            n = sum(bibclusters)/sum(clusters)
        lumiscale_bibfrac = False
        if lumiscale_bibfrac:
            if lumis[-1] != 0:
                n = n/lumis[-1]
            else:
                n = 0
        print(f"Run {run} {run_info[run]['Run type']} {run_info[run]['Project tag']} fill {fill} Ldt {round(lumi/1000,2)} pb-1. {len(clusters)} cluster hot spots ({sum(clusters)}) -> {len(bibclusters)} ({str(sum(bibclusters))}) in BIB region. (bib / all) = {round(n,5)}")
    else:
        print("Run",run, run_info[run]["Run type"], run_info[run]["Project tag"],"fill",fill, "Ldt", round(lumi/1000,2), "pb-1.", len(clusters), "cluster hot spots (",sum(clusters),") ->", len(bibclusters), " ("+str(sum(bibclusters))+") in BIB region. / L = ", round(n,5))
            

    nclus.append(n)


if len(runs)==0:
    print("No eligible runs to plot. Exiting")
    sys.exit()

def plotClusLumi():
    def formatgr(gr, col, style=20, size=1):
        gr.SetMarkerColor(col)
        gr.SetLineColor(col)
        gr.SetMarkerStyle(style)
        gr.SetMarkerSize(size)
        gr.SetTitle("")

    if args.runnb is True:
        xvals = np.array(runs, dtype="d")
        xtitle = "Run #"
        maxdigits = 6
    else:
        #xvals = [ int(d.strftime('%y%m%d%H%M%S')) for d in dates ]
        xvals = [ R.TDatime(d.year, d.month, d.day, d.hour, d.minute, d.second).Convert(True) for d in dates ]
        xvals = np.array(xvals, dtype = "d")
        xtitle = "Date"
        #maxdigits = 12
        maxdigits = 8

    #print("OIOIOI", len(runs),"runs,",len(xvals),"cluster vals")
    g_nclus = R.TGraph(len(runs), xvals, np.array(nclus, dtype="d"))
    
    formatgr(g_nclus, R.kBlue)

    #  label top 2 
    topN_idx = np.argsort(nclus)[-args.topN:]
    lowN_idx = np.argsort(nclus)[::-1][-args.lowN:]
    labels = []
    for idx in topN_idx:
        print(runs[idx], fills[idx], nclus[idx], g_nclus.GetX()[idx], g_nclus.GetY()[idx])
        latex = R.TLatex(g_nclus.GetX()[idx], g_nclus.GetY()[idx],str(runs[idx])+" (fill "+str(fills[idx])+")")
        latex.SetTextSize(0.025)
        latex.SetTextAngle(90)
        g_nclus.GetListOfFunctions().Add(latex)
        labels.append(latex)
    for idx in lowN_idx:
        print(runs[idx], fills[idx], nclus[idx], g_nclus.GetX()[idx], g_nclus.GetY()[idx])
        latex = R.TLatex(g_nclus.GetX()[idx], g_nclus.GetY()[idx],str(runs[idx])+" (fill "+str(fills[idx])+")")
        latex.SetTextSize(0.025)
        latex.SetTextAngle(90)
        g_nclus.GetListOfFunctions().Add(latex)
        labels.append(latex)

        #latex2 = R.TLatex(g_nclus.GetX()[idx]+0.02, g_nclus.GetY()[idx]-0.05,"(fill "+str(fills[idx])+")")
        #latex2.SetTextSize(0.025)
        #g_nclus.GetListOfFunctions().Add(latex2)
        #labels.append(latex2)


    g_lumi = R.TGraph(len(runs),
                      xvals,
                      np.array(lumis, dtype="d"))
    formatgr(g_lumi, R.kRed)


    # OIOI here add option to truncate to same dates 
    g_dcs = None
    if args.DCSfile is not None:
        DCStimes, DCSvals  =  readPumpVals(args.DCSfile)
        g_dcs = R.TGraph(len(DCStimes), DCStimes.astype('d'), DCSvals.astype('d'))
        formatgr(g_dcs, R.kOrange)


    canv = R.TCanvas()
    canv.objs = []
    p1 = R.TPad("p1", "", 0, 0, 1, 1)
    p2 = R.TPad("p2", "", 0, 0, 1, 1)
    p1.SetFillStyle(4000) # transparent
    p2.SetFillStyle(4000) # transparent


    p1.Draw()
    p1.cd()
    g_nclus.Draw("APl")
    g_nclus.GetXaxis().SetTitle(xtitle)
    if args.runnb is True:
        g_nclus.GetXaxis().SetMaxDigits(maxdigits)
    else:
        g_nclus.GetXaxis().SetTimeDisplay(1)
        g_nclus.GetXaxis().SetTimeOffset(0)
        g_nclus.GetXaxis().SetNdivisions(503)
        g_nclus.GetXaxis().SetTimeFormat("%d/%m/%y")
    g_nclus.GetXaxis().SetTitleOffset(1.3)
    if args.bibFrac is True:
        #g_nclus.GetYaxis().SetTitle("( # (noisy clusters, |#phi|<0.2) / # (noisy clusters) ) / L [pb^{-1}]")  #oioioio change
        g_nclus.GetYaxis().SetTitle(f"# (Clusters in {cutstr}) / # (Clusters with {cluster_thr}) ")  
        g_nclus.GetHistogram().SetMaximum(1)
    else:
        g_nclus.GetYaxis().SetTitle("# (Clusters in "+cutstr+") / L [pb^{-1}]")  #oioioio change
    p1.Update()
    canv.objs.append(p1)

    if args.bibFrac is True:
        outname = "plotBIBfrac_"+str(runs[0])+"_"+str(runs[-1])+"_"+str(args.ami)+"tag"
    else:
        outname = "plotBIB_"+str(runs[0])+"_"+str(runs[-1])+"_"+str(args.ami)+"tag"

    if cutstr_filename != "": outname += "_"+cutstr_filename
    canv.Print(outname+".pdf")
    canv.Print(outname+".png")
    
    def plotSecond(gr, ytitle, outstr):
        xmin = p1.GetUxmin()
        xmax = p1.GetUxmax()
        dx = (xmax - xmin) / 0.8 # 10 percent margins left and right
        ymin = gr.GetHistogram().GetMinimum()
        ymax = gr.GetHistogram().GetMaximum()
        dy = (ymax - ymin) / 0.8 # 10 percent margins top and bottom

        p2.Range(xmin-0.1*dx, ymin-0.1*dy, xmax+0.1*dx, ymax+0.1*dy);

        p2.Draw()
        canv.objs.append(p2)
        p2.cd()
        gr.Draw("l")
        gr.GetXaxis().SetTitle(xtitle)
        gr.GetXaxis().SetMaxDigits(6)
        gr.GetYaxis().SetTitle(ytitle)

        axis = R.TGaxis(xmax, ymin, xmax, ymax, ymin, ymax, 510, "+L");
        axis.SetLineColor(gr.GetLineColor())
        axis.SetLabelColor(gr.GetLineColor())
        axis.SetTitleColor(gr.GetLineColor())
        axis.SetTitle(gr.GetYaxis().GetTitle())
        axis.SetTitleOffset(1.3)
        axis.Draw()

        thisoutname = outname + outstr
        canv.Print(thisoutname+".pdf")
        canv.Print(thisoutname+".png")

    plotSecond(g_lumi, "ATLAS ready luminosity [pb^{-1}]", "_vsLumi")

    if g_dcs is not None:
        p2.Clear()
        plotSecond(g_dcs, "LAR TERMO EMECC ECC6 Probe4 temperature", "_vsProbeTemp")



print(len(runs), "runs", runs[0], runs[-1])
plotClusLumi()

