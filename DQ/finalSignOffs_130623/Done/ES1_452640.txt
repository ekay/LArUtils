Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1155

== Noisy cells update for run 452640 (Sun, 21 May 2023 19:29:13 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x743_h421)
Cluster matching: based on Et > 4/10GeV plots requiring at least 150 events
Flagged cells:29
Flagged in PS:6
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:23
SBN in PS:4
HNHG:6
HNHG in PS:2

*****************
The treated cells were:
0 0 0 7 68 0 sporadicBurstNoise  # 0x38034400 -> 0x2d22883c
0 0 0 7 69 0 sporadicBurstNoise  # 0x38034500 -> 0x2d228a3c
0 0 0 7 70 0 sporadicBurstNoise  # 0x38034600 -> 0x2d228c3c
0 0 0 7 71 0 sporadicBurstNoise  # 0x38034700 -> 0x2d228e3c
0 0 4 13 37 0 sporadicBurstNoise  # 0x38262500 -> 0x2d4052bc
0 0 5 1 5 0 highNoiseHG  # 0x38280500 -> 0x2d000228
0 0 5 1 6 0 sporadicBurstNoise  # 0x38280600 -> 0x2d000428
0 0 8 6 31 0 sporadicBurstNoise  # 0x38429f00 -> 0x2d223e1e
0 0 9 1 4 0 highNoiseHG  # 0x38480400 -> 0x2d000018
0 0 9 1 5 0 sporadicBurstNoise  # 0x38480500 -> 0x2d000218
0 0 15 14 40 0 sporadicBurstNoise  # 0x387ea800 -> 0x2d406c0e
0 0 16 13 19 0 sporadicBurstNoise  # 0x38861300 -> 0x2d4049f8
0 0 17 2 12 0 sporadicBurstNoise  # 0x38888c00 -> 0x2d20187a
0 0 17 10 20 0 sporadicBurstNoise  # 0x388c9400 -> 0x2d602bee
0 0 17 10 21 0 highNoiseHG  # 0x388c9500 -> 0x2d602bec
0 0 18 1 5 0 sporadicBurstNoise  # 0x38900500 -> 0x2d000274
0 0 24 8 11 0 sporadicBurstNoise  # 0x38c38b00 -> 0x2d23165e
0 0 24 8 12 0 sporadicBurstNoise  # 0x38c38c00 -> 0x2d23185e
0 0 25 2 5 0 sporadicBurstNoise  # 0x38c88500 -> 0x2d200a5a
0 1 11 13 106 0 sporadicBurstNoise  # 0x395e6a00 -> 0x2dc054bc
0 1 14 1 77 0 sporadicBurstNoise  # 0x39704d00 -> 0x2d804a3a
0 1 21 5 101 0 sporadicBurstNoise  # 0x39aa6500 -> 0x2da1ca56
0 1 21 5 102 0 sporadicBurstNoise  # 0x39aa6600 -> 0x2da1cc56
0 1 21 5 103 0 sporadicBurstNoise  # 0x39aa6700 -> 0x2da1ce56
0 1 23 2 1 0 sporadicBurstNoise  # 0x39b88100 -> 0x2da0025c
0 1 23 2 2 0 sporadicBurstNoise  # 0x39b88200 -> 0x2da0045c
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 122 0 highNoiseHG  # 0x3a53fa00 -> 0x311c1000

