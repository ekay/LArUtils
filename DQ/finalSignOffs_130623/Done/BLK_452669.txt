Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1156

== Noisy cells update for run 452669 (Fri, 9 Jun 2023 17:57:34 +0200) ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 150 events
Flagged cells:34
Flagged in PS:7
Unflagged by DQ shifters:14
Changed to SBN:2
Changed to HNHG:0
SBN:13
SBN in PS:6
HNHG:21
HNHG in PS:1

*****************
The treated cells were:
0 0 5 1 0 0 highNoiseHG  # 0x38280000 -> 0x2d00002a
0 0 5 1 1 0 sporadicBurstNoise  # 0x38280100 -> 0x2d00022a
0 0 5 1 2 0 sporadicBurstNoise  # 0x38280200 -> 0x2d00042a
0 0 5 1 3 0 sporadicBurstNoise  # 0x38280300 -> 0x2d00062a
0 0 5 1 4 0 sporadicBurstNoise  # 0x38280400 -> 0x2d000028
0 0 26 14 52 0 sporadicBurstNoise unflaggedByLADIeS  # 0x38d6b400 -> 0x2d406b56
0 0 26 14 56 0 sporadicBurstNoise unflaggedByLADIeS  # 0x38d6b800 -> 0x2d406d56
0 0 26 14 61 0 highNoiseHG sporadicBurstNoise  # 0x38d6bd00 -> 0x2d406f54
0 0 26 14 62 0 highNoiseHG sporadicBurstNoise  # 0x38d6be00 -> 0x2d406f52
0 1 8 1 70 0 sporadicBurstNoise  # 0x39404600 -> 0x2d804422
0 1 9 1 5 0 sporadicBurstNoise  # 0x39480500 -> 0x2d800226
1 0 2 2 12 0 sporadicBurstNoise  # 0x3a108c00 -> 0x2ca00038
1 0 2 2 16 0 highNoiseHG sporadicBurstNoise  # 0x3a109000 -> 0x2ca00036
1 0 2 2 17 0 sporadicBurstNoise unflaggedByLADIeS  # 0x3a109100 -> 0x2ca40036
1 0 2 2 18 0 sporadicBurstNoise unflaggedByLADIeS  # 0x3a109200 -> 0x2ca40236
1 0 2 2 20 0 highNoiseHG sporadicBurstNoise  # 0x3a109400 -> 0x2ca00034
1 0 2 2 21 0 sporadicBurstNoise unflaggedByLADIeS  # 0x3a109500 -> 0x2ca40034
1 0 2 2 24 0 unflaggedByLADIeS  # 0x3a109800 -> 0x2ca00032
1 0 2 3 65 0 highNoiseHG sporadicBurstNoise  # 0x3a114100 -> 0x2cc000dc
1 0 2 3 66 0 highNoiseHG sporadicBurstNoise  # 0x3a114200 -> 0x2cc000da
1 0 2 3 67 0 highNoiseHG sporadicBurstNoise  # 0x3a114300 -> 0x2cc000d8
1 0 2 3 69 0 highNoiseHG sporadicBurstNoise  # 0x3a114500 -> 0x2cc400dc
1 0 2 3 70 0 sporadicBurstNoise  # 0x3a114600 -> 0x2cc400da
1 0 2 3 71 0 highNoiseHG sporadicBurstNoise  # 0x3a114700 -> 0x2cc400d8
1 0 2 3 73 0 sporadicBurstNoise unflaggedByLADIeS  # 0x3a114900 -> 0x2cc402dc
1 0 2 3 74 0 sporadicBurstNoise  # 0x3a114a00 -> 0x2cc402da
1 0 2 3 75 0 sporadicBurstNoise  # 0x3a114b00 -> 0x2cc402d8
1 0 2 3 79 0 sporadicBurstNoise  # 0x3a114f00 -> 0x2cc404d8
1 0 2 3 80 0 highNoiseHG unflaggedByLADIeS  # 0x3a115000 -> 0x2cc000d6
1 0 2 3 81 0 highNoiseHG sporadicBurstNoise  # 0x3a115100 -> 0x2cc000d4
1 0 2 3 82 0 sporadicBurstNoise unflaggedByLADIeS  # 0x3a115200 -> 0x2cc000d2
1 0 2 3 83 0 sporadicBurstNoise unflaggedByLADIeS  # 0x3a115300 -> 0x2cc000d0
1 0 2 3 84 0 sporadicBurstNoise  # 0x3a115400 -> 0x2cc400d6
1 0 2 3 85 0 highNoiseHG sporadicBurstNoise  # 0x3a115500 -> 0x2cc400d4
1 0 2 3 86 0 highNoiseHG sporadicBurstNoise  # 0x3a115600 -> 0x2cc400d2
1 0 2 3 88 0 highNoiseHG  # 0x3a115800 -> 0x2cc402d6
1 0 2 3 89 0 sporadicBurstNoise  # 0x3a115900 -> 0x2cc402d4
1 0 2 3 90 0 sporadicBurstNoise unflaggedByLADIeS  # 0x3a115a00 -> 0x2cc402d2
1 0 21 4 33 0 highNoiseHG sporadicBurstNoise  # 0x3aa9a100 -> 0x2cc0012c
1 0 21 4 35 0 highNoiseHG sporadicBurstNoise  # 0x3aa9a300 -> 0x2cc00128
1 0 21 4 38 0 highNoiseHG sporadicBurstNoise  # 0x3aa9a600 -> 0x2cc4012a
1 0 21 4 39 0 highNoiseHG sporadicBurstNoise  # 0x3aa9a700 -> 0x2cc40128
1 0 21 4 48 0 highNoiseHG sporadicBurstNoise  # 0x3aa9b000 -> 0x2cc00126

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1156

== Event Veto for run 452669 (Fri, 26 May 2023 01:33:12 +0200) ==
Found Noise or data corruption in run 452669
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452669_Main.db

Found project tag data23_13p6TeV for run 452669
Found 6 Veto Ranges with 14 events
Found 26 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452669_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452669
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452669_Main.db;dbname=CONDBR2
Found a total of 6 corruption periods, covering a total of 3.00 seconds
Lumi loss due to corruption: 56.64 nb-1 out of 301241.75 nb-1 (0.19 per-mil)
Overall Lumi loss is 56.6385874557449 by 3.001594275 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1156

== Event Veto for run 452669 (Fri, 26 May 2023 00:47:17 +0200) ==
Found Noise or data corruption in run 452669
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452669_Main.db

Found project tag data23_13p6TeV for run 452669
Found 6 Veto Ranges with 14 events
Found 26 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452669_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452669
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452669_Main.db;dbname=CONDBR2
Found a total of 6 corruption periods, covering a total of 3.00 seconds
Lumi loss due to corruption: 56.64 nb-1 out of 301241.75 nb-1 (0.19 per-mil)
Overall Lumi loss is 56.6385874557449 by 3.001594275 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1156

== Missing EVs for run 452669 (Sun, 21 May 2023 22:17:48 +0200) ==

Found a total of 88 noisy periods, covering a total of 0.12 seconds
Found a total of 559 Mini noise periods, covering a total of 0.61 seconds
Lumi loss due to noise-bursts: 2.17 nb-1 out of 301241.75 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 11.42 nb-1 out of 301241.75 nb-1 (0.04 per-mil)
Overall Lumi loss is 13.585375866839486 by 0.72771819 s of veto length


