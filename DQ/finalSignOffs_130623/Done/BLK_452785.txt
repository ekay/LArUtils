Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1161

== Missing EVs for run 452785 (Tue, 23 May 2023 02:15:32 +0200) ==

Found a total of 2 noisy periods, covering a total of 0.00 seconds
Found a total of 7 Mini noise periods, covering a total of 0.01 seconds
Lumi loss due to noise-bursts: 0.02 nb-1 out of 9094.00 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.07 nb-1 out of 9094.00 nb-1 (0.01 per-mil)
Overall Lumi loss is 0.09375055456323003 by 0.009237135 s of veto length


