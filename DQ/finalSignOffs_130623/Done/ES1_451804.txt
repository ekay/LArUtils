Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1127

== Noisy cells update for run 451804 (Fri, 12 May 2023 10:09:28 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x738_h418)
Cluster matching: based on Et &gt; 4/10GeV plots requiring at least 200 events</p>
Flagged cells:154
Flagged in PS:34
Unflagged by DQ shifters:1
SBN:138
SBN in PS:29
HNHG:16
HNHG in PS:5

*****************
The treated cells were:
1 0 10 8 94 0 highNoiseHG # 0x3a53de00
1 0 10 8 78 0 highNoiseHG # 0x3a53ce00
0 0 23 13 23 0 sporadicBurstNoise # 0x38be1700
1 1 12 4 15 0 sporadicBurstNoise # 0x3b618f00
1 1 12 4 14 0 sporadicBurstNoise # 0x3b618e00
1 1 12 4 16 0 sporadicBurstNoise # 0x3b619000
0 0 24 8 11 0 sporadicBurstNoise # 0x38c38b00
0 0 24 8 12 0 sporadicBurstNoise # 0x38c38c00
0 0 24 8 13 0 sporadicBurstNoise # 0x38c38d00
0 1 24 3 11 0 sporadicBurstNoise # 0x39c10b00
0 1 24 3 12 0 sporadicBurstNoise # 0x39c10c00
0 1 24 3 13 0 sporadicBurstNoise # 0x39c10d00
0 1 24 3 10 0 sporadicBurstNoise # 0x39c10a00
0 1 24 3 14 0 sporadicBurstNoise # 0x39c10e00
0 0 9 13 96 0 highNoiseHG # 0x384e6000
0 0 9 10 80 0 sporadicBurstNoise # 0x384cd000
0 0 9 13 100 0 sporadicBurstNoise reflaggedByLADIeS # 0x384e6400
0 1 7 13 13 0 sporadicBurstNoise # 0x393e0d00
0 1 7 10 80 0 sporadicBurstNoise # 0x393cd000
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 1 3 8 27 0 sporadicBurstNoise # 0x391b9b00
1 0 5 7 103 0 sporadicBurstNoise # 0x3a2b6700
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
1 0 2 2 84 0 highNoiseHG # 0x3a10d400
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 sporadicBurstNoise # 0x38480500
0 0 22 7 2 0 sporadicBurstNoise # 0x38b30200
0 0 22 7 1 0 sporadicBurstNoise # 0x38b30100
0 0 22 7 3 0 sporadicBurstNoise # 0x38b30300
0 1 21 11 10 0 sporadicBurstNoise # 0x39ad0a00
1 0 10 8 122 0 highNoiseHG # 0x3a53fa00
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
1 0 10 5 94 0 highNoiseHG # 0x3a525e00
0 1 16 1 48 0 highNoiseHG # 0x39803000
0 1 16 1 43 0 sporadicBurstNoise # 0x39802b00
0 1 16 12 44 0 sporadicBurstNoise # 0x3985ac00
0 1 21 3 10 0 sporadicBurstNoise # 0x39a90a00
0 1 21 3 11 0 sporadicBurstNoise # 0x39a90b00
0 1 21 3 12 0 sporadicBurstNoise # 0x39a90c00
0 0 23 2 6 0 sporadicBurstNoise # 0x38b88600
0 0 23 2 5 0 sporadicBurstNoise # 0x38b88500
0 1 20 6 78 0 sporadicBurstNoise # 0x39a2ce00
0 1 30 1 2 0 sporadicBurstNoise # 0x39f00200
0 0 17 2 12 0 sporadicBurstNoise # 0x38888c00
0 0 22 11 117 0 sporadicBurstNoise # 0x38b57500
0 0 29 14 18 0 sporadicBurstNoise # 0x38ee9200
0 1 8 1 70 0 sporadicBurstNoise # 0x39404600
0 0 11 1 23 0 sporadicBurstNoise # 0x38581700
1 0 21 13 123 0 sporadicBurstNoise # 0x3aae7b00
1 0 21 15 115 0 sporadicBurstNoise # 0x3aaf7300
0 0 30 1 13 0 sporadicBurstNoise # 0x38f00d00
0 0 23 14 35 0 sporadicBurstNoise # 0x38bea300
0 0 16 13 19 0 sporadicBurstNoise # 0x38861300
1 0 2 14 52 0 sporadicBurstNoise # 0x3a16b400
1 0 2 14 56 0 sporadicBurstNoise # 0x3a16b800
1 0 2 14 57 0 sporadicBurstNoise # 0x3a16b900
1 0 2 14 60 0 sporadicBurstNoise # 0x3a16bc00
1 0 2 13 28 0 sporadicBurstNoise # 0x3a161c00
0 0 25 1 24 0 sporadicBurstNoise # 0x38c81800
0 0 25 12 15 0 sporadicBurstNoise # 0x38cd8f00
0 1 24 3 9 0 sporadicBurstNoise # 0x39c10900
0 1 24 3 15 0 sporadicBurstNoise # 0x39c10f00
0 1 24 11 55 0 sporadicBurstNoise # 0x39c53700
0 0 5 1 0 0 highNoiseHG # 0x38280000
0 0 5 1 6 0 sporadicBurstNoise # 0x38280600
0 1 5 14 27 0 sporadicBurstNoise # 0x392e9b00
1 0 19 10 105 0 sporadicBurstNoise # 0x3a9ce900
0 0 13 1 35 0 sporadicBurstNoise # 0x38682300
0 0 13 1 28 0 sporadicBurstNoise # 0x38681c00
0 0 13 1 23 0 sporadicBurstNoise # 0x38681700
0 0 11 1 4 0 sporadicBurstNoise # 0x38580400
0 1 16 6 98 0 sporadicBurstNoise # 0x3982e200
0 1 16 6 99 0 sporadicBurstNoise # 0x3982e300
0 1 16 6 97 0 sporadicBurstNoise # 0x3982e100
0 0 26 14 61 0 sporadicBurstNoise # 0x38d6bd00
0 0 26 14 60 0 sporadicBurstNoise # 0x38d6bc00
0 0 26 14 56 0 sporadicBurstNoise # 0x38d6b800
0 0 26 14 63 0 sporadicBurstNoise # 0x38d6bf00
0 0 26 14 62 0 sporadicBurstNoise # 0x38d6be00
0 0 26 14 52 0 sporadicBurstNoise # 0x38d6b400
0 0 26 14 57 0 highNoiseHG # 0x38d6b900
0 0 26 14 53 0 &nbsp;unflaggedByLADIeS # 0x38d6b500
0 0 26 10 96 0 highNoiseHG # 0x38d4e000
0 0 26 10 100 0 sporadicBurstNoise # 0x38d4e400
0 0 26 14 59 0 sporadicBurstNoise # 0x38d6bb00
0 0 26 14 55 0 sporadicBurstNoise # 0x38d6b700
0 0 26 10 101 0 highNoiseHG # 0x38d4e500
0 0 26 10 104 0 sporadicBurstNoise # 0x38d4e800
0 0 26 10 97 0 sporadicBurstNoise reflaggedByLADIeS # 0x38d4e100
0 0 26 14 24 0 sporadicBurstNoise # 0x38d69800
0 0 26 14 48 0 sporadicBurstNoise # 0x38d6b000
0 0 26 14 28 0 sporadicBurstNoise # 0x38d69c00
0 0 26 14 29 0 highNoiseHG # 0x38d69d00
0 0 26 14 20 0 sporadicBurstNoise # 0x38d69400
1 0 2 13 24 0 sporadicBurstNoise # 0x3a161800
1 0 2 14 53 0 sporadicBurstNoise # 0x3a16b500
1 0 2 14 61 0 sporadicBurstNoise # 0x3a16bd00
0 0 25 1 22 0 sporadicBurstNoise # 0x38c81600
0 0 15 5 62 0 sporadicBurstNoise # 0x387a3e00
0 0 15 5 63 0 sporadicBurstNoise # 0x387a3f00
0 1 8 10 48 0 sporadicBurstNoise # 0x3944b000
0 1 18 12 76 0 sporadicBurstNoise # 0x3995cc00
1 0 11 8 12 0 sporadicBurstNoise # 0x3a5b8c00
0 1 23 2 1 0 sporadicBurstNoise # 0x39b88100
0 1 23 2 2 0 sporadicBurstNoise # 0x39b88200
0 0 25 14 14 0 sporadicBurstNoise # 0x38ce8e00
0 0 25 1 64 0 highNoiseHG # 0x38c84000
0 0 17 7 54 0 sporadicBurstNoise # 0x388b3600
0 0 17 7 55 0 sporadicBurstNoise # 0x388b3700
0 1 8 1 77 0 sporadicBurstNoise # 0x39404d00
0 1 8 1 67 0 sporadicBurstNoise # 0x39404300
0 1 11 1 15 0 sporadicBurstNoise # 0x39580f00
0 1 10 13 35 0 sporadicBurstNoise # 0x39562300
0 1 10 13 37 0 sporadicBurstNoise # 0x39562500
0 1 10 13 34 0 sporadicBurstNoise # 0x39562200
0 1 10 13 33 0 sporadicBurstNoise # 0x39562100
0 1 10 13 36 0 sporadicBurstNoise # 0x39562400
0 1 10 13 32 0 sporadicBurstNoise # 0x39562000
0 1 10 13 40 0 sporadicBurstNoise # 0x39562800
0 1 10 13 38 0 sporadicBurstNoise # 0x39562600
0 0 15 14 36 0 sporadicBurstNoise # 0x387ea400
0 0 28 1 15 0 sporadicBurstNoise # 0x38e00f00
0 0 29 9 27 0 sporadicBurstNoise # 0x38ec1b00
0 1 30 1 112 0 sporadicBurstNoise # 0x39f07000
0 0 13 1 24 0 sporadicBurstNoise # 0x38681800
0 0 13 1 27 0 sporadicBurstNoise # 0x38681b00
0 1 17 1 40 0 sporadicBurstNoise # 0x39882800
0 1 16 11 125 0 sporadicBurstNoise # 0x39857d00
0 1 9 1 21 0 sporadicBurstNoise # 0x39481500
0 1 8 11 89 0 sporadicBurstNoise # 0x39455900
0 0 12 11 75 0 sporadicBurstNoise # 0x38654b00
0 0 13 11 4 0 highNoiseHG # 0x386d0400
0 0 11 1 6 0 sporadicBurstNoise # 0x38580600
1 0 13 11 63 0 sporadicBurstNoise # 0x3a6d3f00
1 0 13 11 115 0 sporadicBurstNoise # 0x3a6d7300
0 0 14 2 28 0 sporadicBurstNoise # 0x38709c00
0 1 13 13 102 0 sporadicBurstNoise # 0x396e6600
0 0 30 1 6 0 highNoiseHG # 0x38f00600
0 0 30 1 5 0 sporadicBurstNoise # 0x38f00500
0 0 30 1 4 0 sporadicBurstNoise # 0x38f00400
0 1 16 6 101 0 sporadicBurstNoise # 0x3982e500
0 1 16 6 96 0 sporadicBurstNoise # 0x3982e000
0 1 16 6 100 0 sporadicBurstNoise # 0x3982e400
0 1 16 6 102 0 sporadicBurstNoise # 0x3982e600
0 1 17 6 31 0 sporadicBurstNoise # 0x398a9f00
0 0 11 4 126 0 sporadicBurstNoise # 0x3859fe00
0 0 11 4 127 0 sporadicBurstNoise # 0x3859ff00
0 0 25 1 31 0 sporadicBurstNoise # 0x38c81f00
0 0 26 1 19 0 sporadicBurstNoise # 0x38d01300
0 1 14 12 53 0 sporadicBurstNoise # 0x3975b500
0 1 25 1 7 0 sporadicBurstNoise # 0x39c80700
1 0 5 2 34 0 sporadicBurstNoise # 0x3a28a200
1 0 5 2 33 0 sporadicBurstNoise # 0x3a28a100
1 0 5 2 35 0 sporadicBurstNoise # 0x3a28a300

