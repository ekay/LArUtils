Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1118

== Noisy cells update for run 451611 (Sat, 13 May 2023 19:32:15 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:3 (x739_h419)
Cluster matching: based on Et > 4/10GeV plots requiring at least 500 events
Flagged cells:152
Flagged in PS:39
Unflagged by DQ shifters:18
Changed to SBN:2
Changed to HNHG:0
SBN:125
SBN in PS:24
HNHG:27
HNHG in PS:15

*****************
The treated cells were:
0 0 1 14 52 0 sporadicBurstNoise  # 0x380eb400 -> 0x2d406ae6
0 0 1 14 56 0 sporadicBurstNoise  # 0x380eb800 -> 0x2d406ce6
0 0 4 4 45 0 sporadicBurstNoise  # 0x3821ad00 -> 0x2d215a2e
0 0 4 4 46 0 sporadicBurstNoise  # 0x3821ae00 -> 0x2d215c2e
0 0 4 4 47 0 sporadicBurstNoise  # 0x3821af00 -> 0x2d215e2e
0 0 5 1 0 0 highNoiseHG  # 0x38280000 -> 0x2d00002a
0 0 5 1 5 0 highNoiseHG  # 0x38280500 -> 0x2d000228
0 0 5 1 6 0 highNoiseHG  # 0x38280600 -> 0x2d000428
0 0 5 10 102 0 sporadicBurstNoise  # 0x382ce600 -> 0x2d6032a2
0 0 5 10 103 0 sporadicBurstNoise  # 0x382ce700 -> 0x2d6032a0
0 0 7 14 4 0 sporadicBurstNoise  # 0x383e8400 -> 0x2d40628e
0 0 9 1 1 0 highNoiseHG  # 0x38480100 -> 0x2d00021a
0 0 9 1 2 0 unflaggedByLADIeS  # 0x38480200 -> 0x2d00041a
0 0 9 1 4 0 highNoiseHG  # 0x38480400 -> 0x2d000018
0 0 9 1 5 0 sporadicBurstNoise  # 0x38480500 -> 0x2d000218
0 0 10 9 78 0 sporadicBurstNoise  # 0x38544e00 -> 0x2d600652
0 0 10 11 90 0 sporadicBurstNoise  # 0x38555a00 -> 0x2d400c52
0 0 10 13 67 0 sporadicBurstNoise  # 0x38564300 -> 0x2d404050
0 0 11 1 4 0 sporadicBurstNoise  # 0x38580400 -> 0x2d000010
0 0 11 1 23 0 sporadicBurstNoise  # 0x38581700 -> 0x2d001610
0 0 11 8 72 0 sporadicBurstNoise  # 0x385bc800 -> 0x2d231010
0 0 11 8 73 0 sporadicBurstNoise  # 0x385bc900 -> 0x2d231210
0 0 11 8 74 0 sporadicBurstNoise  # 0x385bca00 -> 0x2d231410
0 0 13 1 35 0 sporadicBurstNoise  # 0x38682300 -> 0x2d00260a
0 0 14 2 28 0 sporadicBurstNoise  # 0x38709c00 -> 0x2d203806
0 0 14 12 86 0 sporadicBurstNoise  # 0x3875d600 -> 0x2d402a12
0 0 15 5 62 0 sporadicBurstNoise  # 0x387a3e00 -> 0x2d21fc02
0 0 15 5 63 0 sporadicBurstNoise  # 0x387a3f00 -> 0x2d21fe02
0 0 15 11 23 0 sporadicBurstNoise  # 0x387d1700 -> 0x2d400a08
0 0 15 12 72 0 sporadicBurstNoise  # 0x387dc800 -> 0x2d402406
0 0 15 14 36 0 sporadicBurstNoise  # 0x387ea400 -> 0x2d406a0e
0 0 15 14 40 0 sporadicBurstNoise  # 0x387ea800 -> 0x2d406c0e
0 0 15 14 44 0 sporadicBurstNoise  # 0x387eac00 -> 0x2d406e0e
0 0 16 13 19 0 sporadicBurstNoise  # 0x38861300 -> 0x2d4049f8
0 0 17 7 54 0 sporadicBurstNoise  # 0x388b3600 -> 0x2d22ec7a
0 0 17 7 55 0 sporadicBurstNoise  # 0x388b3700 -> 0x2d22ee7a
0 0 18 1 5 0 sporadicBurstNoise  # 0x38900500 -> 0x2d000274
0 0 18 1 10 0 sporadicBurstNoise  # 0x38900a00 -> 0x2d000c76
0 0 18 1 14 0 sporadicBurstNoise  # 0x38900e00 -> 0x2d000c74
0 0 21 11 65 0 sporadicBurstNoise  # 0x38ad4100 -> 0x2d4001a4
0 0 22 1 1 0 sporadicBurstNoise  # 0x38b00100 -> 0x2d000266
0 0 22 7 0 0 sporadicBurstNoise  # 0x38b30000 -> 0x2d228066
0 0 22 7 1 0 sporadicBurstNoise  # 0x38b30100 -> 0x2d228266
0 0 22 7 2 0 sporadicBurstNoise  # 0x38b30200 -> 0x2d228466
0 0 22 7 3 0 sporadicBurstNoise  # 0x38b30300 -> 0x2d228666
0 0 22 7 4 0 sporadicBurstNoise  # 0x38b30400 -> 0x2d228866
0 0 22 7 5 0 unflaggedByLADIeS  # 0x38b30500 -> 0x2d228a66
0 0 22 7 6 0 unflaggedByLADIeS  # 0x38b30600 -> 0x2d228c66
0 0 22 11 117 0 sporadicBurstNoise  # 0x38b57500 -> 0x2d401b94
0 0 23 1 63 0 sporadicBurstNoise  # 0x38b83f00 -> 0x2d003e60
0 0 23 2 5 0 sporadicBurstNoise  # 0x38b88500 -> 0x2d200a62
0 0 23 2 6 0 sporadicBurstNoise  # 0x38b88600 -> 0x2d200c62
0 0 23 5 8 0 unflaggedByLADIeS  # 0x38ba0800 -> 0x2d219062
0 0 23 5 9 0 unflaggedByLADIeS  # 0x38ba0900 -> 0x2d219262
0 0 23 5 10 0 unflaggedByLADIeS  # 0x38ba0a00 -> 0x2d219462
0 0 23 5 12 0 sporadicBurstNoise  # 0x38ba0c00 -> 0x2d219862
0 0 23 5 13 0 sporadicBurstNoise  # 0x38ba0d00 -> 0x2d219a62
0 0 23 5 14 0 sporadicBurstNoise  # 0x38ba0e00 -> 0x2d219c62
0 0 23 5 15 0 unflaggedByLADIeS  # 0x38ba0f00 -> 0x2d219e62
0 0 23 14 35 0 sporadicBurstNoise  # 0x38bea300 -> 0x2d406988
0 0 24 8 8 0 sporadicBurstNoise  # 0x38c38800 -> 0x2d23105e
0 0 24 8 9 0 sporadicBurstNoise  # 0x38c38900 -> 0x2d23125e
0 0 24 8 10 0 sporadicBurstNoise  # 0x38c38a00 -> 0x2d23145e
0 0 24 8 11 0 highNoiseHG  # 0x38c38b00 -> 0x2d23165e
0 0 24 8 12 0 sporadicBurstNoise  # 0x38c38c00 -> 0x2d23185e
0 0 24 8 13 0 sporadicBurstNoise  # 0x38c38d00 -> 0x2d231a5e
0 0 24 8 14 0 unflaggedByLADIeS  # 0x38c38e00 -> 0x2d231c5e
0 0 25 1 22 0 sporadicBurstNoise  # 0x38c81600 -> 0x2d001458
0 0 25 1 24 0 sporadicBurstNoise  # 0x38c81800 -> 0x2d00185a
0 0 25 1 64 0 highNoiseHG  # 0x38c84000 -> 0x2d00405a
0 0 25 12 15 0 sporadicBurstNoise  # 0x38cd8f00 -> 0x2d402768
0 0 26 1 59 0 sporadicBurstNoise  # 0x38d03b00 -> 0x2d003e56
0 0 26 10 96 0 sporadicBurstNoise  # 0x38d4e000 -> 0x2d603156
0 0 26 10 97 0 unflaggedByLADIeS  # 0x38d4e100 -> 0x2d603154
0 0 26 10 100 0 sporadicBurstNoise  # 0x38d4e400 -> 0x2d603356
0 0 26 10 101 0 unflaggedByLADIeS  # 0x38d4e500 -> 0x2d603354
0 0 26 10 104 0 unflaggedByLADIeS  # 0x38d4e800 -> 0x2d603556
0 0 26 14 24 0 unflaggedByLADIeS  # 0x38d69800 -> 0x2d406556
0 0 26 14 48 0 unflaggedByLADIeS  # 0x38d6b000 -> 0x2d406956
0 0 26 14 52 0 sporadicBurstNoise  # 0x38d6b400 -> 0x2d406b56
0 0 26 14 53 0 highNoiseHG  # 0x38d6b500 -> 0x2d406b54
0 0 26 14 55 0 unflaggedByLADIeS  # 0x38d6b700 -> 0x2d406b50
0 0 26 14 56 0 sporadicBurstNoise  # 0x38d6b800 -> 0x2d406d56
0 0 26 14 57 0 highNoiseHG  # 0x38d6b900 -> 0x2d406d54
0 0 26 14 59 0 unflaggedByLADIeS  # 0x38d6bb00 -> 0x2d406d50
0 0 26 14 60 0 sporadicBurstNoise  # 0x38d6bc00 -> 0x2d406f56
0 0 26 14 61 0 sporadicBurstNoise  # 0x38d6bd00 -> 0x2d406f54
0 0 26 14 62 0 sporadicBurstNoise  # 0x38d6be00 -> 0x2d406f52
0 0 26 14 63 0 sporadicBurstNoise  # 0x38d6bf00 -> 0x2d406f50
0 0 28 1 14 0 highNoiseHG  # 0x38e00e00 -> 0x2d000c4c
0 0 28 1 15 0 sporadicBurstNoise  # 0x38e00f00 -> 0x2d000e4c
0 0 30 1 1 0 unflaggedByLADIeS  # 0x38f00100 -> 0x2d000246
0 0 30 1 5 0 sporadicBurstNoise  # 0x38f00500 -> 0x2d000244
0 0 30 1 6 0 highNoiseHG  # 0x38f00600 -> 0x2d000444
0 0 30 1 13 0 sporadicBurstNoise  # 0x38f00d00 -> 0x2d000a44
0 0 30 1 15 0 unflaggedByLADIeS  # 0x38f00f00 -> 0x2d000e44
0 0 31 6 4 0 sporadicBurstNoise  # 0x38fa8400 -> 0x2d220842
0 1 1 10 91 0 sporadicBurstNoise  # 0x390cdb00 -> 0x2de02c1e
0 1 3 1 65 0 sporadicBurstNoise  # 0x39184100 -> 0x2d80420c
0 1 3 8 27 0 sporadicBurstNoise  # 0x391b9b00 -> 0x2da3360c
0 1 3 8 28 0 sporadicBurstNoise  # 0x391b9c00 -> 0x2da3380c
0 1 3 8 29 0 sporadicBurstNoise  # 0x391b9d00 -> 0x2da33a0c
0 1 5 1 67 0 sporadicBurstNoise  # 0x39284300 -> 0x2d804614
0 1 5 1 72 0 highNoiseHG  # 0x39284800 -> 0x2d804814
0 1 5 14 27 0 sporadicBurstNoise  # 0x392e9b00 -> 0x2dc0645e
0 1 6 12 64 0 sporadicBurstNoise  # 0x3935c000 -> 0x2dc02068
0 1 7 2 3 0 unflaggedByLADIeS  # 0x39388300 -> 0x2da0061c
0 1 8 1 70 0 highNoiseHG  # 0x39404600 -> 0x2d804422
0 1 8 9 67 0 sporadicBurstNoise  # 0x39444300 -> 0x2de0008e
0 1 8 11 89 0 sporadicBurstNoise  # 0x39455900 -> 0x2dc00c8a
0 1 9 1 21 0 sporadicBurstNoise  # 0x39481500 -> 0x2d801226
0 1 9 1 22 0 highNoiseHG  # 0x39481600 -> 0x2d801426
0 1 9 1 23 0 sporadicBurstNoise  # 0x39481700 -> 0x2d801626
0 1 11 1 15 0 sporadicBurstNoise  # 0x39580f00 -> 0x2d800e2e
0 1 13 13 102 0 sporadicBurstNoise  # 0x396e6600 -> 0x2dc052dc
0 1 15 1 81 0 sporadicBurstNoise  # 0x39785100 -> 0x2d80523c
0 1 15 1 84 0 highNoiseHG  # 0x39785400 -> 0x2d80503e
0 1 15 1 85 0 sporadicBurstNoise  # 0x39785500 -> 0x2d80523e
0 1 15 1 86 0 highNoiseHG  # 0x39785600 -> 0x2d80543e
0 1 16 1 43 0 highNoiseHG  # 0x39802b00 -> 0x2d802e40
0 1 16 1 48 0 highNoiseHG  # 0x39803000 -> 0x2d803040
0 1 16 6 96 0 sporadicBurstNoise  # 0x3982e000 -> 0x2da24042
0 1 16 6 97 0 sporadicBurstNoise  # 0x3982e100 -> 0x2da24242
0 1 16 6 98 0 sporadicBurstNoise  # 0x3982e200 -> 0x2da24442
0 1 16 6 99 0 sporadicBurstNoise  # 0x3982e300 -> 0x2da24642
0 1 16 6 101 0 sporadicBurstNoise  # 0x3982e500 -> 0x2da24a42
0 1 16 10 99 0 sporadicBurstNoise  # 0x3984e300 -> 0x2de0310e
0 1 16 12 42 0 sporadicBurstNoise  # 0x3985aa00 -> 0x2dc03504
0 1 16 13 83 0 sporadicBurstNoise  # 0x39865300 -> 0x2dc0490e
0 1 17 6 31 0 sporadicBurstNoise  # 0x398a9f00 -> 0x2da23e44
0 1 18 12 76 0 sporadicBurstNoise  # 0x3995cc00 -> 0x2dc02728
0 1 21 3 10 0 sporadicBurstNoise  # 0x39a90a00 -> 0x2da09454
0 1 21 3 11 0 sporadicBurstNoise  # 0x39a90b00 -> 0x2da09654
0 1 21 3 12 0 sporadicBurstNoise  # 0x39a90c00 -> 0x2da09854
0 1 21 5 101 0 sporadicBurstNoise  # 0x39aa6500 -> 0x2da1ca56
0 1 21 5 102 0 sporadicBurstNoise  # 0x39aa6600 -> 0x2da1cc56
0 1 21 11 10 0 sporadicBurstNoise  # 0x39ad0a00 -> 0x2dc00554
0 1 23 2 1 0 sporadicBurstNoise  # 0x39b88100 -> 0x2da0025c
0 1 23 2 2 0 sporadicBurstNoise  # 0x39b88200 -> 0x2da0045c
0 1 23 10 38 0 sporadicBurstNoise  # 0x39bca600 -> 0x2de03374
0 1 24 1 91 0 sporadicBurstNoise  # 0x39c05b00 -> 0x2d805e60
0 1 24 11 55 0 sporadicBurstNoise  # 0x39c53700 -> 0x2dc01b86
0 1 25 1 7 0 sporadicBurstNoise  # 0x39c80700 -> 0x2d800666
0 1 26 11 65 0 sporadicBurstNoise  # 0x39d54100 -> 0x2dc001aa
0 1 28 12 6 0 sporadicBurstNoise  # 0x39e58600 -> 0x2dc023c4
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 3 6 111 0 highNoiseHG  # 0x3a1aef00 -> 0x31890000
1 0 5 7 103 0 sporadicBurstNoise  # 0x3a2b6700 -> 0x2cb06e22
1 0 10 5 94 0 highNoiseHG  # 0x3a525e00 -> 0x31048000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 94 0 distorted highNoiseHG  # 0x3a53de00 -> 0x31140000
1 0 10 8 122 0 highNoiseHG  # 0x3a53fa00 -> 0x311c1000
1 0 11 8 12 0 sporadicBurstNoise  # 0x3a5b8c00 -> 0x2ce00636
1 0 13 11 63 0 sporadicBurstNoise  # 0x3a6d3f00 -> 0x2cc425e0
1 0 13 11 115 0 sporadicBurstNoise  # 0x3a6d7300 -> 0x2cc427e0
1 0 14 7 7 0 sporadicBurstNoise  # 0x3a730700 -> 0x2cb04e76
1 0 16 6 115 0 highNoiseHG  # 0x3a82f300 -> 0x318f3000
1 0 19 10 105 0 sporadicBurstNoise  # 0x3a9ce900 -> 0x2cc41b6c
1 0 21 3 41 0 sporadicBurstNoise  # 0x3aa92900 -> 0x2cc4036c
1 0 21 13 123 0 sporadicBurstNoise  # 0x3aae7b00 -> 0x2ce02500
1 0 21 15 115 0 sporadicBurstNoise  # 0x3aaf7300 -> 0x2cc44f00
1 1 9 13 30 0 sporadicBurstNoise  # 0x3b4e1e00 -> 0x2e60269c
1 1 9 13 31 0 sporadicBurstNoise  # 0x3b4e1f00 -> 0x2e60269e
1 1 9 14 59 0 sporadicBurstNoise  # 0x3b4ebb00 -> 0x2e44529e
1 1 9 14 63 0 sporadicBurstNoise  # 0x3b4ebf00 -> 0x2e44549e
1 1 10 6 42 0 highNoiseHG  # 0x3b52aa00 -> 0x3309a000
1 1 12 4 15 0 sporadicBurstNoise  # 0x3b618f00 -> 0x2e2c1e38
1 1 21 4 116 0 sporadicBurstNoise  # 0x3ba9f400 -> 0x2e4401f8
1 1 21 4 120 0 sporadicBurstNoise  # 0x3ba9f800 -> 0x2e4403f8
1 1 22 10 109 0 highNoiseHG  # 0x3bb4ed00 -> 0x32c5f000

