Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1158

== Event Veto for run 452696 (Thu, 25 May 2023 12:59:21 +0200) ==
Found Noise or data corruption in run 452696
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452696_Main.db

Found project tag data23_13p6TeV for run 452696
Found 4 Veto Ranges with 10 events
Found 2 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452696_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452696
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452696_Main.db;dbname=CONDBR2
Found a total of 4 corruption periods, covering a total of 2.00 seconds
Lumi loss due to corruption: 34.73 nb-1 out of 96300.43 nb-1 (0.36 per-mil)
Overall Lumi loss is 34.73041452901571 by 2.00101383 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1158

== Event Veto for run 452696 (Thu, 25 May 2023 12:19:08 +0200) ==
Found Noise or data corruption in run 452696
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452696_Main.db

Found project tag data23_13p6TeV for run 452696
Found 4 Veto Ranges with 10 events
Found 2 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452696_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452696
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452696_Main.db;dbname=CONDBR2
Found a total of 4 corruption periods, covering a total of 2.00 seconds
Lumi loss due to corruption: 34.73 nb-1 out of 96300.43 nb-1 (0.36 per-mil)
Overall Lumi loss is 34.73041452901571 by 2.00101383 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1158

== Missing EVs for run 452696 (Mon, 22 May 2023 17:33:13 +0200) ==

Found a total of 74 noisy periods, covering a total of 0.09 seconds
Found a total of 448 Mini noise periods, covering a total of 0.46 seconds
Lumi loss due to noise-bursts: 1.38 nb-1 out of 96300.43 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 7.83 nb-1 out of 96300.43 nb-1 (0.08 per-mil)
Overall Lumi loss is 9.21277249656012 by 0.5516700999999999 s of veto length


