Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1155

== Noisy cells update for run 452640 (Tue, 30 May 2023 09:12:16 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1352_h421)
Cluster matching: based on Et > 4/10GeV plots requiring at least 150 events
Flagged cells:2
Flagged in PS:2
Unflagged by DQ shifters:0
Changed to SBN:1
Changed to HNHG:0
SBN:1
SBN in PS:1
HNHG:1
HNHG in PS:1

*****************
The treated cells were:
0 0 5 1 2 0 sporadicBurstNoise  # 0x38280200 -> 0x2d00042a

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1155

== Event Veto for run 452640 (Wed, 24 May 2023 02:52:54 +0200) ==
Found Noise or data corruption in run 452640
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452640_Main.db

Found project tag data23_13p6TeV for run 452640
Found 2 Veto Ranges with 4 events
Found 11 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452640_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452640
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452640_Main.db;dbname=CONDBR2
Found a total of 2 corruption periods, covering a total of 1.00 seconds
Lumi loss due to corruption: 19.25 nb-1 out of 166399.89 nb-1 (0.12 per-mil)
Overall Lumi loss is 19.24831411429629 by 1.00028434 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1155

== Event Veto for run 452640 (Tue, 23 May 2023 11:42:53 +0200) ==
Found Noise or data corruption in run 452640
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452640_Main.db

Found project tag data23_13p6TeV for run 452640
Found 2 Veto Ranges with 4 events
Found 11 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452640_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452640
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452640_Main.db;dbname=CONDBR2
Found a total of 2 corruption periods, covering a total of 1.00 seconds
Lumi loss due to corruption: 19.25 nb-1 out of 166399.89 nb-1 (0.12 per-mil)
Overall Lumi loss is 19.24831411429629 by 1.00028434 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1155

== Event Veto for run 452640 (Tue, 23 May 2023 11:26:37 +0200) ==
Found Noise or data corruption in run 452640
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452640_Main.db

Found project tag data23_13p6TeV for run 452640
Found 2 Veto Ranges with 4 events
Found 11 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452640_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452640
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452640_Main.db;dbname=CONDBR2
Found a total of 2 corruption periods, covering a total of 1.00 seconds
Lumi loss due to corruption: 19.25 nb-1 out of 166399.89 nb-1 (0.12 per-mil)
Overall Lumi loss is 19.24831411429629 by 1.00028434 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1155

== Missing EVs for run 452640 (Sat, 20 May 2023 19:17:05 +0200) ==

Found a total of 50 noisy periods, covering a total of 0.06 seconds
Found a total of 298 Mini noise periods, covering a total of 0.33 seconds
Lumi loss due to noise-bursts: 1.15 nb-1 out of 166399.89 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 6.02 nb-1 out of 166399.89 nb-1 (0.04 per-mil)
Overall Lumi loss is 7.169968017987308 by 0.39131412 s of veto length


