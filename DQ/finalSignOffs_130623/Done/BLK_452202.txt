Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1144

== Noisy cells update for run 452202 (Sun, 28 May 2023 15:01:31 +0200) ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:10
Flagged in PS:6
Unflagged by DQ shifters:0
Changed to SBN:5
Changed to HNHG:0
SBN:10
SBN in PS:6
HNHG:0
HNHG in PS:0

*****************
The treated cells were:
0 0 25 2 5 0 sporadicBurstNoise  # 0x38c88500 -> 0x2d200a5a
0 0 25 2 6 0 sporadicBurstNoise  # 0x38c88600 -> 0x2d200c5a
0 0 26 1 81 0 sporadicBurstNoise  # 0x38d05100 -> 0x2d005256
0 0 26 1 82 0 sporadicBurstNoise  # 0x38d05200 -> 0x2d005456
0 0 26 14 20 0 sporadicBurstNoise  # 0x38d69400 -> 0x2d406356
0 1 8 1 65 0 sporadicBurstNoise  # 0x39404100 -> 0x2d804220
0 1 8 1 66 0 sporadicBurstNoise  # 0x39404200 -> 0x2d804420
0 1 8 1 67 0 sporadicBurstNoise  # 0x39404300 -> 0x2d804620
0 1 8 1 79 0 sporadicBurstNoise  # 0x39404f00 -> 0x2d804e22
0 1 23 2 3 0 sporadicBurstNoise  # 0x39b88300 -> 0x2da0065c

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1144

== Event Veto for run 452202 (Wed, 24 May 2023 02:59:42 +0200) ==
Found Noise or data corruption in run 452202
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452202_Main.db

Found project tag data23_13p6TeV for run 452202
Found 13 Veto Ranges with 28 events
Found 62 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452202_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452202
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452202_Main.db;dbname=CONDBR2
Found a total of 13 corruption periods, covering a total of 6.50 seconds
Lumi loss due to corruption: 114.41 nb-1 out of 703206.59 nb-1 (0.16 per-mil)
Overall Lumi loss is 114.41401804090408 by 6.50304187 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1144

== Event Veto for run 452202 (Tue, 23 May 2023 18:32:35 +0200) ==
Found Noise or data corruption in run 452202
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452202_Main.db

Found project tag data23_13p6TeV for run 452202
Found 13 Veto Ranges with 28 events
Found 62 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452202_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452202
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452202_Main.db;dbname=CONDBR2
Found a total of 13 corruption periods, covering a total of 6.50 seconds
Lumi loss due to corruption: 114.41 nb-1 out of 703206.59 nb-1 (0.16 per-mil)
Overall Lumi loss is 114.41401804090408 by 6.50304187 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1144

== Event Veto for run 452202 (Tue, 23 May 2023 15:36:41 +0200) ==
Found Noise or data corruption in run 452202
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452202_Main.db

Found project tag data23_13p6TeV for run 452202
Found 13 Veto Ranges with 28 events
Found 62 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452202_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452202
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452202_Main.db;dbname=CONDBR2
Found a total of 13 corruption periods, covering a total of 6.50 seconds
Lumi loss due to corruption: 114.41 nb-1 out of 703206.59 nb-1 (0.16 per-mil)
Overall Lumi loss is 114.41401804090408 by 6.50304187 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1144

== Missing EVs for run 452202 (Tue, 16 May 2023 11:22:03 +0200) ==

Found a total of 254 noisy periods, covering a total of 0.39 seconds
Found a total of 1033 Mini noise periods, covering a total of 1.11 seconds
Lumi loss due to noise-bursts: 6.34 nb-1 out of 703206.59 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 19.08 nb-1 out of 703206.59 nb-1 (0.03 per-mil)
Overall Lumi loss is 25.419994558577798 by 1.4924311600000002 s of veto length


