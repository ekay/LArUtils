import subprocess, sys
import xmlrpc.client

dqmpassfile = "/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt"
with open(dqmpassfile, "r") as f:       
    cred = f.readline().strip()
    if cred is None:
        print("Problem reading credentials from",dqmpassfile)
dqmapi = xmlrpc.client.ServerProxy('https://'+cred+'@atlasdqm.cern.ch')

infokeys = ["Run type", "Project tag", "Partition name", "Number of events passing Event Filter", "Run start", "Run end", "Number of luminosity blocks", "Data source", "Detector mask", "Recording enabled", "Number of events in physics streams" ]


def getRate(runNo, stream="physics_CosmicCalo"):
    run_spec = {'stream': stream, 'source': 'tier0', 'run_list': [runNo] }
    run_info = dqmapi.get_run_information(run_spec) 

    for ri in run_info.keys():
        run_info[ri] = { ik:li for ik,li in zip(infokeys,run_info[ri]) }
    runNo = str(runNo)

    cmd = f"rucio list-dids {run_info[runNo]['Project tag']}.00{runNo}.{stream}*"
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out,err = process.communicate()
    out = out.decode('ascii')
    out = [ o for o in out.replace("|","").strip(" ").split("\n") if ".RAW" in o]
    if len(out) == 0:
        print(f"Can't find relevant did for {cmd}, exiting")
        return None, None
    elif len(out) > 1:
        print(f"Found too many dids, maybe debug the wildcard {cmd}: {out}")
        return None, None
    else:
        out = out[0]
        out = out.split(" ")
        out = [ o for o in out if ".RAW" in o ][0].split(":")[1]

    cmd = f"rucio get-metadata {out} | grep events"
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out,err = process.communicate()
    out = out.decode('ascii').strip("\n").split(" ")[-1]
    n_stream = int(out)

    n_total = int(run_info[runNo]["Number of events in physics streams"])

    return n_stream, n_total
