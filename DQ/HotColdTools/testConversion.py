import uproot as U
import numpy as np
import pandas as pd
import ROOT as R
import sys
import time
import xmlrpc.client
passfile = open("/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt")
passwd = passfile.read().strip(); passfile.close()  
def getPassURL(usedev):
  if usedev is True:
    passurl = 'https://%s@atlasdqm-dev.cern.ch' % passwd
  else:
    passurl = 'https://%s@atlasdqm.cern.ch' % passwd
  return passurl
passurl = getPassURL(False)
server = xmlrpc.client.ServerProxy(passurl)
    
# from file
filepath = "/eos/atlas/atlastier0/rucio/data24_hi/physics_CosmicCalo/00490085/data24_hi.00490085.physics_CosmicCalo.merge.HIST.x885_h507/data24_hi.00490085.physics_CosmicCalo.merge.HIST.x885_h507._0001.1"
histpath = "run_490085/CaloTopoClusters/CalBAR/Thresh0BAROcc"
infile = U.open(filepath)
hist = infile[histpath]


run=490085
amistr="x"
stream="physics_CosmicCalo"
run_spec = {"source": "tier0", "stream":stream, "low_run":run, "high_run":run}
mapping = server.get_procpass_amitag_mapping(run_spec)
    
if str(run) not in mapping.keys():
    print(f"ERROR: did not find ami tag info for {run} {stream}")
    sys.exit()
mapping = mapping[str(run)]
mapping = [ m for m in mapping if m[2].startswith(amistr) and m[1]==stream ]
if len(mapping) == 0:
    print(f"ERROR: no ami info found for stream {stream} ami {amistr}*")
    sys.exit()
max_proc = max([m[0] for m in mapping])
mapping = [ m for m in mapping if m[0] == max_proc ][0]
run_spec["proc_pass"] = mapping[0]

WDpath = "CaloTopoClusters/CalBAR/Thresh0BAROcc"
xmlHist = server.get_hist_objects(run_spec,WDpath)[str(run)]
hist2 = R.TBufferXML.ConvertFromXML(xmlHist)

def hist2DF_etaPhi(hist):
    try:
        histdata = hist.to_numpy()#flow=True)
        x = histdata[1]
        x = x.repeat(2)[1:-1]
        x = x.reshape(int(len(x)/2),2)
        y = histdata[2]
        y = y.repeat(2)[1:-1]
        y = y.reshape(int(len(y)/2),2)
        
        zvals = histdata[0].flatten()
        print(len(x), "xvals")
        print(len(y), "yvals")
        
        newx = np.repeat(x[None,:],len(y), axis=1)[0]
        newy = np.tile(y,(len(x),1))
        
        xmin = newx[:,0]
        xmax = newx[:,1]
        ymin = newy[:,0]
        ymax = newy[:,1]
        
        xvals = [ (xv[0]+xv[1])/2 for xv in newx ]
        yvals = [ (yv[0]+yv[1])/2 for yv in newy ]
        ieta = [ i for i in range(1,len(x)+1) for _ in range(1,len(y)+1)]
        iphi = list(range(1,len(y)+1)) * len(x)
        nbin = [ int(f"{str(y).zfill(2)}{str(x).zfill(2)}") for x,y in zip(ieta,iphi) ]
        xint = [ pd.Interval(xv[0],xv[1], closed="both") for xv in newx ]
        yint = [ pd.Interval(yv[0],yv[1], closed="both") for yv in newy ]
        
    except:
        xvals, yvals, zvals, ieta, iphi, nbin, xint, yint = [], [], [], [], [], [], [], []
        for x in range(1, hist.GetNbinsX()+1):
            for y in range(1, hist.GetNbinsY()+1):
                xval = hist.GetXaxis().GetBinCenter(x)
                xmin = hist.GetXaxis().GetBinLowEdge(x)
                xmax = hist.GetXaxis().GetBinUpEdge(x)
                yval = hist.GetYaxis().GetBinCenter(y)
                ymin = hist.GetYaxis().GetBinLowEdge(y)
                ymax = hist.GetYaxis().GetBinUpEdge(y)
                zval = hist.GetBinContent(x,y)

                # nbin = hist.FindBin(xval, yval)
                xvals.append(xval)
                yvals.append(yval)
                zvals.append(zval)
                ieta.append(x)
                iphi.append(y)               
                nbin.append(int(f"{str(y).zfill(2)}{str(x).zfill(2)}"))
                xint.append(pd.Interval(xmin,xmax, closed="both"))
                yint.append(pd.Interval(ymin,ymax, closed="both"))

    # To be filled later
    mean_eta, mean_phi, mode_eta, mode_phi = [0]*len(zvals), [0]*len(zvals), [0]*len(zvals), [0]*len(zvals)

    df = pd.DataFrame({'ieta': ieta, 'iphi': iphi, 'nbin/LAronlineID': nbin, 'eta': xvals, 'phi': yvals, 'etaint':xint, 'phiint':yint, 'noise': zvals, 'mean_eta':mean_eta, 'mean_phi':mean_phi, 'mode_eta':mode_eta, 'mode_phi':mode_phi})

    return df

start = time.time()
df  = hist2DF_etaPhi(hist)
end = time.time()
print(end - start)
print(df)

start = time.time()
df  = hist2DF_etaPhi(hist2)
end = time.time()
print(end - start)
print(df)
