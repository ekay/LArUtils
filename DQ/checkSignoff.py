import sys, time
import xmlrpc.client
from argparse import ArgumentParser
from threading import Thread


from LArMonCoolLib import GetIntolerable
all_intol = GetIntolerable()

print("OIOI", [ i for i in all_intol if "LAR" in i ])

dqmsite = "atlasdqm.cern.ch"
dqmpassfile = "/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt"
dqmpass = None
with open(dqmpassfile, "r") as f:
    dqmpass = f.readlines()[0].strip()
    if ":" not in dqmpass:
        print("Problem reading dqmpass")
        sys.exit()
    dqmapi = xmlrpc.client.ServerProxy("https://"+dqmpass+"@"+dqmsite)

#dqmpassfile = "dqmpass.txt"

sys.path.append("/afs/cern.ch/user/l/larmon/public/prod/LArPage1/makeJIRA")
import detmask
                
class mc(Thread):
    ''' Multithreading for running multiple API calls simultaneously '''
    def __init__(self):
        Thread.__init__(self)
        self.server = xmlrpc.client.ServerProxy("https://"+dqmpass+"@"+dqmsite)
        self.multicall = xmlrpc.client.MultiCall( self.server )
    def run(self):
        self.results = tuple(self.multicall())

class bcolours:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'    
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    BOLDUNDERLINE = '\033[1;4m'
    UNDERLINE = '\033[4m'
    ENDBOLD = '\033[21m'
    ENDUNDERLINE = '\033[24m'
    ENDBOLDUNDERLINE = '\033[22;24m'

def main(runlist=None, nruns=None, ignoreNoLAr=False, onlySB=False, lowrun=None, highrun=None):
    cred = None
    with open(dqmpassfile, "r") as f:       
        cred = f.readline().strip()
    if cred is None:
        print("Problem reading credentials from",dqmpassfile)
    dqmapi = xmlrpc.client.ServerProxy('https://'+cred+'@atlasdqm.cern.ch')
    run_spec = {'stream': 'physics_CosmicCalo', 'source': 'tier0'}
    
    if runlist is None:
        if highrun is None:
            if lowrun is not None:
                if nruns is not None:
                    highrun=lowrun+nruns
                else:
                    highrun=dqmapi.get_latest_run()
            else:
                highrun=dqmapi.get_latest_run()

        if lowrun is None:
            if nruns is not None:
                lowrun = highrun-nruns
            else:
                lowrun = highrun-1000
        #print("Low run:",lowrun, ".. High run:",highrun)
        run_spec['low_run'] = lowrun
        run_spec['high_run'] = highrun

        runlist = dqmapi.get_completed_runs(run_spec,True)
        print(len(runlist),"runs")
        del run_spec['low_run']
        del run_spec['high_run']

    #else:
    run_spec['run_list'] = runlist

    print("Run spec:")
    for k in run_spec.keys():
        print(k,":",run_spec[k])

    print("*"*10)
    print("Clean list of runs:")
    print(" ".join([str(s) for s in runlist]))
    print("*"*10)
    
    nrun = 0
    if 'low_run' in run_spec.keys() and 'high_run' in run_spec.keys():
        nrun = run_spec['high_run'] - run_spec['low_run'] 
    elif 'run_list' in run_spec.keys():
        nrun = max(run_spec['run_list']) - min(run_spec['run_list'])

    print(run_spec, nrun)
    if nrun < 20:
        defects = dqmapi.get_defects_lb(run_spec)
        try:
            CLdeadline = dqmapi.get_end_of_calibration_period(run_spec)
        except:
            CLdeadline = {}
        run_info = dqmapi.get_run_information(run_spec)
        run_streams = dqmapi.get_runs_streams(run_spec)
        run_ami = dqmapi.get_procpass_amitag_mapping(run_spec)
        run_mag = dqmapi.get_run_magfields(run_spec)
        beam_info = dqmapi.get_run_beamluminfo(run_spec)
    else:
        calls = []
        callName = []
        this_runspec = run_spec.copy()
        defects = {}
        CLdeadline = {}
        run_info = {}
        run_streams = {}
        run_ami = {}
        run_mag = {}
        beam_info = {}


        chunksize = 12
        if 'low_run' in run_spec.keys() and 'high_run' in run_spec.keys():

            del this_runspec['low_run']
            del this_runspec['high_run']
            theseRuns = [ (x,x+(chunksize-1)) for x in range(run_spec['low_run'], run_spec['high_run']+1,chunksize) ]
        else:
            del this_runspec['run_list']
            theseRuns = [run_spec['run_list'][i:i+(chunksize)] for i in range(0,len(run_spec['run_list']),chunksize)]

            
        for chunk in theseRuns:
            print(f"Chunk {theseRuns.index(chunk)+1} / {len(theseRuns)} ({chunk})")
            if isinstance(chunk,tuple):
                this_runspec['low_run'] = chunk[0]
                if chunk[1] <= run_spec['high_run']:
                    this_runspec['high_run'] = chunk[1]
                else:
                    this_runspec['high_run'] = run_spec['high_run']
            else:
                this_runspec['run_list'] = chunk
            defects.update(dqmapi.get_defects_lb(this_runspec))
            try:
                CLdeadline.update(dqmapi.get_end_of_calibration_period(this_runspec))
            except:
                print("couldn't get CL deadline")
            run_info.update(dqmapi.get_run_information(this_runspec))
            run_streams.update(dqmapi.get_runs_streams(this_runspec))
            run_ami.update(dqmapi.get_procpass_amitag_mapping(this_runspec))
            run_mag.update(dqmapi.get_run_magfields(this_runspec))
            beam_info.update(dqmapi.get_run_beamluminfo(this_runspec))

    CLdeadline = { k : [ time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(CLdeadline[k][0])), CLdeadline[k][1] ] for k in CLdeadline.keys() }

    infokeys = ["Run type", "Project tag", "Partition name", "Number of events passing Event Filter", "Run start", "Run end", "Number of luminosity blocks", "Data source", "Detector mask", "Recording enabled", "Number of events in physics streams" ]


    #try:
    #    run_periods = dqmapi.get_data_periods(run_spec)
    #except xmlrpc.client.ProtocolError as e:
    #    print("Problem getting run periods")
    #    run_periods = {}

    beamkeys = ["Max beam energy during run", "Stable beam flag enabled during run", "ATLAS ready flag enabled during run", "Total integrated luminosity", "ATLAS ready luminosity (/nb)"]

    magkeys = ["Soleniod actual current", "Toroid actual current", "Solenoid set current", "Toroid set current"]

    for ri in run_info.keys():
        run_info[ri] = { ik:li for ik,li in zip(infokeys,run_info[ri]) }
    for ra in run_ami.keys():
        run_ami[ra] = [str(b)+" (pass "+str(a)+", ["+str(c)+"|http://ami.in2p3.fr:8080/?subapp=tagsShow&userdata="+str(c)+"])" for a,b,c in run_ami[ra]]
    for bi in beam_info.keys():
        beam_info[bi] = { ik:li for ik,li in zip(beamkeys,beam_info[bi]) }
    for mi in run_mag.keys():
        run_mag[mi] = { ik:li for ik,li in zip(magkeys,run_mag[mi]) }

    #for pi in list(run_periods.keys()):
    #    if len(run_periods[pi]) == 0:
    #        del run_periods[pi]
    #        continue
    #    run_periods[pi] = "Period "+", ".join(run_periods[pi])
    #print(run_info)
    runs = sorted(list(defects.keys()))


    final = []
    for run in runs:
        notcon = False
        if "GLOBAL_NOTCONSIDERED" in defects[run].keys(): 
            #print("GLOBAL_NOTCONSIDERED defect present for", run)
            notcon = True
            continue
        lardefects = [ k for k in defects[run].keys() if "lar" in k.lower() ]
        #if len(lardefects) == 0: continue
        # if not any( [ "LAR" in k for k in defects[run].keys() ]): continue
        thesedefects = {k: defects[run][k] for k in lardefects}

        printstr = "Run "+str(run)+" "
        if len(thesedefects.keys()) == 0 or len([ d for d in thesedefects.keys() if "UNCHECKED" in d])==0:
            printstr += "FULLY SIGNED OFF. "
        if len(thesedefects.keys()) > 0:
            printstr += "Defects: "+(", ").join(thesedefects.keys())+" "

        if "LAR_FIRSTDEFECTS_UNCHECKED" in thesedefects.keys() or "LAR_UNCHECKED" in thesedefects.keys():
            if run in CLdeadline.keys():
                printstr += "CL deadline: "+CLdeadline[run][0]+" "
        if run in beam_info.keys():
            if beam_info[run]["Stable beam flag enabled during run"] == 0:
                printstr += "(NON-SB) "
            else:
                printstr += "STABLE!! "
        else:
            printstr += "NO BEAM INFO?? " #+(", ").join([d for d in defects[run].keys()])
        #if run in run_periods.keys():
        #    printstr += run_periods[run]+" "

        if "NON-SB" in printstr and onlySB:
            #print(run,"NOT SB")
            continue
        if run in run_info.keys():
            detectors = detmask.DecodeDetectorMaskToString(int(run_info[run]["Detector mask"]), False)
            larenabled = "lar" in detectors[0].lower()
            printstr += run_info[run]["Project tag"]+" "
            if not larenabled: 
                printstr += "(LAR NOT ENABLED)"
        if "LAR NOT ENABLED" in printstr and ignoreNoLAr:
            #print(run,"LAr not enabled")
            continue
        if run in run_streams.keys():
            if 'physics_Main' not in run_streams[run]:
                printstr += " (no Main stream!)"
        if run in run_mag.keys():
            if run_mag[run]["Toroid set current"] == 0:
                printstr += " (Toroid OFF)"
            #printstr += str(run_mag[run]["Soleniod actual current"])+" "
            #printstr += str(run_mag[run]["Toroid actual current"])+" "
            #printstr += str(run_mag[run]["Solenoid set current"])+" "
            #printstr += str(run_mag[run]["Toroid set current"])+" "

        col = None
        if notcon: 
            col = bcolours.HEADER
            #printstr += "GLOBAL_NOTENABLED"
            continue
        elif "LAR_FIRSTDEFECTS_UNCHECKED" in thesedefects.keys() or "LAR_UNCHECKED" in thesedefects.keys():
            if "LAR NOT ENABLED" in printstr:
                col = bcolours.WARNING
            else:
                col = bcolours.FAIL
        else:
            if "LAR_BULK_UNCHECKED" in printstr:
                col = bcolours.OKBLUE
            elif "LAR_UNCHECKED_FINAL" in printstr:
                col = bcolours.OKCYAN
                final.append(run)
            else:
                col = bcolours.OKGREEN


        if any(intol in printstr for intol in all_intol):
            matched =  [ i for i in all_intol if i in printstr ]
            for m in matched:
                printstr = printstr.replace(m, f"{bcolours.BOLDUNDERLINE}{m}{bcolours.ENDBOLDUNDERLINE}")
        if col is not None:            
            print(col, printstr, bcolours.ENDC)
        else:
            print(printstr)
    print("*"*10)
    print(f"{len(final)} Runs ready for final sign-off:")
    print(" ".join([str(s) for s in final]))
    print("*"*10)
if __name__ == "__main__":

    
    parser = ArgumentParser()
    parser.add_argument('-n', '--nruns', dest='nruns', default=None, type=int, help='If a run range is not defined - search for this number of runs above high run / below low run')
    parser.add_argument('-s', '--lowrun', dest='lowrun', default=None, type=int, help='Search for runs starting from this run number')
    parser.add_argument('-e', '--highrun', dest='highrun', default=None, type=int, help='Search for runs with a maximum of this run number')
    parser.add_argument('-r', '--runlist', dest='runList', help='Run number which you would like to get information for', type=int, nargs='+', default=None,required=False)
    parser.add_argument('--onlyLAr', dest='ignoreNoLAr', action='store_true', help='Only show runs where LAr is enabled')
    parser.add_argument('--onlySB', dest='onlySB', action='store_true', help='Only look at stable beam runs')
    args = parser.parse_args()

    main(args.runList, args.nruns, args.ignoreNoLAr, args.onlySB, args.lowrun, args.highrun )
