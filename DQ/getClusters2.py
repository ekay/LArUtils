import xmlrpc.client
from threading import Thread
import re, time

passfile = open("/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt")
passwd = passfile.read().strip(); passfile.close()  

def getPassURL(usedev):
  if usedev is True:
    passurl = 'https://%s@atlasdqm-dev.cern.ch' % passwd
  else:
    passurl = 'https://%s@atlasdqm.cern.ch' % passwd
  return passurl
