#!/bin/python3
# Scritp which compares BCID calibration files
import sys, os
import pandas as pd 
import printMapping
want = ["LATOME_SOURCE_ID", "LATOME_FIBRE", "LATOME_NAME"] # "BadSC"
inputs={"want":want,"showdecimal":True} #,"run":479720}

#orig = "/afs/cern.ch/user/a/admilic/public/forEllis/BCID_ATLAS_v30"

#new = []
#new1 = "/eos/user/e/echapon/work/shared/ATLAS/LAr/DT/bcidcalib_full_samples_imax.txt"
#new2 = "/eos/user/e/echapon/work/shared/ATLAS/LAr/DT/bcidcalib_full_sid_max_of_et.txt"

def get_df(infile):
    df = pd.read_csv(infile, sep=" ", dtype=str)#, names=["sourceId","fiberNumber","calib"])
    b16 = lambda x: int(x,16)
    #df["sourceId"] = df["sourceId"].apply(int, base=16)
    df['fullId'] = df[['sourceId', 'fiberNumber']].agg('_'.join, axis=1)
    df = df.drop_duplicates(keep='last')
    duplicate = df[df['fullId'].duplicated() == True]
    if len(duplicate.index) > 0:
        print(infile, "has", len(duplicate.index), "duplicates")
    df.set_index("fullId", inplace=True)
    return df




def comp(df1, df2):
    print("querying printMapping for",inputs)
    pmout = printMapping.query(**inputs)
    pmdf = pd.DataFrame(pmout, columns = want)
    pmdf = pmdf.astype({'LATOME_SOURCE_ID':'int'})


    def fname(sourceId_before, fiberNumber_before):
        sourceId = int(sourceId_before,16)
        fiberNumber = int(fiberNumber_before)+1
        match = pmdf.loc[ ( pmdf.LATOME_SOURCE_ID == sourceId ) & ( pmdf.LATOME_FIBRE.str.endswith(f"_F{fiberNumber}") ) ]
        if len(match.LATOME_FIBRE.values) == 0:
            match = pmdf.loc[ ( pmdf.LATOME_SOURCE_ID == sourceId ) ].LATOME_NAME.unique()[0]
            # print("OIOI", "sourceId", sourceId, sourceId_before, "..fibre from txt:", fiberNumber_before, "..value to match:", fiberNumber, "..LATOME NAME:", match.LATOME_NAME.unique()[0])
            return f"{match}_???"
        return(match.LATOME_FIBRE.values[0])

    def latomeName(sourceId_before):
        sourceId = int(sourceId_before,16)
        return pmdf.loc[ ( pmdf.LATOME_SOURCE_ID == sourceId ) ].LATOME_NAME.unique()[0]


    
    diff_id = df2[~df2.isin(df1).all(axis=1)].index.values
    diff_id2 = df1[~df1.isin(df2).all(axis=1)].index.values
    print(len(diff_id), len(diff_id2), len(df1.index.values), len(df2.index.values))
    merged = df1.join(df2, lsuffix='_1', rsuffix='_2')
    merged['fiberName'] = merged.apply(lambda x: fname(x.sourceId_1, x.fiberNumber_1), axis=1)
    merged['LATOME_NAME'] = merged.apply(lambda x: latomeName(x.sourceId_1), axis=1)
    diff = merged.loc[diff_id]
    print(diff[["fiberName", "calib_1", "calib_2"]])
    diff["calib_diff"] = ( diff.calib_1.astype(int) - diff.calib_2.astype(int) ).abs()    
    print(len(diff.index), "fibres differ on", len(diff.LATOME_NAME.unique()), "LATOMEs")
    print(f"Max difference is {diff.calib_diff.max()}, mean difference is {diff.calib_diff.mean()}")

    
if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Please provide two BCID calibration files as inputs.")
        sys.exit()
              
    def checkFile(f):
        if not os.path.isfile(f):
            print(f"File {f} does not exist!")
            sys.exit()
        return f

    f1 = checkFile(sys.argv[1])
    f2 = checkFile(sys.argv[2])
    
    df1 = get_df(f1)
    df2 = get_df(f2)

    comp(df1, df2)
