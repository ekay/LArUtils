import os, sys, errno, glob, subprocess, pathlib, getpass, datetime
import pandas as pd
import argparse
import ROOT as R
import printMapping



class logFile():
    def __init__(self, logpath):
        self.logfile = open(logpath, "r", newline="\n")
        self.lines = self.logfile.readlines()    
        self.runs = self.runsFromLog()
        self.name = pathlib.Path(logpath).stem
        
    def runsFromLog(self):
        runs = []
        for line in self.lines:
            if not line.startswith("Run "): continue
            runs.append(int(line.split("Run")[1].split(":")[0]))
        return runs
    def __str__(self):
        toprint = []
        for line in self.lines:
            if not line.startswith("Run "): continue
            line = line.strip("\n")
            toprint.append(line)
        return "\n".join(toprint)
    def getTopDirs(self, topdir):
        self.topdirs = []
        self.rootFiles = []
        for run in self.runs:
            self.infiles = glob.glob(f"{topdir}/*{run}*/root_files/*{run}*") 
            self.topdirs.extend( list(set([ str(pathlib.Path(i).parents[1]) for i in self.infiles ])) )
            self.rootFiles.extend( list(set([ i for i in self.infiles ])) )
        self.topdirs = list(set(self.topdirs))
        self.sqldbs = self.getDBfiles()
        self.bcsnapshots = self.getBCSnapshots()
        return self.topdirs
    
    def getDBfiles(self):        
        indbs = [ g for pd in self.topdirs for g in glob.glob(f"{pd}/*/mysql.db") ]
        indbs = list(set(indbs))
        self.pooldirs = list(set([ str(pathlib.Path(i).parents[0]) for i in indbs ]) )
        return indbs

    def getBCSnapshots(self):
        indbs = [ g for pd in self.topdirs for g in glob.glob(f"{pd}/*/SnapshotBadChannel.db") ]
        indbs = list(set(indbs))
        return indbs
        # /eos/project/a/atlas-larcalib/AP/00473386_00473391_00473394_FMhighEtaBack_HIGH_2/pool_files/SnapshotBadChannel.db

if __name__ == "__main__":  
    parser = argparse.ArgumentParser()
    #parser.add_argument( '-inputLog', '-l', type=str, required=True, dest='inputLog', help='Name of input log file which lists the runs' )
    parser.add_argument( 'inputLog', type=str,  help='Name of input log file which lists the runs' )
    parser.add_argument( '-logDir', type=str, dest='logDir', default="/afs/cern.ch/user/l/lardaq/public/hfec-calibration-logs", help="Input path for log files. Default %(default)s." )
    parser.add_argument( '-inDir', '-i', type=str, default='/eos/project-a/atlas-larcalib/AP', help="Input directory containing root files from the processing. Default %(default)s.")
    args   = parser.parse_args()


    if not args.inputLog.endswith(".log"):
        args.inputLog += ".log"

    logPath = f"{args.logDir}/{args.inputLog}"

    if not os.path.isfile(logPath):
        print(f"Couldn't find file {logPath}")
        sys.exit()

    theLog = logFile(logPath)

    # Get the list of .db sql files & BadChannels snapshots
    theLog.getTopDirs(args.inDir)
    
    rampFiles = [ f for f in theLog.rootFiles if "LArRamp" in f ]
    #print(len(rampFiles), "ramp root files")
    treeName = "RAMPS"

    branches = [ "channelId","X","Xi" ]
    step_size = "500 MB"
    verbose=True
    #cut = "Xi==2"

    chans = []
    rampcut = 500
    for rf in rampFiles:
        #print(rf)
        infile = R.TFile(rf,"READ")            
        df = R.RDataFrame(infile.Get(treeName))
        #print(df.Count().GetValue())
        df = df.Filter("Xi == 2")
        #print(df.Count().GetValue())

        df = df.Filter(f"abs(X[1]) > {rampcut}")
        #print(df.Count().GetValue())
        chans.extend([int(ch) for ch in df.AsNumpy(["channelId"])["channelId"]])
        
    
    criteria = {}
    criteria["want"] = ["SC_ONL_ID", "LATOME_FIBRE", "CALIB", "BadSC"]#, "CL"]
    criteria["showdecimal"] = True
    criteria["have"] = ["SC_ONL_ID"]
    criteria["have_val"] = [ ",".join([str(ch) for ch in chans])]
    if len(chans) > 100:
        print(len(chans), "channels with ramp >", rampcut, "... probably a bad set, so won't query")
        sys.exit()
    elif len(chans) == 0:
        print("NO CHANNELS WITH RAMP >", rampcut)
        sys.exit()
    print(len(chans), "channels with ramp >", rampcut, ":",chans)
    pmdf = pd.DataFrame(printMapping.query(**criteria), columns=criteria["want"])
    pmdf = pmdf.astype({'SC_ONL_ID':'int'})
    print(pmdf)
    unflagged = pmdf[pmdf.BadSC == '-']
    
    #print(pmdf[pmdf['SC_ONL_ID'].isin(chans)])
          
