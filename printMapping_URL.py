import sys
import requests
import pandas as pd
from requests.structures import CaseInsensitiveDict
import urllib3
urllib3.disable_warnings()

import argparse
        
    
def getDetName(benc, pn, ft):
    if pn == 0 :
        side = "C"
    elif pn == 1:
        side = "A"
    if benc == 0: 
        part = "EMB"
    elif benc == 1:
        part = "EMEC"
        if ft == 3 or ft == 10 or ft == 16 or ft == 22:
            part = "HEC"
        elif ft == 6:
            part = "FCAL"
    return part+side

def getDetNameFromFEB(onId):
    if "x" in str(onId):
        onId = int(onId, 16)
    else:
        onId = int(onId)
    pn = int( ((onId)>>24)&0x1 )
    benc = int( ((onId)>> 25)&0x1 )
    ft = int( ((onId)>>19)&0x1F )
    sl = int( (((onId)>>15)&0xF)+1 )
    detname = getDetName(benc, pn, ft)
    return detname

class FEB():
    def __init__(self,onId):
        if "x" in str(onId):
            self.onId = int(onId, 16)
        else:
            self.onId = int(onId)
        self.pn = int( ((onId)>>24)&0x1 )
        self.benc = int( ((onId)>> 25)&0x1 )
        self.ft = int( ((onId)>>19)&0x1F )
        self.sl = int( (((onId)>>15)&0xF)+1 )
        self.part, self.side = self.getDetName(self.benc, self.pn, self.ft)
        

def query(want):
    pmdfcols = want # ['ONL_ID','CH','SAM','HVLINES','HVCORR','FEB_ID','OFF_ID','CL','TT_COOL_ID']
    print(want)
    url = f"https://atlas-larmon.cern.ch/LArIdTranslatorBackend/api/data?columns={','.join(pmdfcols)}"
    print(url)

    headers = CaseInsensitiveDict()
    headers["Accept"] = "application/json"

    try:
        resp = requests.get(url, headers=headers, verify=False)
    except:
        print("Problem url")
        sys.exit()

    print(type(resp.content))
    #df = pd.read_json(resp.content)

    try:
        j = resp.json()
    except:
        print("Problem json")
        sys.exit()
    try:
        df = pd.DataFrame.from_dict(j)
        print(df)
    except:
        print(j)
        print("DF didn't work")
        sys.exit()

    
    #df['DETNAME'] = df.FEB_ID.apply(getDetNameFromFEB)
    #print(df)
    #chans = list([int(d) for d in df.ONL_ID.values])
    #if 947419136 not in chans: print("not there")

    #print(len(chans))

    for SCg in df.SC_GROUP.unique():
        TTs = list(df[df.SC_GROUP == SCg].TT_COOL_ID.unique())
        print(SCg,":", TTs)
        print(len(TTs))
    
    
if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-want', '-w', nargs='+', type=str, dest='want', help='Variable(s) which you want to retrieve from the database, e.g. ONL_ID ETA PHI') # add example
    args   = parser.parse_args()
    query(args.want)
