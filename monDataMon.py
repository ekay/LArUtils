from subprocess import Popen, PIPE
from datetime import datetime

def printStyle(string, style):
    if style.lower() == "purple":
        start = '\033[95m'
    elif style.lower() == "cyan":
        start = '\033[96m'
    elif style.lower() == "darkcyan":
        start = '\033[36m'
    elif style.lower() == "blue":
        style = '\033[94m'
    elif style.lower() == "green":
        start = '\033[92m'
    elif style.lower() == "yellow":
        start = '\033[93m'
    elif style.lower() == "red":
        start = '\033[91m'
    elif style.lower() == "bold":
        start = '\033[1m'
    elif style.lower() == "underline":
        start = '\033[4m'
    else:
        start = None
    if start is None:
        print(string)
    else:
        end = '\033[0m'
        print(start + string + end)

def dataLists(pc, path='/data/via_script/',ext=".dat"):
    stdout, stderr = Popen(['ssh', pc, 'ls  '+path+'*'+ext+'*'],stdout=PIPE).communicate()
    stdout = [s for s in stdout.decode("utf-8").split("\n") if s != '']
    return stdout, stderr


def dataInfoLists(pc, path='/data/via_script/',ext=".dat"):
    stdout, stderr = Popen(['ssh', pc, 'ls -lh '+path+'*'+ext+'*'],stdout=PIPE).communicate()
    stdout = [s for s in stdout.decode("utf-8").split("\n") if s != '']
    return stdout, stderr

def serverStatus(pc):
    if pc == 'pc-lar-mon-01' or pc == 'pc-lar-mon-02':
        txtPath = '/data/stop_LDPB.txt'
    else:
        txtPath = '/data/*/stop_LDPB.txt'
        
    stdout, stderr = Popen(['ssh', pc, 'for f in '+txtPath+'; do content=$(cat $f); echo $f : $content; done'],stdout=PIPE).communicate()
    stdout = [s for s in stdout.decode("utf-8").split("\n") if s != '']
    return stdout, stderr


def getDates(filenames):
    dates = []
    for fi in filenames:
        f = fi.split(".")[-2]
        if  f.count("-")>1:
            datestr = ('-').join(f.split("-")[-2:])
        else:
            datestr = f.split("_")[-1]

        datetime_object = datetime.strptime(datestr, '%y%m%d-%H%M%S')
        dates.append(datetime_object)
    return dates

def main():
    print("Current time is",datetime.now())
    for mon in [ 'pc-lar-mon-01', 'pc-lar-mon-02', 'pc-lar-mon-03' ]:
        printStyle("** "+mon+" **", "bold")
        
        dataList, stderr = dataLists(mon)
        dataInfoList, stderr = dataInfoLists(mon)

        
        logList, stderr = dataLists(mon, path='/data/via_script/logs/',ext=".log")

        dataTimes = getDates(dataList)
        dataSizes = {}
        for dil in dataInfoList:
            di = [ d for d in dil.split(" ") if d != "" ]
            f = di[-1]
            dataSizes[f] = di[4]

        logTimes = getDates(logList)
        print(len(dataList), ".dat files")
        print(len(logList), ".log files")
        print("- .dat files are:")

        dataDict = dict(zip(dataList, dataTimes))
        logDict = dict(zip(logList, logTimes))

        
        sortedData = sorted(dataDict.items(), key=lambda p: p[1], reverse=True)
        sortedLogs = sorted(logDict.items(), key=lambda p: p[1], reverse=True)
        
        printStyle("Latest data:","underline")
        for d in sortedData:
            if sortedData.index(d) < 5:
                print(d[0], "DATE:", d[1],"SIZE:", dataSizes[d[0]])
        printStyle("Latest logs:","underline")
        for d in sortedLogs:
            if sortedLogs.index(d) < 5:
                print(d[0], "("+str(d[1])+")")

        print("- Server status:")
        status, stderr1 = serverStatus(mon)
        for stat in status:
            statno = int(stat.split(":")[1].strip())
            if statno == 1:
                printStyle(stat, "red")
            else:
                printStyle(stat, "green")


if __name__ == "__main__":
    main()
