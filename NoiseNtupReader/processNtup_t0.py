import os, sys, glob, time
import pandas as pd
import numpy as np
import ROOT as R
R.gROOT.SetBatch(True)

import uproot as U

stream = "*"
camp = "data23_hi"
amiglob="f"
run = "*"

if run != "*":
    run = str(run).zfill(8)

treename = "LAr/NoisyRO/Summary/LArNoise"
#camp = "data22_13p6TeV"
rootpath = "/eos/atlas/atlastier0/rucio/"+camp+"/"+stream+"/"+run+"/"+camp+".*."+stream+".merge.HIST."+amiglob+"*/"+camp+".*."+stream+".merge.HIST."+amiglob+"*.1"
print("Wildcard:",rootpath)
rootfiles = glob.glob(rootpath)
runs = sorted(list([int(r.split("/")[7]) for r in rootfiles]))
runs = sorted(list(set(runs)),reverse=True)
print(len(runs), "runs:")
print(runs)
infiles = {}
for run in runs:
    print("*"*10, "Run",run)
    rfile = [r for r in rootfiles if "/00"+str(run)+"/" in r]
    amis = sorted(list(set(([ r.split(".")[-3] for r in rfile]))))
    # newest_ami = amis[-1]
    #newest_ami = "f1408_h449"
    amis_streamcount = { a: list(set([ r.split("/")[6] for r in rfile if "."+a+"." in r ])) for a in list(set(amis)) }
    amis = [ a for a in amis if len(amis_streamcount[a]) > 2]
    print("Streams per ami tag:",amis_streamcount)

    if len(amis) == 0 :
        print("NOT ENOUGH STREAMS... skipping")
        continue
    newest_ami = amis[-1]
    print("Chosen AMI tag:",newest_ami)
    rfile = [r for r in rfile if  "."+newest_ami+"." in r]
    streams = list(set([ r.split("/")[6] for r in rfile ]))
    print("Streams:",streams)
    if len(streams) <2: continue

    dfs = []
    this_tree = "run_"+str(run)+"/"+treename
    totlen = 0
    for arrays,report in U.iterate({r:this_tree for r in rfile}, report=True, step_size="1 GB",allow_missing=True,library="pd"):
        print("Reading chunk from:", report.file_path)
        arrlen = len(arrays.index.values)
        print(arrlen,"entries")
        totlen += arrlen
        dfs.append(arrays)
    print(totlen, "entries total")
    if totlen == 0: continue
    df = pd.concat(dfs, ignore_index=True)

    cats = sorted(list(df.algo.unique()))
    print(cats)
    df = df.sort_values(by=['time_ns'])

    NoisyRO_types = {"BADFEBS":0x1,"TIGHTSATURATEDQ":0x2,"BADFEBS_W":0x8,"MININOISEBURSTLOOSE":0x10,"MININOISEBURSTTIGHT":0x20,"MININOISEBURSTTIGHT_PSVETO":0x40,"burstveto":0x4}

    print(list(NoisyRO_types.keys()))

    for nrt in NoisyRO_types.keys():
        df[nrt] = df["algo"]  & NoisyRO_types[nrt]  == NoisyRO_types[nrt]
        #df[nrt] = NoisyRO_types[nrt] & df["algo"] == NoisyRO_types[nrt]

        this_df = df[df[nrt] == True]
        print(nrt, len(this_df.index.values))
        if len(this_df.index.values) == 0 or len(this_df.index.values) == 1 : continue

        timevals = this_df.time_ns.values.astype('d') * 1e-6

        diffs = this_df.time_ns.diff().values.astype('d') * 1e-6
        diffs = diffs[~np.isnan(diffs)]

        nametag = f"{nrt}_{run}"
        hist_time = R.TH1D(f"time_{nametag}","Time of Flagged Event",len(timevals)-1,timevals[0],timevals[-1])
        hist_time.GetXaxis().SetTitle("Time ms)")
        hist_time.FillN(len(timevals), timevals, np.ones_like(timevals))

        #hist_diffs = R.TH1D(f"diffs_{nametag}","Time Difference Between Flagged Event",len(diffs)-1,diffs[0],diffs[-1])
        hist_diffs = R.TH1D(f"diffs_{nametag}","Time Difference Between Flagged Event",len(diffs)-1,-5,diffs[-1])
        hist_diffs.GetXaxis().SetTitle("Time Difference Since Last Flag of Same Type (ms)")
        hist_diffs.FillN(len(diffs), diffs, np.ones_like(diffs))

        
        canv = R.TCanvas()
        hist_time.Draw("hist")
        canv.Print(f"Time_{nametag}.png")

        hist_diffs.Draw("hist")
        canv.Print(f"TimeDiffs_{nametag}.png")
        
#BADFEBS: m_NoiseTime.algo |= 0x1;
#TIGHTSATURATEDQ: m_NoiseTime.algo |= 0x2;
#BADFEBS_W: m_NoiseTime.algo |= 0x8;
#MININOISEBURSTLOOSE: m_NoiseTime.algo |= 0x10;
#MININOISEBURSTTIGHT : m_NoiseTime.algo |= 0x20;
#MININOISEBURSTTIGHT_PSVETO: m_NoiseTime.algo |= 0x40;
# if ( m_NoiseTime.algo != 0 )  { if ( burstveto ) m_NoiseTime.algo |= 0x4;
