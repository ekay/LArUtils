import os, sys, glob, time
import ROOT as R
import uproot as U

def process(run, stream, ami="x"):
    topdir="/eos/atlas/atlascerngroupdisk/det-larg/Tier0/perm/"

    dirs = glob.glob(f"{topdir}/*/{stream}/00{run}")
    if len(dirs) == 0:
        print("Found no directory for run and stream")
        sys.exit()
    topdir= dirs[0]

    dirs = glob.glob(f"{topdir}/*{run}*")

    if len(dirs) == 0:
        print(f"Found no subdirectory in {topdir}/*{run}*")
        sys.exit()
    #/eos/atlas/atlascerngroupdisk/det-larg/Tier0/perm/data23_hi/physics_CosmicCalo/00462022/data23_hi.00462022.physics_CosmicCalo.merge.NTUP_LARNOISE.x786_c1427_c1375/data23_hi.00462022.physics_CosmicCalo.merge.NTUP_LARNOISE.x786_c1427_c1375._000
    dirs = [ d for d in dirs if f"LARNOISE.{ami}" in d ]
    if len(dirs) == 0:
        print(f"Found no directory with requested ami tag {ami} - checked {dirs} for 'LARNOISE.{ami}'")
        sys.exit()
              
    topdir = dirs[0]
    files = glob.glob(f"{topdir}/*{run}*")
    print(f"Found {len(files)} files")
    if len(files) == 0 : sys.exit()
    for infile in files:
        print(f"** Reading from {infile} **")
        tree = U.open(infile)["CollectionTree"]
        #print(tree.keys())
        #print(tree.typenames())
        start = time.time()
        nev=0

        testbr = [ br for br in tree.typenames().keys() if "std::vector<std::vector" not in tree.typenames()[br] ]

        for arrays in tree.iterate(expressions=testbr,step_size=300):
            nev+=len(arrays)
            print(repr(arrays))
            print(arrays.NoisyCellQFactor)
        print("took", time.time()-start, nev,"events")


if __name__ == "__main__":
    runnum = int(sys.argv[1])
    if len(sys.argv) > 2:
        stream = sys.argv[2]
    else:
        stream = "physics_CosmicCalo"

    print(f"Processing run {runnum} in stream {stream}")


    process(runnum,stream)


