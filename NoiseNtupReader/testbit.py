test = 0

def convert(num, in_num=test):
    val = in_num
    val |= num
    print(val, hex(val), bin(val))
    return val
print(bin(0))
test_1 = convert(0x1)
test_2 = convert(0x2)
test_8 = convert(0x8)
test_10 = convert(0x10)
test_20 = convert(0x20)
test_40 = convert(0x40)

test_1_2 = convert(0x2, test_1)
print(test_1, test_2, test_8, test_10, test_20, test_40)
print(test_1_2)
