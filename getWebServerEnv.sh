#!/bin/sh

#LCG=$1
#export latest_LCG=$(ls -p -t -d  /cvmfs/sft.cern.ch/lcg/views/*ATLAS* | head -1)
#test=$(ls $latest_LCG/$(uname -p)*el9*-opt/setup.sh)
#echo $test
#source $latest_LCG/$(uname -p)*el9*-opt/setup.sh
#vars=( PYTHONPATH PYTHONHOME LD_LIBRARY_PATH ROOTSYS C_INCLUDE_PATH CPLUS_INCLUDE_PATH CMAKE_PREFIX_PATH MANPATH PATH ROOT_INCLUDE_PATH CORAL_AUTH_PATH CORAL_DBLOOKUP_PATH TNS_ADMIN )

source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh
vars=( PATH PYTHONPATH LD_LIBRARY_PATH )


#cmd="source /cvmfs/sft.cern.ch/lcg/views/${LCG}/x86_64-el9-gcc13-opt/setup.sh"
echo ${cmd}
eval ${cmd}


apache=1

for var in ${vars[@]}; do
    if [ -n "${var}" ]; then
        if [ $apache -eq 1 ]; then
	        echo "SetEnv ${var} \"${!var}\""
        else
	        echo "export ${var}=\"${!var}\""
        fi
    else
        if [ $apache -eq 1 ]; then
	        echo "SetEnv ${var} \"\""
        else
	        echo "export ${var} \"\""
        fi
    fi
    
done
