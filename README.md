# LAr Utils

## PulsePattern.py
Pulse pattern translator

```
python PulsePattern.py $path_to_pattern_file $partition
```

## printMapping.py

```
python printMapping.py -h 
```
for available options. 
This is a new version of the script written by Clement. It should return any value which is available in the database. 
Some special options, e.g. the DET can be the full det name, such as 'EMBA', rather than just 'EMB'

e.g.
```
python printMapping.py -t DET -i EMBA -w ONL_ID -d LArId.db -f -dec
```
Prints all online IDs for EMBA using local database LArId.db, in fancy format (-f) with decimal values for online IDs shown in parentheses next to the hex (-dec)

## checkPingingOutput.py
```
python checkPingingOutput.py $path_to_pinging_log
```
Prints out the TTCRx for all of the pinged boards in a more easily readable format


## BadChanEditor.py

# Philosophy 
- Loop through bad channels file 
- Look for bad channels which would be affected by moved FEBs 
- If feb was swapped, remove the entry, change it for new position online and offline ID

# Execution
- Dump athena bad channels text file:
```
athena.py -c 'IOVEndRun=9999999;OutputFile="bad_chan.txt";tag="LARBadChannelsOflBadChannels-RUN2-UPD3-00"' LArBadChannelTool/LArBadChannel2Ascii.py
```

- Scan the bad channels file for specific entries:
```
python BadChanEditor.py -i infilename.txt -scan "FT=14&&status=unstable,lowNoiseHG"
```

- Add a new bad channel:
```
python BadChanEditor.py -o outfilename.txt -add "0 0 4 15 50 0 distorted"
```
- Remove one of the bad channels:
```
python BadChanEditor.py -o outfilename.txt -rem "0 0 4 15 50 0 distorted"
```
- Switch some entries around:
```
python BadChanEditor.py -switch "FT=4&&slot=8&&channel=127&&status=distorted->status=deadCalib"
```

Run with `-help` for command line options breakdown. In some cases, another output file called <outfilename>_annotated.txt will be created, documenting the changes made. 


# For Swapped FEBs Consideration

- Get Online and Offline ID mapping:
```
for part in EMBA EMBC EMECA EMECC HECA HECC FCALA FCALC; do
python printMapping.py -t DET -i ${part} -w OFF_ID,ONL_ID -d ../SwappedFEBs/LArId.db > BadChannels/OffID_OnlID_${part}.txt
done

cat OffID_OnlID_* > OffID_OnlID_ALL.txt
```

- Get combined list of swapped online/offline IDs using getFEBswaps

- Run with the `doSwaps` flag:
```
python BadChanEditor.py -doSwaps -o badchansswapped.txt"
```


# Information on ATHENA Bad Channels Tools
You can either use the recipe for bad channels updated on:https://twiki.cern.ch/twiki/pub/AtlasComputing/LArDatabaseUpdateHowTo/Instructions_BC_May2017.txt 

OR

Use just one substep of this by directly dumping the text file, as demonstrated above. This uses `athena/LArCalorimeter/LArBadChannelTool/src/LArBadChannel2Ascii.cxx`
The layout of this text file is as follows:
```
barrel_ec pos_neg FT Slot channel 0 status(es)  # channelID (onlineID)-> offlineID
```

A few different tags are used for bad channels: UPD3-00 is used in calibration, UPD1-00 is used online (in HLT), UPD4-21 is used in offline T0 reconstruction...
￼
There is one cavetat in moving these dead channels: deadPhys is probably not going to move with swapped FEBs, as well as deadCalib.

