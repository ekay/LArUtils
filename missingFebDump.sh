#!/bin/bash

if [[ $# -lt 1 ]]; then
    echo "Please provide the run number as an argument"
fi

runnumber=$1
lbnumber=0
Folder="/LAR/BadChannelsOfl/MissingFEBs"


#Get UPD4-nn tag connected to 'current':
echo "Resolving current folder-level tag suffix for ${Folder} ...."
fulltag=`getCurrentFolderTag.py "COOLOFL_LAR/CONDBR2" $Folder` 
echo "Full tag is ${fulltag}"
upd4TagName=`echo $fulltag | grep -o "RUN2-UPD4-[0-9][0-9]"` 
gtag=`echo $fulltag | grep BLKPA | awk '{print $2}'`
echo "Found $gtag $upd4TagName"
fulltages=`getCurrentFolderTag.py "COOLOFL_LAR/CONDBR2" $Folder True` 
upd1TagName=`echo $fulltages | grep -o "RUN2-UPD1-[0-9][0-9]"` 
echo "Found $upd1TagName"
#create a tag string from folder
IFS='/' 
read -r -a array <<< "$Folder"
fldtag=""
for i in ${array[@]}
do
  fldtag+=$i
done
IFS=' ' 
echo "fldtag is $fldtag"
echo "upd4tag is $upd4TagName"
upd4TagName=RUN2-UPD4-00
oldTextFile="mf_previous.txt"


echo "Running athena to read current database content..."
acmd='athena.py -c "OutputFile=\"'${oldTextFile}'\";RunNumber='${runnumber}';LBNumber='${lbnumber}';Folder=\"'${Folder}'\";GlobalTag=\"'${gtag}'\";tag=\"'${fldtag}'-'${upd4TagName}'\";" LArBadChannelTool/LArMissingFebs2Ascii.py'
echo ${acmd}
eval ${acmd} > oracle2ascii.log 2>&1
if [ $? -ne 0 ];  then
    echo "Athena reported an error reading back sqlite file ! Please check oracle2ascii.log!"
    exit 5
fi
