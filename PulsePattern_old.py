#!/bin/python
# Translate pulse patters, with info about number of pulsed cells
# Author: Ellis Kay <ellis.kay@cern.ch>
# Date: 14/12/2020
################################################################################

# TO DO
# Add full pulse pattern list (i.e. order of pulsing)
# Add input arguments to query given cell/SC pulsed
# Pattern writer? Show neighbouring cells?


import sys,os


def getlist( cmd ):
    # print( cmd )
    translator=os.popen(cmd).read()
    layout = []
    if "ERROR" in translator:
        print(translator)
        sys.exit()
    for feb in translator.strip().split('\n'):
        if feb.strip() != "":
            line = feb.strip().split(" ")
            layout.append(line)
    return layout

def getEtaPhiRange(partition,SC=False):
    if SC:
        cmd = "python3 printMapping.py -t DET -i "+partition+" -w SCETA,SCPHI"
    else:
        cmd = "python3 printMapping.py -t DET -i "+partition+" -w ETA,PHI"
    result = getlist(cmd)
    etas = list(set([ float(r[0]) for r in result ]))
    phis = list(set([ float(r[1]) for r in result ]))
    etas = [ e for e in etas if int(e)!=-9999 ]
    phis = [ p for p in phis if int(p)!=-9999 ]

    return min(etas),max(etas),len(etas),min(phis),max(phis),len(phis)


class patternReader:
    def getPulsed(self, line):
        v = []
        for i in range(0,32):
            if (int(line[0],16) & (1 << i)): v.append(i)
            if (int(line[1],16) & (1 << i)): v.append(i + 32)
            if (int(line[2],16) & (1 << i)): v.append(i + 64)
            if (int(line[3],16) & (1 << i)): v.append(i + 96)
        return v

    def __init__(self, infile):
        self.infile = infile
        self.data = {}
        self.defs = [ "nDAC","DAC","nDel","Del","nPattern" ]

        with open(self.infile, "r") as f:            
            for linenum,line in enumerate(f,0):
                if linenum < len(self.defs):
                    self.data[self.defs[linenum]] = [ int(s) for s in line.strip().split() ]
                else:
                    self.data["PatternLines "+str(linenum-len(self.defs)+1)] = line.strip().split()
                    self.data["Pattern "+str(linenum-len(self.defs)+1)] = self.getPulsed(line.strip().split())
                    

    def getData(self):
        return self.data

    def print_data(self):
        for k in self.data.keys():
            if "Pattern" in k:
                print( k.rjust(9), ":", self.data[k], "( len =",len(self.data[k]),")")
            else:
                print( k.rjust(9), ":", self.data[k])



def makePlot(coords, partition, name, SC=False):
    # Plotting
    etas = [ coords[ind][0] for ind,x in enumerate(coords) ]
    phis = [ coords[ind][0] for ind,x in enumerate(coords) ]
    lays = [ coords[ind][2] for ind,x in enumerate(coords) ]
    fig = plt.figure(figsize=(10,10))
    ax = plt.axes(projection='3d')
    ax.set_xlabel('eta')
    ax.set_ylabel('phi')
    ax.set_zlabel('layer')
    etamin,etamax,neta,phimin,phimax,nphi = getEtaPhiRange(partition,SC)
    #print(etamin,etamax,neta,phimin,phimax,nphi)
    if not SC: neta/=9
    if not SC: nphi/=8
    etabins = np.arange(etamin,etamax,(etamax-etamin)/neta)
    phibins = np.arange(phimin,phimax,(phimax-phimin)/nphi)     
    ax.set_xlim(etamin,etamax)    
    ax.set_ylim(phimin,phimax)
    ax.set_zlim(0,3)
    ax.set_xticks(etabins)
    ax.set_yticks(phibins)
    ax.tick_params(axis='x', labelrotation=45, labelsize=9)
    ax.tick_params(axis='y', labelrotation=-45, labelsize=9)

    for label in ax.xaxis.get_ticklabels()[::2]:
        label.set_visible(False)
    for tick in ax.xaxis.get_major_ticks():
        tick.set_pad(0)
    for label in ax.yaxis.get_ticklabels()[::2]:
        label.set_visible(False)
    for tick in ax.yaxis.get_major_ticks():
        tick.set_pad(0)

    #colour1=(1/256, 2/256, 4/256, 5/256)
    #colour2=(1/256, 2/256, 4/256, 5/256)
    
    #colourmap = np.array([1/256, 2/256, 4/256, 5/256])
    layers = np.array(lays,dtype='i')

    ax.zaxis.set_major_locator(MaxNLocator(integer=True))
    ax.scatter3D(np.array(etas,dtype='d'),
                 np.array(phis,dtype='d'),
                 np.array(lays,dtype='d'),
                 # c=colourmap[layers],
                 c=layers,
                 cmap='hsv',
                 vmin=0, vmax=3,
                 s=.5 )

    # 'gray')
    plt.savefig(name+"_"+partition+".png", bbox_inches='tight')

if __name__ == "__main__":
    #PrintMappingDir = '/afs/cern.ch/user/l/lardaq/public/detlar/scripts/checks'
    #dbpath = '/afs/cern.ch/user/l/larmon/public/prod/LArIdtranslator/LArId.db'
    if len(sys.argv) < 2:
        print("Need to pass patterns file as an argument")
        sys.exit()
    patternfile = sys.argv[1]
    if len(sys.argv) > 2:
        partition = sys.argv[2]
    else:
        partition = "EMBA"

    doPlot = False
    if doPlot:
        import numpy as np
        import matplotlib.pyplot as plt
        from mpl_toolkits import mplot3d
        from matplotlib.ticker import MaxNLocator
    print("*"*30)
    print("Pattern file = "+patternfile)
    print("Partition    = "+partition)
    print("*"*30)

    #printList = False
    printList = "Supercells"

    patterns = patternReader(patternfile)
    patterns.print_data()
    patternlines = patterns.getData()
    print("")
    # Print some info from the LArID db
    query = [ "FEB","LATOME_NAME","SAM","SC_ONL_ID","SCETA","SCPHI","ONL_ID","ETA","PHI" ] # "LTDB"

    coords = {}
    coordsSC = {}
    for k in patternlines.keys():
        if k.startswith("Pattern "):
            coords[k] = []
            coordsSC[k] = []
            
            for CL in patternlines[k]:
                cmd = "python3 printMapping.py -t DET,CL -i "+partition+","+str(CL)+" -w "+(",").join(query) # -d "+dbpath
                print(cmd)
                result = getlist(cmd)
                count={}
                allcount={}
                countstr=[]
                #print("oioi",len(result))
                coords[k].extend([(sublist[query.index("ETA")],
                                   sublist[query.index("PHI")],
                                   sublist[query.index("SAM")]) for sublist in result ])
                coordsSC[k].extend([(sublist[query.index("SCETA")],
                                   sublist[query.index("SCPHI")],
                                   sublist[query.index("SAM")]) for sublist in result ])
                for q in query:
                    if "ONL_ID" in q or "FEB" in q or "LTDB" in q or "LATOME" in q or "SAM" in q:
                        count[q] = list(set([ sublist[query.index(q)] for sublist in result ]))
                        allcount[q] = [ sublist[query.index(q)] for sublist in result ]
                        if (q=="FEB" or q=="LTDB"): name = q+"s"
                        elif q == "SAM": name = "Layers"              
                        elif "LATOME" in q: name = "LATOMEs"
                        elif "SC" in q: name = "Supercells"
                        elif q == "ONL_ID": name = "LAr Cells"
                        else: name = q

                        if q == "SAM":
                            countstr.append(str(len(count[q])).rjust(4)+" "+name+" ("+(", ").join(count[q])+")")
                        else:
                            countstr.append(str(len(count[q])).rjust(4)+" "+name+ "("+str(len(allcount[q]))+")")

                        if printList and name == printList:
                            print("List of "+printList+": "+(", ").join(count[q]))

                if len(countstr)!=0:
                    print("Calibration Line",CL,":",(", ").join(countstr))

            if doPlot:
                makePlot(coords[k], partition, k.replace(" ","_"))
                makePlot(coordsSC[k], partition, k.replace(" ","_")+"_SC",True)
