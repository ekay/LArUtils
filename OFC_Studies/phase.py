import ROOT as R
import sys, os, getpass
import uproot as U
import awkward as ak
import pandas as pd
import numpy as np
from argparse import ArgumentParser
user = getpass.getuser()

R.gROOT.SetBatch(True)
sys.path.append('../pickPhase')
from newShift import getPMinfo, getOFC, getPed
import condSub

from collections import deque

def split_list(input_list, chunk_size):
  # Create a deque object from the input list
  deque_obj = deque(input_list)
  # While the deque object is not empty
  while deque_obj:
      # Pop chunk_size elements from the left side of the deque object
      # and append them to the chunk list
      chunk = []
      for _ in range(chunk_size):
        if deque_obj:
          chunk.append(deque_obj.popleft())
        
      # Yield the chunk
      yield chunk

def plotPhases(ch, enes, taus, phases, maxe, mint, samplevec, ofca, ofcb, timings=["before", "after", "intime"], outdir="."):  # Make plot of phases for all timings

    tcols = {"before":R.kMagenta-7, "intime":R.kBlack, "after":R.kViolet+5}
    lcols = {"before":R.kMagenta-10, "intime":R.kGray, "after":R.kViolet-9}
    
    def makeLine(gr, val, col, style=1):
        line = R.TLine(val, gr.GetHistogram().GetMinimum(), val, gr.GetHistogram().GetMaximum())
        line.SetLineColor(col)
        line.SetLineWidth(2)
        line.SetLineStyle(style)
        return line

    gene, gtau = {}, {}
    for timing in timings:
        gene[timing] = R.TGraph(len(enes[timing]), np.array(phases,dtype='d'), enes[timing])
        gene[timing].SetTitle(f"Energy per phase for channel {ch} from {len(samplevec)} pulse(s)")
        gene[timing].SetMarkerStyle(20)
        gene[timing].SetMarkerColor(tcols[timing])
        gene[timing].SetName(f"ene_{timing}")

        gtau[timing] = R.TGraph(len(taus[timing]), np.array(phases,dtype='d'), taus[timing])
        gtau[timing].SetTitle(f"|#tau| per phase for channel {ch} from {len(samplevec)} pulse(s)")
        gtau[timing].SetMarkerStyle(20)
        gtau[timing].SetMarkerColor(tcols[timing])
        gtau[timing].SetName("tau")

    for timing in timings:
        gene[timing].GetHistogram().SetMaximum(max([gene[k].GetHistogram().GetMaximum() for k in gene.keys()]))
        gene[timing].GetHistogram().SetMinimum(min([gene[k].GetHistogram().GetMinimum() for k in gene.keys()]))

    canv = R.TCanvas(f"canv_{ch}", f"canv_{ch}", 1400, 800)
    canv.objs=[]

    leg_etau = R.TLegend(.89,.5,.98,.95)
    leg_etau.SetFillStyle(0)
    leg_etau.SetTextColor(R.kBlack)
    leg_etau.SetTextSize(.037)
    leg_etau.SetBorderSize(0)

    canv.Divide(2,2)
    canv.cd(1)
    R.gPad.SetLeftMargin(0.065)
    R.gPad.SetRightMargin(0.105)
    eline = {}
    tline = {}
    for timing in timings:
        gene[timing].GetHistogram().GetXaxis().SetLimits(0,49)
        if timings.index(timing)==0:
            gene[timing].Draw("ap")
        else:
            gene[timing].Draw("same,p")
        gene[timing].GetXaxis().SetTitle("Phase")
        gene[timing].GetYaxis().SetTitle("#Sigma(ai*si)")

        eline[timing] = makeLine(gene[timing], maxe[timing][0], tcols[timing])
        tline[timing] = makeLine(gene[timing], mint[timing][0], tcols[timing], 9)
        leg_etau.AddEntry(eline[timing], f"max E: {maxe[timing][0]}", "l")
        leg_etau.AddEntry(tline[timing], f"min |#tau|: {mint[timing][0]}", "l")
        eline[timing].Draw("samel")
        tline[timing].Draw("samel")
        canv.objs.append(eline[timing])
        canv.objs.append(tline[timing])

    for timing in timings:
        leg_etau.AddEntry(gene[timing], timing, "p")

    leg_etau.Draw("same")

    canv.objs.append(gene)

    eline2 = {}
    tline2 = {}
    canv.cd(2)
    R.gPad.SetLeftMargin(0.065)
    R.gPad.SetRightMargin(0.105)
    for timing in timings:
        gtau[timing].GetHistogram().SetMaximum(30)
        gtau[timing].GetHistogram().SetMinimum(0)
        gtau[timing].GetHistogram().GetXaxis().SetLimits(0,49)
        if timings.index(timing) == 0:
            gtau[timing].Draw("ap")
        else:
            gtau[timing].Draw("same,p")
        gtau[timing].GetXaxis().SetTitle("Phase")
        gtau[timing].GetYaxis().SetTitle("#Sigma(bi*si)/#Sigma(ai*si)")

        eline2[timing] = makeLine(gtau[timing], maxe[timing][0], tcols[timing])
        tline2[timing] = makeLine(gtau[timing], mint[timing][0], tcols[timing], 9)
        eline2[timing].Draw("samel")
        tline2[timing].Draw("samel")
        canv.objs.append(eline2[timing])
        canv.objs.append(tline2[timing])

    leg_etau.Draw("same")
    canv.objs.append(gtau)

    canv.cd(3)
    R.gPad.SetLeftMargin(0.065)
    R.gPad.SetRightMargin(0.02)
    #print(samplevec)
    meanpulse = np.array(ak.mean(samplevec, axis=0), dtype='d')
    maxsam = ak.max(samplevec) #ak.max(samplevec,axis=1)[0]
    minsam = ak.min(samplevec) #ak.min(samplevec,axis=1)[0]
    #print(meanpulse, maxsam,minsam)
    pulsegr = {}
    ngr=0
    for ev in samplevec:
        #print(ev)
        pulsegr[ngr] = R.TGraph(len(meanpulse), np.arange(0,len(meanpulse),dtype='d'), ev.to_numpy().astype('d'))
        pulsegr[ngr].SetTitle("Pulse Shape")
        pulsegr[ngr].GetHistogram().SetMinimum(minsam-(abs(minsam)*.05))
        pulsegr[ngr].GetHistogram().SetMaximum(maxsam+(abs(maxsam)*.05))
        pulsegr[ngr].GetHistogram().GetXaxis().SetLimits(0,5)
        if ngr == 0:
            pulsegr[ngr].Draw("Apl")
            pulsegr[ngr].GetXaxis().SetTitle("Sample")
            pulsegr[ngr].GetXaxis().SetTitle("ADC_BAS/8 - ped")
        else:
            pulsegr[ngr].Draw("same,pl")        
        canv.objs.append(pulsegr[ngr])
        ngr+=1
    meangr = R.TGraph(len(meanpulse), np.arange(0,len(meanpulse),dtype='d'), meanpulse)
    meangr.SetMarkerStyle(20)
    meangr.SetMarkerColor(R.kRed)
    meangr.Draw("same,p")
    canv.objs.append(meangr)


    leg_ofc = R.TLegend(.89,.5,.98,.95)
    leg_ofc.SetFillStyle(0)
    leg_ofc.SetTextColor(R.kBlack)
    leg_ofc.SetTextSize(.037)
    leg_ofc.SetBorderSize(0)

    canv.cd(4)
    R.gPad.SetLeftMargin(0.065)
    R.gPad.SetRightMargin(0.105)
    ofc_max = max(ak.max(ofca), ak.max(ofcb))
    ofc_min = min(ak.min(ofca), ak.min(ofcb))

    ofca_gr = R.TGraph(len(ofca), np.arange(0,len(ofca),dtype='d'), ofca.to_numpy().astype('d'))
    ofca_gr.SetTitle(f"OFCs for phase {maxe['intime'][0]}")
    ofca_gr.GetHistogram().SetMaximum(ofc_max+(abs(ofc_max)*0.05))
    ofca_gr.GetHistogram().SetMinimum(ofc_min+(abs(ofc_min)*0.05))
    ofca_gr.GetHistogram().GetXaxis().SetLimits(0,3)
    
    ofcb_gr = R.TGraph(len(ofcb), np.arange(0,len(ofcb),dtype='d'), ofcb.to_numpy().astype('d'))
    ofcb_gr.GetHistogram().SetMaximum(ofc_max+(abs(ofc_max)*0.05))
    ofcb_gr.GetHistogram().SetMinimum(ofc_min+(abs(ofc_min)*0.05))

    ofca_gr.SetLineColor(R.kRed)
    ofca_gr.SetMarkerColor(R.kRed)
    ofcb_gr.SetLineColor(R.kBlue)
    ofcb_gr.SetMarkerColor(R.kBlue)

    ofca_gr.Draw("Apl")
    leg_ofc.AddEntry(ofca_gr, "OFCa", "l")
    ofca_gr.GetXaxis().SetTitle("Sample")
    ofca_gr.GetYaxis().SetTitle("OFC")
    canv.objs.append(ofca_gr)
    ofcb_gr.Draw("same,pl")
    leg_ofc.AddEntry(ofcb_gr, "OFCb", "l")
    canv.objs.append(ofcb_gr)
    leg_ofc.Draw("same")
    
    canv.Print(f"{outdir}/Energies_Taus_{ch}.png")


def processPhases(inputFile, runnum, weekly_ped, treename="SCDIGITS", latomeSourceId=[], channelId=[], highMu=True, outdir=".", step_size="2 GB"):

    outstr = f"{runnum}_ped{weekly_ped}"
    
    if len(latomeSourceId) != 0:
        if len(latomeSourceId) == 1:
            outdir=f"{outdir}/LATOME_{latomeSourceId[0]}"
            outstr=f"{outstr}_LATOME_{latomeSourceId[0]}"
        else:
            outdir=f"{outdir}/LATOME_{latomeSourceId[0]}-{latomeSourceId[-1]}"
            outstr=f"{outstr}_LATOME_{latomeSourceId[0]}-{latomeSourceId[-1]}"
    elif len(channelId) != 0:
        if len(channelId) == 1:
            outdir=f"{outdir}/SC_ONL_ID_{channelId[0]}"
            outstr=f"{outstr}_SC_ONL_ID_{channelId[0]}"
        else:
            outdir=f"{outdir}/SC_ONL_ID_{channelId[0]}-{channelId[-1]}"
            outstr=f"{outstr}_SC_ONL_ID_{channelId[0]}-{channelId[-1]}"

            
            
    condSub.chmkDir(outdir)
            
    ofcData, ofcChids = getOFC(weekly_ped, isCali=False, verbose=True, highMu=highMu)
    pedData, pedChids = getPed(weekly_ped, verbose=True)
    pmdf = getPMinfo(run=runnum)

    chans = list(set(ofcChids))
    chdiff = []
    if len(ofcChids) != len(pedChids):
        chdiff = [ ch for ch in ofcChids if ch not in pedChids ]
        chdiff.extend([ch for ch in pedChids if ch not in ofcChids])
        chdiff = list(set(chdiff))
        print("PED AND OFC CHIDs DIFFER:", ", ".join([str(s) for s in chdiff]))

    branches = ["channelId","samples_ADC_BAS"]

    cut=None
    
    if len(latomeSourceId) != 0:
        branches.append("latomeSourceId")
        cut = "|".join([ f"( latomeSourceId == {lid} )" for lid in latomeSourceId ]) # too long if more than fcal
        print("Using cut:", cut)
    elif len(channelId) != 0:
        cut = "|".join([ f"( channelId == {lid} )" for lid in channelId ]) # too long if more than fcal
        print("Using cut:", cut)
    verbose=True

    fibvals = {}

    out_phasechange = open(f"{outdir}/changedPhases_{outstr}.log", "w")
    printcols = ["Channel", "oldph_e", "oldph_t", "newph"]
    outformat = "{:<12s}"*len(printcols)
    printcols.extend(["LATOME", "FIBRE", "LTDB_SCA", "CALIB", "BadSC"])
    outformat += "{:<24s}{:<24s}{:<10s}{:<20s}{:<30s}"
    out_phasechange.write(outformat.format(*printcols) + "\n")

    out_phaselist = open(f"{outdir}/phaseList_{outstr}.txt", "w")


    phaselist = {}

    timings = ["before", "after", "intime"]
    outfile = {}
    for timing in timings:
        outfile[timing] = open(f"{outdir}/phases_{timing}_{outstr}.txt","w")

    for arrays,report in U.iterate(files=[f'{inputFile}:{treename}'], expressions=branches, step_size=step_size, recover=True, library="ak", report=True, allow_missing=True, cut=cut):

        these_ch = list(set(list(arrays.channelId)))
        if verbose: print("Reading chunk from:", report.file_path, "...", len(these_ch), "channels")


        for ch in these_ch:
            pm_info = pmdf.loc[int(ch)]

            fibre = pm_info['LATOME_FIBRE']
            badSC = pm_info['BadSC']
            if fibre not in fibvals.keys():
                fibvals[fibre] = [0,0]
            this_arr = arrays[arrays.channelId==ch]
            if ch in ofcChids:
                ofcs = ofcData[ofcData.channelId==ch]
            else:
                out_phaselist.write(f"{ch} NONE - NO OFC\n")
                continue
            if ch in pedChids:
                ped = pedData[pedData.channelId==ch].ped[0]
            else:
                out_phaselist.write(f"{ch} NONE - NO PED\n")
                continue

            this_arr["samples"] = this_arr.samples_ADC_BAS/8 - ped
            this_arr["maxind"] = ak.argmax(this_arr.samples,axis=1)
            # Remove small pulses
            #this_arr = this_arr[(ak.max(this_arr.samples,axis=1) - ak.min(this_arr.samples,axis=1) > 1000)]
            maxover = 10
            this_arr = this_arr[(ak.max(this_arr.samples,axis=1) >= maxover)]
            if len(this_arr)==0:
                print(f"All pulses for channel {ch} have peak < {maxover}")
                out_phaselist.write(f"{ch} NONE - ALL PULSES PEAK-PED < {maxover}\n")
                continue

            this_arr["intime"] = this_arr.samples[:,1:5] # first and last sample should be out of time
            this_arr["before"] = this_arr.samples[:,0:4] # first and last sample should be out of time
            this_arr["after"] = this_arr.samples[:,2:6] # first and last sample should be out of time
            ofca = ofcs.OFCa[:,0:4]
            ofcb = ofcs.OFCb[:,0:4]

            phases = ofcs.Phase

            def getEneTau(these_samples, outstr):
                maxe = [22,-1000]
                mint = [22,1000]
                phdict = {}

                for ph in phases: # probably could do this in a columnar way
                    ofca_ph = ak.Array(np.tile(ofca[ph],(len(these_samples),1)))
                    ofcb_ph = ak.Array(np.tile(ofcb[ph],(len(these_samples),1)))
                    sam_ofca = these_samples*ofca_ph
                    sam_ofcb = these_samples*ofcb_ph
                    ene = ak.sum(sam_ofca,axis=1)
                    tau = ak.sum(sam_ofcb,axis=1) / ene
                    avene = ak.mean(ene)
                    avtau = ak.mean(tau)
                    if avene > maxe[1]:
                        maxe = [ph,avene]
                    if abs(avtau) < abs(mint[1]):
                        mint = [ph,avtau]
                    phdict[ph] = [avene,abs(avtau)]

                if maxe[0] == mint[0]:
                    outfile[outstr].write(f"{ch}  {maxe[0]}  # {fibre}\n")
                    if "intime" in outstr:
                        fibvals[fibre][0] += 1
                else:
                    outfile[outstr].write(f"!!!! {ch}  {maxe[0]}  {mint[0]} # {fibre}\n")
                    if "intime" in outstr:
                        fibvals[fibre][1] += 1
                enes = np.array([phdict[ph][0] for ph in phdict.keys()], dtype='d')
                taus = np.array([phdict[ph][1] for ph in phdict.keys()], dtype='d')

                return enes, taus, maxe, mint




            enes, taus, maxe, mint = {}, {}, {}, {}
            for timing in timings:
                enes[timing],taus[timing],maxe[timing],mint[timing] = getEneTau(this_arr[timing], timing)

                if maxe[timing][0] != mint[timing][0]:
                    tmp_newph =  -1
                    print("*"*20)
                    print("Bad", ch, "timing", timing, maxe[timing], mint[timing])
                    sorted_e = np.argsort(enes[timing])[::-1]
                    sorted_t = np.argsort(abs(taus[timing]))
                    print("Phases for max E, ordered:", list(sorted_e))
                    print("Phases for min t, ordered:", list(sorted_t))
                    for ph in sorted_e[0:5]:
                        if mint[timing][0] == ph:
                            tmp_newph = ph
                    # Should we add protection against picking 0 or 49?    
                    if tmp_newph == -1: #  and maxe[0]==0 or maxe[0]==49:
                        if np.isin(mint[timing][0], sorted_e[0:10]):
                            print(ch, "timing", timing, "FIXED", mint[timing][0], "in top 10")
                            tmp_newph = mint[timing][0]
                        else:
                            intercept = sorted_t[0:10][np.in1d(sorted_t[0:10], sorted_e[0:10])]
                            if len(intercept) > 0:
                                print(ch, "timing", timing, "FIXED", intercept[0],"in",intercept, sorted_e[0:10], sorted_t[0:10])
                                tmp_newph = intercept[0]
                            else:
                                print(ch, "timing", timing, "Couldn't improve", maxe[timing][0], mint[timing][0])

                    if timing == "intime":
                        toprint = [str(s) for s in [ ch, maxe[timing][0], mint[timing][0], tmp_newph]]
                        toprint.extend([pm_info["LATOME_NAME"], pm_info["LATOME_FIBRE"], pm_info["LTDB_SCA"], pm_info["CALIB"], pm_info["BadSC"]])

                        out_phasechange.write(outformat.format(*toprint) + "\n")


                    if tmp_newph != -1:
                        maxe[timing] = [tmp_newph, enes[timing][tmp_newph]]
                        mint[timing] = [tmp_newph, taus[timing][tmp_newph]]
                        # print("Fixed", ch, "timing", timing, maxe[timing], mint[timing])
                    print("*"*20)
            if ch not in phaselist.keys():
                phaselist[ch] = maxe["intime"][0]
            else:
                print("HMMMMM", ch, phaselist[ch], maxe["intime"][0])
            out_phaselist.write(f"{ch} {phaselist[ch]}\n")

            # Save plots if the picked phases seem suspicious
            if ( maxe["intime"][0] != mint["intime"][0] ) or maxe["intime"][0] < 5 or maxe["intime"][0] > 44:
                plotPhases(ch, enes, taus, phases, maxe, mint, this_arr.samples, ofca[maxe["intime"][0]], ofcb[maxe["intime"][0]], timings, outdir=outdir)  # Make plot of phases
        for timing in timings:
            outfile[timing].close()

        break # Let's stop after the first 'chunk' of the file

    for fibre in fibvals.keys():
        bad = fibvals[fibre][1]
        good = fibvals[fibre][0]
        total = good + bad
        SCs = list(set(pmdf[pmdf.LATOME_FIBRE == fibre].index.values))
        #if total != len(SCs):
        #    print(f"!!!! Fibre {fibre} has {total} entries in this data, but we expect {len(SCs)} SCs")

        if total != 0:
            if bad / total >= .5:
                print(f"Fibre {fibre} good: {good}, bad: {bad} (expect {len(SCs)} connected SC)")

    out_phasechange.close()
    out_phaselist.close()



if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-i', '--dataFile', dest='dataFile', default="/eos/home-e/ekay/LAr/LArPrompt/486658_skim/output/output_allskim.root", help="Input SCDIGITS file. Default %(default)s.")
    parser.add_argument('-o', '--outDir', dest='outDir', default=f"{os.getcwd()}/output_phases", help="Output directory for log files and plots. Default %(default)s.")
    parser.add_argument('-p', '--ped', dest='weekly_ped', type=int, default=479769, help="Pedestal number from weekly set to be used (for OFCs). Default %(default)s.")
    parser.add_argument('-r', '--runNum', dest='runNum', default=486658, help="Run number. Default %(default)s")
    parser.add_argument('-l', type=int ,dest='latomeSourceId', nargs='+', default=[], help='List of latome source IDs to scan over (optional)')
    parser.add_argument('-c', type=int ,dest='channelId', nargs='+', default=[], help='List of SC IDs to scan over (optional)')
    parser.add_argument('--runBatch', dest='runBatch', action='store_true', default=False, help='Submit jobs to condor?')
    parser.add_argument('--logDir', type=str, dest='logDir', default="/afs/cern.ch/work/"+user[0]+"/"+user+"/public/submit_phaseCheck", help="Log directory. Default %(default)s")
    parser.add_argument('-jobtime','--timePerJob', type=int, dest='jobtime', default=86400, help="Time limit (in seconds) for each condor job. Default %(default)s")
    args = parser.parse_args()

    if not os.path.isfile(args.dataFile):
        print(f"PROVIDED FILE {args.dataFile} DOES NOT EXIST!")
        sys.exit()


    condSub.chmkDir(args.outDir)

    treename = "SCDIGITS"

    if len(args.latomeSourceId) == 0 and len(args.channelId) == 0:
      thefile = U.open(args.dataFile)
      chids = list(set(thefile[treename].arrays(["channelId"], library="np")["channelId"]))
      latomeSourceIds = list(set(thefile[treename].arrays(["latomeSourceId"], library="np")["latomeSourceId"]))
      print(len(chids), "and", len(latomeSourceIds), "LATOMEs in data")
      #args.channelId = chids
      args.latomeSourceId = latomeSourceIds

    if len(args.latomeSourceId) != 0:
        chunks_latome = list(split_list(args.latomeSourceId, 4))
        print(len(chunks_latome), "jobs for LATOMEs")
        
    if len(args.channelId) != 0:    
        chunks_chid = list(split_list(args.channelId, 1000))
        print(len(chunks_chid), "jobs for channels")
   
    outstr = f"{args.runNum}_ped{args.weekly_ped}"

    
    if args.runBatch:
        condSub.chmkDir(args.logDir)
        jobtag = f"run_phaseCheck_{outstr}"
        tmprundir = f"{args.logDir}/{outstr}"
        thistmpdir = tmprundir+"/sub"
        condSub.chmkDir(thistmpdir)
        logdir = tmprundir+"/log"
        errdir = tmprundir+"/err"
        outdir = tmprundir+"/out"
        condSub.chmkDir(logdir)
        condSub.chmkDir(errdir)
        condSub.chmkDir(outdir)


        # Write input arguments
        argsname = f"{thistmpdir}/runArgs_phaseCheck_{outstr}.txt"
        
        runargs = []
        for latomeSet in chunks_latome:
            pyopts = [f"-i {args.dataFile}"]
            pyopts.append(f"-r {args.runNum}")
            pyopts.append(f"-p {args.weekly_ped}")
            pyopts.append(f"-l {' '.join([str(s) for s in latomeSet])}")
            pyopts.append(f"-o {args.outDir}")

            runargs.append(" ".join(pyopts))

        outf = open(argsname,"w")
        for ra in runargs:
            outf.write(f"{ra} \n")
        outf.close()

        subNow=False

        condSub.writeBash( bashdir=thistmpdir,
                           bashname=jobtag,
                           executable=f"{os.getcwd()}/phase.py",  # temporary
                           argsFileName=argsname,
                           isexe=False,
                           runwith="python3",
                           setupscript=os.getcwd()+"/setup.sh" )

        condSub.writeCond( bashdir=thistmpdir,
                           bashname=jobtag,
                           argsFileName=argsname,
                           taskName=jobtag,
                           runTime=args.jobtime,
                           subNow=subNow,
                           logdir=logdir,
                           errdir=errdir,
                           outdir=outdir )    

        
    else:
        if len(args.latomeSourceId) != 0 or len(args.channelId) != 0:
            print(f"processPhases({args.dataFile}, {args.runNum}, {args.weekly_ped}, treename={treename}, latomeSourceId={args.latomeSourceId}, channelId={args.channelId}, highMu=True, outdir={args.outDir})")
            processPhases(args.dataFile, args.runNum, args.weekly_ped, treename=treename, latomeSourceId=args.latomeSourceId, channelId=args.channelId, highMu=True, outdir=args.outDir)

        else:
            print("FOR NOW... NO")
            sys.exit()
