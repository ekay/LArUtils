import sys
import glob
import re

import uproot as U
folders = glob.glob("/eos/atlas/atlascerngroupdisk/det-larg/ElecCalib/DTAuto/LATOMERun_BarrelEndcapWeekly_23*")


def num_sort(test_string):
    return list(map(int, re.findall(r'\d+', test_string)))[0]
 
for folder in folders:
    if "230425-15" not in folder: continue

    files = glob.glob(f"{folder}/LArOFCPhys*4samples.root")
    if len(files)<4: continue
    
    files.sort(key=num_sort)

    ECfile = U.open(files[-1])

    print( folder )
    print(files[-1] )
    for key in ECfile.keys():
        if "OFC" not in key:
            continue

        vals = ECfile[key].arrays()

        test = vals[( (vals.OFCa[:,0]>0.325) & (vals.OFCa[:,0]<0.326)  & ( (vals.OFCb[:,0]<-14.001) & (vals.OFCb[:,0]>-14.008) ) )]
        print(key,test)

