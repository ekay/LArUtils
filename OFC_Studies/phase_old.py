import ROOT as R
import sys
import uproot as U
import awkward as ak
import numpy as np
R.gROOT.SetBatch(True)

#datafile="/eos/atlas/atlastier0/tzero/prod/caf/LAr/data24_13p6TeV/express_express/00477048/data24_13p6TeV.00477048.express_express.merge.NTUP_SCMON.c1505_m2220/data24_13p6TeV.00477048.express_express.merge.NTUP_SCMON.c1505_m2220._02*"
datafile="/afs/cern.ch/user/r/rpozzi/work/storage/OFC-optimisation/phase-optimisation/new-phasePicking-2024/skimmedData_477048.root"
ofcfile="/eos/atlas/atlascerngroupdisk/det-larg/ElecCalib/DTAuto/LATOMERun_BarrelEndcapWeekly_230425-153106/LArOFCPhys_00450653_4samples.root"
pedfile="/eos/atlas/atlascerngroupdisk/det-larg/ElecCalib/DTAuto/LATOMERun_BarrelEndcapWeekly_230425-153106/LArPedAutoCorr_00450640.root"

ofcfile = U.open(ofcfile)
pedfile = U.open(pedfile)

ofctree = ofcfile["OFC_1ns_mu"].arrays(["channelId","Phase","OFCa","OFCb","FT"],cut="FT==6") # FCAL
pedtree = pedfile["PEDESTALS"].arrays(["channelId","ped","FT"],cut="FT==6") # FCAL

chans = list(set(ofctree.channelId))

print(len(chans))

chan = 993072128

print(chan in chans)
phases = ofctree[ofctree.channelId==chan].Phase

#step_size = "500 MB"
step_size = "1.5 GB"
treename="SCDIGITS"
branches = ["channelId","samples_ADC_BAS"]
verbose=True

cut = "|".join([ f"( channelId == {ch} )" for ch in chans ]) # too long if more than fcal

outf = open("phases_FCAL.txt","w")

for arrays,report in U.iterate(files=[f'{datafile}:{treename}'], expressions=branches, step_size=step_size, recover=True, library="ak", report=True, allow_missing=True, cut=cut):
    if verbose is True: print("Reading chunk from:", report.file_path)

    for ch in chans:
        this_arr = arrays[arrays.channelId==chan]
        ped = pedtree[pedtree.channelId==ch].ped[0]
        ofcs = ofctree[ofctree.channelId==ch]
        this_arr["samples"] = this_arr.samples_ADC_BAS/8 - ped
        this_arr["maxind"] = ak.argmax(this_arr.samples,axis=1)
        # Remove small pulses
        #this_arr = this_arr[(ak.max(this_arr.samples,axis=1) - ak.min(this_arr.samples,axis=1) > 1000)]
        this_arr = this_arr[(ak.max(this_arr.samples,axis=1) > 300)]
        if len(this_arr)==0:
            continue

        this_arr["intime"] = this_arr.samples[:,1:5] # first and last sample should be out of time
        ofca= ofcs.OFCa[:,0:4]
        ofcb= ofcs.OFCb[:,0:4]

        maxe = [22,-1000]
        mint = [22,1000]
        phdict = {}

        for ph in phases: # probably could do this in a columnar way
            ofca_ph = ak.Array(np.tile(ofca[ph],(len(this_arr.intime),1)))
            ofcb_ph = ak.Array(np.tile(ofcb[ph],(len(this_arr.intime),1)))
            sam_ofca = this_arr.intime*ofca_ph
            sam_ofcb = this_arr.intime*ofcb_ph
            ene = ak.sum(sam_ofca,axis=1)
            tau = ak.sum(sam_ofcb,axis=1) / ene
            avene = ak.mean(ene)
            avtau = ak.mean(tau)
            if avene > maxe[1]:
                maxe = [ph,avene]
            if abs(avtau) < abs(mint[1]):
                mint = [ph,avtau]
            phdict[ph] = [avene,avtau]


        print(ch, len(this_arr), maxe, mint)

        if maxe[0] == mint[0]:
            outf.write(f"{ch}  {maxe[0]}\n")
        else:
            outf.write(f"!!!! {ch}  {maxe[0]}  {mint[0]}\n")
            
        
        enes = np.array([phdict[ph][0] for ph in phdict.keys()], dtype='d')
        taus = np.array([phdict[ph][1] for ph in phdict.keys()], dtype='d')
        def makeLine(gr, val, col, style=1):
            line = R.TLine(val, gr.GetHistogram().GetMinimum(), val, gr.GetHistogram().GetMaximum())
            line.SetLineColor(col)
            line.SetLineWidth(2)
            return line

        gene = R.TGraph(len(enes), np.array(phases,dtype='d'), enes)
        gene.SetTitle(f"Energy per phase for channel {ch} from {len(this_arr.intime)} pulse(s)")
        gene.SetMarkerStyle(20)
        gene.SetName("ene")
        
        gtau = R.TGraph(len(taus), np.array(phases,dtype='d'), taus)
        gtau.SetTitle(f"#tau per phase for channel {ch} from {len(this_arr.intime)} pulse(s)")
        gtau.SetMarkerStyle(20)
        gtau.SetName("tau")
        
        canv = R.TCanvas("canv", "canv", 700, 1200)
        canv.objs=[]

        legend = R.TLegend(.9,.5,.99,.95)
        legend.SetFillStyle(0)
        legend.SetTextColor(R.kBlack)
        legend.SetTextSize(.037)
        legend.SetBorderSize(0)

        canv.Divide(1,3)
        canv.cd(1)
        gene.Draw("ap")
        gene.GetXaxis().SetTitle("Phase")
        gene.GetYaxis().SetTitle("#Sigma(ai*si)")
        eline1 = makeLine(gene, maxe[0], R.kBlue)
        tline1 = makeLine(gene, mint[0], R.kRed, 9)

        legend.AddEntry(eline1, f"max E: {maxe[0]}", "l")
        legend.AddEntry(tline1, f"min |#tau|: {mint[0]}", "l")
        eline1.Draw("samel")
        tline1.Draw("samel")
        legend.Draw("same")
        
        canv.objs.extend([gene,eline1,tline1])

        canv.cd(2)
        gtau.GetHistogram().SetMaximum(25)
        gtau.GetHistogram().SetMinimum(-25)
        gtau.Draw("ap")
        gtau.GetXaxis().SetTitle("Phase")
        gtau.GetYaxis().SetTitle("#Sigma(bi*si)/#Sigma(ai*si)")
        eline2 = makeLine(gtau, maxe[0], R.kBlue)
        tline2 = makeLine(gtau, mint[0] , R.kRed, 9)
        eline2.Draw("samel")
        tline2.Draw("samel")
        legend.Draw("same")
        canv.objs.extend([gene,eline2,tline2])

        canv.cd(3)
        #print(this_arr.samples)
        meanpulse = np.array(ak.mean(this_arr.samples, axis=0), dtype='d')
        maxsam = ak.max(this_arr.samples,axis=1)[0]
        minsam = ak.min(this_arr.samples,axis=1)[0]
        #print(meanpulse, maxsam,minsam)
        pulsegr = {}
        ngr=0
        for ev in this_arr.samples:
            #print(ev)
            pulsegr[ngr] = R.TGraph(len(meanpulse), np.arange(0,len(meanpulse),dtype='d'), ev.to_numpy().astype('d'))
            pulsegr[ngr].SetTitle("Pulse Shape")
            pulsegr[ngr].GetHistogram().SetMinimum(minsam-(abs(minsam)*.05))
            pulsegr[ngr].GetHistogram().SetMaximum(maxsam+(abs(maxsam)*.05))
            if ngr == 0:
                pulsegr[ngr].Draw("Ap")
                pulsegr[ngr].GetXaxis().SetTitle("Sample")
            else:
                pulsegr[ngr].Draw("same,p")
            canv.objs.append(pulsegr[ngr])
        meangr = R.TGraph(len(meanpulse), np.arange(0,len(meanpulse),dtype='d'), meanpulse)
        meangr.SetMarkerStyle(20)
        meangr.SetMarkerColor(R.kRed)
        meangr.Draw("same,p")
        canv.objs.append(meangr)

        canv.Print(f"Energies_Taus_{ch}.png")

    sys.exit() # Let's stop after the first 'chunk' of the file
            
    
outf.close()
