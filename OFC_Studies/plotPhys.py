import ROOT as R
R.gROOT.SetBatch(True)
import uproot as U
import numpy as np



f0="/eos/atlas/atlascerngroupdisk/det-larg/ElecCalib/DTAuto/LATOMERun_BarrelEndcapWeekly_230425-153106/LArPhysWave_RTM_00450653_.root"
f1="/eos/atlas/atlascerngroupdisk/det-larg/ElecCalib/DTAuto/LATOMERun_BarrelEndcapWeekly_230425-153106_old/LArPhysWave_RTM_00450653_.root"
f0 = U.open(f0)
f1 = U.open(f1)

t0 = f0["PHYSWAVE"]
t1 = f1["PHYSWAVE"]
chan = 993072128
vals0 = t0.arrays(None,f"channelId=={chan}")
vals1 = t1.arrays(None,f"channelId=={chan}")

amps0 = vals0.Amplitude[0].to_numpy().flatten()
amps1 = vals1.Amplitude[0].to_numpy().flatten()
times0 = vals0.Time[0].to_numpy().flatten().astype('d')
times1 = vals1.Time[0].to_numpy().flatten().astype('d')

gr0 = R.TGraph(len(amps0), times0, amps0)
gr0.SetName("gr0")
gr0.SetLineColor(R.kRed)
gr0.SetMarkerColor(R.kRed)
gr1 = R.TGraph(len(amps1), times1,amps1)
gr1.SetName("gr1")
gr1.SetLineColor(R.kBlue)
gr1.SetMarkerColor(R.kBlue)
print(len(amps0), len(times0))
canv = R.TCanvas()
canv.objs = []
gr0.Draw("Apl*")
canv.objs.append(gr0)
gr1.Draw("same,pl")
canv.objs.append(gr1)
canv.Print("PhysWave.png")


f0="/eos/atlas/atlascerngroupdisk/det-larg/ElecCalib/DTAuto/LATOMERun_BarrelEndcapWeekly_230425-153106/LArOFCPhys_00450653_4samples.root"
f1="/eos/atlas/atlascerngroupdisk/det-larg/ElecCalib/DTAuto/LATOMERun_BarrelEndcapWeekly_230425-153106_old/LArOFCPhys_00450653_4samples.root"
f0 = U.open(f0)
f1 = U.open(f1)

t0 = f0["OFC_1ns_mu"]
t1 = f1["OFC_1ns_mu"]
chan = 993072128
vals0 = t0.arrays(None,f"(channelId=={chan}) & (Phase==30)")
vals1 = t1.arrays(None,f"(channelId=={chan}) & (Phase==30)")

ofca0 = vals0.OFCa[0].to_numpy().flatten().astype('d')
ofca1 = vals1.OFCa[0].to_numpy().flatten().astype('d')
ofcb0 = vals0.OFCb[0].to_numpy().flatten().astype('d')
ofcb1 = vals1.OFCb[0].to_numpy().flatten().astype('d')

gr0 = R.TGraph(len(ofca0), np.arange(6,dtype='d'), ofca0)
gr0.SetName("gr0")
gr0.SetLineColor(R.kRed)
gr0.SetMarkerColor(R.kRed)
gr1 = R.TGraph(len(ofca1), np.arange(6,dtype='d'), ofca1)
gr1.SetName("gr1")
gr1.SetLineColor(R.kBlue)
gr1.SetMarkerColor(R.kBlue)
canv = R.TCanvas()
canv.objs = []
gr0.Draw("Apl*")
canv.objs.append(gr0)
gr1.Draw("same,pl")
canv.objs.append(gr1)
canv.Print("OFCa_Phase30.png")



f0="/eos/atlas/atlascerngroupdisk/det-larg/ElecCalib/DTAuto/LATOMERun_BarrelEndcapWeekly_230425-153106/LArPhysAutoCorr_00450653_.root"
f1="/eos/atlas/atlascerngroupdisk/det-larg/ElecCalib/DTAuto/LATOMERun_BarrelEndcapWeekly_230425-153106_old/LArPhysAutoCorr_00450653_.root"
f0 = U.open(f0)
f1 = U.open(f1)

t0 = f0["AUTOCORR"]
t1 = f1["AUTOCORR"]
chan = 993072128
vals0 = t0.arrays(None,f"(channelId=={chan})")
vals1 = t1.arrays(None,f"(channelId=={chan})")

corr0 = vals0.covr[0].to_numpy().flatten().astype('d')
corr1 = vals1.covr[0].to_numpy().flatten().astype('d')

gr0 = R.TGraph(len(corr0), np.arange(len(corr0)+1,dtype='d'), corr0)
gr0.SetName("gr0")
gr0.SetLineColor(R.kRed)
gr0.SetMarkerColor(R.kRed)
gr1 = R.TGraph(len(corr1), np.arange(len(corr1)+1,dtype='d'), corr1)
gr1.SetName("gr1")
gr1.SetLineColor(R.kBlue)
gr1.SetMarkerColor(R.kBlue)
canv = R.TCanvas()
canv.objs = []
gr0.Draw("Apl*")
canv.objs.append(gr0)
gr1.Draw("same,pl")
canv.objs.append(gr1)
canv.Print("PhysAutoCorr.png")
