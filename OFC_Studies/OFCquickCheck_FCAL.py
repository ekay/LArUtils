import ROOT as R
import numpy as np

R.gROOT.SetBatch(True)

infile = "/afs/cern.ch/work/e/ekay/public/P1CalibrationProcessing/lar/calib_ntuples/ntuples/LATOMERun_BarrelEndcapWeekly_230425-153106/LArOFCPhys_00450653_4samples.root"

tree = "OFC_1ns_mu"

df = R.RDataFrame(tree,infile)
print(dir(df))

chan = 993072128
df = df.Filter(f'channelId == {chan}') 
phases = [ 25, 26, 27, 28, 29, 30, 31, 32 ]
cols = [ R.kRed, R.kBlue, R.kGreen, R.kOrange, R.kViolet+6, R.kMagenta, R.kCyan, R.kGreen-2 ]
OFCa_grs = {}
OFCb_grs = {}
OFCa_range = [1000,-1000]
OFCb_range = [1000,-1000]
for ph in phases:
    thisdata = df.Filter(f'Phase=={ph}')

    OFCa = thisdata.AsNumpy(columns=["OFCa"])
    OFCb = thisdata.AsNumpy(columns=["OFCb"])

    OFCa = [np.array(v).tolist() for v in OFCa["OFCa"]][0]
    OFCb = [np.array(v).tolist() for v in OFCb["OFCb"]][0]

    print(f"Phase {ph}, OFCa: {OFCa}, OFCb: {OFCb}")

    OFCa_grs[ph] = R.TGraph(len(OFCa))
    OFCb_grs[ph] = R.TGraph(len(OFCb))
    OFCa_grs[ph].SetTitle(f"Channel {chan}")
    OFCb_grs[ph].SetTitle(f"Channel {chan}")
    for ofc in OFCa:
        OFCa_grs[ph].SetPoint(OFCa.index(ofc), OFCa.index(ofc), ofc)
        OFCa_grs[ph].SetLineColor(cols[phases.index(ph)])
        OFCa_grs[ph].SetLineWidth(2)
        OFCa_grs[ph].SetMarkerColor(cols[phases.index(ph)])
        if ofc > OFCa_range[1]: OFCa_range[1] = ofc
        if ofc < OFCa_range[0]: OFCa_range[0] = ofc
    
    for ofc in OFCb:
        OFCb_grs[ph].SetPoint(OFCb.index(ofc), OFCb.index(ofc), ofc)
        OFCb_grs[ph].SetLineColor(cols[phases.index(ph)])
        OFCb_grs[ph].SetLineWidth(2)
        OFCb_grs[ph].SetMarkerColor(cols[phases.index(ph)])
        if ofc > OFCb_range[1]: OFCb_range[1] = ofc
        if ofc < OFCb_range[0]: OFCb_range[0] = ofc

canv = R.TCanvas()

legend = R.TLegend(.92,.5,.99,.95)
legend.SetFillStyle(0)
legend.SetTextColor(R.kBlack)
legend.SetTextSize(.035)
legend.SetBorderSize(0)

outstr = f"_{chan}_phases_{'_'.join([str(ph) for ph in phases])}"
for ph in phases:
    OFCa_grs[ph].GetHistogram().SetMaximum(OFCa_range[1])
    OFCa_grs[ph].GetHistogram().SetMinimum(OFCa_range[0])
    legend.AddEntry(OFCa_grs[ph], f"{ph}", "l")
    if phases.index(ph) == 0:
        OFCa_grs[ph].Draw("APl")
    else:
        OFCa_grs[ph].Draw("Pl")
    OFCa_grs[ph].GetXaxis().SetTitle("Sample")
    OFCa_grs[ph].GetYaxis().SetTitle("OFCa")
    canv.Update()
    legend.Draw("same")
canv.Print(f"OFCa{outstr}.png")
print(OFCa_range, OFCb_range)
for ph in phases:
    OFCb_grs[ph].GetHistogram().SetMaximum(OFCb_range[1])
    OFCb_grs[ph].GetHistogram().SetMinimum(OFCb_range[0])

    if phases.index(ph) == 0:
        OFCb_grs[ph].Draw("APl")
    else:
        OFCb_grs[ph].Draw("Pl")
    OFCb_grs[ph].GetXaxis().SetTitle("Sample")
    OFCb_grs[ph].GetYaxis().SetTitle("OFCb")
    canv.Update()
    legend.Draw("same")
canv.Print(f"OFCb{outstr}.png")
