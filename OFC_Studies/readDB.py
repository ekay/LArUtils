import sys
from PyCool import cool
sys.path.append("/afs/cern.ch/user/l/larmon/public/prod/Misc")
from LArMonCoolLib import GetRunStartStopTimes, openDB
from zlib import crc32
import array
import numpy as np
#fullconn = "sqlite://;schema=dumped_BCID.db;dbname=CONDBR2"
#dbSvc=cool.DatabaseSvcFactory.databaseService()
#db=dbSvc.openDatabase(fullconn)

db = openDB("COOLONL_LAR")

folderName = "/LAR/Configuration/SCConfig/BCID"

bgkey_fname='/TRIGGER/LVL1/BunchGroupKey'
if not db.existsFolder(folderName) :
    print(f"NOOOO no folder {folderName}")
    sys.exit()
folder = db.getFolder(folderName)

run = 478115

chansel = cool.ChannelSelection(0)
(SOR,EOR,NLB) = GetRunStartStopTimes(run)
objs = folder.browseObjects(SOR,EOR,chansel,"LARConfigurationSCConfigBCID-ATLAS-UPD1-00")
while objs.goToNext():
    obj = objs.currentRef()
    payload = obj.payload()
    blob = payload[0]

    blobdata = blob.read()
    if isinstance(blobdata,bytes):
        beginpos = blob.pos
        endpos = blob.size()

        vartype='i'
        bytearr = array.array(vartype,blobdata)
        factor = len(bytearr)/5568
        #pos = endpos
        #buf = blob.startingAddress()
        #print(dir(buf))
        #buf.SetSize(blob.size())
        #print(buf[beginpos:endpos])
        
        bytearr = np.reshape(bytearr,(int(len(bytearr)//factor),int(factor)))
        print(factor, len(bytearr)//factor)
        for line in bytearr:
            print(" ".join([str(l) for l in line]))
