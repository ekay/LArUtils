import sys,os
import pandas as pd
sys.path.append("..")
import printMapping

onlid = "SC_ONL_ID"
eta = "SCETA"
phi = "SCPHI"                
query = [ onlid, eta, phi, "SAM", "DETNAME", "QUADRANT", "FEB", "CALIB", "FTNAME", "LATOME_NAME", "LATOME_FIBRE", "LTDB"]
criteria = {"want":query}    
pmdf = pd.DataFrame(printMapping.query(**criteria), columns=query)

pmdf = pmdf.set_index(onlid)

infile=open("badSC_888888_RUN3-UPD1-00.txt","r")

masks = {}
all_reasons = []
for line in infile:
    line=line.strip()

    ID = line.split("# ")[1].split(" ->")[0]
    #reasons = line.split("  # ")[0].split(" 0 ")[-1]
    reasons = line.split("  # ")[0].split(" ")[6]
    reasons = reasons.split(" ")
    if ID in masks.keys():
        print("ALREADY IN LIST")
    all_reasons.extend(reasons)
    masks[ID] = reasons
    if "0" in reasons:
        print("OIOI", line)
    
    
channels = masks.keys()
set_reasons = list(set(all_reasons))
for reason in set_reasons:
    chans = [ ch for ch in channels if reason in masks[ch] ]
    print("**",reason,"**", len(chans), "channels")
    vals = { x:[] for x in pmdf.columns }
    vals[onlid] = []
    for chan in chans:
        vals[onlid].append(chan)
        row = pmdf.loc[chan]
        try:
            rowdict = row.to_dict('records')[0]
        except Exception as e:
            rowdict = row.to_frame()[chan].to_dict()
            
        for val in vals.keys():
            if val not in rowdict.keys(): continue
            if rowdict[val] not in vals[val]:
                vals[val].append(rowdict[val])
            #else:
            #    print("OI", rowdict[val], "already in",vals[val])
        
    for val in vals:
        if len(vals[val]) < 10:
            print(f"{val} ({len(vals[val])} distinct values): {', '.join(vals[val])}")
        else:
            print(f"{val} has {len(vals[val])} distinct values")
    print("\n\n")
