import sys,os
import pandas as pd
sys.path.append("..")
import printMapping

onlid = "SC_ONL_ID"
eta = "SCETA"
phi = "SCPHI"                
query = [ onlid, eta, phi, "SAM", "DETNAME", "QUADRANT", "FEB", "CALIB", "FTNAME", "LATOME_NAME", "LATOME_FIBRE", "LTDB"]
criteria = {"want":query}    
pmdf = pd.DataFrame(printMapping.query(**criteria), columns=query)

pmdf = pmdf.set_index(onlid)

LTDBs = [ "A02L", "A02H", "A01L", "I05L", "I12R", "I01L", "A06L", "A09R", "C03R", "A02H", "A03L", "A12H", "C04L", "C12L", "H01L", "I16L", "C06L", "C09R", "A10R", "I15L" ]
#LTDBs = [ "A01L", "A10R" ]

#infile=open("newbadSC.txt","r")
#infile=open("bad_hi_noOSUM.txt", "r")
infile=open("newbad_clean.txt", "r")

masks = {}
all_reasons = []
for line in infile:
    line=line.strip()
    if line == "": continue
    ID = line.split("# ")[1].split(" ->")[0]
    #reasons = line.split("  # ")[0].split(" 0 ")[-1]
    reasons = line.split("# ")[0].split(" ")[6:]
    reasons = [ r.strip() for r in reasons if r != "" ]
    if ID in masks.keys():
        print("ALREADY IN LIST")
    all_reasons.extend(reasons)
    masks[ID] = reasons
    if "0" in reasons:
        print("OIOI", line)
    row = pmdf.loc[ID]
    try:
        rowdict = row.to_dict('records')[0]
    except Exception as e:
        rowdict = row.to_frame()[ID].to_dict()

    #if rowdict["LTDB"] in LTDBs:
    #    print(line) # , rowdict["LTDB"])
    if rowdict["LTDB"] not in LTDBs:
        print(line, rowdict["LTDB"])
        #continue 
    elif "maskedOSUM" not in reasons or len(reasons) > 1:
        print(line, rowdict["LTDB"])
        #continue
    else:
        #print(line, "maskedOSUM" in reasons, len(reasons), reasons) #continue
        continue
    #else:
    #    print("SKIPPING THIS ONE",line, rowdict["LTDB"])
sys.exit()
channels = masks.keys()
set_reasons = list(set(all_reasons))
for reason in set_reasons:
    chans = [ ch for ch in channels if reason in masks[ch] ]
    print("**",reason,"**", len(chans), "channels")
    vals = { x:[] for x in pmdf.columns }
    vals[onlid] = []
    for chan in chans:
        vals[onlid].append(chan)
        row = pmdf.loc[chan]
        try:
            rowdict = row.to_dict('records')[0]
        except Exception as e:
            rowdict = row.to_frame()[chan].to_dict()
            
        for val in vals.keys():
            if val not in rowdict.keys(): continue
            if rowdict[val] not in vals[val]:
                vals[val].append(rowdict[val])
            #else:
            #    print("OI", rowdict[val], "already in",vals[val])
        
    for val in vals:
        if len(vals[val]) < 10:
            print(f"{val} ({len(vals[val])} distinct values): {', '.join(vals[val])}")
        else:
            print(f"{val} has {len(vals[val])} distinct values")
    print("\n\n")
