#!/bin/python
import sys, os, errno
import ROOT as R
import uproot4 as U
import numpy as np
import math
import operator
import datetime as dt
import platform

detKey={0:"EMB",1:"EMECOW",2:"EMECIW",3:"HEC",4:"FCAL"}
sideKey={0:"C",1:"A"}
# TO DO
# Add thresholds - list channels outside these
# Add plot showing all available BCID containers
# Plots of available energy containers
# Colour hists for different layers?

def tidypad(th2=False):
    R.gPad.SetBorderSize(0)
    R.gPad.SetBottomMargin(.3)
    R.gPad.SetLeftMargin(.08)
    if th2:
        R.gPad.SetRightMargin(.14)
    else:
        R.gPad.SetRightMargin(.04)
    R.gStyle.SetOptStat(0)
    R.gPad.SetBorderMode(0)
    R.gStyle.SetPadTickY(1)
    R.gStyle.SetPadTickX(1)


def InitEtaPhiHist(detector, side, layer, zvar, title, zlo, zhi):
    thisdet = detKey[detector]
    if detector==1 or detector==2:
        thisdet = "EMEC"
    hname = zvar+"_"+thisdet+sideKey[side]+"_layer"+str(layer)+"_etaphi"
    plottitle = title+" "+thisdet+sideKey[side]+" Layer "+str(layer)
    neta=0
    if detector == 0:
        if layer == 0 or layer == 3: neta = 15
        if layer == 1 or layer == 2: neta = 60

        try:
            h_etaphi = R.TH2F( hname, plottitle, neta, 0, 1.5, 64, -3.141592, 3.141592)
        except Exception as e:
            print( hname, plottitle, neta, 0, 1.5, 64, -3.141592, 3.141592 ) 
            print( type(hname), type(plottitle), type(neta) )
            print( e )
            sys.exit()
            
    else:
        if detector == 1 or detector == 2: # EMEC
            if layer == 0 or layer == 3:
                eta = [ 1.375, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5 ]
            elif layer == 1:
                eta = [ 1.375, 1.5, 1.525, 1.55, 1.575, 1.6, 1.625, 1.65, 1.675, 1.7, 1.725, 1.75, 1.775, 1.8, 1.833, 1.866, 1.9, 1.933, 1.966, 2.0, 2.025, 2.05, 2.075, 2.1, 2.125, 2.15, 2.175, 2.2, 2.225, 2.25, 2.275, 2.3, 2.325, 2.35, 2.375, 2.4, 2.5, 2.7, 2.9, 3.1, 3.2 ]
            elif layer == 2: 
                eta = [ 1.375, 1.4375, 1.4625, 1.4875, 1.5, 1.525, 1.55, 1.575, 1.6, 1.625, 1.65, 1.675, 1.7, 1.725, 1.75, 1.775, 1.8, 1.825, 1.85, 1.875, 1.9, 1.925, 1.95, 1.975, 2.0, 2.025, 2.05, 2.075, 2.1, 2.125, 2.15, 2.175, 2.2, 2.225, 2.25, 2.275, 2.3, 2.325, 2.35, 2.375, 2.4, 2.425, 2.45, 2.475, 2.5, 2.7, 2.9, 3.1, 3.2 ]
        elif detector == 3: # HEC
            eta = [ 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.7, 2.9, 3.1, 3.2 ]
        elif detector == 4: # FCAL
            eta = [ 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.85, 4.0, 4.2, 4.45, 4.7, 4.9 ]
        # layer 1 ??
    
        #mineta = [ -e for e in eta[::-1] ]
        #mineta.append(0)
        #mineta.extend(eta)

        #eta=mineta

        try:
            h_etaphi = R.TH2F( hname, plottitle, len(eta)-1, np.array(eta, dtype='d'), 64, -3.141592, 3.141592 )		   
        except Exception as e:
            print( hname, plottitle, len(eta)-1, np.array(eta, dtype='d'), 64, -3.141592, 3.141592 )
            print( type(hname), type(plottitle), type(eta) )
            print( e )
            sys.exit()
        #print(detector,layer)
        #h_etaphi.Print("base")
    R.TGaxis.SetMaxDigits(3)
    h_etaphi.GetZaxis().SetRangeUser(zlo, zhi)
    #h_etaphi.GetYaxis().SetLimits(-3.15, 3.15)
    #h_etaphi.GetXaxis().SetLimits(0, 5)
    h_etaphi.GetZaxis().SetTitle(title)
    h_etaphi.GetXaxis().SetTitle("|#eta|")
    h_etaphi.GetYaxis().SetTitle("#phi")
    #h_etaphi.GetXaxis().SetNdivisions(5)
    h_etaphi.SetNdivisions(510, "Y")
    h_etaphi.SetNdivisions(510, "X")
    
    h_etaphi.GetXaxis().SetLabelSize(0.02)
    h_etaphi.GetYaxis().SetLabelSize(0.02)
    h_etaphi.GetZaxis().SetLabelSize(0.02)
    
    h_etaphi.SetTitleFont(43)
    h_etaphi.SetTitleSize(23)

    return h_etaphi


def chmkDir( path ):
    os.umask(0)
    try:
        # print( "mkdir "+path )
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise
        pass


def listsAgree(l1,l2,l3=None):
    if isinstance(l1,list) and not isinstance(l2,list):
        if l2 in l1:
            return l1.index(l2)+1, l1
    elif isinstance(l2,list) and not isinstance(l1,list):
        if l1 in l2:
            return l2.index(l1)+1, l1
    else:
        return 100, 100

def histsAgree(h1,h2,h3=None):
    for xb in range(1, h1.GetXaxis().GetNbins()+1):
        y1 = h1.GetBinContent(xb)
        y2 = h2.GetBinContent(xb)
        if h3 is not None:
            y3 = h3.GetBinContent(xb)
            if y1 == y2 and y2 == y3:
                return xb, y1 #h1.GetBinCenter(xb)
        else:
            if y1 == y2:
                return xb, y1
    return None, None
    # loop bins
    # get y of all hists
    # if all agree, return that bin
    # if some agree, add it to list
    # at end return list or None




def cleanLeg(legend, textsize=0.035, cols=2):
    if cols>1: legend.SetNColumns(cols)
    legend.SetFillColor(R.kWhite)
    legend.SetTextColor(R.kBlack)
    legend.SetTextSize(textsize)
    legend.SetBorderSize(0)

def makeLeg(legx1, legx2, legy, samples, legheight=0.04, textsize=0.035, cols=2): 

    legh = legheight
    for s in samples:
        legh+=legheight
    legend = R.TLegend(legx1,legy-(legh/cols),legx2,legy)

    cleanLeg(legend, textsize=textsize)
    return legend

def detName( barrel_ec, pos_neg ):
    det=None
    side=None
    if barrel_ec == 0:
        det = "EMB"
    elif barrel_ec == 1:
        det = "EMEC"
    if pos_neg == 0:
        side = "C"
    elif pos_neg == 1:
        side = "A"
    if det is not None and side is not None:
        return det+side
    else:
        print("detName: can't work out detector from barrel_ec = "+str(barrel_ec)+" and pos_neg = "+str(pos_neg))
        sys.exit()


def makePlots(tree, outdir, LATOME):
    Nsamples = int(tree["Nsamples"].array()[0])
    #print("Converting tree to numpy array")
    br=["samples","latomeSourceId","channelId","latomeChannel","eta","phi","layer","bcidVec","bcidLATOMEHEAD","detector","pos_neg"] # choose certain branches?
    #tree = tree.arrays(library="np")
    startopen=dt.datetime.now()
    tree = tree.arrays(expressions=br,cut="latomeSourceId=="+str(LATOME),library="np")

    #tree = tree.arrays(expressions=br,library="ak",how="zip")

    endopen=dt.datetime.now()
    #print( "Conversion took",str(endopen-startopen) )

    LATOMEs = list(dict.fromkeys(tree["latomeSourceId"]))
    channels = list(dict.fromkeys(tree["channelId"]))
    possdetectors = list(dict.fromkeys(tree["detector"]))
    posslayers = list(dict.fromkeys(tree["layer"]))
    posssides = list(dict.fromkeys(tree["pos_neg"]))

    latomechans = list(dict.fromkeys(tree["latomeChannel"]))
    strch = [ str(ch) for ch in channels ]

    nentries = len(tree["samples"])
    nCh = len(channels)
    ndata = float(nentries)/float(nCh);
    #print( str(Nsamples)+" samples" )
    #print( str(nentries)+" entries" )
    #print( str(nCh)+" channels" )
    #print( str(ndata)+" events" )

    print( f"LATOME {LATOME} ({hex(LATOME)}): {Nsamples} samples, {nentries} entries, {nCh} channels, {ndata} events" )
    #print("detectors",possdetectors)

    vals = {}
    from collections import OrderedDict
    vardict = OrderedDict([
        ("ped", [ "Pedestal", "Pedestal value for each channel, averaged over events"] ), 
        ("rmsped", [ "Pedestal RMS", "Pedestal RMS value for each channel"] ),
        ("maxSamp", [ "Max ADC minus ped", "Maximum ADC value - Pedestal value"] ),
        ("rmsmaxsamp", [ "RMS of max ADC", "RMS of max ADC"] ), 
        ("maxSampInd", [ "Max sampling", "Sample where max ADC occurs, averaged over events"] ),
        ("rmsmaxsampind", [ "RMS of peak sampling", "RMS of sample where max ADC occurs"] ), 
        ("firstpulse", [ "ADC", "Pulse shape for the first event"] ) 
    ])
    variables = list(vardict.keys())       

    layercol = [ R.kGreen, R.kBlue, R.kBlack, R.kRed ]


    for v in variables:
        vals[v]={str(ch) : 0 for ch in channels}
    
    latomeID = LATOME

    chHists={}
    chvars=[ "samples", "bcidVec" ]
    for ch in chvars:
        chHists[ch]={}
    chCanv=R.TCanvas()
    
    etas=[]
    phis=[]
    layers=[]

    detHists={}
    #detMaxSampHist={}
    #detBCIDdiffHist={}
    outf = R.TFile(outdir+"/dethists"+str(hex(LATOME))+".root","RECREATE")
    outlog = open(outdir+"/"+str(hex(LATOME))+".log","w+")
    for det in possdetectors:
        if det == 2: continue  # same plot for IW and OW EMEC
        for side in posssides:
            for lay in posslayers:
                detHists[str(det)+"_"+str(side)+"_"+str(lay)]={}
                detHists[str(det)+"_"+str(side)+"_"+str(lay)]["maxSampInd"] = InitEtaPhiHist(det, side, lay, "MaxSampling", "Max Sampling", 0, 31)
                detHists[str(det)+"_"+str(side)+"_"+str(lay)]["maxSamp"] = InitEtaPhiHist(det, side, lay, "MaxADC", "Max ADC - pedestal", 0, 1000)
                detHists[str(det)+"_"+str(side)+"_"+str(lay)]["bcidDiff"] = InitEtaPhiHist(det, side, lay, "BCIDDiff", "Peak BCID - Event BCID", -3, 3)

    for chanID in channels:
        chanNum = str(latomechans[channels.index(chanID)])
        

        events = np.where( tree["channelId"] == chanID )[0]  # Need the 0 because it returns a tuple

        eta = str([ tree["eta"][ev] for ev in events ][0])
        phi = str([ tree["phi"][ev] for ev in events ][0])
        layer = str([ tree["layer"][ev] for ev in events ][0])
        detector = str([ tree["detector"][ev] for ev in events ][0])
        side = str([ tree["pos_neg"][ev] for ev in events ][0])

        etas.append(eta)
        phis.append(phi)
        layers.append(layer)
        
        #sumsamp = sum( [tree["samples"][ev] for ev in events] )
        #avgsamp = [ s/len(events) for s in sumsamp ]

        for chv in chvars:
            chHists[chv][str(chanID)] = R.TH1F( chv+"_"+str(chanID), chv+"_"+str(chanID), Nsamples, -0.5, Nsamples-0.5 )       
            chHists[chv][str(chanID)].FillN(Nsamples, np.arange(Nsamples).astype('d'), np.array(tree[chv][events[0]],dtype='d') )
            chHists[chv][str(chanID)].SetTitle(chv+" for the first event for channel "+str(chanID) +" ("+chanNum+", #eta = "+eta+", #phi = "+phi+", layer = "+layer+" )" )
            chHists[chv][str(chanID)].GetXaxis().SetTitle("Sample")
            if "samples" in chv: 
                ytitle = "ADC"
                pdfname = "SamplesEachChannel"
            if "bcid" in chv: 
                ytitle = "BCID"
                pdfname = "BCIDEachChannel"
            chHists[chv][str(chanID)].GetYaxis().SetTitle(ytitle)
            chHists[chv][str(chanID)].Draw("hist")
            if "bcid" in chv:                
                l = R.TLatex()
                l.SetNDC()
                l.SetTextFont(42)
                l.SetTextColor(R.kRed)
                l.SetTextSize(0.04)
                details = str(Nsamples)+" samples, "+str(nCh)+" channels, "+str(ndata)+" events"
                l.DrawLatex(0.2, 0.8, "LATOME header BCID = "+str(tree["bcidLATOMEHEAD"][events[0]]) )
                bcidl = R.TLine( -0.5, tree["bcidLATOMEHEAD"][events[0]],
                                 Nsamples-0.5, tree["bcidLATOMEHEAD"][events[0]] )
                bcidl.SetLineColor(R.kRed)
                bcidl.SetLineStyle(2)
                bcidl.Draw("samel")

            #R.gPad.SetLogy()
            if channels.index(chanID)==0:
                chCanv.Print(outdir+"/"+pdfname+"_"+str(hex(LATOME))+".pdf(","pdf")
            elif channels.index(chanID)== len(channels)-1:
                chCanv.Print(outdir+"/"+pdfname+"_"+str(hex(LATOME))+".pdf)","pdf")
            else:
                chCanv.Print(outdir+"/"+pdfname+"_"+str(hex(LATOME))+".pdf","pdf")

        vals["maxSampInd"][str(chanID)] = sum( [np.argmax(tree["samples"][ev]) for ev in events] ) / len(events)

        maxsampind0 = np.argmax(tree["samples"][events[0]])
        #bciddiff = int(tree["bcidVec"][events[0]][int(vals["maxSampInd"][str(chanID)]-1)])-int(tree["bcidLATOMEHEAD"][events[0]] )
        bciddiff = int(tree["bcidVec"][events[0]][int(maxsampind0)])-int(tree["bcidLATOMEHEAD"][events[0]] )
        #print(bciddiff, int(tree["bcidVec"][events[0]][int(maxsampind0)]), int(tree["bcidLATOMEHEAD"][events[0]] ))
        if abs(bciddiff) > 3564/2:
            #print("oi",bciddiff)
            bciddiff = 3564-abs(bciddiff)
            #print("--",bciddiff)
            
        #print("oi",bciddiff, type(bciddiff), tree["bcidVec"][events[0]][int(vals["maxSampInd"][str(chanID)]-1)], tree["bcidLATOMEHEAD"][events[0]] ) #, tree["bcidLATOMEHEAD"][events[0], tree["bcidVec"][events[0]][int(vals["maxSampInd"][str(chanID)])])
        #print("***", layer, eta, phi, sum( [np.argmax(tree["samples"][ev])+1 for ev in events] ) / len(events) )
        vals["maxSamp"][str(chanID)] = sum( [np.max(tree["samples"][ev])-tree["samples"][ev][0] for ev in events] ) / len(events)
        vals["ped"][str(chanID)] = sum( [tree["samples"][ev][0] for ev in events] ) / len(events)


        vals["firstpulse"][str(chanID)] = np.array(tree["samples"][events[0]],dtype='d')

        thisdet = str(detector)
        if thisdet == "2": thisdet = "1"
        thisx = detHists[thisdet+"_"+str(side)+"_"+str(layer)]["bcidDiff"].GetXaxis().FindBin(abs(float(eta)))
        thisy = detHists[thisdet+"_"+str(side)+"_"+str(layer)]["bcidDiff"].GetYaxis().FindBin(abs(float(phi)))
        thisz = detHists[thisdet+"_"+str(side)+"_"+str(layer)]["bcidDiff"].GetBinContent(thisx,thisy)

        if thisz != 0:
            print("WARNING, bin",thisx, thisy, "already filled with",thisz,"for detector", detKey[int(detector)]+sideKey[int(side)], "layer", str(layer), "eta",eta,"phi",phi,str(chanID), str(hex(chanID)))
        outlog.write(str(chanID)+" "+detKey[int(detector)]+sideKey[int(side)]+" layer "+str(layer)+" eta "+eta+" phi "+phi+" maxsampind "+str(vals["maxSampInd"][str(chanID)])+" maxsamp "+str(vals["maxSamp"][str(chanID)])+" bciddiff "+str(bciddiff)+"( = "+str(tree["bcidVec"][events[0]][int(maxsampind0)])+"-"+str(tree["bcidLATOMEHEAD"][events[0]])+" ("+str(thisx)+","+str(thisy)+") "+str(thisz)+"\n")


        detHists[thisdet+"_"+str(side)+"_"+str(layer)]["maxSampInd"].Fill(abs(float(eta)), float(phi), vals["maxSampInd"][str(chanID)])
        detHists[thisdet+"_"+str(side)+"_"+str(layer)]["bcidDiff"].Fill(abs(float(eta)), float(phi), bciddiff)
        detHists[thisdet+"_"+str(side)+"_"+str(layer)]["maxSamp"].Fill(abs(float(eta)), float(phi), vals["maxSamp"][str(chanID)])
        #print("oi",eta,phi,bciddiff, vals["maxSampInd"][str(chanID)])
        # RMS variables
        peds = np.array([ tree["samples"][ev][0] for ev in events ])
        maxsamps = np.array([ np.max(tree["samples"][ev]) for ev in events ])
        maxsampinds = np.array([np.argmax(tree["samples"][ev]) for ev in events])
        vals["rmsped"][str(chanID)] = np.std( peds )
        vals["rmsmaxsamp"][str(chanID)] = np.std( maxsamps ) 
        vals["rmsmaxsampind"][str(chanID)] = np.std( maxsampinds )
    

    # test det plot
    if len(detHists.keys())%2 == 0:
        ndeth = int(len(detHists.keys())/2)
    else:
        ndeth = int(len(detHists.keys())+1/2)
        
    deths = detHists[list(detHists.keys())[0]].keys()

    detCanv = R.TCanvas()
    R.gStyle.SetPalette(55)
    #for dh in deths:
    #    canv=R.TCanvas(dh+"_"+str(LATOME), dh+"_"+str(LATOME), 
    #                   1200, 600*ndeth)
    
    #   if not hasattr(canv, "objs"):
    #       canv.objs = []
    
    #   canv.Divide(2,ndeth)
    nh = len(list(detHists.keys()))*len(deths)
    n=0
    for dh in deths:
        for ls in detHists.keys():
            h = list(detHists.keys()).index(ls)
            #canv.cd(h+1)
            R.gStyle.SetOptStat(0)
            R.gPad.SetGrid(1, 1)
            R.gPad.SetTicks(1, 1)
            tidypad(True)
            R.gStyle.SetPaintTextFormat(".0f")
            detHists[ls][dh].SetMarkerSize(0.6)
            detHists[ls][dh].Draw("colztext")
            detHists[ls][dh].Write()
            #detHists[ls][dh].GetXaxis().SetRangeUser(min([float(e) for e in etas]), max([float(e) for e in etas]))
            #detHists[ls][dh].GetYaxis().SetRangeUser(min([float(p) for p in phis]), max([float(p) for p in phis]))
            
            if n==0:
                detCanv.Print(outdir+"/detPlots_"+str(hex(LATOME))+".pdf(","pdf")
            elif n == nh-1:
                detCanv.Print(outdir+"/detPlots_"+str(hex(LATOME))+".pdf)","pdf")
            else:
                detCanv.Print(outdir+"/detPlots_"+str(hex(LATOME))+".pdf","pdf")
            n+=1
        #canv.Print(outdir+"/"+dh+"_"+str(hex(LATOME))+".png")
    outf.Close()
    outlog.close()

    plots = {}

    def defineH( title, layerinfo="", binslist=channels ):
        h = R.TH1F( title.replace(" ","_")+layerinfo, title+" for LATOME "+str(hex(LATOME)), 
                    len(binslist), -0.5, len(binslist)-0.5 )
        xax = h.GetXaxis()
        yax = h.GetYaxis()
        
        if len(binslist) == len(channels):
            labelevery = 10
            for chan in latomechans:
                ch = latomechans.index(chan) # need this?
                bin_index = xax.FindBin(ch)
                if ch%labelevery==0 or ch==0:
                    xax.SetBinLabel(bin_index, str(chan) )
                else:
                    xax.SetBinLabel(bin_index,"")
            xax.SetTitle("Channel")

        else:
            xax.SetTitle("Sample")
            yax.SetTitle("ADC")

        return h

    for var in variables:
        v = variables.index(var)
        plots[var] = {}

        for layer in sorted(list(set(layers)),key=float):
            if var!="firstpulse":
                plots[var][layer] = R.TGraph()
                title = vardict[var][1]
                layerinfo = "_layer"+layer
                plots[var][layer].SetName(title.replace(" ","_")+layerinfo)  
                plots[var][layer].SetTitle(title+" for LATOME "+str(hex(LATOME))) 
                plots[var][layer].GetXaxis().SetTitle("Channel")
                plots[var][layer].GetXaxis().SetRangeUser(0, len(channels)+1)

            else:
                plots[var][layer] = defineH( vardict[var][1], "_layer"+layer, binslist=np.arange(Nsamples) )

    #R.TGaxis.SetMaxDigits(9)
    layercount = {l:0 for l in layers}
    for chanID in channels:
        ch = channels.index(chanID)
        layer = layers[strch.index(str(chanID))]
        for var in variables: 
            if isinstance(plots[var][layer], R.TGraph):
                plots[var][layer].SetPoint(plots[var][layer].GetN(),
                                           ch+1, vals[var][str(chanID)] )
                #print(var, layer, plots[var][layer].GetN(), ch+1, vals[var][str(chanID)] )
            else:
                if layer in plots[var].keys():
                    layercount[layer]+=1 
                    lc = str(layercount[layer])
                    plots[var][layer+"_"+lc] = plots[var][layer].Clone(plots[var][layer].GetName()+"_"+lc)
                    plots[var][layer+"_"+lc].FillN(Nsamples, np.arange(Nsamples).astype('d'),
                                                   vals[var][str(chanID)] )
                else:
                    plots[var][layer].FillN(Nsamples, np.arange(Nsamples).astype('d'),
                                            vals[var][str(chanID)] )
            #if "Ind" in var:
            #    print(layer, ch+1, chanID, vals[var][str(chanID)] )

    # Get max and min values
    maxes={}
    mins={}
    for v in variables:       
        if "firstpulse" not in v:
            maxes[v] = max(vals[v].items(), key=operator.itemgetter(1))[0]
            mins[v] = min(vals[v].items(), key=operator.itemgetter(1))[0]
        else:
            maxes[v]=0
            mins[v]=0

    def plotH( var, ytitle ):
        histos = [ plots[var][l] for l in plots[var].keys() ]
        hmin=1000
        hmax=0        
        for hist in histos:
            #shape = (hist.GetNbinsX() + 2)
            #contents = [ hist.GetBinContent(b) for b in range(0,hist.GetNbinsX()+1) ]
            #contents = [ c for c in contents if c > -1000000 ]
            #thismin = min(contents)
            if isinstance(hist, R.TGraph):
                thismin = hist.GetHistogram().GetMinimum()
                thismax = hist.GetHistogram().GetMaximum()
            else:
                thismin = hist.GetMinimum()
                thismax = hist.GetMaximum()
            #if hist.GetMinimum()<hmin: hmin=hist.GetMinimum()
            if thismin<hmin: hmin=thismin
            #if hist.GetMaximum()>hmax: hmax=hist.GetMaximum()
            if thismax>hmax: hmax=thismax
        #canv=R.TCanvas(ytitle.replace(" ","_")+"_"+str(LATOME))
        if hmin == 0: # need to set it below 0 to see the points at 0
            hmin = -(hmax/20)
        else:
            hmin = hmin-abs(hmin)*0.05
        hmax = hmax*1.05

        tidypad()

        for hist in histos:
            hist.SetMarkerStyle(20)
            l = hist.GetName().split("_layer")[1]            
            #hist.SetMarkerColor( layercol[histos.index(hist)] )
            addtoleg=1
            if "_" in l: 
                l = l.split("_")[0]
                addtoleg=0
            hist.SetMarkerColor( layercol[int(l)] )            
            #hist.SetMarkerStyle( layercol[histos.index(hist)]+19 )
            #hist.SetMarkerSize(1)
            xax = hist.GetXaxis()
            yax = hist.GetYaxis()
            yax.SetTitle(ytitle)
            xax.SetTitleOffset(1.5)
            if isinstance(hist, R.TGraph):
                drawopt="P"
                legopt="p"
                hist.GetYaxis().SetRangeUser(hmin, hmax)
            else:
                hist.SetLineColor( layercol[int(l)] )
                drawopt="hist"
                legopt="l"
                hist.SetMaximum(hmax)
                hist.SetMinimum(hmin)
            plotfirst = (histos.index(hist) == 0)
            if plotfirst:
                descr = hist.GetTitle()
                if isinstance(hist, R.TGraph):
                    hist.Draw("A"+drawopt)
                    hist.GetXaxis().SetRangeUser(0, len(channels)+1) # change later, annoying tgraph feature
                    hist.Draw("A"+drawopt)
                else:
                    hist.Draw(drawopt)
                leg = R.TLegend(0.1, 0.9, 0.9, 0.95)
                cleanLeg(leg, cols=4)
                #leg = makeLeg(0.1, 0.9, 0.9, list(plots[var].keys()), legheight=0.1, textsize=0.09, cols=len(plots[var].keys()) )
            else:
                hist.Draw("same,"+drawopt)
            if addtoleg: leg.AddEntry(hist, "Layer "+l, legopt)
            canv.objs.append(hist)
        leg.Draw()
        canv.objs.append(leg)
        if hmin < 0:
            zerol = R.TLine( xax.GetXmin(), 0, xax.GetXmax(), 0 )
            zerol.SetLineColor(R.kRed)
            zerol.SetLineStyle(2)
            zerol.Draw("samel")
            canv.objs.append(zerol)
        if "firstpulse" not in var:
            str1 = "Minimum = "+str(round(vals[var][mins[var]],2))+" for channel "+mins[var]+" ("+str(latomechans[strch.index(mins[var])])+", #eta = "+etas[strch.index(mins[var])]+", #phi = "+phis[strch.index(mins[var])]+", layer = "+layers[strch.index(mins[var])]+")"
            str2 = "Maximum = "+str(round(vals[var][maxes[var]],2))+" for channel "+maxes[var]+" ("+str(latomechans[strch.index(maxes[var])])+", #eta = "+etas[strch.index(maxes[var])]+", #phi = "+phis[strch.index(maxes[var])]+", layer = "+layers[strch.index(mins[var])]+")"
        else:
            str1 = "" 
            str2 = ""
        # Info on plot
        l = R.TLatex()
        l.SetNDC()
        l.SetTextFont(42)
        l.SetTextColor(R.kBlack)
        l.SetTextSize(0.04)
        details = str(Nsamples)+" samples, "+str(nCh)+" channels, "+str(ndata)+" events"
        stringy=0.2
        #l.DrawLatex(0.01, stringy, descr)
        stringy-=0.05
        l.DrawLatex(0.01, stringy, details)
        stringy-=0.05
        l.DrawLatex(0.02, stringy, str1)
        stringy-=0.05
        l.DrawLatex(0.02, stringy, str2)


        
    canv=R.TCanvas("summary_"+str(LATOME), "summary_"+str(LATOME), 
                   600, 400*len(plots.keys()))
    if not hasattr(canv, "objs"):
        canv.objs = []
    canv.Divide(1,len(plots.keys()) )
    for hist in plots.keys():
        h = list(plots.keys()).index(hist)       
        canv.cd(h+1)
        plotH(hist, vardict[hist][0])
    canv.Print(outdir+"/summary_"+str(hex(LATOME))+".png")



def main():
    started = dt.datetime.now()
    print("Started job at "+str(started) )
    
    #R.gROOT.SetStyle("ATLAS")

    ver = platform.python_version()
    if "3.7.9" in ver or "3.8" in ver:
        print("Running in parallel")
        doParallel=True
    else:
        print("Can't run in parallel with this python version")
        doParallel=False

    if len(sys.argv) < 2:
        print("Pass the input file as an argument")
        sys.exit()
    infile = sys.argv[1]


    run = infile.split("/")[-1].split("_")[1].split(".root")[0]

    if len(sys.argv) > 2:
        outdir  = sys.argv[2]
    else:
        outdir = "plots_"+run

    chmkDir(outdir)

    tree = U.open(infile)["LARDIGITS"]
    #branches = tree.keys()

    lidtree = tree.arrays(expressions=["latomeSourceId"],library="np")
    LATOMEs = list(dict.fromkeys(lidtree["latomeSourceId"]))
    print( len(LATOMEs), "LATOMEs" )

    args = [[tree,outdir, LATOME] for LATOME in LATOMEs]

    if doParallel:
        sys.path.append("../..")
        from mp import parallel_exec
        parallel_exec( target=makePlots,
                       args_list=args,
                       nproc=9,
                       name=[str(LATOME) for LATOME in LATOMEs]  )
    else:
        for LATOME in LATOMEs:
            print("LATOME", LATOMEs.index(LATOME)+1, "/", len(LATOMEs))
            makePlots(tree, outdir, LATOME)
    finished = dt.datetime.now()
    timeforjob = finished - started
    print( "Done - Time taken for job:", str(timeforjob) )

if __name__ == "__main__":
    R.gErrorIgnoreLevel = R.kWarning
    R.gROOT.SetBatch(True)
    main()

