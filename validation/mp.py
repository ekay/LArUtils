## From C.Gwilliam

import os
import sys
import time
import multiprocessing as mp

NCORES = mp.cpu_count()
def test(num):
    print( ">>> Starting thread", num )
    time.sleep(20)
    print( ">>> Finishing thread", num )
def test_quiet(num):
    time.sleep(20)
def wait_completion(pids, sleep=30):
    """Wait until completion of one of the launched jobs"""
    time.sleep(10)
    while True:
        for pid in pids:
            if not pid.is_alive():
                try:
                    print( ">>> Completed: ", pid.name, "(", pid.pid, ")" )
                except AttributeError:
                    print( ">>> Completed: ", pid.pid )
                pids.remove(pid)
                return
        print( "Awaiting completion of jobs..." )
        time.sleep(sleep) # wait before retrying
    return
        
def wait_all(pids):
    """Wait until completion of all launched jobs"""
    #while len(pids)>0:
    #wait_completion(pids)
    for p in pids:
        p.join()    
    print( "All jobs finished !" )
    return
def parallel_exec(target, args_list, nproc=int(NCORES/2), name=None, sleep = 30):
    if nproc >= NCORES:
        nproc = NCORES - 1
        
    pids = []
    for a in args_list:
        if len(pids) >= nproc:
            wait_completion(pids, sleep=sleep)
        p = mp.Process(target=target, args=a)        
        #n = name(a) if name is not None else str(p.pid)
        n = name[args_list.index(a)] if name is not None else str(p.pid)
        print( ">>> Started: ", n )
        #p = RedirectProcess(target=target, args=a)
        p.name = n
        pids.append(p)
        p.start()
    wait_all(pids)    
    return



if __name__ == '__main__':
    ncores = 4 # mp.cpu_count()
    nprocs = 13
    args_list = [(n,) for n in xrange(nprocs)]
    parallel_exec(test, args_list, ncores, lambda a : "***%s***" % a[0])
#     pids = []
#     for n in xrange(nprocs):
#         if len(pids) >= ncores:
#             wait_completion(pids)
# 
#         p = mp.Process(target=test, args=(n,))
#         pids.append(p)
#         p.start()
# 
#     wait_all(pids)
