import printMapping
import pandas as pd
import sys
import glob

queries=[]
# BarrelEndPS
run = 443340
logdir = "/afs/cern.ch/user/l/lardaq/public/hfec-calibration-logs"


query = {"want":["SC_ONL_ID", "ONL_ID"], "showdecimal":True}
SCmap = pd.DataFrame(printMapping.query(**query), columns=query["want"])
SCmap = SCmap.astype({'SC_ONL_ID':'int', 'ONL_ID':'int'})
SCdict = SCmap.set_index('SC_ONL_ID').groupby('SC_ONL_ID').apply(lambda x : x.to_numpy().flatten().tolist()).to_dict()


files = glob.glob(logdir+"/parameters_*"+str(run)+".dat")

for f in files:
    parts = f.split("_")[1]
    print(f, parts)
    
    detquery = {"have":["DET"]}
    if parts == "barrel":
        detquery["have_val"] = ["EMBA,EMBC"]
    elif parts == "hec":
        detquery["have_val"] = ["HECA,HECC"]
    elif parts == "fcal":
        detquery["have_val"] = ["FCALA,FCALC"]
    
    
sys.exit()


queries.append({"have":["DET"], "have_val":["EMBA,EMBC"]})
queries.append({"have":["DET","SAM"], "have_val":["EMECA,EMECC",0]})

# FMhighEtaBack
queries.append({"have":["DET"], "have_val":["EMBA,EMBC"]})
queries.append({"have":["DET","SAM"], "have_val":["EMECA,EMECC",0]})



CL=118
query = [ "DET","SAM","FEB","LATOME_NAME","SC_ONL_ID","SCETA","SCPHI","ONL_ID","ETA","PHI" ] # "LTDB"
for q in queries:
    q["want"] = query
    q["have"].append("CL")
    q["have_val"].append(CL)
    print("****", q)

    if queries.index(q) == 0:
        pmdf = pd.DataFrame(printMapping.query(**q), columns=query)
    else:
        pmdf.append(pd.DataFrame(printMapping.query(**q), columns=query))
                    
print(pmdf)
    
